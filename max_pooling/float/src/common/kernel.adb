with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx

package body Kernel with
  SPARK_Mode
is

   procedure Max_Pooling
     (A, B                               : Matrix_Device_Access;
      Horizontal_Stride, Vertical_Stride : Positive)
   is

      B_X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);
      B_Y : Integer := Integer (Block_Dim.Y * Block_Idx.Y + Thread_Idx.Y);

      A_X : Integer := B_X * Horizontal_Stride;
      A_Y : Integer := B_Y * Vertical_Stride;

      Max_Value : Float := Float'First;

   begin

      if B_Y < B'Length (1) and B_X < B'Length (2) then

         for I in A_Y .. A_Y + Vertical_Stride - 1 loop
            for J in A_X .. A_X + Horizontal_Stride - 1 loop
               Max_Value := Float'Max (Max_Value, A (I, J));
            end loop;
         end loop;

         B (B_Y, B_X) := Max_Value;

      end if;

   end Max_Pooling;

end Kernel;
