with Text_IO;
with Ada.Numerics.Float_Random;
with Ada.Numerics.Discrete_Random;

package body Matrix_Utilities is

   procedure Max_Pooling_Serial
     (A                                  : Matrix; B : out Matrix;
      Horizontal_Stride, Vertical_Stride : Positive)
   is

      A_I_Start, A_J_Start : Integer;
      Max_Value            : Float;

   begin

      for B_I in B'Range (1) loop
         for B_J in B'Range (2) loop

            A_I_Start := B_I * Vertical_Stride;
            A_J_Start := B_J * Horizontal_Stride;

            Max_Value := Float'First;

            for A_I in A_I_Start .. A_I_Start + Vertical_Stride - 1 loop
               for A_J in A_J_Start .. A_J_Start + Horizontal_Stride - 1 loop
                  Max_Value := Float'Max (Max_Value, A (A_I, A_J));
               end loop;
            end loop;

            B (B_I, B_J) := Max_Value;

         end loop;
      end loop;

   end Max_Pooling_Serial;

   procedure Generate_Matrix (M : out Matrix) is

      FG : Ada.Numerics.Float_Random.Generator;
      package Integer_Random is new Ada.Numerics.Discrete_Random (Integer);
      DG : Integer_Random.Generator;

   begin

      -- Initialize the random number generator
      Integer_Random.Reset (DG);
      Ada.Numerics.Float_Random.Reset (FG, Integer_Random.Random (DG));

      M :=
        (others =>
           (others =>
              2.0 * Value_Range * Ada.Numerics.Float_Random.Random (FG) -
              Value_Range));

   end Generate_Matrix;

   procedure Print_Matrix (M : Matrix) is
   begin

      for i in M'Range (1) loop
         for j in M'Range (2) loop
            Text_IO.Put (Float'Image (M (i, j)));
            Text_IO.Put (" ");
         end loop;
         Text_IO.New_Line;
      end loop;

   end Print_Matrix;

end Matrix_Utilities;
