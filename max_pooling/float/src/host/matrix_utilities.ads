with Kernel; use Kernel;

package Matrix_Utilities is

   Value_Range : Float := 1_000.0;

   procedure Max_Pooling_Serial
     (A                                  : Matrix; B : out Matrix;
      Horizontal_Stride, Vertical_Stride : Positive) with
     Pre =>
      B'Length (1) = A'Length (1) / Vertical_Stride and
      B'Length (2) = A'Length (2) / Horizontal_Stride;

   procedure Generate_Matrix (M : out Matrix);

   procedure Print_Matrix (M : Matrix);

end Matrix_Utilities;
