with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernel with
  SPARK_Mode
is

   type Matrix is array (Natural range <>, Natural range <>) of Integer;

   type Matrix_Device_Access is access Matrix with
     Designated_Storage_Model => CUDA.Storage_Models.Model;

   procedure Max_Pooling
     (A, B                               : Matrix_Device_Access;
      Horizontal_Stride, Vertical_Stride : Positive) with
     Pre =>
      B'Length (1) = A'Length (1) / Vertical_Stride and
      B'Length (2) = A'Length (2) / Horizontal_Stride,
     Cuda_Global;

end Kernel;
