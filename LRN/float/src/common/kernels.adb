with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Kernels with
  SPARK_Mode
is

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural with
     SPARK_Mode => Off
   is
   begin

      return Natural (Block_Dim * Block_Idx + Thread_Idx);

   end Cuda_Index;

   procedure LRN_kernel
     (A : not null Vector_Device_Constant_Access;
      B : not null Vector_Device_Access)
   is

      -------- Mirror wrapper's precondition semantics with assumptions --------
      X : Natural := Cuda_Index (Block_Dim.X, Block_Idx.X, Thread_Idx.X);

      pragma Assume (A'First = 0 and B'First = 0);
      pragma Assume (A'Last = B'Last);
      pragma Assume (A'Last <= Integer'Last - 31);

      Max_X : Integer := ((A'Last + 31) / 32) * 32;
      pragma Assume (X in 0 .. Max_X);
      --------------------------------------------------------------------------

      K     : Float := 2.0;
      ALPHA : Float := 10.0e-4;
      BETA  : Float := 0.75;

   begin

      if X <= A'Last then
         -- TODO: use floating point exponent
         --B (X) := A (X) / ((K + ALPHA * A (X)**2.0)**BETA);
         B (X) :=
           A (X) / Exp (BETA * Log (K + ALPHA * Exp (2.0 * Log (A (X)))));
      end if;

   end LRN_kernel;

end Kernels;
