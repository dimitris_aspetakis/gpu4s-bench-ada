with Kernels; use Kernels;

package Vector_Utilities with
  SPARK_Mode => On
is

   procedure LRN_cpu (A : Vector; B : out Vector) with
     Pre => A'First = B'First and A'Last = B'Last;

   procedure Generate_Vector (V : out Vector);

   procedure Print_Vector (V : Vector);

end Vector_Utilities;
