with Text_IO;
with Ada.Numerics.Float_Random;
with Ada.Numerics.Discrete_Random;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Vector_Utilities is

   procedure LRN_cpu (A : Vector; B : out Vector) is

      K     : Float := 2.0;
      ALPHA : Float := 10.0e-4;
      BETA  : Float := 0.75;

   begin

      for I in A'Range loop
         -- TODO: use floating point exponent
         --B (I) := A (I) / ((K + ALPHA * A (I)**2.0)**BETA);
         B (I) :=
           A (I) / Exp (BETA * Log (K + ALPHA * Exp (2.0 * Log (A (I)))));
      end loop;

   end LRN_cpu;

   procedure Generate_Vector (V : out Vector) is

      FG : Ada.Numerics.Float_Random.Generator;
      package Integer_Random is new Ada.Numerics.Discrete_Random (Integer);
      DG : Integer_Random.Generator;

      Value_Range : Float := 100.0;

   begin

      -- Initialize the random number generator
      Integer_Random.Reset (DG);
      Ada.Numerics.Float_Random.Reset (FG, Integer_Random.Random (DG));

      V := (others => Value_Range * Ada.Numerics.Float_Random.Random (FG));

   end Generate_Vector;

   procedure Print_Vector (V : Vector) is
   begin

      for i in V'Range loop
         Text_IO.Put (Float'Image (V (i)));
         Text_IO.Put (" ");
      end loop;
      Text_IO.New_Line;

   end Print_Vector;

end Vector_Utilities;
