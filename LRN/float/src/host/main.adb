with Ada.Text_IO; use Ada.Text_IO;

with Kernels;          use Kernels;
with Kernel_Wrappers;  use Kernel_Wrappers;
with Vector_Utilities; use Vector_Utilities;

procedure Main with
  SPARK_Mode
is

   Vector_Size : constant Positive := 2**18;

   A, B, B_Host : Vector (0 .. Vector_Size - 1);

   Threads_Per_Block : Pos3 := (2**8, 1, 1);
   Blocks_Per_Grid   : Pos3 :=
     ((Vector_Size + Threads_Per_Block.X - 1) / Threads_Per_Block.X, 1, 1);

begin

   -- Initialize vector
   Generate_Vector (A);

   -- Call sequential implementation
   LRN_cpu (A, B_Host);

   -- Call GPU implementation
   LRN_kernel_wrapper (Threads_Per_Block, Blocks_Per_Grid, A, B);

   for I in B'Range loop
      if abs (B (I) - B_Host (I)) > 10.0e-5 then
         Put_Line ("Test FAILED");
         return;
      end if;
   end loop;

   Put_Line ("Test SUCCEEDED");

end Main;
