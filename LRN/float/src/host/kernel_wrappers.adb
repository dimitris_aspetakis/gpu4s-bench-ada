with CUDA.Runtime_Api; use CUDA.Runtime_Api;
with Kernels;          use Kernels;
with Ada.Unchecked_Deallocation;
with Ada.Unchecked_Conversion;

package body Kernel_Wrappers with
  SPARK_Mode => Off -- TODO: Maybe turn it on when Cuda_Execute is supported.
is

   procedure LRN_kernel_wrapper
     (Threads_Per_Block :     Pos3; Blocks_Per_Grid : Pos3; A : Vector;
      B                 : out Vector)
   is

      procedure Free is new Ada.Unchecked_Deallocation
        (Vector, Vector_Device_Access);

      D_A : Vector_Device_Access := new Vector (A'Range);
      D_B : Vector_Device_Access := new Vector (B'Range);

   begin

      D_A.all := A;

      pragma Cuda_Execute
        (LRN_kernel (Vector_Device_Constant_Access (D_A), D_B),
         (Threads_Per_Block.X, Threads_Per_Block.Y, Threads_Per_Block.Z),
         (Blocks_Per_Grid.X, Blocks_Per_Grid.Y, Blocks_Per_Grid.Z));

      B := D_B.all;

      Free (D_A);
      Free (D_B);

   end LRN_kernel_wrapper;

end Kernel_Wrappers;
