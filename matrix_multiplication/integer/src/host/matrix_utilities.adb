with Text_IO;
with Ada.Numerics.Discrete_Random;

package body Matrix_Utilities is

   procedure Multiply_Serial (A : Matrix; B : Matrix; C : out Matrix) is
   begin

      for i in A'Range (1) loop
         for j in B'Range (2) loop
            C (i, j) := 0;
            for k in A'Range (2) loop
               C (i, j) := C (i, j) + A (i, k) * B (k, j);
            end loop;
         end loop;
      end loop;

   end Multiply_Serial;

   procedure Generate_Matrix (M : out Matrix) is

      package Rand_Integer is new Ada.Numerics.Discrete_Random (Integer);
      G : Rand_Integer.Generator;

   begin

      -- Initialize the random number generator
      Rand_Integer.Reset (G);

      M := (others => (others => (Rand_Integer.Random (G) rem Value_Range)));

   end Generate_Matrix;

   procedure Print_Matrix (M : Matrix) is
   begin

      for i in M'Range (1) loop
         for j in M'Range (2) loop
            Text_IO.Put (Integer'Image (M (i, j)));
            Text_IO.Put (" ");
         end loop;
         Text_IO.New_Line;
      end loop;

   end Print_Matrix;

end Matrix_Utilities;
