with System;
with Interfaces.C; use Interfaces.C;

with Ada.Real_Time; use Ada.Real_Time;
with Ada.Text_IO;   use Ada.Text_IO;

with CUDA.Driver_Types;   use CUDA.Driver_Types;
with CUDA.Runtime_Api;    use CUDA.Runtime_Api;
with CUDA.Vector_Types;   use CUDA.Vector_Types;
with CUDA.Stddef;
with CUDA.Storage_Models; use CUDA.Storage_Models;

with Kernel;           use Kernel;
with Matrix_Utilities; use Matrix_Utilities;

with Ada.Unchecked_Deallocation;
with Ada.Unchecked_Conversion;

procedure Main is

   type Matrix_Host_Access is access all Matrix;

   procedure Free is new Ada.Unchecked_Deallocation
     (Matrix, Matrix_Host_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Matrix, Matrix_Device_Access);

   Matrix_Dimension : Integer := 1_024;

   H_A, H_B, H_C, C_Model : Matrix_Host_Access;
   D_A, D_B, D_C          : Matrix_Device_Access;

   Threads_Per_Block : Dim3 := (32, 32, 1);
   Blocks_Per_Grid   : Dim3 :=
     ((unsigned (Matrix_Dimension) + Threads_Per_Block.X - 1) /
      Threads_Per_Block.X,
      (unsigned (Matrix_Dimension) + Threads_Per_Block.Y - 1) /
      Threads_Per_Block.Y,
      1);

   Start_Time   : Time;
   Elapsed_Time : Time_Span;
   Err          : Error_T;

begin

   -- Allocate host matrices
   H_A := new Matrix (0 .. Matrix_Dimension - 1, 0 .. Matrix_Dimension - 1);
   H_B := new Matrix (0 .. Matrix_Dimension - 1, 0 .. Matrix_Dimension - 1);
   H_C := new Matrix (0 .. Matrix_Dimension - 1, 0 .. Matrix_Dimension - 1);
   C_Model :=
     new Matrix (0 .. Matrix_Dimension - 1, 0 .. Matrix_Dimension - 1);

   if H_A = null or else H_B = null or else H_C = null then
      Put_Line ("Failed to allocate host vectors!");
      return;
   end if;

   -- Initialize matrices
   Generate_Matrix (H_A.all);
   Generate_Matrix (H_B.all);

   -- Construct the model output
   Multiply_Serial (H_A.all, H_B.all, C_Model.all);

   -- Allocate and initialize device matrices
   D_A := new Matrix'(H_A.all);
   D_B := new Matrix'(H_B.all);
   D_C := new Matrix (H_C.all'Range (1), H_C.all'Range (2));

   Put_Line
     ("CUDA kernel launch with " & Blocks_Per_Grid'Img & " blocks of " &
      Threads_Per_Block'Img & "  threads");

   -- Multiply A and B, resulting in C (while measuring elapsed time too)
   Start_Time := Clock;
   pragma Cuda_Execute
     (Multiply (D_A, D_B, D_C), Blocks_Per_Grid, Threads_Per_Block);

   Err := Get_Last_Error;

   -- Copy output data from the CUDA device to the host memory
   H_C.all      := D_C.all;
   Elapsed_Time := Clock - Start_Time;

   -- Test with serial implementation
   for X in H_C'Range (1) loop
      for Y in H_C'Range (2) loop
         if H_C (X, Y) /= C_Model (X, Y) then
            Put_Line ("Test FAILED");

            Free (D_A);
            Free (D_B);
            Free (D_C);

            Free (H_A);
            Free (H_B);
            Free (H_C);

            return;
         end if;
      end loop;
   end loop;

   -- Print the duration of the benchmark's kernel
   Put_Line ("Elapsed kernel time: " & Time_Span'Image (Elapsed_Time));

   -- Free matrices
   Free (D_A);
   Free (D_B);
   Free (D_C);

   Free (H_A);
   Free (H_B);
   Free (H_C);

   Put_Line ("Test SUCCEEDED");

end Main;
