with Kernel; use Kernel;

package Matrix_Utilities is

   Value_Range : Float := 1_000.0;

   procedure Multiply_Serial (A : Matrix; B : Matrix; C : out Matrix) with
     Pre =>
      A'Length (2) = B'Length (1) and A'Length (1) = C'Length (1) and
      B'Length (2) = C'Length (2);

   procedure Generate_Matrix (M : out Matrix);

   procedure Print_Matrix (M : Matrix);

end Matrix_Utilities;
