with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernel with
  SPARK_Mode
is

   type Matrix is array (Natural range <>, Natural range <>) of Float;

   type Matrix_Device_Access is access Matrix with
     Designated_Storage_Model => CUDA.Storage_Models.Model;

   procedure Multiply
     (A : Matrix_Device_Access; B : Matrix_Device_Access;
      C : Matrix_Device_Access) with
     Pre =>
      A'Length (2) = B'Length (1) and A'Length (1) = C'Length (1) and
      B'Length (2) = C'Length (2),
     Cuda_Global;

end Kernel;
