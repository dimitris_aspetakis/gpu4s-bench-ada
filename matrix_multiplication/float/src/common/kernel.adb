with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx

package body Kernel with
  SPARK_Mode
is

   procedure Multiply
     (A : Matrix_Device_Access; B : Matrix_Device_Access;
      C : Matrix_Device_Access)
   is
      X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);
      Y : Integer := Integer (Block_Dim.Y * Block_Idx.Y + Thread_Idx.Y);
   begin

      if Y < C'Length (1) and X < C'Length (2) then
         C (Y, X) := 0.0;
         for I in C'Range (1) loop
            C (Y, X) := C (Y, X) + A (Y, I) * B (I, X);
         end loop;
      end if;

   end Multiply;

end Kernel;
