with Ada.Command_Line;
with Fast_Fourier_Transform;
with Ada.Real_Time;
with Text_IO;
with Ada.Unchecked_Deallocation;

procedure Bench is

   use Fast_Fourier_Transform;
   use Ada.Real_Time;
   use Text_IO;

   Start_Time   : Time;
   Elapsed_Time : Time_Span;

begin

   declare

      type Data_Vector_Ref is access Data_Vector;

      procedure Free_Data_Vector is new Ada.Unchecked_Deallocation
        (Object => Data_Vector, Name => Data_Vector_Ref);

      Size : constant Positive :=
        Positive'Value (Ada.Command_Line.Argument (1));
      V : Data_Vector_Ref;

   begin

      -- Allocate and initialize matrices
      V := new Data_Vector (0 .. Long_Integer (Size - 1));

      -- Populate vector V
      Generate_Data_Vector (V.all);

      -- Apply fft to V (while measuring elapsed time)
      Start_Time := Clock;
      Fast_Fourier_Transform.Fast_Fourier_Transform (V.all);
      Elapsed_Time := Clock - Start_Time;

      -- Free vector
      Free_Data_Vector (V);

      -- Print the duration of the benchmark's kernel
      Put_Line ("Elapsed kernel time: " & Time_Span'Image (Elapsed_Time));

   end;

end Bench;
