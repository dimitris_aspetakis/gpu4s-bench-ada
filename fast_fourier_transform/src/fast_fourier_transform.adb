with Text_IO;
with Ada.Numerics;
with Ada.Numerics.Float_Random;
with Ada.Numerics.Discrete_Random;
with Ada.Numerics.Elementary_Functions;

package body Fast_Fourier_Transform with
   SPARK_Mode
is

   procedure Swap (X, Y : in out T) is

      Tmp : constant T := X;

   begin

      X := Y;
      Y := Tmp;

   end Swap;

   procedure Generate_Data_Vector (V : out Data_Vector) is

      pragma SPARK_Mode (Off);
      FG : Ada.Numerics.Float_Random.Generator;
      package Integer_Random is new Ada.Numerics.Discrete_Random (Integer);
      DG : Integer_Random.Generator;

   begin

      -- Initialize the random number generator
      Integer_Random.Reset (DG);
      Ada.Numerics.Float_Random.Reset (FG, Integer_Random.Random (DG));

      for i in V'Range loop
         V (i) :=
           2.0 * Value_Range * Ada.Numerics.Float_Random.Random (FG) -
           Value_Range;
      end loop;

   end Generate_Data_Vector;

   procedure Fast_Fourier_Transform (V : in out Data_Vector) is

      use Ada.Numerics;
      use Ada.Numerics.Elementary_Functions;

      nn : Long_Integer := Long_Integer (V'Length) / 2;
      n, mmax, m, j, istep, i                      : Long_Integer;
      wtemp, wr, wpr, wpi, wi, theta, tempr, tempi : Float;

      procedure Swap_Floats is new Swap (Float);

   begin

      -- Reverse-binary reindexing
      n := nn * 2;
      i := 1;
      j := 1;

      while i < n loop
         if j > i then
            Swap_Floats (V (j - 1), V (i - 1));
            Swap_Floats (V (j), V (i));
         end if;
         m := nn;
         while m >= 2 and j > m loop
            j := j - m;
            m := m / 2;
         end loop;
         j := j + m;
         i := i + 2;
      end loop;

      -- Danielson-Lanczos section
      mmax := 2;
      while n > mmax loop
         istep := mmax * 2;
         theta := -(2.0 * Pi / Float (mmax));
         wtemp := Sin (0.5 * theta);
         wpr   := -2.0 * wtemp**2;
         wpi   := Sin (theta);
         wr    := 1.0;
         wi    := 0.0;

         m := 1;
         while m < mmax loop
            i := m;
            while i <= n loop
               j     := i + mmax;
               tempr := wr * V (j - 1) - wi * V (j);
               tempi := wr * V (j) + wi * V (j - 1);

               V (j - 1) := V (i - 1) - tempr;
               V (j)     := V (i) - tempi;
               V (i - 1) := V (i - 1) + tempr;
               V (i)     := V (i) + tempi;

               i := i + istep;
            end loop;

            wtemp := wr;
            wr    := wr + wr * wpr - wi * wpi;
            wi    := wi + wi * wpr + wtemp * wpi;

            m := m + 2;
         end loop;
         mmax := istep;
      end loop;

   end Fast_Fourier_Transform;

   procedure Print_Data_Vector (V : Data_Vector) is
   begin

      for i in V'Range loop
         Text_IO.Put (Float'Image (V (i)));
         Text_IO.Put (" ");
      end loop;

   end Print_Data_Vector;

end Fast_Fourier_Transform;
