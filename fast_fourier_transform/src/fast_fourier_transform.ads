package Fast_Fourier_Transform with
   SPARK_Mode
is
   type Data_Vector is array (Long_Integer range <>) of Float;

   Value_Range : Float := 1_000.0;

   generic
      type T is private;
   procedure Swap (X, Y : in out T) with
      Depends => (X => Y, Y => X);

   procedure Generate_Data_Vector (V : out Data_Vector);

   procedure Fast_Fourier_Transform (V : in out Data_Vector);

   procedure Print_Data_Vector (V : Data_Vector);

end Fast_Fourier_Transform;
