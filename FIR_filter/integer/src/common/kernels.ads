with Interfaces.C;        use Interfaces.C;
with CUDA.Vector_Types;   use CUDA.Vector_Types;
with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernels with
  SPARK_Mode
is

   type Pos3 is record
      X : Positive;
      Y : Positive;
      Z : Positive;
   end record;

   type Vector is array (Natural range <>) of Integer;

   type Vector_Device_Access is access Vector with
     Designated_Storage_Model => CUDA.Storage_Models.Model;

   type Vector_Device_Constant_Access is access constant Vector;

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural;

   procedure FIR_kernel
     (A, Kernel : not null Vector_Device_Constant_Access;
      B         : not null Vector_Device_Access) with
     Pre => A /= null and then B /= null and then Kernel /= null, Cuda_Global;

end Kernels;
