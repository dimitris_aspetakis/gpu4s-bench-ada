with Kernels; use Kernels;

package Vector_Utilities with
  SPARK_Mode => On
is

   procedure FIR_cpu (A, Kernel : Vector; B : out Vector) with
     Pre =>
      (A'First = 0 and B'First = 0 and Kernel'First = 0 and
       B'Length = A'Length - Kernel'Length + 1);

   procedure Generate_Vector (V : out Vector);

   procedure Print_Vector (V : Vector);

end Vector_Utilities;
