with Interfaces.C; use Interfaces.C;
with Kernels;      use Kernels;

pragma Overflow_Mode (General => Eliminated, Assertions => Eliminated);

package Kernel_Wrappers with
  SPARK_Mode => On
is

   procedure FIR_kernel_wrapper
     (Threads_Per_Block :     Pos3; Blocks_Per_Grid : Pos3; A, Kernel : Vector;
      B                 : out Vector) with
     Pre =>
      Threads_Per_Block.X * Blocks_Per_Grid.X in Positive'Range
      and then
      (A'First = 0 and B'First = 0 and Kernel'First = 0 and
       B'Length = A'Length - Kernel'Length + 1 and
       B'Last >= (Threads_Per_Block.X * Blocks_Per_Grid.X) - 1);

end Kernel_Wrappers;
