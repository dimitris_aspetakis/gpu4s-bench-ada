with Text_IO;
with Ada.Numerics.Discrete_Random;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Vector_Utilities is

   procedure FIR_cpu (A, Kernel : Vector; B : out Vector) is

      Sum : Integer;

   begin

      for X in B'Range loop
         Sum := 0;

         for I in Kernel'Range loop
            Sum := Sum + Kernel (I) * A ((X + Kernel'Last) - I);
         end loop;

         B (X) := Sum;
      end loop;

   end FIR_cpu;

   procedure Generate_Vector (V : out Vector) is

      package Integer_Random is new Ada.Numerics.Discrete_Random (Integer);
      G : Integer_Random.Generator;

      Value_Range : Positive := 100;

   begin

      -- Initialize the random number generator
      Integer_Random.Reset (G);

      V := (others => Integer_Random.Random (G) mod Value_Range);

   end Generate_Vector;

   procedure Print_Vector (V : Vector) is
   begin

      for i in V'Range loop
         Text_IO.Put (Integer'Image (V (i)));
         Text_IO.Put (" ");
      end loop;
      Text_IO.New_Line;

   end Print_Vector;

end Vector_Utilities;
