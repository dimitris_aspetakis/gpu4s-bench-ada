with Text_IO;
with Ada.Numerics.Float_Random;
with Ada.Numerics.Discrete_Random;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Vector_Utilities is

   procedure FIR_cpu (A, Kernel : Vector; B : out Vector) is

      Sum : Float;

   begin

      for X in B'Range loop
         Sum := 0.0;

         for I in Kernel'Range loop
            Sum := Sum + Kernel (I) * A ((X + Kernel'Last) - I);
         end loop;

         B (X) := Sum;
      end loop;

   end FIR_cpu;

   procedure Generate_Vector (V : out Vector) is

      FG : Ada.Numerics.Float_Random.Generator;
      package Integer_Random is new Ada.Numerics.Discrete_Random (Integer);
      DG : Integer_Random.Generator;

      Value_Range : Float := 100.0;

   begin

      -- Initialize the random number generator
      Integer_Random.Reset (DG);
      Ada.Numerics.Float_Random.Reset (FG, Integer_Random.Random (DG));

      V := (others => Value_Range * Ada.Numerics.Float_Random.Random (FG));

   end Generate_Vector;

   procedure Print_Vector (V : Vector) is
   begin

      for i in V'Range loop
         Text_IO.Put (Float'Image (V (i)));
         Text_IO.Put (" ");
      end loop;
      Text_IO.New_Line;

   end Print_Vector;

end Vector_Utilities;
