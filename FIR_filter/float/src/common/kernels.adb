with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

package body Kernels with
  SPARK_Mode
is

   function Cuda_Index
     (Block_Dim, Block_Idx, Thread_Idx : unsigned) return Natural with
     SPARK_Mode => Off
   is
   begin

      return Natural (Block_Dim * Block_Idx + Thread_Idx);

   end Cuda_Index;

   procedure FIR_kernel
     (A, Kernel : not null Vector_Device_Constant_Access;
      B         : not null Vector_Device_Access)
   is

      -------- Mirror wrapper's precondition semantics with assumptions --------
      X : Natural := Cuda_Index (Block_Dim.X, Block_Idx.X, Thread_Idx.X);

      pragma Assume (A'First = 0 and B'First = 0 and Kernel'First = 0);
      pragma Assume (B'Length = A'Length - Kernel'Length + 1);
      pragma Assume (B'Last <= Integer'Last - 31);

      Max_X : Integer := ((B'Last + 31) / 32) * 32;
      pragma Assume (X in 0 .. Max_X);
      --------------------------------------------------------------------------

      Sum : Float;

   begin

      if X <= B'Last then
         pragma Assert (X in B'Range);
         Sum := 0.0;

         for I in Kernel'Range loop
            Sum := Sum + Kernel (I) * A ((X + Kernel'Last) - I);
         end loop;

         B (X) := Sum;
      end if;

   end FIR_kernel;

end Kernels;
