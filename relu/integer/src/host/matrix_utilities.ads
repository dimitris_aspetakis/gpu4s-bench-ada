with Kernel; use Kernel;

package Matrix_Utilities is

   Value_Range : Positive := 1_000;

   procedure Relu_Serial (A : Matrix; B : out Matrix) with
     Pre => A'Length (1) = B'Length (1) and A'Length (2) = B'Length (2);

   procedure Generate_Matrix (M : out Matrix);

   procedure Print_Matrix (M : Matrix);

end Matrix_Utilities;
