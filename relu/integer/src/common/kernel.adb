with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx

package body Kernel with
  SPARK_Mode
is

   procedure Relu (A : Matrix_Device_Access; B : Matrix_Device_Access) is
      X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);
      Y : Integer := Integer (Block_Dim.Y * Block_Idx.Y + Thread_Idx.Y);
   begin

      if Y < A'Length (1) and X < A'Length (2) then
         if A (Y, X) > 0 then
            B (Y, X) := A (Y, X);
         else
            B (Y, X) := 0;
         end if;
      end if;

   end Relu;

end Kernel;
