with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernel with
  SPARK_Mode
is

   type Matrix is array (Natural range <>, Natural range <>) of Float;

   type Matrix_Device_Access is access Matrix with
     Designated_Storage_Model => CUDA.Storage_Models.Model;

   type Int_Device_Access is access Integer with
     Designated_Storage_Model => CUDA.Storage_Models.Model;

   procedure Softmax
     (A   : Matrix_Device_Access; B : Matrix_Device_Access;
      Sum : Int_Device_Access) with
     Pre =>
      A'Length (1) = B'Length (1) and A'Length (2) = B'Length (2) and
      Sum.all = 0,
     Cuda_Global;

   procedure Softmax_Finish
     (B : Matrix_Device_Access; Sum : Int_Device_Access) with
     Cuda_Global;

end Kernel;
