with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx
with CUDA.Device_Atomic_Functions; use CUDA.Device_Atomic_Functions;
with Ada.Numerics.Generic_Elementary_Functions;

package body Kernel with
  SPARK_Mode
is

   procedure Softmax
     (A   : Matrix_Device_Access; B : Matrix_Device_Access;
      Sum : Int_Device_Access)
   is

      package Float_Elementary_Functions is new Ada.Numerics
        .Generic_Elementary_Functions
        (Float);

      Unused : Integer;

      X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);
      Y : Integer := Integer (Block_Dim.Y * Block_Idx.Y + Thread_Idx.Y);

   begin

      if Y < A'Length (1) and X < A'Length (2) then
         B (Y, X) := Float_Elementary_Functions.Exp (A (Y, X));

         -- TODO: Sum should be a Float once Atomic_Add for floats is supported.
         Unused := Atomic_Add (Sum, Integer (B (Y, X)));
      end if;

   end Softmax;

   procedure Softmax_Finish (B : Matrix_Device_Access; Sum : Int_Device_Access)
   is

      X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);
      Y : Integer := Integer (Block_Dim.Y * Block_Idx.Y + Thread_Idx.Y);

   begin

      if Y < B'Length (1) and X < B'Length (2) then
         B (Y, X) := B (Y, X) / Float (Sum.all);
      end if;

   end Softmax_Finish;

end Kernel;
