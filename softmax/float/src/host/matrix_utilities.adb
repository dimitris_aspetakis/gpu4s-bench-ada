with Text_IO;
with Ada.Numerics.Float_Random;
with Ada.Numerics.Discrete_Random;
with Ada.Numerics.Generic_Elementary_Functions;

package body Matrix_Utilities is

   procedure Softmax_Serial (A : Matrix; B : out Matrix) is

      package Float_Elementary_Functions is new Ada.Numerics
        .Generic_Elementary_Functions
        (Float);
      Sum : Integer := 0;   -- TODO: should be a Float once Atomic_Add for
      --       floats is supported.

   begin

      for i in A'Range (1) loop
         for j in A'Range (2) loop
            B (i, j) := Float_Elementary_Functions.Exp (A (i, j));
            Sum      := Sum + Integer (B (i, j));
         end loop;
      end loop;

      for i in A'Range (1) loop
         for j in A'Range (2) loop
            B (i, j) := B (i, j) / Float (Sum);
         end loop;
      end loop;

   end Softmax_Serial;

   procedure Generate_Matrix (M : out Matrix) is

      FG : Ada.Numerics.Float_Random.Generator;
      package Integer_Random is new Ada.Numerics.Discrete_Random (Integer);
      DG : Integer_Random.Generator;

   begin

      -- Initialize the random number generator
      Integer_Random.Reset (DG);
      Ada.Numerics.Float_Random.Reset (FG, Integer_Random.Random (DG));

      M :=
        (others =>
           (others =>
              2.0 * Value_Range * Ada.Numerics.Float_Random.Random (FG) -
              Value_Range));

   end Generate_Matrix;

   procedure Print_Matrix (M : Matrix) is
   begin

      for i in M'Range (1) loop
         for j in M'Range (2) loop
            Text_IO.Put (Float'Image (M (i, j)));
            Text_IO.Put (" ");
         end loop;
         Text_IO.New_Line;
      end loop;

   end Print_Matrix;

end Matrix_Utilities;
