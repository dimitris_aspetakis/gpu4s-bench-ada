with System;
with Interfaces.C; use Interfaces.C;

with Ada.Real_Time; use Ada.Real_Time;
with Ada.Text_IO;   use Ada.Text_IO;

with CUDA.Driver_Types;   use CUDA.Driver_Types;
with CUDA.Runtime_Api;    use CUDA.Runtime_Api;
with CUDA.Vector_Types;   use CUDA.Vector_Types;
with CUDA.Stddef;
with CUDA.Storage_Models; use CUDA.Storage_Models;

with Kernel;           use Kernel;
with Matrix_Utilities; use Matrix_Utilities;

with Ada.Unchecked_Deallocation;
with Ada.Unchecked_Conversion;

procedure Main is

   type Matrix_Host_Access is access all Matrix;

   procedure Free is new Ada.Unchecked_Deallocation
     (Matrix, Matrix_Host_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Matrix, Matrix_Device_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Integer, Int_Device_Access);

   Matrix_Dimension : Integer := 8_192;

   H_A, H_B, B_Model : Matrix_Host_Access;
   D_A, D_B          : Matrix_Device_Access;

   D_Sum : Int_Device_Access := new Integer;

   Threads_Per_Block : Dim3 := (32, 32, 1);
   Blocks_Per_Grid   : Dim3 :=
     ((unsigned (Matrix_Dimension) + Threads_Per_Block.X - 1) /
      Threads_Per_Block.X,
      (unsigned (Matrix_Dimension) + Threads_Per_Block.Y - 1) /
      Threads_Per_Block.Y,
      1);

   Start_Time   : Time;
   Elapsed_Time : Time_Span;
   Err          : Error_T;

begin

   -- Allocate host matrices
   H_A := new Matrix (0 .. Matrix_Dimension - 1, 0 .. Matrix_Dimension - 1);
   H_B := new Matrix (0 .. Matrix_Dimension - 1, 0 .. Matrix_Dimension - 1);
   B_Model :=
     new Matrix (0 .. Matrix_Dimension - 1, 0 .. Matrix_Dimension - 1);

   if H_A = null or else H_B = null then
      Put_Line ("Failed to allocate host vectors!");
      return;
   end if;

   -- Initialize matrices
   Generate_Matrix (H_A.all);

   -- Construct the model output
   Softmax_Serial (H_A.all, B_Model.all);

   -- Allocate and initialize device matrices
   D_A := new Matrix'(H_A.all);
   D_B := new Matrix (H_B.all'Range (1), H_B.all'Range (2));

   Put_Line
     ("CUDA kernel launch with " & Blocks_Per_Grid'Img & " blocks of " &
      Threads_Per_Block'Img & "  threads");

   -- Softmax A to B (while measuring elapsed time too)
   Start_Time := Clock;
   D_Sum.all  := 0;
   pragma Cuda_Execute
     (Softmax (D_A, D_B, D_Sum), Blocks_Per_Grid, Threads_Per_Block);
   pragma Cuda_Execute
     (Softmax_Finish (D_B, D_Sum), Blocks_Per_Grid, Threads_Per_Block);

   Err := Get_Last_Error;

   -- Copy output data from the CUDA device to the host memory
   H_B.all      := D_B.all;
   Elapsed_Time := Clock - Start_Time;

   -- Test with serial implementation
   for X in H_B'Range (1) loop
      for Y in H_B'Range (2) loop
         if abs (H_B (X, Y) - B_Model (X, Y)) >= 1.0E-7 then
            Put_Line ("Test FAILED");

            Free (D_A);
            Free (D_B);
            Free (D_Sum);

            Free (H_A);
            Free (H_B);

            return;
         end if;
      end loop;
   end loop;

   -- Print the duration of the benchmark's kernel
   Put_Line ("Elapsed kernel time: " & Time_Span'Image (Elapsed_Time));

   -- Free matrices
   Free (D_A);
   Free (D_B);
   Free (D_Sum);

   Free (H_A);
   Free (H_B);

   Put_Line ("Test SUCCEEDED");

end Main;
