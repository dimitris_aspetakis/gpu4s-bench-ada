pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with System;
with stddef_h;
with uvector_types_h;

package udriver_types_h is

   cudaHostAllocDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:85
   cudaHostAllocPortable : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:86
   cudaHostAllocMapped : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:87
   cudaHostAllocWriteCombined : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:88

   cudaHostRegisterDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:90
   cudaHostRegisterPortable : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:91
   cudaHostRegisterMapped : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:92
   cudaHostRegisterIoMemory : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:93
   cudaHostRegisterReadOnly : constant := 16#08#;  --  /usr/local/cuda-11.5/include//driver_types.h:94

   cudaPeerAccessDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:96

   cudaStreamDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:98
   cudaStreamNonBlocking : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:99
   --  unsupported macro: cudaStreamLegacy ((cudaStream_t)0x1)
   --  unsupported macro: cudaStreamPerThread ((cudaStream_t)0x2)

   cudaEventDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:121
   cudaEventBlockingSync : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:122
   cudaEventDisableTiming : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:123
   cudaEventInterprocess : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:124

   cudaEventRecordDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:126
   cudaEventRecordExternal : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:127

   cudaEventWaitDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:129
   cudaEventWaitExternal : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:130

   cudaDeviceScheduleAuto : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:132
   cudaDeviceScheduleSpin : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:133
   cudaDeviceScheduleYield : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:134
   cudaDeviceScheduleBlockingSync : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:135
   cudaDeviceBlockingSync : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:136

   cudaDeviceScheduleMask : constant := 16#07#;  --  /usr/local/cuda-11.5/include//driver_types.h:139
   cudaDeviceMapHost : constant := 16#08#;  --  /usr/local/cuda-11.5/include//driver_types.h:140
   cudaDeviceLmemResizeToMax : constant := 16#10#;  --  /usr/local/cuda-11.5/include//driver_types.h:141
   cudaDeviceMask : constant := 16#1f#;  --  /usr/local/cuda-11.5/include//driver_types.h:142

   cudaArrayDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:144
   cudaArrayLayered : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:145
   cudaArraySurfaceLoadStore : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:146
   cudaArrayCubemap : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:147
   cudaArrayTextureGather : constant := 16#08#;  --  /usr/local/cuda-11.5/include//driver_types.h:148
   cudaArrayColorAttachment : constant := 16#20#;  --  /usr/local/cuda-11.5/include//driver_types.h:149
   cudaArraySparse : constant := 16#40#;  --  /usr/local/cuda-11.5/include//driver_types.h:150

   cudaIpcMemLazyEnablePeerAccess : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:152

   cudaMemAttachGlobal : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:154
   cudaMemAttachHost : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:155
   cudaMemAttachSingle : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:156

   cudaOccupancyDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:158
   cudaOccupancyDisableCachingOverride : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:159
   --  unsupported macro: cudaCpuDeviceId ((int)-1)
   --  unsupported macro: cudaInvalidDeviceId ((int)-2)

   cudaCooperativeLaunchMultiDeviceNoPreSync : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:169

   cudaCooperativeLaunchMultiDeviceNoPostSync : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:176

   cudaArraySparsePropertiesSingleMipTail : constant := 16#1#;  --  /usr/local/cuda-11.5/include//driver_types.h:1144
   --  unsupported macro: cudaDevicePropDontCare { {'\0'}, {{0}}, {'\0'}, 0, 0, 0, 0, 0, 0, 0, {0, 0, 0}, {0, 0, 0}, 0, 0, -1, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, {0, 0}, {0, 0}, {0, 0, 0}, {0, 0}, {0, 0, 0}, {0, 0, 0}, 0, {0, 0}, {0, 0, 0}, {0, 0}, 0, {0, 0}, {0, 0, 0}, {0, 0}, {0, 0, 0}, 0, {0, 0}, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, }

   CUDA_IPC_HANDLE_SIZE : constant := 64;  --  /usr/local/cuda-11.5/include//driver_types.h:2367

   cudaExternalMemoryDedicated : constant := 16#1#;  --  /usr/local/cuda-11.5/include//driver_types.h:2426

   cudaExternalSemaphoreSignalSkipNvSciBufMemSync : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:2435

   cudaExternalSemaphoreWaitSkipNvSciBufMemSync : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:2444

   cudaNvSciSyncAttrSignal : constant := 16#1#;  --  /usr/local/cuda-11.5/include//driver_types.h:2451

   cudaNvSciSyncAttrWait : constant := 16#2#;  --  /usr/local/cuda-11.5/include//driver_types.h:2458

   subtype cudaError is unsigned;
   cudaError_cudaSuccess : constant cudaError := 0;
   cudaError_cudaErrorInvalidValue : constant cudaError := 1;
   cudaError_cudaErrorMemoryAllocation : constant cudaError := 2;
   cudaError_cudaErrorInitializationError : constant cudaError := 3;
   cudaError_cudaErrorCudartUnloading : constant cudaError := 4;
   cudaError_cudaErrorProfilerDisabled : constant cudaError := 5;
   cudaError_cudaErrorProfilerNotInitialized : constant cudaError := 6;
   cudaError_cudaErrorProfilerAlreadyStarted : constant cudaError := 7;
   cudaError_cudaErrorProfilerAlreadyStopped : constant cudaError := 8;
   cudaError_cudaErrorInvalidConfiguration : constant cudaError := 9;
   cudaError_cudaErrorInvalidPitchValue : constant cudaError := 12;
   cudaError_cudaErrorInvalidSymbol : constant cudaError := 13;
   cudaError_cudaErrorInvalidHostPointer : constant cudaError := 16;
   cudaError_cudaErrorInvalidDevicePointer : constant cudaError := 17;
   cudaError_cudaErrorInvalidTexture : constant cudaError := 18;
   cudaError_cudaErrorInvalidTextureBinding : constant cudaError := 19;
   cudaError_cudaErrorInvalidChannelDescriptor : constant cudaError := 20;
   cudaError_cudaErrorInvalidMemcpyDirection : constant cudaError := 21;
   cudaError_cudaErrorAddressOfConstant : constant cudaError := 22;
   cudaError_cudaErrorTextureFetchFailed : constant cudaError := 23;
   cudaError_cudaErrorTextureNotBound : constant cudaError := 24;
   cudaError_cudaErrorSynchronizationError : constant cudaError := 25;
   cudaError_cudaErrorInvalidFilterSetting : constant cudaError := 26;
   cudaError_cudaErrorInvalidNormSetting : constant cudaError := 27;
   cudaError_cudaErrorMixedDeviceExecution : constant cudaError := 28;
   cudaError_cudaErrorNotYetImplemented : constant cudaError := 31;
   cudaError_cudaErrorMemoryValueTooLarge : constant cudaError := 32;
   cudaError_cudaErrorStubLibrary : constant cudaError := 34;
   cudaError_cudaErrorInsufficientDriver : constant cudaError := 35;
   cudaError_cudaErrorCallRequiresNewerDriver : constant cudaError := 36;
   cudaError_cudaErrorInvalidSurface : constant cudaError := 37;
   cudaError_cudaErrorDuplicateVariableName : constant cudaError := 43;
   cudaError_cudaErrorDuplicateTextureName : constant cudaError := 44;
   cudaError_cudaErrorDuplicateSurfaceName : constant cudaError := 45;
   cudaError_cudaErrorDevicesUnavailable : constant cudaError := 46;
   cudaError_cudaErrorIncompatibleDriverContext : constant cudaError := 49;
   cudaError_cudaErrorMissingConfiguration : constant cudaError := 52;
   cudaError_cudaErrorPriorLaunchFailure : constant cudaError := 53;
   cudaError_cudaErrorLaunchMaxDepthExceeded : constant cudaError := 65;
   cudaError_cudaErrorLaunchFileScopedTex : constant cudaError := 66;
   cudaError_cudaErrorLaunchFileScopedSurf : constant cudaError := 67;
   cudaError_cudaErrorSyncDepthExceeded : constant cudaError := 68;
   cudaError_cudaErrorLaunchPendingCountExceeded : constant cudaError := 69;
   cudaError_cudaErrorInvalidDeviceFunction : constant cudaError := 98;
   cudaError_cudaErrorNoDevice : constant cudaError := 100;
   cudaError_cudaErrorInvalidDevice : constant cudaError := 101;
   cudaError_cudaErrorDeviceNotLicensed : constant cudaError := 102;
   cudaError_cudaErrorSoftwareValidityNotEstablished : constant cudaError := 103;
   cudaError_cudaErrorStartupFailure : constant cudaError := 127;
   cudaError_cudaErrorInvalidKernelImage : constant cudaError := 200;
   cudaError_cudaErrorDeviceUninitialized : constant cudaError := 201;
   cudaError_cudaErrorMapBufferObjectFailed : constant cudaError := 205;
   cudaError_cudaErrorUnmapBufferObjectFailed : constant cudaError := 206;
   cudaError_cudaErrorArrayIsMapped : constant cudaError := 207;
   cudaError_cudaErrorAlreadyMapped : constant cudaError := 208;
   cudaError_cudaErrorNoKernelImageForDevice : constant cudaError := 209;
   cudaError_cudaErrorAlreadyAcquired : constant cudaError := 210;
   cudaError_cudaErrorNotMapped : constant cudaError := 211;
   cudaError_cudaErrorNotMappedAsArray : constant cudaError := 212;
   cudaError_cudaErrorNotMappedAsPointer : constant cudaError := 213;
   cudaError_cudaErrorECCUncorrectable : constant cudaError := 214;
   cudaError_cudaErrorUnsupportedLimit : constant cudaError := 215;
   cudaError_cudaErrorDeviceAlreadyInUse : constant cudaError := 216;
   cudaError_cudaErrorPeerAccessUnsupported : constant cudaError := 217;
   cudaError_cudaErrorInvalidPtx : constant cudaError := 218;
   cudaError_cudaErrorInvalidGraphicsContext : constant cudaError := 219;
   cudaError_cudaErrorNvlinkUncorrectable : constant cudaError := 220;
   cudaError_cudaErrorJitCompilerNotFound : constant cudaError := 221;
   cudaError_cudaErrorUnsupportedPtxVersion : constant cudaError := 222;
   cudaError_cudaErrorJitCompilationDisabled : constant cudaError := 223;
   cudaError_cudaErrorUnsupportedExecAffinity : constant cudaError := 224;
   cudaError_cudaErrorInvalidSource : constant cudaError := 300;
   cudaError_cudaErrorFileNotFound : constant cudaError := 301;
   cudaError_cudaErrorSharedObjectSymbolNotFound : constant cudaError := 302;
   cudaError_cudaErrorSharedObjectInitFailed : constant cudaError := 303;
   cudaError_cudaErrorOperatingSystem : constant cudaError := 304;
   cudaError_cudaErrorInvalidResourceHandle : constant cudaError := 400;
   cudaError_cudaErrorIllegalState : constant cudaError := 401;
   cudaError_cudaErrorSymbolNotFound : constant cudaError := 500;
   cudaError_cudaErrorNotReady : constant cudaError := 600;
   cudaError_cudaErrorIllegalAddress : constant cudaError := 700;
   cudaError_cudaErrorLaunchOutOfResources : constant cudaError := 701;
   cudaError_cudaErrorLaunchTimeout : constant cudaError := 702;
   cudaError_cudaErrorLaunchIncompatibleTexturing : constant cudaError := 703;
   cudaError_cudaErrorPeerAccessAlreadyEnabled : constant cudaError := 704;
   cudaError_cudaErrorPeerAccessNotEnabled : constant cudaError := 705;
   cudaError_cudaErrorSetOnActiveProcess : constant cudaError := 708;
   cudaError_cudaErrorContextIsDestroyed : constant cudaError := 709;
   cudaError_cudaErrorAssert : constant cudaError := 710;
   cudaError_cudaErrorTooManyPeers : constant cudaError := 711;
   cudaError_cudaErrorHostMemoryAlreadyRegistered : constant cudaError := 712;
   cudaError_cudaErrorHostMemoryNotRegistered : constant cudaError := 713;
   cudaError_cudaErrorHardwareStackError : constant cudaError := 714;
   cudaError_cudaErrorIllegalInstruction : constant cudaError := 715;
   cudaError_cudaErrorMisalignedAddress : constant cudaError := 716;
   cudaError_cudaErrorInvalidAddressSpace : constant cudaError := 717;
   cudaError_cudaErrorInvalidPc : constant cudaError := 718;
   cudaError_cudaErrorLaunchFailure : constant cudaError := 719;
   cudaError_cudaErrorCooperativeLaunchTooLarge : constant cudaError := 720;
   cudaError_cudaErrorNotPermitted : constant cudaError := 800;
   cudaError_cudaErrorNotSupported : constant cudaError := 801;
   cudaError_cudaErrorSystemNotReady : constant cudaError := 802;
   cudaError_cudaErrorSystemDriverMismatch : constant cudaError := 803;
   cudaError_cudaErrorCompatNotSupportedOnDevice : constant cudaError := 804;
   cudaError_cudaErrorMpsConnectionFailed : constant cudaError := 805;
   cudaError_cudaErrorMpsRpcFailure : constant cudaError := 806;
   cudaError_cudaErrorMpsServerNotReady : constant cudaError := 807;
   cudaError_cudaErrorMpsMaxClientsReached : constant cudaError := 808;
   cudaError_cudaErrorMpsMaxConnectionsReached : constant cudaError := 809;
   cudaError_cudaErrorStreamCaptureUnsupported : constant cudaError := 900;
   cudaError_cudaErrorStreamCaptureInvalidated : constant cudaError := 901;
   cudaError_cudaErrorStreamCaptureMerge : constant cudaError := 902;
   cudaError_cudaErrorStreamCaptureUnmatched : constant cudaError := 903;
   cudaError_cudaErrorStreamCaptureUnjoined : constant cudaError := 904;
   cudaError_cudaErrorStreamCaptureIsolation : constant cudaError := 905;
   cudaError_cudaErrorStreamCaptureImplicit : constant cudaError := 906;
   cudaError_cudaErrorCapturedEvent : constant cudaError := 907;
   cudaError_cudaErrorStreamCaptureWrongThread : constant cudaError := 908;
   cudaError_cudaErrorTimeout : constant cudaError := 909;
   cudaError_cudaErrorGraphExecUpdateFailure : constant cudaError := 910;
   cudaError_cudaErrorExternalDevice : constant cudaError := 911;
   cudaError_cudaErrorUnknown : constant cudaError := 999;
   cudaError_cudaErrorApiFailureBase : constant cudaError := 10000;  -- /usr/local/cuda-11.5/include//driver_types.h:201

   type cudaChannelFormatKind is 
     (cudaChannelFormatKindSigned,
      cudaChannelFormatKindUnsigned,
      cudaChannelFormatKindFloat,
      cudaChannelFormatKindNone,
      cudaChannelFormatKindNV12,
      cudaChannelFormatKindUnsignedNormalized8X1,
      cudaChannelFormatKindUnsignedNormalized8X2,
      cudaChannelFormatKindUnsignedNormalized8X4,
      cudaChannelFormatKindUnsignedNormalized16X1,
      cudaChannelFormatKindUnsignedNormalized16X2,
      cudaChannelFormatKindUnsignedNormalized16X4,
      cudaChannelFormatKindSignedNormalized8X1,
      cudaChannelFormatKindSignedNormalized8X2,
      cudaChannelFormatKindSignedNormalized8X4,
      cudaChannelFormatKindSignedNormalized16X1,
      cudaChannelFormatKindSignedNormalized16X2,
      cudaChannelFormatKindSignedNormalized16X4,
      cudaChannelFormatKindUnsignedBlockCompressed1,
      cudaChannelFormatKindUnsignedBlockCompressed1SRGB,
      cudaChannelFormatKindUnsignedBlockCompressed2,
      cudaChannelFormatKindUnsignedBlockCompressed2SRGB,
      cudaChannelFormatKindUnsignedBlockCompressed3,
      cudaChannelFormatKindUnsignedBlockCompressed3SRGB,
      cudaChannelFormatKindUnsignedBlockCompressed4,
      cudaChannelFormatKindSignedBlockCompressed4,
      cudaChannelFormatKindUnsignedBlockCompressed5,
      cudaChannelFormatKindSignedBlockCompressed5,
      cudaChannelFormatKindUnsignedBlockCompressed6H,
      cudaChannelFormatKindSignedBlockCompressed6H,
      cudaChannelFormatKindUnsignedBlockCompressed7,
      cudaChannelFormatKindUnsignedBlockCompressed7SRGB)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1070

   type cudaChannelFormatDesc is record
      x : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1110
      y : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1111
      z : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1112
      w : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1113
      f : aliased cudaChannelFormatKind;  -- /usr/local/cuda-11.5/include//driver_types.h:1114
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1108

   type cudaArray is null record;   -- incomplete struct

   type cudaArray_t is access all cudaArray;  -- /usr/local/cuda-11.5/include//driver_types.h:1120

   type cudaArray_const_t is access constant cudaArray;  -- /usr/local/cuda-11.5/include//driver_types.h:1125

   type cudaMipmappedArray is null record;   -- incomplete struct

   type cudaMipmappedArray_t is access all cudaMipmappedArray;  -- /usr/local/cuda-11.5/include//driver_types.h:1132

   type cudaMipmappedArray_const_t is access constant cudaMipmappedArray;  -- /usr/local/cuda-11.5/include//driver_types.h:1137

   type anon_struct965 is record
      width : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1151
      height : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1152
      depth : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1153
   end record
   with Convention => C_Pass_By_Copy;
   type anon_array967 is array (0 .. 3) of aliased unsigned;
   type cudaArraySparseProperties is record
      tileExtent : aliased anon_struct965;  -- /usr/local/cuda-11.5/include//driver_types.h:1154
      miptailFirstLevel : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1155
      miptailSize : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:1156
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1157
      reserved : aliased anon_array967;  -- /usr/local/cuda-11.5/include//driver_types.h:1158
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1149

   type cudaMemoryType is 
     (cudaMemoryTypeUnregistered,
      cudaMemoryTypeHost,
      cudaMemoryTypeDevice,
      cudaMemoryTypeManaged)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1164

   type cudaMemcpyKind is 
     (cudaMemcpyHostToHost,
      cudaMemcpyHostToDevice,
      cudaMemcpyDeviceToHost,
      cudaMemcpyDeviceToDevice,
      cudaMemcpyDefault)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1175

   type cudaPitchedPtr is record
      ptr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1191
      pitch : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1192
      xsize : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1193
      ysize : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1194
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1189

   type cudaExtent is record
      width : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1204
      height : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1205
      depth : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1206
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1202

   type cudaPos is record
      x : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1216
      y : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1217
      z : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1218
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1214

   type cudaMemcpy3DParms is record
      srcArray : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1226
      srcPos : aliased cudaPos;  -- /usr/local/cuda-11.5/include//driver_types.h:1227
      srcPtr : aliased cudaPitchedPtr;  -- /usr/local/cuda-11.5/include//driver_types.h:1228
      dstArray : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1230
      dstPos : aliased cudaPos;  -- /usr/local/cuda-11.5/include//driver_types.h:1231
      dstPtr : aliased cudaPitchedPtr;  -- /usr/local/cuda-11.5/include//driver_types.h:1232
      extent : aliased cudaExtent;  -- /usr/local/cuda-11.5/include//driver_types.h:1234
      kind : aliased cudaMemcpyKind;  -- /usr/local/cuda-11.5/include//driver_types.h:1235
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1224

   type cudaMemcpy3DPeerParms is record
      srcArray : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1243
      srcPos : aliased cudaPos;  -- /usr/local/cuda-11.5/include//driver_types.h:1244
      srcPtr : aliased cudaPitchedPtr;  -- /usr/local/cuda-11.5/include//driver_types.h:1245
      srcDevice : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1246
      dstArray : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1248
      dstPos : aliased cudaPos;  -- /usr/local/cuda-11.5/include//driver_types.h:1249
      dstPtr : aliased cudaPitchedPtr;  -- /usr/local/cuda-11.5/include//driver_types.h:1250
      dstDevice : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1251
      extent : aliased cudaExtent;  -- /usr/local/cuda-11.5/include//driver_types.h:1253
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1241

   type cudaMemsetParams is record
      dst : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1260
      pitch : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1261
      value : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1262
      elementSize : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1263
      width : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1264
      height : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1265
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1259

   type cudaAccessProperty is 
     (cudaAccessPropertyNormal,
      cudaAccessPropertyStreaming,
      cudaAccessPropertyPersisting)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1271

   type cudaAccessPolicyWindow is record
      base_ptr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1289
      num_bytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1290
      hitRatio : aliased float;  -- /usr/local/cuda-11.5/include//driver_types.h:1291
      hitProp : aliased cudaAccessProperty;  -- /usr/local/cuda-11.5/include//driver_types.h:1292
      missProp : aliased cudaAccessProperty;  -- /usr/local/cuda-11.5/include//driver_types.h:1293
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1288

   type cudaHostFn_t is access procedure (arg1 : System.Address)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1306

   type cudaHostNodeParams is record
      fn : cudaHostFn_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1312
      userData : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1313
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1311

   type cudaStreamCaptureStatus is 
     (cudaStreamCaptureStatusNone,
      cudaStreamCaptureStatusActive,
      cudaStreamCaptureStatusInvalidated)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1319

   type cudaStreamCaptureMode is 
     (cudaStreamCaptureModeGlobal,
      cudaStreamCaptureModeThreadLocal,
      cudaStreamCaptureModeRelaxed)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1330

   subtype cudaSynchronizationPolicy is unsigned;
   cudaSynchronizationPolicy_cudaSyncPolicyAuto : constant cudaSynchronizationPolicy := 1;
   cudaSynchronizationPolicy_cudaSyncPolicySpin : constant cudaSynchronizationPolicy := 2;
   cudaSynchronizationPolicy_cudaSyncPolicyYield : constant cudaSynchronizationPolicy := 3;
   cudaSynchronizationPolicy_cudaSyncPolicyBlockingSync : constant cudaSynchronizationPolicy := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:1336

   subtype cudaStreamAttrID is unsigned;
   cudaStreamAttrID_cudaStreamAttributeAccessPolicyWindow : constant cudaStreamAttrID := 1;
   cudaStreamAttrID_cudaStreamAttributeSynchronizationPolicy : constant cudaStreamAttrID := 3;  -- /usr/local/cuda-11.5/include//driver_types.h:1346

   type cudaStreamAttrValue (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            accessPolicyWindow : aliased cudaAccessPolicyWindow;  -- /usr/local/cuda-11.5/include//driver_types.h:1355
         when others =>
            syncPolicy : aliased cudaSynchronizationPolicy;  -- /usr/local/cuda-11.5/include//driver_types.h:1356
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;  -- /usr/local/cuda-11.5/include//driver_types.h:1354

   type cudaStreamUpdateCaptureDependenciesFlags is 
     (cudaStreamAddCaptureDependencies,
      cudaStreamSetCaptureDependencies)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1362

   subtype cudaUserObjectFlags is unsigned;
   cudaUserObjectFlags_cudaUserObjectNoDestructorSync : constant cudaUserObjectFlags := 1;  -- /usr/local/cuda-11.5/include//driver_types.h:1370

   subtype cudaUserObjectRetainFlags is unsigned;
   cudaUserObjectRetainFlags_cudaGraphUserObjectMove : constant cudaUserObjectRetainFlags := 1;  -- /usr/local/cuda-11.5/include//driver_types.h:1377

   type cudaGraphicsResource is null record;   -- incomplete struct

   subtype cudaGraphicsRegisterFlags is unsigned;
   cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsNone : constant cudaGraphicsRegisterFlags := 0;
   cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsReadOnly : constant cudaGraphicsRegisterFlags := 1;
   cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsWriteDiscard : constant cudaGraphicsRegisterFlags := 2;
   cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsSurfaceLoadStore : constant cudaGraphicsRegisterFlags := 4;
   cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsTextureGather : constant cudaGraphicsRegisterFlags := 8;  -- /usr/local/cuda-11.5/include//driver_types.h:1389

   type cudaGraphicsMapFlags is 
     (cudaGraphicsMapFlagsNone,
      cudaGraphicsMapFlagsReadOnly,
      cudaGraphicsMapFlagsWriteDiscard)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1401

   type cudaGraphicsCubeFace is 
     (cudaGraphicsCubeFacePositiveX,
      cudaGraphicsCubeFaceNegativeX,
      cudaGraphicsCubeFacePositiveY,
      cudaGraphicsCubeFaceNegativeY,
      cudaGraphicsCubeFacePositiveZ,
      cudaGraphicsCubeFaceNegativeZ)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1411

   subtype cudaKernelNodeAttrID is unsigned;
   cudaKernelNodeAttrID_cudaKernelNodeAttributeAccessPolicyWindow : constant cudaKernelNodeAttrID := 1;
   cudaKernelNodeAttrID_cudaKernelNodeAttributeCooperative : constant cudaKernelNodeAttrID := 2;  -- /usr/local/cuda-11.5/include//driver_types.h:1424

   type cudaKernelNodeAttrValue (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            accessPolicyWindow : aliased cudaAccessPolicyWindow;  -- /usr/local/cuda-11.5/include//driver_types.h:1433
         when others =>
            cooperative : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1434
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;  -- /usr/local/cuda-11.5/include//driver_types.h:1432

   type cudaResourceType is 
     (cudaResourceTypeArray,
      cudaResourceTypeMipmappedArray,
      cudaResourceTypeLinear,
      cudaResourceTypePitch2D)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1440

   type cudaResourceViewFormat is 
     (cudaResViewFormatNone,
      cudaResViewFormatUnsignedChar1,
      cudaResViewFormatUnsignedChar2,
      cudaResViewFormatUnsignedChar4,
      cudaResViewFormatSignedChar1,
      cudaResViewFormatSignedChar2,
      cudaResViewFormatSignedChar4,
      cudaResViewFormatUnsignedShort1,
      cudaResViewFormatUnsignedShort2,
      cudaResViewFormatUnsignedShort4,
      cudaResViewFormatSignedShort1,
      cudaResViewFormatSignedShort2,
      cudaResViewFormatSignedShort4,
      cudaResViewFormatUnsignedInt1,
      cudaResViewFormatUnsignedInt2,
      cudaResViewFormatUnsignedInt4,
      cudaResViewFormatSignedInt1,
      cudaResViewFormatSignedInt2,
      cudaResViewFormatSignedInt4,
      cudaResViewFormatHalf1,
      cudaResViewFormatHalf2,
      cudaResViewFormatHalf4,
      cudaResViewFormatFloat1,
      cudaResViewFormatFloat2,
      cudaResViewFormatFloat4,
      cudaResViewFormatUnsignedBlockCompressed1,
      cudaResViewFormatUnsignedBlockCompressed2,
      cudaResViewFormatUnsignedBlockCompressed3,
      cudaResViewFormatUnsignedBlockCompressed4,
      cudaResViewFormatSignedBlockCompressed4,
      cudaResViewFormatUnsignedBlockCompressed5,
      cudaResViewFormatSignedBlockCompressed5,
      cudaResViewFormatUnsignedBlockCompressed6H,
      cudaResViewFormatSignedBlockCompressed6H,
      cudaResViewFormatUnsignedBlockCompressed7)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1451

   type anon_struct998 is record
      c_array : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1498
   end record
   with Convention => C_Pass_By_Copy;
   type anon_struct999 is record
      mipmap : cudaMipmappedArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1501
   end record
   with Convention => C_Pass_By_Copy;
   type anon_struct1000 is record
      devPtr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1504
      desc : aliased cudaChannelFormatDesc;  -- /usr/local/cuda-11.5/include//driver_types.h:1505
      sizeInBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1506
   end record
   with Convention => C_Pass_By_Copy;
   type anon_struct1001 is record
      devPtr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1509
      desc : aliased cudaChannelFormatDesc;  -- /usr/local/cuda-11.5/include//driver_types.h:1510
      width : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1511
      height : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1512
      pitchInBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1513
   end record
   with Convention => C_Pass_By_Copy;
   type anon_union997 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            c_array : aliased anon_struct998;  -- /usr/local/cuda-11.5/include//driver_types.h:1499
         when 1 =>
            mipmap : aliased anon_struct999;  -- /usr/local/cuda-11.5/include//driver_types.h:1502
         when 2 =>
            linear : aliased anon_struct1000;  -- /usr/local/cuda-11.5/include//driver_types.h:1507
         when others =>
            pitch2D : aliased anon_struct1001;  -- /usr/local/cuda-11.5/include//driver_types.h:1514
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type cudaResourceDesc is record
      resType : aliased cudaResourceType;  -- /usr/local/cuda-11.5/include//driver_types.h:1494
      res : aliased anon_union997;  -- /usr/local/cuda-11.5/include//driver_types.h:1515
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1493

   type cudaResourceViewDesc is record
      format : aliased cudaResourceViewFormat;  -- /usr/local/cuda-11.5/include//driver_types.h:1523
      width : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1524
      height : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1525
      depth : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1526
      firstMipmapLevel : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1527
      lastMipmapLevel : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1528
      firstLayer : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1529
      lastLayer : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1530
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1521

   type cudaPointerAttributes is record
      c_type : aliased cudaMemoryType;  -- /usr/local/cuda-11.5/include//driver_types.h:1542
      device : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1553
      devicePointer : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1559
      hostPointer : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1568
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1536

   type cudaFuncAttributes is record
      sharedSizeBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1581
      constSizeBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1587
      localSizeBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1592
      maxThreadsPerBlock : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1599
      numRegs : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1604
      ptxVersion : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1611
      binaryVersion : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1618
      cacheModeCA : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1624
      maxDynamicSharedSizeBytes : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1631
      preferredShmemCarveout : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1640
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:1574

   subtype cudaFuncAttribute is unsigned;
   cudaFuncAttribute_cudaFuncAttributeMaxDynamicSharedMemorySize : constant cudaFuncAttribute := 8;
   cudaFuncAttribute_cudaFuncAttributePreferredSharedMemoryCarveout : constant cudaFuncAttribute := 9;
   cudaFuncAttribute_cudaFuncAttributeMax : constant cudaFuncAttribute := 10;  -- /usr/local/cuda-11.5/include//driver_types.h:1695

   type cudaFuncCache is 
     (cudaFuncCachePreferNone,
      cudaFuncCachePreferShared,
      cudaFuncCachePreferL1,
      cudaFuncCachePreferEqual)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1713

   type cudaSharedMemConfig is 
     (cudaSharedMemBankSizeDefault,
      cudaSharedMemBankSizeFourByte,
      cudaSharedMemBankSizeEightByte)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1725

   subtype cudaSharedCarveout is int;
   cudaSharedCarveout_cudaSharedmemCarveoutDefault : constant cudaSharedCarveout := -1;
   cudaSharedCarveout_cudaSharedmemCarveoutMaxShared : constant cudaSharedCarveout := 100;
   cudaSharedCarveout_cudaSharedmemCarveoutMaxL1 : constant cudaSharedCarveout := 0;  -- /usr/local/cuda-11.5/include//driver_types.h:1735

   type cudaComputeMode is 
     (cudaComputeModeDefault,
      cudaComputeModeExclusive,
      cudaComputeModeProhibited,
      cudaComputeModeExclusiveProcess)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1755

   type cudaLimit is 
     (cudaLimitStackSize,
      cudaLimitPrintfFifoSize,
      cudaLimitMallocHeapSize,
      cudaLimitDevRuntimeSyncDepth,
      cudaLimitDevRuntimePendingLaunchCount,
      cudaLimitMaxL2FetchGranularity,
      cudaLimitPersistingL2CacheSize)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1766

   subtype cudaMemoryAdvise is unsigned;
   cudaMemoryAdvise_cudaMemAdviseSetReadMostly : constant cudaMemoryAdvise := 1;
   cudaMemoryAdvise_cudaMemAdviseUnsetReadMostly : constant cudaMemoryAdvise := 2;
   cudaMemoryAdvise_cudaMemAdviseSetPreferredLocation : constant cudaMemoryAdvise := 3;
   cudaMemoryAdvise_cudaMemAdviseUnsetPreferredLocation : constant cudaMemoryAdvise := 4;
   cudaMemoryAdvise_cudaMemAdviseSetAccessedBy : constant cudaMemoryAdvise := 5;
   cudaMemoryAdvise_cudaMemAdviseUnsetAccessedBy : constant cudaMemoryAdvise := 6;  -- /usr/local/cuda-11.5/include//driver_types.h:1780

   subtype cudaMemRangeAttribute is unsigned;
   cudaMemRangeAttribute_cudaMemRangeAttributeReadMostly : constant cudaMemRangeAttribute := 1;
   cudaMemRangeAttribute_cudaMemRangeAttributePreferredLocation : constant cudaMemRangeAttribute := 2;
   cudaMemRangeAttribute_cudaMemRangeAttributeAccessedBy : constant cudaMemRangeAttribute := 3;
   cudaMemRangeAttribute_cudaMemRangeAttributeLastPrefetchLocation : constant cudaMemRangeAttribute := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:1793

   type cudaOutputMode is 
     (cudaKeyValuePair,
      cudaCSV)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1804

   subtype cudaFlushGPUDirectRDMAWritesOptions is unsigned;
   cudaFlushGPUDirectRDMAWritesOptions_cudaFlushGPUDirectRDMAWritesOptionHost : constant cudaFlushGPUDirectRDMAWritesOptions := 1;
   cudaFlushGPUDirectRDMAWritesOptions_cudaFlushGPUDirectRDMAWritesOptionMemOps : constant cudaFlushGPUDirectRDMAWritesOptions := 2;  -- /usr/local/cuda-11.5/include//driver_types.h:1813

   subtype cudaGPUDirectRDMAWritesOrdering is unsigned;
   cudaGPUDirectRDMAWritesOrdering_cudaGPUDirectRDMAWritesOrderingNone : constant cudaGPUDirectRDMAWritesOrdering := 0;
   cudaGPUDirectRDMAWritesOrdering_cudaGPUDirectRDMAWritesOrderingOwner : constant cudaGPUDirectRDMAWritesOrdering := 100;
   cudaGPUDirectRDMAWritesOrdering_cudaGPUDirectRDMAWritesOrderingAllDevices : constant cudaGPUDirectRDMAWritesOrdering := 200;  -- /usr/local/cuda-11.5/include//driver_types.h:1821

   subtype cudaFlushGPUDirectRDMAWritesScope is unsigned;
   cudaFlushGPUDirectRDMAWritesScope_cudaFlushGPUDirectRDMAWritesToOwner : constant cudaFlushGPUDirectRDMAWritesScope := 100;
   cudaFlushGPUDirectRDMAWritesScope_cudaFlushGPUDirectRDMAWritesToAllDevices : constant cudaFlushGPUDirectRDMAWritesScope := 200;  -- /usr/local/cuda-11.5/include//driver_types.h:1830

   type cudaFlushGPUDirectRDMAWritesTarget is 
     (cudaFlushGPUDirectRDMAWritesTargetCurrentDevice)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:1838

   subtype cudaDeviceAttr is unsigned;
   cudaDeviceAttr_cudaDevAttrMaxThreadsPerBlock : constant cudaDeviceAttr := 1;
   cudaDeviceAttr_cudaDevAttrMaxBlockDimX : constant cudaDeviceAttr := 2;
   cudaDeviceAttr_cudaDevAttrMaxBlockDimY : constant cudaDeviceAttr := 3;
   cudaDeviceAttr_cudaDevAttrMaxBlockDimZ : constant cudaDeviceAttr := 4;
   cudaDeviceAttr_cudaDevAttrMaxGridDimX : constant cudaDeviceAttr := 5;
   cudaDeviceAttr_cudaDevAttrMaxGridDimY : constant cudaDeviceAttr := 6;
   cudaDeviceAttr_cudaDevAttrMaxGridDimZ : constant cudaDeviceAttr := 7;
   cudaDeviceAttr_cudaDevAttrMaxSharedMemoryPerBlock : constant cudaDeviceAttr := 8;
   cudaDeviceAttr_cudaDevAttrTotalConstantMemory : constant cudaDeviceAttr := 9;
   cudaDeviceAttr_cudaDevAttrWarpSize : constant cudaDeviceAttr := 10;
   cudaDeviceAttr_cudaDevAttrMaxPitch : constant cudaDeviceAttr := 11;
   cudaDeviceAttr_cudaDevAttrMaxRegistersPerBlock : constant cudaDeviceAttr := 12;
   cudaDeviceAttr_cudaDevAttrClockRate : constant cudaDeviceAttr := 13;
   cudaDeviceAttr_cudaDevAttrTextureAlignment : constant cudaDeviceAttr := 14;
   cudaDeviceAttr_cudaDevAttrGpuOverlap : constant cudaDeviceAttr := 15;
   cudaDeviceAttr_cudaDevAttrMultiProcessorCount : constant cudaDeviceAttr := 16;
   cudaDeviceAttr_cudaDevAttrKernelExecTimeout : constant cudaDeviceAttr := 17;
   cudaDeviceAttr_cudaDevAttrIntegrated : constant cudaDeviceAttr := 18;
   cudaDeviceAttr_cudaDevAttrCanMapHostMemory : constant cudaDeviceAttr := 19;
   cudaDeviceAttr_cudaDevAttrComputeMode : constant cudaDeviceAttr := 20;
   cudaDeviceAttr_cudaDevAttrMaxTexture1DWidth : constant cudaDeviceAttr := 21;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DWidth : constant cudaDeviceAttr := 22;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DHeight : constant cudaDeviceAttr := 23;
   cudaDeviceAttr_cudaDevAttrMaxTexture3DWidth : constant cudaDeviceAttr := 24;
   cudaDeviceAttr_cudaDevAttrMaxTexture3DHeight : constant cudaDeviceAttr := 25;
   cudaDeviceAttr_cudaDevAttrMaxTexture3DDepth : constant cudaDeviceAttr := 26;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DLayeredWidth : constant cudaDeviceAttr := 27;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DLayeredHeight : constant cudaDeviceAttr := 28;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DLayeredLayers : constant cudaDeviceAttr := 29;
   cudaDeviceAttr_cudaDevAttrSurfaceAlignment : constant cudaDeviceAttr := 30;
   cudaDeviceAttr_cudaDevAttrConcurrentKernels : constant cudaDeviceAttr := 31;
   cudaDeviceAttr_cudaDevAttrEccEnabled : constant cudaDeviceAttr := 32;
   cudaDeviceAttr_cudaDevAttrPciBusId : constant cudaDeviceAttr := 33;
   cudaDeviceAttr_cudaDevAttrPciDeviceId : constant cudaDeviceAttr := 34;
   cudaDeviceAttr_cudaDevAttrTccDriver : constant cudaDeviceAttr := 35;
   cudaDeviceAttr_cudaDevAttrMemoryClockRate : constant cudaDeviceAttr := 36;
   cudaDeviceAttr_cudaDevAttrGlobalMemoryBusWidth : constant cudaDeviceAttr := 37;
   cudaDeviceAttr_cudaDevAttrL2CacheSize : constant cudaDeviceAttr := 38;
   cudaDeviceAttr_cudaDevAttrMaxThreadsPerMultiProcessor : constant cudaDeviceAttr := 39;
   cudaDeviceAttr_cudaDevAttrAsyncEngineCount : constant cudaDeviceAttr := 40;
   cudaDeviceAttr_cudaDevAttrUnifiedAddressing : constant cudaDeviceAttr := 41;
   cudaDeviceAttr_cudaDevAttrMaxTexture1DLayeredWidth : constant cudaDeviceAttr := 42;
   cudaDeviceAttr_cudaDevAttrMaxTexture1DLayeredLayers : constant cudaDeviceAttr := 43;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DGatherWidth : constant cudaDeviceAttr := 45;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DGatherHeight : constant cudaDeviceAttr := 46;
   cudaDeviceAttr_cudaDevAttrMaxTexture3DWidthAlt : constant cudaDeviceAttr := 47;
   cudaDeviceAttr_cudaDevAttrMaxTexture3DHeightAlt : constant cudaDeviceAttr := 48;
   cudaDeviceAttr_cudaDevAttrMaxTexture3DDepthAlt : constant cudaDeviceAttr := 49;
   cudaDeviceAttr_cudaDevAttrPciDomainId : constant cudaDeviceAttr := 50;
   cudaDeviceAttr_cudaDevAttrTexturePitchAlignment : constant cudaDeviceAttr := 51;
   cudaDeviceAttr_cudaDevAttrMaxTextureCubemapWidth : constant cudaDeviceAttr := 52;
   cudaDeviceAttr_cudaDevAttrMaxTextureCubemapLayeredWidth : constant cudaDeviceAttr := 53;
   cudaDeviceAttr_cudaDevAttrMaxTextureCubemapLayeredLayers : constant cudaDeviceAttr := 54;
   cudaDeviceAttr_cudaDevAttrMaxSurface1DWidth : constant cudaDeviceAttr := 55;
   cudaDeviceAttr_cudaDevAttrMaxSurface2DWidth : constant cudaDeviceAttr := 56;
   cudaDeviceAttr_cudaDevAttrMaxSurface2DHeight : constant cudaDeviceAttr := 57;
   cudaDeviceAttr_cudaDevAttrMaxSurface3DWidth : constant cudaDeviceAttr := 58;
   cudaDeviceAttr_cudaDevAttrMaxSurface3DHeight : constant cudaDeviceAttr := 59;
   cudaDeviceAttr_cudaDevAttrMaxSurface3DDepth : constant cudaDeviceAttr := 60;
   cudaDeviceAttr_cudaDevAttrMaxSurface1DLayeredWidth : constant cudaDeviceAttr := 61;
   cudaDeviceAttr_cudaDevAttrMaxSurface1DLayeredLayers : constant cudaDeviceAttr := 62;
   cudaDeviceAttr_cudaDevAttrMaxSurface2DLayeredWidth : constant cudaDeviceAttr := 63;
   cudaDeviceAttr_cudaDevAttrMaxSurface2DLayeredHeight : constant cudaDeviceAttr := 64;
   cudaDeviceAttr_cudaDevAttrMaxSurface2DLayeredLayers : constant cudaDeviceAttr := 65;
   cudaDeviceAttr_cudaDevAttrMaxSurfaceCubemapWidth : constant cudaDeviceAttr := 66;
   cudaDeviceAttr_cudaDevAttrMaxSurfaceCubemapLayeredWidth : constant cudaDeviceAttr := 67;
   cudaDeviceAttr_cudaDevAttrMaxSurfaceCubemapLayeredLayers : constant cudaDeviceAttr := 68;
   cudaDeviceAttr_cudaDevAttrMaxTexture1DLinearWidth : constant cudaDeviceAttr := 69;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DLinearWidth : constant cudaDeviceAttr := 70;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DLinearHeight : constant cudaDeviceAttr := 71;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DLinearPitch : constant cudaDeviceAttr := 72;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DMipmappedWidth : constant cudaDeviceAttr := 73;
   cudaDeviceAttr_cudaDevAttrMaxTexture2DMipmappedHeight : constant cudaDeviceAttr := 74;
   cudaDeviceAttr_cudaDevAttrComputeCapabilityMajor : constant cudaDeviceAttr := 75;
   cudaDeviceAttr_cudaDevAttrComputeCapabilityMinor : constant cudaDeviceAttr := 76;
   cudaDeviceAttr_cudaDevAttrMaxTexture1DMipmappedWidth : constant cudaDeviceAttr := 77;
   cudaDeviceAttr_cudaDevAttrStreamPrioritiesSupported : constant cudaDeviceAttr := 78;
   cudaDeviceAttr_cudaDevAttrGlobalL1CacheSupported : constant cudaDeviceAttr := 79;
   cudaDeviceAttr_cudaDevAttrLocalL1CacheSupported : constant cudaDeviceAttr := 80;
   cudaDeviceAttr_cudaDevAttrMaxSharedMemoryPerMultiprocessor : constant cudaDeviceAttr := 81;
   cudaDeviceAttr_cudaDevAttrMaxRegistersPerMultiprocessor : constant cudaDeviceAttr := 82;
   cudaDeviceAttr_cudaDevAttrManagedMemory : constant cudaDeviceAttr := 83;
   cudaDeviceAttr_cudaDevAttrIsMultiGpuBoard : constant cudaDeviceAttr := 84;
   cudaDeviceAttr_cudaDevAttrMultiGpuBoardGroupID : constant cudaDeviceAttr := 85;
   cudaDeviceAttr_cudaDevAttrHostNativeAtomicSupported : constant cudaDeviceAttr := 86;
   cudaDeviceAttr_cudaDevAttrSingleToDoublePrecisionPerfRatio : constant cudaDeviceAttr := 87;
   cudaDeviceAttr_cudaDevAttrPageableMemoryAccess : constant cudaDeviceAttr := 88;
   cudaDeviceAttr_cudaDevAttrConcurrentManagedAccess : constant cudaDeviceAttr := 89;
   cudaDeviceAttr_cudaDevAttrComputePreemptionSupported : constant cudaDeviceAttr := 90;
   cudaDeviceAttr_cudaDevAttrCanUseHostPointerForRegisteredMem : constant cudaDeviceAttr := 91;
   cudaDeviceAttr_cudaDevAttrReserved92 : constant cudaDeviceAttr := 92;
   cudaDeviceAttr_cudaDevAttrReserved93 : constant cudaDeviceAttr := 93;
   cudaDeviceAttr_cudaDevAttrReserved94 : constant cudaDeviceAttr := 94;
   cudaDeviceAttr_cudaDevAttrCooperativeLaunch : constant cudaDeviceAttr := 95;
   cudaDeviceAttr_cudaDevAttrCooperativeMultiDeviceLaunch : constant cudaDeviceAttr := 96;
   cudaDeviceAttr_cudaDevAttrMaxSharedMemoryPerBlockOptin : constant cudaDeviceAttr := 97;
   cudaDeviceAttr_cudaDevAttrCanFlushRemoteWrites : constant cudaDeviceAttr := 98;
   cudaDeviceAttr_cudaDevAttrHostRegisterSupported : constant cudaDeviceAttr := 99;
   cudaDeviceAttr_cudaDevAttrPageableMemoryAccessUsesHostPageTables : constant cudaDeviceAttr := 100;
   cudaDeviceAttr_cudaDevAttrDirectManagedMemAccessFromHost : constant cudaDeviceAttr := 101;
   cudaDeviceAttr_cudaDevAttrMaxBlocksPerMultiprocessor : constant cudaDeviceAttr := 106;
   cudaDeviceAttr_cudaDevAttrMaxPersistingL2CacheSize : constant cudaDeviceAttr := 108;
   cudaDeviceAttr_cudaDevAttrMaxAccessPolicyWindowSize : constant cudaDeviceAttr := 109;
   cudaDeviceAttr_cudaDevAttrReservedSharedMemoryPerBlock : constant cudaDeviceAttr := 111;
   cudaDeviceAttr_cudaDevAttrSparseCudaArraySupported : constant cudaDeviceAttr := 112;
   cudaDeviceAttr_cudaDevAttrHostRegisterReadOnlySupported : constant cudaDeviceAttr := 113;
   cudaDeviceAttr_cudaDevAttrTimelineSemaphoreInteropSupported : constant cudaDeviceAttr := 114;
   cudaDeviceAttr_cudaDevAttrMaxTimelineSemaphoreInteropSupported : constant cudaDeviceAttr := 114;
   cudaDeviceAttr_cudaDevAttrMemoryPoolsSupported : constant cudaDeviceAttr := 115;
   cudaDeviceAttr_cudaDevAttrGPUDirectRDMASupported : constant cudaDeviceAttr := 116;
   cudaDeviceAttr_cudaDevAttrGPUDirectRDMAFlushWritesOptions : constant cudaDeviceAttr := 117;
   cudaDeviceAttr_cudaDevAttrGPUDirectRDMAWritesOrdering : constant cudaDeviceAttr := 118;
   cudaDeviceAttr_cudaDevAttrMemoryPoolSupportedHandleTypes : constant cudaDeviceAttr := 119;
   cudaDeviceAttr_cudaDevAttrMax : constant cudaDeviceAttr := 120;  -- /usr/local/cuda-11.5/include//driver_types.h:1846

   subtype cudaMemPoolAttr is unsigned;
   cudaMemPoolAttr_cudaMemPoolReuseFollowEventDependencies : constant cudaMemPoolAttr := 1;
   cudaMemPoolAttr_cudaMemPoolReuseAllowOpportunistic : constant cudaMemPoolAttr := 2;
   cudaMemPoolAttr_cudaMemPoolReuseAllowInternalDependencies : constant cudaMemPoolAttr := 3;
   cudaMemPoolAttr_cudaMemPoolAttrReleaseThreshold : constant cudaMemPoolAttr := 4;
   cudaMemPoolAttr_cudaMemPoolAttrReservedMemCurrent : constant cudaMemPoolAttr := 5;
   cudaMemPoolAttr_cudaMemPoolAttrReservedMemHigh : constant cudaMemPoolAttr := 6;
   cudaMemPoolAttr_cudaMemPoolAttrUsedMemCurrent : constant cudaMemPoolAttr := 7;
   cudaMemPoolAttr_cudaMemPoolAttrUsedMemHigh : constant cudaMemPoolAttr := 8;  -- /usr/local/cuda-11.5/include//driver_types.h:1970

   type cudaMemLocationType is 
     (cudaMemLocationTypeInvalid,
      cudaMemLocationTypeDevice)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:2038

   type cudaMemLocation is record
      c_type : aliased cudaMemLocationType;  -- /usr/local/cuda-11.5/include//driver_types.h:2049
      id : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2050
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2048

   subtype cudaMemAccessFlags is unsigned;
   cudaMemAccessFlags_cudaMemAccessFlagsProtNone : constant cudaMemAccessFlags := 0;
   cudaMemAccessFlags_cudaMemAccessFlagsProtRead : constant cudaMemAccessFlags := 1;
   cudaMemAccessFlags_cudaMemAccessFlagsProtReadWrite : constant cudaMemAccessFlags := 3;  -- /usr/local/cuda-11.5/include//driver_types.h:2056

   type cudaMemAccessDesc is record
      location : aliased cudaMemLocation;  -- /usr/local/cuda-11.5/include//driver_types.h:2066
      flags : aliased cudaMemAccessFlags;  -- /usr/local/cuda-11.5/include//driver_types.h:2067
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2065

   subtype cudaMemAllocationType is unsigned;
   cudaMemAllocationType_cudaMemAllocationTypeInvalid : constant cudaMemAllocationType := 0;
   cudaMemAllocationType_cudaMemAllocationTypePinned : constant cudaMemAllocationType := 1;
   cudaMemAllocationType_cudaMemAllocationTypeMax : constant cudaMemAllocationType := 2147483647;  -- /usr/local/cuda-11.5/include//driver_types.h:2073

   subtype cudaMemAllocationHandleType is unsigned;
   cudaMemAllocationHandleType_cudaMemHandleTypeNone : constant cudaMemAllocationHandleType := 0;
   cudaMemAllocationHandleType_cudaMemHandleTypePosixFileDescriptor : constant cudaMemAllocationHandleType := 1;
   cudaMemAllocationHandleType_cudaMemHandleTypeWin32 : constant cudaMemAllocationHandleType := 2;
   cudaMemAllocationHandleType_cudaMemHandleTypeWin32Kmt : constant cudaMemAllocationHandleType := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:2085

   type anon_array1028 is array (0 .. 63) of aliased unsigned_char;
   type cudaMemPoolProps is record
      allocType : aliased cudaMemAllocationType;  -- /usr/local/cuda-11.5/include//driver_types.h:2096
      handleTypes : aliased cudaMemAllocationHandleType;  -- /usr/local/cuda-11.5/include//driver_types.h:2097
      location : aliased cudaMemLocation;  -- /usr/local/cuda-11.5/include//driver_types.h:2098
      win32SecurityAttributes : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2105
      reserved : aliased anon_array1028;  -- /usr/local/cuda-11.5/include//driver_types.h:2106
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2095

   type cudaMemPoolPtrExportData is record
      reserved : aliased anon_array1028;  -- /usr/local/cuda-11.5/include//driver_types.h:2113
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2112

   type cudaMemAllocNodeParams is record
      poolProps : aliased cudaMemPoolProps;  -- /usr/local/cuda-11.5/include//driver_types.h:2124
      accessDescs : access constant cudaMemAccessDesc;  -- /usr/local/cuda-11.5/include//driver_types.h:2125
      accessDescCount : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2126
      bytesize : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2127
      dptr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2128
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2119

   subtype cudaGraphMemAttributeType is unsigned;
   cudaGraphMemAttributeType_cudaGraphMemAttrUsedMemCurrent : constant cudaGraphMemAttributeType := 1;
   cudaGraphMemAttributeType_cudaGraphMemAttrUsedMemHigh : constant cudaGraphMemAttributeType := 2;
   cudaGraphMemAttributeType_cudaGraphMemAttrReservedMemCurrent : constant cudaGraphMemAttributeType := 3;
   cudaGraphMemAttributeType_cudaGraphMemAttrReservedMemHigh : constant cudaGraphMemAttributeType := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:2134

   subtype cudaDeviceP2PAttr is unsigned;
   cudaDeviceP2PAttr_cudaDevP2PAttrPerformanceRank : constant cudaDeviceP2PAttr := 1;
   cudaDeviceP2PAttr_cudaDevP2PAttrAccessSupported : constant cudaDeviceP2PAttr := 2;
   cudaDeviceP2PAttr_cudaDevP2PAttrNativeAtomicSupported : constant cudaDeviceP2PAttr := 3;
   cudaDeviceP2PAttr_cudaDevP2PAttrCudaArrayAccessSupported : constant cudaDeviceP2PAttr := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:2167

   subtype anon_array1037 is Interfaces.C.char_array (0 .. 15);
   type CUuuid_st is record
      bytes : aliased anon_array1037;  -- /usr/local/cuda-11.5/include//driver_types.h:2180
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2179

   subtype CUuuid is CUuuid_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2182

   subtype cudaUUID_t is CUuuid_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2184

   subtype anon_array1042 is Interfaces.C.char_array (0 .. 255);
   subtype anon_array1044 is Interfaces.C.char_array (0 .. 7);
   type anon_array1046 is array (0 .. 2) of aliased int;
   type anon_array1048 is array (0 .. 1) of aliased int;
   type cudaDeviceProp is record
      name : aliased anon_array1042;  -- /usr/local/cuda-11.5/include//driver_types.h:2191
      uuid : aliased cudaUUID_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2192
      luid : aliased anon_array1044;  -- /usr/local/cuda-11.5/include//driver_types.h:2193
      luidDeviceNodeMask : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2194
      totalGlobalMem : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2195
      sharedMemPerBlock : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2196
      regsPerBlock : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2197
      warpSize : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2198
      memPitch : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2199
      maxThreadsPerBlock : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2200
      maxThreadsDim : aliased anon_array1046;  -- /usr/local/cuda-11.5/include//driver_types.h:2201
      maxGridSize : aliased anon_array1046;  -- /usr/local/cuda-11.5/include//driver_types.h:2202
      clockRate : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2203
      totalConstMem : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2204
      major : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2205
      minor : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2206
      textureAlignment : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2207
      texturePitchAlignment : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2208
      deviceOverlap : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2209
      multiProcessorCount : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2210
      kernelExecTimeoutEnabled : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2211
      integrated : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2212
      canMapHostMemory : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2213
      computeMode : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2214
      maxTexture1D : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2215
      maxTexture1DMipmap : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2216
      maxTexture1DLinear : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2217
      maxTexture2D : aliased anon_array1048;  -- /usr/local/cuda-11.5/include//driver_types.h:2218
      maxTexture2DMipmap : aliased anon_array1048;  -- /usr/local/cuda-11.5/include//driver_types.h:2219
      maxTexture2DLinear : aliased anon_array1046;  -- /usr/local/cuda-11.5/include//driver_types.h:2220
      maxTexture2DGather : aliased anon_array1048;  -- /usr/local/cuda-11.5/include//driver_types.h:2221
      maxTexture3D : aliased anon_array1046;  -- /usr/local/cuda-11.5/include//driver_types.h:2222
      maxTexture3DAlt : aliased anon_array1046;  -- /usr/local/cuda-11.5/include//driver_types.h:2223
      maxTextureCubemap : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2224
      maxTexture1DLayered : aliased anon_array1048;  -- /usr/local/cuda-11.5/include//driver_types.h:2225
      maxTexture2DLayered : aliased anon_array1046;  -- /usr/local/cuda-11.5/include//driver_types.h:2226
      maxTextureCubemapLayered : aliased anon_array1048;  -- /usr/local/cuda-11.5/include//driver_types.h:2227
      maxSurface1D : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2228
      maxSurface2D : aliased anon_array1048;  -- /usr/local/cuda-11.5/include//driver_types.h:2229
      maxSurface3D : aliased anon_array1046;  -- /usr/local/cuda-11.5/include//driver_types.h:2230
      maxSurface1DLayered : aliased anon_array1048;  -- /usr/local/cuda-11.5/include//driver_types.h:2231
      maxSurface2DLayered : aliased anon_array1046;  -- /usr/local/cuda-11.5/include//driver_types.h:2232
      maxSurfaceCubemap : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2233
      maxSurfaceCubemapLayered : aliased anon_array1048;  -- /usr/local/cuda-11.5/include//driver_types.h:2234
      surfaceAlignment : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2235
      concurrentKernels : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2236
      ECCEnabled : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2237
      pciBusID : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2238
      pciDeviceID : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2239
      pciDomainID : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2240
      tccDriver : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2241
      asyncEngineCount : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2242
      unifiedAddressing : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2243
      memoryClockRate : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2244
      memoryBusWidth : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2245
      l2CacheSize : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2246
      persistingL2CacheMaxSize : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2247
      maxThreadsPerMultiProcessor : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2248
      streamPrioritiesSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2249
      globalL1CacheSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2250
      localL1CacheSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2251
      sharedMemPerMultiprocessor : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2252
      regsPerMultiprocessor : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2253
      managedMemory : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2254
      isMultiGpuBoard : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2255
      multiGpuBoardGroupID : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2256
      hostNativeAtomicSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2257
      singleToDoublePrecisionPerfRatio : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2258
      pageableMemoryAccess : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2259
      concurrentManagedAccess : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2260
      computePreemptionSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2261
      canUseHostPointerForRegisteredMem : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2262
      cooperativeLaunch : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2263
      cooperativeMultiDeviceLaunch : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2264
      sharedMemPerBlockOptin : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2265
      pageableMemoryAccessUsesHostPageTables : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2266
      directManagedMemAccessFromHost : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2267
      maxBlocksPerMultiProcessor : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2268
      accessPolicyMaxWindowSize : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2269
      reservedSharedMemPerBlock : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2270
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2189

   subtype anon_array1050 is Interfaces.C.char_array (0 .. 63);
   type cudaIpcEventHandle_st is record
      reserved : aliased anon_array1050;  -- /usr/local/cuda-11.5/include//driver_types.h:2374
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2372

   subtype cudaIpcEventHandle_t is cudaIpcEventHandle_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2375

   type cudaIpcMemHandle_st is record
      reserved : aliased anon_array1050;  -- /usr/local/cuda-11.5/include//driver_types.h:2382
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2380

   subtype cudaIpcMemHandle_t is cudaIpcMemHandle_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2383

   subtype cudaExternalMemoryHandleType is unsigned;
   cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeOpaqueFd : constant cudaExternalMemoryHandleType := 1;
   cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeOpaqueWin32 : constant cudaExternalMemoryHandleType := 2;
   cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeOpaqueWin32Kmt : constant cudaExternalMemoryHandleType := 3;
   cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeD3D12Heap : constant cudaExternalMemoryHandleType := 4;
   cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeD3D12Resource : constant cudaExternalMemoryHandleType := 5;
   cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeD3D11Resource : constant cudaExternalMemoryHandleType := 6;
   cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeD3D11ResourceKmt : constant cudaExternalMemoryHandleType := 7;
   cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeNvSciBuf : constant cudaExternalMemoryHandleType := 8;  -- /usr/local/cuda-11.5/include//driver_types.h:2388

   type anon_struct1057 is record
      handle : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2494
      name : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2499
   end record
   with Convention => C_Pass_By_Copy;
   type anon_union1056 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fd : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2474
         when 1 =>
            win32 : aliased anon_struct1057;  -- /usr/local/cuda-11.5/include//driver_types.h:2500
         when others =>
            nvSciBufObject : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2505
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type cudaExternalMemoryHandleDesc is record
      c_type : aliased cudaExternalMemoryHandleType;  -- /usr/local/cuda-11.5/include//driver_types.h:2467
      handle : aliased anon_union1056;  -- /usr/local/cuda-11.5/include//driver_types.h:2506
      size : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2510
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2514
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2463

   type cudaExternalMemoryBufferDesc is record
      offset : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2524
      size : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2528
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2532
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2520

   type cudaExternalMemoryMipmappedArrayDesc is record
      offset : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2543
      formatDesc : aliased cudaChannelFormatDesc;  -- /usr/local/cuda-11.5/include//driver_types.h:2547
      extent : aliased cudaExtent;  -- /usr/local/cuda-11.5/include//driver_types.h:2551
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2556
      numLevels : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2560
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2538

   subtype cudaExternalSemaphoreHandleType is unsigned;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeOpaqueFd : constant cudaExternalSemaphoreHandleType := 1;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeOpaqueWin32 : constant cudaExternalSemaphoreHandleType := 2;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeOpaqueWin32Kmt : constant cudaExternalSemaphoreHandleType := 3;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeD3D12Fence : constant cudaExternalSemaphoreHandleType := 4;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeD3D11Fence : constant cudaExternalSemaphoreHandleType := 5;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeNvSciSync : constant cudaExternalSemaphoreHandleType := 6;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeKeyedMutex : constant cudaExternalSemaphoreHandleType := 7;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeKeyedMutexKmt : constant cudaExternalSemaphoreHandleType := 8;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeTimelineSemaphoreFd : constant cudaExternalSemaphoreHandleType := 9;
   cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeTimelineSemaphoreWin32 : constant cudaExternalSemaphoreHandleType := 10;  -- /usr/local/cuda-11.5/include//driver_types.h:2566

   type anon_struct1063 is record
      handle : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2644
      name : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2649
   end record
   with Convention => C_Pass_By_Copy;
   type anon_union1062 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fd : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2624
         when 1 =>
            win32 : aliased anon_struct1063;  -- /usr/local/cuda-11.5/include//driver_types.h:2650
         when others =>
            nvSciSyncObj : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2654
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type cudaExternalSemaphoreHandleDesc is record
      c_type : aliased cudaExternalSemaphoreHandleType;  -- /usr/local/cuda-11.5/include//driver_types.h:2616
      handle : aliased anon_union1062;  -- /usr/local/cuda-11.5/include//driver_types.h:2655
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2659
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2612

   type anon_struct1066 is record
      value : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2674
   end record
   with Convention => C_Pass_By_Copy;
   type anon_union1067 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fence : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2681
         when others =>
            reserved : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2682
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type anon_struct1068 is record
      key : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2691
   end record
   with Convention => C_Pass_By_Copy;
   type anon_struct1065 is record
      fence : aliased anon_struct1066;  -- /usr/local/cuda-11.5/include//driver_types.h:2675
      nvSciSync : aliased anon_union1067;  -- /usr/local/cuda-11.5/include//driver_types.h:2683
      keyedMutex : aliased anon_struct1068;  -- /usr/local/cuda-11.5/include//driver_types.h:2692
   end record
   with Convention => C_Pass_By_Copy;
   type cudaExternalSemaphoreSignalParams_v1 is record
      params : aliased anon_struct1065;  -- /usr/local/cuda-11.5/include//driver_types.h:2693
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2704
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2665

   type anon_struct1071 is record
      value : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2719
   end record
   with Convention => C_Pass_By_Copy;
   type anon_union1072 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fence : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2726
         when others =>
            reserved : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2727
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type anon_struct1073 is record
      key : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2736
      timeoutMs : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2740
   end record
   with Convention => C_Pass_By_Copy;
   type anon_struct1070 is record
      fence : aliased anon_struct1071;  -- /usr/local/cuda-11.5/include//driver_types.h:2720
      nvSciSync : aliased anon_union1072;  -- /usr/local/cuda-11.5/include//driver_types.h:2728
      keyedMutex : aliased anon_struct1073;  -- /usr/local/cuda-11.5/include//driver_types.h:2741
   end record
   with Convention => C_Pass_By_Copy;
   type cudaExternalSemaphoreWaitParams_v1 is record
      params : aliased anon_struct1070;  -- /usr/local/cuda-11.5/include//driver_types.h:2742
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2753
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2710

   type anon_struct1076 is record
      value : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2768
   end record
   with Convention => C_Pass_By_Copy;
   type anon_union1077 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fence : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2775
         when others =>
            reserved : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2776
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type anon_struct1078 is record
      key : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2785
   end record
   with Convention => C_Pass_By_Copy;
   type anon_array1080 is array (0 .. 11) of aliased unsigned;
   type anon_struct1075 is record
      fence : aliased anon_struct1076;  -- /usr/local/cuda-11.5/include//driver_types.h:2769
      nvSciSync : aliased anon_union1077;  -- /usr/local/cuda-11.5/include//driver_types.h:2777
      keyedMutex : aliased anon_struct1078;  -- /usr/local/cuda-11.5/include//driver_types.h:2786
      reserved : aliased anon_array1080;  -- /usr/local/cuda-11.5/include//driver_types.h:2787
   end record
   with Convention => C_Pass_By_Copy;
   type anon_array1081 is array (0 .. 15) of aliased unsigned;
   type cudaExternalSemaphoreSignalParams is record
      params : aliased anon_struct1075;  -- /usr/local/cuda-11.5/include//driver_types.h:2788
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2799
      reserved : aliased anon_array1081;  -- /usr/local/cuda-11.5/include//driver_types.h:2800
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2759

   type anon_struct1084 is record
      value : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2815
   end record
   with Convention => C_Pass_By_Copy;
   type anon_union1085 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fence : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2822
         when others =>
            reserved : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2823
      end case;
   end record
   with Convention => C_Pass_By_Copy,
        Unchecked_Union => True;
   type anon_struct1086 is record
      key : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2832
      timeoutMs : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2836
   end record
   with Convention => C_Pass_By_Copy;
   type anon_array1088 is array (0 .. 9) of aliased unsigned;
   type anon_struct1083 is record
      fence : aliased anon_struct1084;  -- /usr/local/cuda-11.5/include//driver_types.h:2816
      nvSciSync : aliased anon_union1085;  -- /usr/local/cuda-11.5/include//driver_types.h:2824
      keyedMutex : aliased anon_struct1086;  -- /usr/local/cuda-11.5/include//driver_types.h:2837
      reserved : aliased anon_array1088;  -- /usr/local/cuda-11.5/include//driver_types.h:2838
   end record
   with Convention => C_Pass_By_Copy;
   type cudaExternalSemaphoreWaitParams is record
      params : aliased anon_struct1083;  -- /usr/local/cuda-11.5/include//driver_types.h:2839
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2850
      reserved : aliased anon_array1081;  -- /usr/local/cuda-11.5/include//driver_types.h:2851
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2806

   subtype cudaError_t is cudaError;  -- /usr/local/cuda-11.5/include//driver_types.h:2864

   type CUstream_st is null record;   -- incomplete struct

   type cudaStream_t is access all CUstream_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2869

   type CUevent_st is null record;   -- incomplete struct

   type cudaEvent_t is access all CUevent_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2874

   type cudaGraphicsResource_t is access all cudaGraphicsResource;  -- /usr/local/cuda-11.5/include//driver_types.h:2879

   subtype cudaOutputMode_t is cudaOutputMode;  -- /usr/local/cuda-11.5/include//driver_types.h:2884

   type CUexternalMemory_st is null record;   -- incomplete struct

   type cudaExternalMemory_t is access all CUexternalMemory_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2889

   type CUexternalSemaphore_st is null record;   -- incomplete struct

   type cudaExternalSemaphore_t is access all CUexternalSemaphore_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2894

   type CUgraph_st is null record;   -- incomplete struct

   type cudaGraph_t is access all CUgraph_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2899

   type CUgraphNode_st is null record;   -- incomplete struct

   type cudaGraphNode_t is access all CUgraphNode_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2904

   type CUuserObject_st is null record;   -- incomplete struct

   type cudaUserObject_t is access all CUuserObject_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2909

   type CUfunc_st is null record;   -- incomplete struct

   type cudaFunction_t is access all CUfunc_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2914

   type CUmemPoolHandle_st is null record;   -- incomplete struct

   type cudaMemPool_t is access all CUmemPoolHandle_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2919

   type cudaCGScope is 
     (cudaCGScopeInvalid,
      cudaCGScopeGrid,
      cudaCGScopeMultiGrid)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:2924

   type cudaLaunchParams is record
      func : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2935
      gridDim : aliased uvector_types_h.dim3;  -- /usr/local/cuda-11.5/include//driver_types.h:2936
      blockDim : aliased uvector_types_h.dim3;  -- /usr/local/cuda-11.5/include//driver_types.h:2937
      args : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2938
      sharedMem : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2939
      stream : cudaStream_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2940
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2933

   type cudaKernelNodeParams is record
      func : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2947
      gridDim : aliased uvector_types_h.dim3;  -- /usr/local/cuda-11.5/include//driver_types.h:2948
      blockDim : aliased uvector_types_h.dim3;  -- /usr/local/cuda-11.5/include//driver_types.h:2949
      sharedMemBytes : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2950
      kernelParams : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2951
      extra : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2952
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2946

   type cudaExternalSemaphoreSignalNodeParams is record
      extSemArray : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2959
      paramsArray : access constant cudaExternalSemaphoreSignalParams;  -- /usr/local/cuda-11.5/include//driver_types.h:2960
      numExtSems : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2961
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2958

   type cudaExternalSemaphoreWaitNodeParams is record
      extSemArray : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2968
      paramsArray : access constant cudaExternalSemaphoreWaitParams;  -- /usr/local/cuda-11.5/include//driver_types.h:2969
      numExtSems : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2970
   end record
   with Convention => C_Pass_By_Copy;  -- /usr/local/cuda-11.5/include//driver_types.h:2967

   type cudaGraphNodeType is 
     (cudaGraphNodeTypeKernel,
      cudaGraphNodeTypeMemcpy,
      cudaGraphNodeTypeMemset,
      cudaGraphNodeTypeHost,
      cudaGraphNodeTypeGraph,
      cudaGraphNodeTypeEmpty,
      cudaGraphNodeTypeWaitEvent,
      cudaGraphNodeTypeEventRecord,
      cudaGraphNodeTypeExtSemaphoreSignal,
      cudaGraphNodeTypeExtSemaphoreWait,
      cudaGraphNodeTypeMemAlloc,
      cudaGraphNodeTypeMemFree,
      cudaGraphNodeTypeCount)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:2976

   type CUgraphExec_st is null record;   -- incomplete struct

   type cudaGraphExec_t is access all CUgraphExec_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2995

   type cudaGraphExecUpdateResult is 
     (cudaGraphExecUpdateSuccess,
      cudaGraphExecUpdateError,
      cudaGraphExecUpdateErrorTopologyChanged,
      cudaGraphExecUpdateErrorNodeTypeChanged,
      cudaGraphExecUpdateErrorFunctionChanged,
      cudaGraphExecUpdateErrorParametersChanged,
      cudaGraphExecUpdateErrorNotSupported,
      cudaGraphExecUpdateErrorUnsupportedFunctionChange)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:3000

   type cudaGetDriverEntryPointFlags is 
     (cudaEnableDefault,
      cudaEnableLegacyStream,
      cudaEnablePerThreadDefaultStream)
   with Convention => C;  -- /usr/local/cuda-11.5/include//driver_types.h:3015

   subtype cudaGraphDebugDotFlags is unsigned;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsVerbose : constant cudaGraphDebugDotFlags := 1;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsKernelNodeParams : constant cudaGraphDebugDotFlags := 4;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsMemcpyNodeParams : constant cudaGraphDebugDotFlags := 8;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsMemsetNodeParams : constant cudaGraphDebugDotFlags := 16;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsHostNodeParams : constant cudaGraphDebugDotFlags := 32;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsEventNodeParams : constant cudaGraphDebugDotFlags := 64;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsExtSemasSignalNodeParams : constant cudaGraphDebugDotFlags := 128;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsExtSemasWaitNodeParams : constant cudaGraphDebugDotFlags := 256;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsKernelNodeAttributes : constant cudaGraphDebugDotFlags := 512;
   cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsHandles : constant cudaGraphDebugDotFlags := 1024;  -- /usr/local/cuda-11.5/include//driver_types.h:3024

   subtype cudaGraphInstantiateFlags is unsigned;
   cudaGraphInstantiateFlags_cudaGraphInstantiateFlagAutoFreeOnLaunch : constant cudaGraphInstantiateFlags := 1;  -- /usr/local/cuda-11.5/include//driver_types.h:3040

end udriver_types_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
