pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package stddef_h is

   --  unsupported macro: NULL ((void *)0)
   --  arg-macro: procedure offsetof (TYPE, MEMBER)
   --    __builtin_offsetof (TYPE, MEMBER)
   subtype ptrdiff_t is long;  -- /home/daspetak/.local/gnat/lib/gcc/x86_64-pc-linux-gnu/12.2.1/include/stddef.h:145

   subtype size_t is unsigned_long;  -- /home/daspetak/.local/gnat/lib/gcc/x86_64-pc-linux-gnu/12.2.1/include/stddef.h:214

   subtype wchar_t is int;  -- /home/daspetak/.local/gnat/lib/gcc/x86_64-pc-linux-gnu/12.2.1/include/stddef.h:329

   type max_align_t is record
      uu_max_align_ll : aliased Long_Long_Integer;  -- /home/daspetak/.local/gnat/lib/gcc/x86_64-pc-linux-gnu/12.2.1/include/stddef.h:426
      uu_max_align_ld : aliased long_double;  -- /home/daspetak/.local/gnat/lib/gcc/x86_64-pc-linux-gnu/12.2.1/include/stddef.h:427
   end record
   with Convention => C_Pass_By_Copy;  -- /home/daspetak/.local/gnat/lib/gcc/x86_64-pc-linux-gnu/12.2.1/include/stddef.h:436

end stddef_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
