pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package udevice_types_h is

   type cudaRoundMode is 
     (cudaRoundNearest,
      cudaRoundZero,
      cudaRoundPosInf,
      cudaRoundMinInf)
   with Convention => C;  -- /usr/local/cuda-11.5/include//device_types.h:68

end udevice_types_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
