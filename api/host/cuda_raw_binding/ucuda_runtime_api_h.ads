pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with udriver_types_h;
with stddef_h;
with Interfaces.C.Strings;
with System;
with Interfaces.C.Extensions;
with uvector_types_h;
with utexture_types_h;
with usurface_types_h;

package ucuda_runtime_api_h is

   CUDART_VERSION : constant := 11050;  --  /usr/local/cuda-11.5/include//cuda_runtime_api.h:138
   --  unsupported macro: cudaSignalExternalSemaphoresAsync __CUDART_API_PTSZ(cudaSignalExternalSemaphoresAsync_v2)
   --  unsupported macro: cudaWaitExternalSemaphoresAsync __CUDART_API_PTSZ(cudaWaitExternalSemaphoresAsync_v2)
   --  unsupported macro: CUDART_DEVICE __device__

   function cudaDeviceReset return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:306
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceReset";

   function cudaDeviceSynchronize return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:327
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceSynchronize";

   function cudaDeviceSetLimit (limit : udriver_types_h.cudaLimit; value : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:414
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceSetLimit";

   function cudaDeviceGetLimit (pValue : access stddef_h.size_t; limit : udriver_types_h.cudaLimit) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:449
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetLimit";

   function cudaDeviceGetTexture1DLinearMaxWidth
     (maxWidthInElements : access stddef_h.size_t;
      fmtDesc : access constant udriver_types_h.cudaChannelFormatDesc;
      device : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:472
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetTexture1DLinearMaxWidth";

   function cudaDeviceGetCacheConfig (pCacheConfig : access udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:506
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetCacheConfig";

   function cudaDeviceGetStreamPriorityRange (leastPriority : access int; greatestPriority : access int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:543
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetStreamPriorityRange";

   function cudaDeviceSetCacheConfig (cacheConfig : udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:587
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceSetCacheConfig";

   function cudaDeviceGetSharedMemConfig (pConfig : access udriver_types_h.cudaSharedMemConfig) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:618
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetSharedMemConfig";

   function cudaDeviceSetSharedMemConfig (config : udriver_types_h.cudaSharedMemConfig) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:662
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceSetSharedMemConfig";

   function cudaDeviceGetByPCIBusId (device : access int; pciBusId : Interfaces.C.Strings.chars_ptr) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:689
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetByPCIBusId";

   function cudaDeviceGetPCIBusId
     (pciBusId : Interfaces.C.Strings.chars_ptr;
      len : int;
      device : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:719
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetPCIBusId";

   function cudaIpcGetEventHandle (handle : access udriver_types_h.cudaIpcEventHandle_st; event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:767
   with Import => True, 
        Convention => C, 
        External_Name => "cudaIpcGetEventHandle";

   function cudaIpcOpenEventHandle (event : System.Address; handle : udriver_types_h.cudaIpcEventHandle_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:808
   with Import => True, 
        Convention => C, 
        External_Name => "cudaIpcOpenEventHandle";

   function cudaIpcGetMemHandle (handle : access udriver_types_h.cudaIpcMemHandle_st; devPtr : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:851
   with Import => True, 
        Convention => C, 
        External_Name => "cudaIpcGetMemHandle";

   function cudaIpcOpenMemHandle
     (devPtr : System.Address;
      handle : udriver_types_h.cudaIpcMemHandle_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:915
   with Import => True, 
        Convention => C, 
        External_Name => "cudaIpcOpenMemHandle";

   function cudaIpcCloseMemHandle (devPtr : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:951
   with Import => True, 
        Convention => C, 
        External_Name => "cudaIpcCloseMemHandle";

   function cudaDeviceFlushGPUDirectRDMAWrites (target : udriver_types_h.cudaFlushGPUDirectRDMAWritesTarget; scope : udriver_types_h.cudaFlushGPUDirectRDMAWritesScope) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:983
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceFlushGPUDirectRDMAWrites";

   function cudaThreadExit return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1027
   with Import => True, 
        Convention => C, 
        External_Name => "cudaThreadExit";

   function cudaThreadSynchronize return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1053
   with Import => True, 
        Convention => C, 
        External_Name => "cudaThreadSynchronize";

   function cudaThreadSetLimit (limit : udriver_types_h.cudaLimit; value : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1102
   with Import => True, 
        Convention => C, 
        External_Name => "cudaThreadSetLimit";

   function cudaThreadGetLimit (pValue : access stddef_h.size_t; limit : udriver_types_h.cudaLimit) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1135
   with Import => True, 
        Convention => C, 
        External_Name => "cudaThreadGetLimit";

   function cudaThreadGetCacheConfig (pCacheConfig : access udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1171
   with Import => True, 
        Convention => C, 
        External_Name => "cudaThreadGetCacheConfig";

   function cudaThreadSetCacheConfig (cacheConfig : udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1218
   with Import => True, 
        Convention => C, 
        External_Name => "cudaThreadSetCacheConfig";

   function cudaGetLastError return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1281
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetLastError";

   function cudaPeekAtLastError return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1329
   with Import => True, 
        Convention => C, 
        External_Name => "cudaPeekAtLastError";

   function cudaGetErrorName (error : udriver_types_h.cudaError_t) return Interfaces.C.Strings.chars_ptr  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1345
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetErrorName";

   function cudaGetErrorString (error : udriver_types_h.cudaError_t) return Interfaces.C.Strings.chars_ptr  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1361
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetErrorString";

   function cudaGetDeviceCount (count : access int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1389
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetDeviceCount";

   function cudaGetDeviceProperties (prop : access udriver_types_h.cudaDeviceProp; device : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1667
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetDeviceProperties";

   function cudaDeviceGetAttribute
     (value : access int;
      attr : udriver_types_h.cudaDeviceAttr;
      device : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1868
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetAttribute";

   function cudaDeviceGetDefaultMemPool (memPool : System.Address; device : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1886
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetDefaultMemPool";

   function cudaDeviceSetMemPool (device : int; memPool : udriver_types_h.cudaMemPool_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1910
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceSetMemPool";

   function cudaDeviceGetMemPool (memPool : System.Address; device : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1930
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetMemPool";

   function cudaDeviceGetNvSciSyncAttributes
     (nvSciSyncAttrList : System.Address;
      device : int;
      flags : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1978
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetNvSciSyncAttributes";

   function cudaDeviceGetP2PAttribute
     (value : access int;
      attr : udriver_types_h.cudaDeviceP2PAttr;
      srcDevice : int;
      dstDevice : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2018
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetP2PAttribute";

   function cudaChooseDevice (device : access int; prop : access constant udriver_types_h.cudaDeviceProp) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2039
   with Import => True, 
        Convention => C, 
        External_Name => "cudaChooseDevice";

   function cudaSetDevice (device : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2080
   with Import => True, 
        Convention => C, 
        External_Name => "cudaSetDevice";

   function cudaGetDevice (device : access int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2101
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetDevice";

   function cudaSetValidDevices (device_arr : access int; len : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2132
   with Import => True, 
        Convention => C, 
        External_Name => "cudaSetValidDevices";

   function cudaSetDeviceFlags (flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2197
   with Import => True, 
        Convention => C, 
        External_Name => "cudaSetDeviceFlags";

   function cudaGetDeviceFlags (flags : access unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2241
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetDeviceFlags";

   function cudaStreamCreate (pStream : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2281
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamCreate";

   function cudaStreamCreateWithFlags (pStream : System.Address; flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2313
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamCreateWithFlags";

   function cudaStreamCreateWithPriority
     (pStream : System.Address;
      flags : unsigned;
      priority : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2359
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamCreateWithPriority";

   function cudaStreamGetPriority (hStream : udriver_types_h.cudaStream_t; priority : access int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2386
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamGetPriority";

   function cudaStreamGetFlags (hStream : udriver_types_h.cudaStream_t; flags : access unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2411
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamGetFlags";

   function cudaCtxResetPersistingL2Cache return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2426
   with Import => True, 
        Convention => C, 
        External_Name => "cudaCtxResetPersistingL2Cache";

   function cudaStreamCopyAttributes (dst : udriver_types_h.cudaStream_t; src : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2446
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamCopyAttributes";

   function cudaStreamGetAttribute
     (hStream : udriver_types_h.cudaStream_t;
      attr : udriver_types_h.cudaStreamAttrID;
      value_out : access udriver_types_h.cudaStreamAttrValue) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2467
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamGetAttribute";

   function cudaStreamSetAttribute
     (hStream : udriver_types_h.cudaStream_t;
      attr : udriver_types_h.cudaStreamAttrID;
      value : access constant udriver_types_h.cudaStreamAttrValue) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2491
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamSetAttribute";

   function cudaStreamDestroy (stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2525
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamDestroy";

   function cudaStreamWaitEvent
     (stream : udriver_types_h.cudaStream_t;
      event : udriver_types_h.cudaEvent_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2556
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamWaitEvent";

   type cudaStreamCallback_t is access procedure
        (arg1 : udriver_types_h.cudaStream_t;
         arg2 : udriver_types_h.cudaError_t;
         arg3 : System.Address)
   with Convention => C;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2564

   function cudaStreamAddCallback
     (stream : udriver_types_h.cudaStream_t;
      callback : cudaStreamCallback_t;
      userData : System.Address;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2631
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamAddCallback";

   function cudaStreamSynchronize (stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2655
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamSynchronize";

   function cudaStreamQuery (stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2680
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamQuery";

   function cudaStreamAttachMemAsync
     (stream : udriver_types_h.cudaStream_t;
      devPtr : System.Address;
      length : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2766
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamAttachMemAsync";

   function cudaStreamBeginCapture (stream : udriver_types_h.cudaStream_t; mode : udriver_types_h.cudaStreamCaptureMode) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2803
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamBeginCapture";

   function cudaThreadExchangeStreamCaptureMode (mode : access udriver_types_h.cudaStreamCaptureMode) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2854
   with Import => True, 
        Convention => C, 
        External_Name => "cudaThreadExchangeStreamCaptureMode";

   function cudaStreamEndCapture (stream : udriver_types_h.cudaStream_t; pGraph : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2882
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamEndCapture";

   function cudaStreamIsCapturing (stream : udriver_types_h.cudaStream_t; pCaptureStatus : access udriver_types_h.cudaStreamCaptureStatus) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2920
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamIsCapturing";

   function cudaStreamGetCaptureInfo
     (stream : udriver_types_h.cudaStream_t;
      pCaptureStatus : access udriver_types_h.cudaStreamCaptureStatus;
      pId : access Extensions.unsigned_long_long) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2952
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamGetCaptureInfo";

   function cudaStreamGetCaptureInfo_v2
     (stream : udriver_types_h.cudaStream_t;
      captureStatus_out : access udriver_types_h.cudaStreamCaptureStatus;
      id_out : access Extensions.unsigned_long_long;
      graph_out : System.Address;
      dependencies_out : System.Address;
      numDependencies_out : access stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3007
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamGetCaptureInfo_v2";

   function cudaStreamUpdateCaptureDependencies
     (stream : udriver_types_h.cudaStream_t;
      dependencies : System.Address;
      numDependencies : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3040
   with Import => True, 
        Convention => C, 
        External_Name => "cudaStreamUpdateCaptureDependencies";

   function cudaEventCreate (event : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3077
   with Import => True, 
        Convention => C, 
        External_Name => "cudaEventCreate";

   function cudaEventCreateWithFlags (event : System.Address; flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3114
   with Import => True, 
        Convention => C, 
        External_Name => "cudaEventCreateWithFlags";

   function cudaEventRecord (event : udriver_types_h.cudaEvent_t; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3154
   with Import => True, 
        Convention => C, 
        External_Name => "cudaEventRecord";

   function cudaEventRecordWithFlags
     (event : udriver_types_h.cudaEvent_t;
      stream : udriver_types_h.cudaStream_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3201
   with Import => True, 
        Convention => C, 
        External_Name => "cudaEventRecordWithFlags";

   function cudaEventQuery (event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3233
   with Import => True, 
        Convention => C, 
        External_Name => "cudaEventQuery";

   function cudaEventSynchronize (event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3263
   with Import => True, 
        Convention => C, 
        External_Name => "cudaEventSynchronize";

   function cudaEventDestroy (event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3292
   with Import => True, 
        Convention => C, 
        External_Name => "cudaEventDestroy";

   function cudaEventElapsedTime
     (ms : access float;
      start : udriver_types_h.cudaEvent_t;
      c_end : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3335
   with Import => True, 
        Convention => C, 
        External_Name => "cudaEventElapsedTime";

   function cudaImportExternalMemory (extMem_out : System.Address; memHandleDesc : access constant udriver_types_h.cudaExternalMemoryHandleDesc) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3515
   with Import => True, 
        Convention => C, 
        External_Name => "cudaImportExternalMemory";

   function cudaExternalMemoryGetMappedBuffer
     (devPtr : System.Address;
      extMem : udriver_types_h.cudaExternalMemory_t;
      bufferDesc : access constant udriver_types_h.cudaExternalMemoryBufferDesc) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3570
   with Import => True, 
        Convention => C, 
        External_Name => "cudaExternalMemoryGetMappedBuffer";

   function cudaExternalMemoryGetMappedMipmappedArray
     (mipmap : System.Address;
      extMem : udriver_types_h.cudaExternalMemory_t;
      mipmapDesc : access constant udriver_types_h.cudaExternalMemoryMipmappedArrayDesc) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3630
   with Import => True, 
        Convention => C, 
        External_Name => "cudaExternalMemoryGetMappedMipmappedArray";

   function cudaDestroyExternalMemory (extMem : udriver_types_h.cudaExternalMemory_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3654
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDestroyExternalMemory";

   function cudaImportExternalSemaphore (extSem_out : System.Address; semHandleDesc : access constant udriver_types_h.cudaExternalSemaphoreHandleDesc) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3807
   with Import => True, 
        Convention => C, 
        External_Name => "cudaImportExternalSemaphore";

   function cudaSignalExternalSemaphoresAsync_v2
     (extSemArray : System.Address;
      paramsArray : access constant udriver_types_h.cudaExternalSemaphoreSignalParams;
      numExtSems : unsigned;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3874
   with Import => True, 
        Convention => C, 
        External_Name => "cudaSignalExternalSemaphoresAsync_v2";

   function cudaWaitExternalSemaphoresAsync_v2
     (extSemArray : System.Address;
      paramsArray : access constant udriver_types_h.cudaExternalSemaphoreWaitParams;
      numExtSems : unsigned;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3950
   with Import => True, 
        Convention => C, 
        External_Name => "cudaWaitExternalSemaphoresAsync_v2";

   function cudaDestroyExternalSemaphore (extSem : udriver_types_h.cudaExternalSemaphore_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3973
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDestroyExternalSemaphore";

   function cudaLaunchKernel
     (func : System.Address;
      gridDim : uvector_types_h.dim3;
      blockDim : uvector_types_h.dim3;
      args : System.Address;
      sharedMem : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4040
   with Import => True, 
        Convention => C, 
        External_Name => "cudaLaunchKernel";

   function cudaLaunchCooperativeKernel
     (func : System.Address;
      gridDim : uvector_types_h.dim3;
      blockDim : uvector_types_h.dim3;
      args : System.Address;
      sharedMem : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4097
   with Import => True, 
        Convention => C, 
        External_Name => "cudaLaunchCooperativeKernel";

   function cudaLaunchCooperativeKernelMultiDevice
     (launchParamsList : access udriver_types_h.cudaLaunchParams;
      numDevices : unsigned;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4198
   with Import => True, 
        Convention => C, 
        External_Name => "cudaLaunchCooperativeKernelMultiDevice";

   function cudaFuncSetCacheConfig (func : System.Address; cacheConfig : udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4245
   with Import => True, 
        Convention => C, 
        External_Name => "cudaFuncSetCacheConfig";

   function cudaFuncSetSharedMemConfig (func : System.Address; config : udriver_types_h.cudaSharedMemConfig) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4300
   with Import => True, 
        Convention => C, 
        External_Name => "cudaFuncSetSharedMemConfig";

   function cudaFuncGetAttributes (attr : access udriver_types_h.cudaFuncAttributes; func : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4333
   with Import => True, 
        Convention => C, 
        External_Name => "cudaFuncGetAttributes";

   function cudaFuncSetAttribute
     (func : System.Address;
      attr : udriver_types_h.cudaFuncAttribute;
      value : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4370
   with Import => True, 
        Convention => C, 
        External_Name => "cudaFuncSetAttribute";

   function cudaSetDoubleForDevice (d : access double) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4396
   with Import => True, 
        Convention => C, 
        External_Name => "cudaSetDoubleForDevice";

   function cudaSetDoubleForHost (d : access double) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4420
   with Import => True, 
        Convention => C, 
        External_Name => "cudaSetDoubleForHost";

   function cudaLaunchHostFunc
     (stream : udriver_types_h.cudaStream_t;
      fn : udriver_types_h.cudaHostFn_t;
      userData : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4488
   with Import => True, 
        Convention => C, 
        External_Name => "cudaLaunchHostFunc";

   function cudaOccupancyMaxActiveBlocksPerMultiprocessor
     (numBlocks : access int;
      func : System.Address;
      blockSize : int;
      dynamicSMemSize : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4545
   with Import => True, 
        Convention => C, 
        External_Name => "cudaOccupancyMaxActiveBlocksPerMultiprocessor";

   function cudaOccupancyAvailableDynamicSMemPerBlock
     (dynamicSmemSize : access stddef_h.size_t;
      func : System.Address;
      numBlocks : int;
      blockSize : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4574
   with Import => True, 
        Convention => C, 
        External_Name => "cudaOccupancyAvailableDynamicSMemPerBlock";

   function cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
     (numBlocks : access int;
      func : System.Address;
      blockSize : int;
      dynamicSMemSize : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4619
   with Import => True, 
        Convention => C, 
        External_Name => "cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags";

   function cudaMallocManaged
     (devPtr : System.Address;
      size : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4742
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMallocManaged";

   function cudaMalloc (devPtr : System.Address; size : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4773
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMalloc";

   function cudaMallocHost (ptr : System.Address; size : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4806
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMallocHost";

   function cudaMallocPitch
     (devPtr : System.Address;
      pitch : access stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4849
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMallocPitch";

   function cudaMallocArray
     (c_array : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4898
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMallocArray";

   function cudaFree (devPtr : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4927
   with Import => True, 
        Convention => C, 
        External_Name => "cudaFree";

   function cudaFreeHost (ptr : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4950
   with Import => True, 
        Convention => C, 
        External_Name => "cudaFreeHost";

   function cudaFreeArray (c_array : udriver_types_h.cudaArray_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4973
   with Import => True, 
        Convention => C, 
        External_Name => "cudaFreeArray";

   function cudaFreeMipmappedArray (mipmappedArray : udriver_types_h.cudaMipmappedArray_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4996
   with Import => True, 
        Convention => C, 
        External_Name => "cudaFreeMipmappedArray";

   function cudaHostAlloc
     (pHost : System.Address;
      size : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5062
   with Import => True, 
        Convention => C, 
        External_Name => "cudaHostAlloc";

   function cudaHostRegister
     (ptr : System.Address;
      size : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5155
   with Import => True, 
        Convention => C, 
        External_Name => "cudaHostRegister";

   function cudaHostUnregister (ptr : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5178
   with Import => True, 
        Convention => C, 
        External_Name => "cudaHostUnregister";

   function cudaHostGetDevicePointer
     (pDevice : System.Address;
      pHost : System.Address;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5223
   with Import => True, 
        Convention => C, 
        External_Name => "cudaHostGetDevicePointer";

   function cudaHostGetFlags (pFlags : access unsigned; pHost : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5245
   with Import => True, 
        Convention => C, 
        External_Name => "cudaHostGetFlags";

   function cudaMalloc3D (pitchedDevPtr : access udriver_types_h.cudaPitchedPtr; extent : udriver_types_h.cudaExtent) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5284
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMalloc3D";

   function cudaMalloc3DArray
     (c_array : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      extent : udriver_types_h.cudaExtent;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5426
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMalloc3DArray";

   function cudaMallocMipmappedArray
     (mipmappedArray : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      extent : udriver_types_h.cudaExtent;
      numLevels : unsigned;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5568
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMallocMipmappedArray";

   function cudaGetMipmappedArrayLevel
     (levelArray : System.Address;
      mipmappedArray : udriver_types_h.cudaMipmappedArray_const_t;
      level : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5601
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetMipmappedArrayLevel";

   function cudaMemcpy3D (p : access constant udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5706
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy3D";

   function cudaMemcpy3DPeer (p : access constant udriver_types_h.cudaMemcpy3DPeerParms) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5737
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy3DPeer";

   function cudaMemcpy3DAsync (p : access constant udriver_types_h.cudaMemcpy3DParms; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5855
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy3DAsync";

   function cudaMemcpy3DPeerAsync (p : access constant udriver_types_h.cudaMemcpy3DPeerParms; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5881
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy3DPeerAsync";

   function cudaMemGetInfo (free : access stddef_h.size_t; total : access stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5904
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemGetInfo";

   function cudaArrayGetInfo
     (desc : access udriver_types_h.cudaChannelFormatDesc;
      extent : access udriver_types_h.cudaExtent;
      flags : access unsigned;
      c_array : udriver_types_h.cudaArray_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5930
   with Import => True, 
        Convention => C, 
        External_Name => "cudaArrayGetInfo";

   function cudaArrayGetPlane
     (pPlaneArray : System.Address;
      hArray : udriver_types_h.cudaArray_t;
      planeIdx : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5959
   with Import => True, 
        Convention => C, 
        External_Name => "cudaArrayGetPlane";

   function cudaArrayGetSparseProperties (sparseProperties : access udriver_types_h.cudaArraySparseProperties; c_array : udriver_types_h.cudaArray_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5987
   with Import => True, 
        Convention => C, 
        External_Name => "cudaArrayGetSparseProperties";

   function cudaMipmappedArrayGetSparseProperties (sparseProperties : access udriver_types_h.cudaArraySparseProperties; mipmap : udriver_types_h.cudaMipmappedArray_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6017
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMipmappedArrayGetSparseProperties";

   function cudaMemcpy
     (dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6062
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy";

   function cudaMemcpyPeer
     (dst : System.Address;
      dstDevice : int;
      src : System.Address;
      srcDevice : int;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6097
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyPeer";

   function cudaMemcpy2D
     (dst : System.Address;
      dpitch : stddef_h.size_t;
      src : System.Address;
      spitch : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6146
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy2D";

   function cudaMemcpy2DToArray
     (dst : udriver_types_h.cudaArray_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      src : System.Address;
      spitch : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6196
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy2DToArray";

   function cudaMemcpy2DFromArray
     (dst : System.Address;
      dpitch : stddef_h.size_t;
      src : udriver_types_h.cudaArray_const_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6246
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy2DFromArray";

   function cudaMemcpy2DArrayToArray
     (dst : udriver_types_h.cudaArray_t;
      wOffsetDst : stddef_h.size_t;
      hOffsetDst : stddef_h.size_t;
      src : udriver_types_h.cudaArray_const_t;
      wOffsetSrc : stddef_h.size_t;
      hOffsetSrc : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6293
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy2DArrayToArray";

   function cudaMemcpyToSymbol
     (symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6336
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyToSymbol";

   function cudaMemcpyFromSymbol
     (dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6379
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyFromSymbol";

   function cudaMemcpyAsync
     (dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6436
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyAsync";

   function cudaMemcpyPeerAsync
     (dst : System.Address;
      dstDevice : int;
      src : System.Address;
      srcDevice : int;
      count : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6471
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyPeerAsync";

   function cudaMemcpy2DAsync
     (dst : System.Address;
      dpitch : stddef_h.size_t;
      src : System.Address;
      spitch : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6534
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy2DAsync";

   function cudaMemcpy2DToArrayAsync
     (dst : udriver_types_h.cudaArray_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      src : System.Address;
      spitch : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6592
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy2DToArrayAsync";

   function cudaMemcpy2DFromArrayAsync
     (dst : System.Address;
      dpitch : stddef_h.size_t;
      src : udriver_types_h.cudaArray_const_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6649
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpy2DFromArrayAsync";

   function cudaMemcpyToSymbolAsync
     (symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6700
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyToSymbolAsync";

   function cudaMemcpyFromSymbolAsync
     (dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6751
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyFromSymbolAsync";

   function cudaMemset
     (devPtr : System.Address;
      value : int;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6780
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemset";

   function cudaMemset2D
     (devPtr : System.Address;
      pitch : stddef_h.size_t;
      value : int;
      width : stddef_h.size_t;
      height : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6814
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemset2D";

   function cudaMemset3D
     (pitchedDevPtr : udriver_types_h.cudaPitchedPtr;
      value : int;
      extent : udriver_types_h.cudaExtent) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6860
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemset3D";

   function cudaMemsetAsync
     (devPtr : System.Address;
      value : int;
      count : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6896
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemsetAsync";

   function cudaMemset2DAsync
     (devPtr : System.Address;
      pitch : stddef_h.size_t;
      value : int;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6937
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemset2DAsync";

   function cudaMemset3DAsync
     (pitchedDevPtr : udriver_types_h.cudaPitchedPtr;
      value : int;
      extent : udriver_types_h.cudaExtent;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6990
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemset3DAsync";

   function cudaGetSymbolAddress (devPtr : System.Address; symbol : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7018
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetSymbolAddress";

   function cudaGetSymbolSize (size : access stddef_h.size_t; symbol : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7045
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetSymbolSize";

   function cudaMemPrefetchAsync
     (devPtr : System.Address;
      count : stddef_h.size_t;
      dstDevice : int;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7115
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPrefetchAsync";

   function cudaMemAdvise
     (devPtr : System.Address;
      count : stddef_h.size_t;
      advice : udriver_types_h.cudaMemoryAdvise;
      device : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7231
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemAdvise";

   function cudaMemRangeGetAttribute
     (data : System.Address;
      dataSize : stddef_h.size_t;
      attribute : udriver_types_h.cudaMemRangeAttribute;
      devPtr : System.Address;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7290
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemRangeGetAttribute";

   function cudaMemRangeGetAttributes
     (data : System.Address;
      dataSizes : access stddef_h.size_t;
      attributes : access udriver_types_h.cudaMemRangeAttribute;
      numAttributes : stddef_h.size_t;
      devPtr : System.Address;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7329
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemRangeGetAttributes";

   function cudaMemcpyToArray
     (dst : udriver_types_h.cudaArray_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7389
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyToArray";

   function cudaMemcpyFromArray
     (dst : System.Address;
      src : udriver_types_h.cudaArray_const_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7431
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyFromArray";

   function cudaMemcpyArrayToArray
     (dst : udriver_types_h.cudaArray_t;
      wOffsetDst : stddef_h.size_t;
      hOffsetDst : stddef_h.size_t;
      src : udriver_types_h.cudaArray_const_t;
      wOffsetSrc : stddef_h.size_t;
      hOffsetSrc : stddef_h.size_t;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7474
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyArrayToArray";

   function cudaMemcpyToArrayAsync
     (dst : udriver_types_h.cudaArray_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7525
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyToArrayAsync";

   function cudaMemcpyFromArrayAsync
     (dst : System.Address;
      src : udriver_types_h.cudaArray_const_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7575
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemcpyFromArrayAsync";

   function cudaMallocAsync
     (devPtr : System.Address;
      size : stddef_h.size_t;
      hStream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7644
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMallocAsync";

   function cudaFreeAsync (devPtr : System.Address; hStream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7670
   with Import => True, 
        Convention => C, 
        External_Name => "cudaFreeAsync";

   function cudaMemPoolTrimTo (memPool : udriver_types_h.cudaMemPool_t; minBytesToKeep : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7695
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolTrimTo";

   function cudaMemPoolSetAttribute
     (memPool : udriver_types_h.cudaMemPool_t;
      attr : udriver_types_h.cudaMemPoolAttr;
      value : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7733
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolSetAttribute";

   function cudaMemPoolGetAttribute
     (memPool : udriver_types_h.cudaMemPool_t;
      attr : udriver_types_h.cudaMemPoolAttr;
      value : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7771
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolGetAttribute";

   function cudaMemPoolSetAccess
     (memPool : udriver_types_h.cudaMemPool_t;
      descList : access constant udriver_types_h.cudaMemAccessDesc;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7786
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolSetAccess";

   function cudaMemPoolGetAccess
     (flags : access udriver_types_h.cudaMemAccessFlags;
      memPool : udriver_types_h.cudaMemPool_t;
      location : access udriver_types_h.cudaMemLocation) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7799
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolGetAccess";

   function cudaMemPoolCreate (memPool : System.Address; poolProps : access constant udriver_types_h.cudaMemPoolProps) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7819
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolCreate";

   function cudaMemPoolDestroy (memPool : udriver_types_h.cudaMemPool_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7841
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolDestroy";

   function cudaMallocFromPoolAsync
     (ptr : System.Address;
      size : stddef_h.size_t;
      memPool : udriver_types_h.cudaMemPool_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7877
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMallocFromPoolAsync";

   function cudaMemPoolExportToShareableHandle
     (shareableHandle : System.Address;
      memPool : udriver_types_h.cudaMemPool_t;
      handleType : udriver_types_h.cudaMemAllocationHandleType;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7902
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolExportToShareableHandle";

   function cudaMemPoolImportFromShareableHandle
     (memPool : System.Address;
      shareableHandle : System.Address;
      handleType : udriver_types_h.cudaMemAllocationHandleType;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7929
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolImportFromShareableHandle";

   function cudaMemPoolExportPointer (exportData : access udriver_types_h.cudaMemPoolPtrExportData; ptr : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7952
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolExportPointer";

   function cudaMemPoolImportPointer
     (ptr : System.Address;
      memPool : udriver_types_h.cudaMemPool_t;
      exportData : access udriver_types_h.cudaMemPoolPtrExportData) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7981
   with Import => True, 
        Convention => C, 
        External_Name => "cudaMemPoolImportPointer";

   function cudaPointerGetAttributes (attributes : access udriver_types_h.cudaPointerAttributes; ptr : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8133
   with Import => True, 
        Convention => C, 
        External_Name => "cudaPointerGetAttributes";

   function cudaDeviceCanAccessPeer
     (canAccessPeer : access int;
      device : int;
      peerDevice : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8174
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceCanAccessPeer";

   function cudaDeviceEnablePeerAccess (peerDevice : int; flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8216
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceEnablePeerAccess";

   function cudaDeviceDisablePeerAccess (peerDevice : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8238
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceDisablePeerAccess";

   function cudaGraphicsUnregisterResource (resource : udriver_types_h.cudaGraphicsResource_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8302
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphicsUnregisterResource";

   function cudaGraphicsResourceSetMapFlags (resource : udriver_types_h.cudaGraphicsResource_t; flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8337
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphicsResourceSetMapFlags";

   function cudaGraphicsMapResources
     (count : int;
      resources : System.Address;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8376
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphicsMapResources";

   function cudaGraphicsUnmapResources
     (count : int;
      resources : System.Address;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8411
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphicsUnmapResources";

   function cudaGraphicsResourceGetMappedPointer
     (devPtr : System.Address;
      size : access stddef_h.size_t;
      resource : udriver_types_h.cudaGraphicsResource_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8443
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphicsResourceGetMappedPointer";

   function cudaGraphicsSubResourceGetMappedArray
     (c_array : System.Address;
      resource : udriver_types_h.cudaGraphicsResource_t;
      arrayIndex : unsigned;
      mipLevel : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8481
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphicsSubResourceGetMappedArray";

   function cudaGraphicsResourceGetMappedMipmappedArray (mipmappedArray : System.Address; resource : udriver_types_h.cudaGraphicsResource_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8510
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphicsResourceGetMappedMipmappedArray";

   function cudaBindTexture
     (offset : access stddef_h.size_t;
      texref : access constant utexture_types_h.textureReference;
      devPtr : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      size : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8581
   with Import => True, 
        Convention => C, 
        External_Name => "cudaBindTexture";

   function cudaBindTexture2D
     (offset : access stddef_h.size_t;
      texref : access constant utexture_types_h.textureReference;
      devPtr : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      pitch : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8640
   with Import => True, 
        Convention => C, 
        External_Name => "cudaBindTexture2D";

   function cudaBindTextureToArray
     (texref : access constant utexture_types_h.textureReference;
      c_array : udriver_types_h.cudaArray_const_t;
      desc : access constant udriver_types_h.cudaChannelFormatDesc) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8678
   with Import => True, 
        Convention => C, 
        External_Name => "cudaBindTextureToArray";

   function cudaBindTextureToMipmappedArray
     (texref : access constant utexture_types_h.textureReference;
      mipmappedArray : udriver_types_h.cudaMipmappedArray_const_t;
      desc : access constant udriver_types_h.cudaChannelFormatDesc) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8718
   with Import => True, 
        Convention => C, 
        External_Name => "cudaBindTextureToMipmappedArray";

   function cudaUnbindTexture (texref : access constant utexture_types_h.textureReference) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8744
   with Import => True, 
        Convention => C, 
        External_Name => "cudaUnbindTexture";

   function cudaGetTextureAlignmentOffset (offset : access stddef_h.size_t; texref : access constant utexture_types_h.textureReference) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8773
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetTextureAlignmentOffset";

   function cudaGetTextureReference (texref : System.Address; symbol : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8803
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetTextureReference";

   function cudaBindSurfaceToArray
     (surfref : access constant usurface_types_h.surfaceReference;
      c_array : udriver_types_h.cudaArray_const_t;
      desc : access constant udriver_types_h.cudaChannelFormatDesc) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8848
   with Import => True, 
        Convention => C, 
        External_Name => "cudaBindSurfaceToArray";

   function cudaGetSurfaceReference (surfref : System.Address; symbol : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8873
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetSurfaceReference";

   function cudaGetChannelDesc (desc : access udriver_types_h.cudaChannelFormatDesc; c_array : udriver_types_h.cudaArray_const_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8908
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetChannelDesc";

   function cudaCreateChannelDesc
     (x : int;
      y : int;
      z : int;
      w : int;
      f : udriver_types_h.cudaChannelFormatKind) return udriver_types_h.cudaChannelFormatDesc  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8938
   with Import => True, 
        Convention => C, 
        External_Name => "cudaCreateChannelDesc";

   function cudaCreateTextureObject
     (pTexObject : access utexture_types_h.cudaTextureObject_t;
      pResDesc : access constant udriver_types_h.cudaResourceDesc;
      pTexDesc : access constant utexture_types_h.cudaTextureDesc;
      pResViewDesc : access constant udriver_types_h.cudaResourceViewDesc) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9156
   with Import => True, 
        Convention => C, 
        External_Name => "cudaCreateTextureObject";

   function cudaDestroyTextureObject (texObject : utexture_types_h.cudaTextureObject_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9176
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDestroyTextureObject";

   function cudaGetTextureObjectResourceDesc (pResDesc : access udriver_types_h.cudaResourceDesc; texObject : utexture_types_h.cudaTextureObject_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9196
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetTextureObjectResourceDesc";

   function cudaGetTextureObjectTextureDesc (pTexDesc : access utexture_types_h.cudaTextureDesc; texObject : utexture_types_h.cudaTextureObject_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9216
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetTextureObjectTextureDesc";

   function cudaGetTextureObjectResourceViewDesc (pResViewDesc : access udriver_types_h.cudaResourceViewDesc; texObject : utexture_types_h.cudaTextureObject_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9237
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetTextureObjectResourceViewDesc";

   function cudaCreateSurfaceObject (pSurfObject : access usurface_types_h.cudaSurfaceObject_t; pResDesc : access constant udriver_types_h.cudaResourceDesc) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9282
   with Import => True, 
        Convention => C, 
        External_Name => "cudaCreateSurfaceObject";

   function cudaDestroySurfaceObject (surfObject : usurface_types_h.cudaSurfaceObject_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9302
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDestroySurfaceObject";

   function cudaGetSurfaceObjectResourceDesc (pResDesc : access udriver_types_h.cudaResourceDesc; surfObject : usurface_types_h.cudaSurfaceObject_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9321
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetSurfaceObjectResourceDesc";

   function cudaDriverGetVersion (driverVersion : access int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9355
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDriverGetVersion";

   function cudaRuntimeGetVersion (runtimeVersion : access int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9380
   with Import => True, 
        Convention => C, 
        External_Name => "cudaRuntimeGetVersion";

   function cudaGraphCreate (pGraph : System.Address; flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9427
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphCreate";

   function cudaGraphAddKernelNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      pNodeParams : access constant udriver_types_h.cudaKernelNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9524
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddKernelNode";

   function cudaGraphKernelNodeGetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access udriver_types_h.cudaKernelNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9557
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphKernelNodeGetParams";

   function cudaGraphKernelNodeSetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access constant udriver_types_h.cudaKernelNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9582
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphKernelNodeSetParams";

   function cudaGraphKernelNodeCopyAttributes (hSrc : udriver_types_h.cudaGraphNode_t; hDst : udriver_types_h.cudaGraphNode_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9602
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphKernelNodeCopyAttributes";

   function cudaGraphKernelNodeGetAttribute
     (hNode : udriver_types_h.cudaGraphNode_t;
      attr : udriver_types_h.cudaKernelNodeAttrID;
      value_out : access udriver_types_h.cudaKernelNodeAttrValue) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9625
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphKernelNodeGetAttribute";

   function cudaGraphKernelNodeSetAttribute
     (hNode : udriver_types_h.cudaGraphNode_t;
      attr : udriver_types_h.cudaKernelNodeAttrID;
      value : access constant udriver_types_h.cudaKernelNodeAttrValue) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9649
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphKernelNodeSetAttribute";

   function cudaGraphAddMemcpyNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      pCopyParams : access constant udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9699
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddMemcpyNode";

   function cudaGraphAddMemcpyNodeToSymbol
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9758
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddMemcpyNodeToSymbol";

   function cudaGraphAddMemcpyNodeFromSymbol
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9827
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddMemcpyNodeFromSymbol";

   function cudaGraphAddMemcpyNode1D
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9895
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddMemcpyNode1D";

   function cudaGraphMemcpyNodeGetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9927
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphMemcpyNodeGetParams";

   function cudaGraphMemcpyNodeSetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access constant udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9953
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphMemcpyNodeSetParams";

   function cudaGraphMemcpyNodeSetParamsToSymbol
     (node : udriver_types_h.cudaGraphNode_t;
      symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9992
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphMemcpyNodeSetParamsToSymbol";

   function cudaGraphMemcpyNodeSetParamsFromSymbol
     (node : udriver_types_h.cudaGraphNode_t;
      dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10038
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphMemcpyNodeSetParamsFromSymbol";

   function cudaGraphMemcpyNodeSetParams1D
     (node : udriver_types_h.cudaGraphNode_t;
      dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10084
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphMemcpyNodeSetParams1D";

   function cudaGraphAddMemsetNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      pMemsetParams : access constant udriver_types_h.cudaMemsetParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10131
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddMemsetNode";

   function cudaGraphMemsetNodeGetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access udriver_types_h.cudaMemsetParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10154
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphMemsetNodeGetParams";

   function cudaGraphMemsetNodeSetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access constant udriver_types_h.cudaMemsetParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10177
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphMemsetNodeSetParams";

   function cudaGraphAddHostNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      pNodeParams : access constant udriver_types_h.cudaHostNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10218
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddHostNode";

   function cudaGraphHostNodeGetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access udriver_types_h.cudaHostNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10241
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphHostNodeGetParams";

   function cudaGraphHostNodeSetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access constant udriver_types_h.cudaHostNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10264
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphHostNodeSetParams";

   function cudaGraphAddChildGraphNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      childGraph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10304
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddChildGraphNode";

   function cudaGraphChildGraphNodeGetGraph (node : udriver_types_h.cudaGraphNode_t; pGraph : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10331
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphChildGraphNodeGetGraph";

   function cudaGraphAddEmptyNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10368
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddEmptyNode";

   function cudaGraphAddEventRecordNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10411
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddEventRecordNode";

   function cudaGraphEventRecordNodeGetEvent (node : udriver_types_h.cudaGraphNode_t; event_out : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10438
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphEventRecordNodeGetEvent";

   function cudaGraphEventRecordNodeSetEvent (node : udriver_types_h.cudaGraphNode_t; event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10465
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphEventRecordNodeSetEvent";

   function cudaGraphAddEventWaitNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10511
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddEventWaitNode";

   function cudaGraphEventWaitNodeGetEvent (node : udriver_types_h.cudaGraphNode_t; event_out : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10538
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphEventWaitNodeGetEvent";

   function cudaGraphEventWaitNodeSetEvent (node : udriver_types_h.cudaGraphNode_t; event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10565
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphEventWaitNodeSetEvent";

   function cudaGraphAddExternalSemaphoresSignalNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      nodeParams : access constant udriver_types_h.cudaExternalSemaphoreSignalNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10614
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddExternalSemaphoresSignalNode";

   function cudaGraphExternalSemaphoresSignalNodeGetParams (hNode : udriver_types_h.cudaGraphNode_t; params_out : access udriver_types_h.cudaExternalSemaphoreSignalNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10647
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExternalSemaphoresSignalNodeGetParams";

   function cudaGraphExternalSemaphoresSignalNodeSetParams (hNode : udriver_types_h.cudaGraphNode_t; nodeParams : access constant udriver_types_h.cudaExternalSemaphoreSignalNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10674
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExternalSemaphoresSignalNodeSetParams";

   function cudaGraphAddExternalSemaphoresWaitNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      nodeParams : access constant udriver_types_h.cudaExternalSemaphoreWaitNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10723
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddExternalSemaphoresWaitNode";

   function cudaGraphExternalSemaphoresWaitNodeGetParams (hNode : udriver_types_h.cudaGraphNode_t; params_out : access udriver_types_h.cudaExternalSemaphoreWaitNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10756
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExternalSemaphoresWaitNodeGetParams";

   function cudaGraphExternalSemaphoresWaitNodeSetParams (hNode : udriver_types_h.cudaGraphNode_t; nodeParams : access constant udriver_types_h.cudaExternalSemaphoreWaitNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10783
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExternalSemaphoresWaitNodeSetParams";

   function cudaGraphAddMemAllocNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      nodeParams : access udriver_types_h.cudaMemAllocNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10860
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddMemAllocNode";

   function cudaGraphMemAllocNodeGetParams (node : udriver_types_h.cudaGraphNode_t; params_out : access udriver_types_h.cudaMemAllocNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10887
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphMemAllocNodeGetParams";

   function cudaGraphAddMemFreeNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      dptr : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10947
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddMemFreeNode";

   function cudaGraphMemFreeNodeGetParams (node : udriver_types_h.cudaGraphNode_t; dptr_out : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10971
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphMemFreeNodeGetParams";

   function cudaDeviceGraphMemTrim (device : int) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10999
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGraphMemTrim";

   function cudaDeviceGetGraphMemAttribute
     (device : int;
      attr : udriver_types_h.cudaGraphMemAttributeType;
      value : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11036
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceGetGraphMemAttribute";

   function cudaDeviceSetGraphMemAttribute
     (device : int;
      attr : udriver_types_h.cudaGraphMemAttributeType;
      value : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11070
   with Import => True, 
        Convention => C, 
        External_Name => "cudaDeviceSetGraphMemAttribute";

   function cudaGraphClone (pGraphClone : System.Address; originalGraph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11098
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphClone";

   function cudaGraphNodeFindInClone
     (pNode : System.Address;
      originalNode : udriver_types_h.cudaGraphNode_t;
      clonedGraph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11126
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphNodeFindInClone";

   function cudaGraphNodeGetType (node : udriver_types_h.cudaGraphNode_t; pType : access udriver_types_h.cudaGraphNodeType) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11157
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphNodeGetType";

   function cudaGraphGetNodes
     (graph : udriver_types_h.cudaGraph_t;
      nodes : System.Address;
      numNodes : access stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11188
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphGetNodes";

   function cudaGraphGetRootNodes
     (graph : udriver_types_h.cudaGraph_t;
      pRootNodes : System.Address;
      pNumRootNodes : access stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11219
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphGetRootNodes";

   function cudaGraphGetEdges
     (graph : udriver_types_h.cudaGraph_t;
      from : System.Address;
      to : System.Address;
      numEdges : access stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11253
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphGetEdges";

   function cudaGraphNodeGetDependencies
     (node : udriver_types_h.cudaGraphNode_t;
      pDependencies : System.Address;
      pNumDependencies : access stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11284
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphNodeGetDependencies";

   function cudaGraphNodeGetDependentNodes
     (node : udriver_types_h.cudaGraphNode_t;
      pDependentNodes : System.Address;
      pNumDependentNodes : access stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11316
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphNodeGetDependentNodes";

   function cudaGraphAddDependencies
     (graph : udriver_types_h.cudaGraph_t;
      from : System.Address;
      to : System.Address;
      numDependencies : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11347
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphAddDependencies";

   function cudaGraphRemoveDependencies
     (graph : udriver_types_h.cudaGraph_t;
      from : System.Address;
      to : System.Address;
      numDependencies : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11378
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphRemoveDependencies";

   function cudaGraphDestroyNode (node : udriver_types_h.cudaGraphNode_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11408
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphDestroyNode";

   function cudaGraphInstantiate
     (pGraphExec : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pErrorNode : System.Address;
      pLogBuffer : Interfaces.C.Strings.chars_ptr;
      bufferSize : stddef_h.size_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11446
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphInstantiate";

   function cudaGraphInstantiateWithFlags
     (pGraphExec : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      flags : Extensions.unsigned_long_long) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11489
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphInstantiateWithFlags";

   function cudaGraphExecKernelNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      pNodeParams : access constant udriver_types_h.cudaKernelNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11533
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecKernelNodeSetParams";

   function cudaGraphExecMemcpyNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      pNodeParams : access constant udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11583
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecMemcpyNodeSetParams";

   function cudaGraphExecMemcpyNodeSetParamsToSymbol
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11638
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecMemcpyNodeSetParamsToSymbol";

   function cudaGraphExecMemcpyNodeSetParamsFromSymbol
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11701
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecMemcpyNodeSetParamsFromSymbol";

   function cudaGraphExecMemcpyNodeSetParams1D
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11762
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecMemcpyNodeSetParams1D";

   function cudaGraphExecMemsetNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      pNodeParams : access constant udriver_types_h.cudaMemsetParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11816
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecMemsetNodeSetParams";

   function cudaGraphExecHostNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      pNodeParams : access constant udriver_types_h.cudaHostNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11855
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecHostNodeSetParams";

   function cudaGraphExecChildGraphNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      childGraph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11901
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecChildGraphNodeSetParams";

   function cudaGraphExecEventRecordNodeSetEvent
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hNode : udriver_types_h.cudaGraphNode_t;
      event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11945
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecEventRecordNodeSetEvent";

   function cudaGraphExecEventWaitNodeSetEvent
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hNode : udriver_types_h.cudaGraphNode_t;
      event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11989
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecEventWaitNodeSetEvent";

   function cudaGraphExecExternalSemaphoresSignalNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hNode : udriver_types_h.cudaGraphNode_t;
      nodeParams : access constant udriver_types_h.cudaExternalSemaphoreSignalNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12036
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecExternalSemaphoresSignalNodeSetParams";

   function cudaGraphExecExternalSemaphoresWaitNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hNode : udriver_types_h.cudaGraphNode_t;
      nodeParams : access constant udriver_types_h.cudaExternalSemaphoreWaitNodeParams) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12083
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecExternalSemaphoresWaitNodeSetParams";

   function cudaGraphExecUpdate
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hGraph : udriver_types_h.cudaGraph_t;
      hErrorNode_out : System.Address;
      updateResult_out : access udriver_types_h.cudaGraphExecUpdateResult) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12158
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecUpdate";

   function cudaGraphUpload (graphExec : udriver_types_h.cudaGraphExec_t; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12183
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphUpload";

   function cudaGraphLaunch (graphExec : udriver_types_h.cudaGraphExec_t; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12214
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphLaunch";

   function cudaGraphExecDestroy (graphExec : udriver_types_h.cudaGraphExec_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12237
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphExecDestroy";

   function cudaGraphDestroy (graph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12258
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphDestroy";

   function cudaGraphDebugDotPrint
     (graph : udriver_types_h.cudaGraph_t;
      path : Interfaces.C.Strings.chars_ptr;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12277
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphDebugDotPrint";

   function cudaUserObjectCreate
     (object_out : System.Address;
      ptr : System.Address;
      destroy : udriver_types_h.cudaHostFn_t;
      initialRefcount : unsigned;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12313
   with Import => True, 
        Convention => C, 
        External_Name => "cudaUserObjectCreate";

   function cudaUserObjectRetain (object : udriver_types_h.cudaUserObject_t; count : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12337
   with Import => True, 
        Convention => C, 
        External_Name => "cudaUserObjectRetain";

   function cudaUserObjectRelease (object : udriver_types_h.cudaUserObject_t; count : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12365
   with Import => True, 
        Convention => C, 
        External_Name => "cudaUserObjectRelease";

   function cudaGraphRetainUserObject
     (graph : udriver_types_h.cudaGraph_t;
      object : udriver_types_h.cudaUserObject_t;
      count : unsigned;
      flags : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12393
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphRetainUserObject";

   function cudaGraphReleaseUserObject
     (graph : udriver_types_h.cudaGraph_t;
      object : udriver_types_h.cudaUserObject_t;
      count : unsigned) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12418
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGraphReleaseUserObject";

   function cudaGetDriverEntryPoint
     (symbol : Interfaces.C.Strings.chars_ptr;
      funcPtr : System.Address;
      flags : Extensions.unsigned_long_long) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12484
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetDriverEntryPoint";

   function cudaGetExportTable (ppExportTable : System.Address; pExportTableId : access constant udriver_types_h.CUuuid_st) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12489
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetExportTable";

   function cudaGetFuncBySymbol (functionPtr : System.Address; symbolPtr : System.Address) return udriver_types_h.cudaError_t  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12665
   with Import => True, 
        Convention => C, 
        External_Name => "cudaGetFuncBySymbol";

end ucuda_runtime_api_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
