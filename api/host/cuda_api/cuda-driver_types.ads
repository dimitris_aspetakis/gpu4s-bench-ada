with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;
with System;
with CUDA.Stddef;
with stddef_h;
with CUDA.Vector_Types;
with uvector_types_h;
with udriver_types_h;

package CUDA.Driver_Types is

   subtype Error is unsigned;
   type Channel_Format_Kind is
     (Channel_Format_Kind_Signed, Channel_Format_Kind_Unsigned,
      Channel_Format_Kind_Float, Channel_Format_Kind_None,
      Channel_Format_Kind_NV12, Channel_Format_Kind_Unsigned_Normalized8_X1,
      Channel_Format_Kind_Unsigned_Normalized8_X2,
      Channel_Format_Kind_Unsigned_Normalized8_X4,
      Channel_Format_Kind_Unsigned_Normalized16_X1,
      Channel_Format_Kind_Unsigned_Normalized16_X2,
      Channel_Format_Kind_Unsigned_Normalized16_X4,
      Channel_Format_Kind_Signed_Normalized8_X1,
      Channel_Format_Kind_Signed_Normalized8_X2,
      Channel_Format_Kind_Signed_Normalized8_X4,
      Channel_Format_Kind_Signed_Normalized16_X1,
      Channel_Format_Kind_Signed_Normalized16_X2,
      Channel_Format_Kind_Signed_Normalized16_X4,
      Channel_Format_Kind_Unsigned_Block_Compressed1,
      Channel_Format_Kind_Unsigned_Block_Compressed1_SRGB,
      Channel_Format_Kind_Unsigned_Block_Compressed2,
      Channel_Format_Kind_Unsigned_Block_Compressed2_SRGB,
      Channel_Format_Kind_Unsigned_Block_Compressed3,
      Channel_Format_Kind_Unsigned_Block_Compressed3_SRGB,
      Channel_Format_Kind_Unsigned_Block_Compressed4,
      Channel_Format_Kind_Signed_Block_Compressed4,
      Channel_Format_Kind_Unsigned_Block_Compressed5,
      Channel_Format_Kind_Signed_Block_Compressed5,
      Channel_Format_Kind_Unsigned_Block_Compressed6_H,
      Channel_Format_Kind_Signed_Block_Compressed6_H,
      Channel_Format_Kind_Unsigned_Block_Compressed7,
      Channel_Format_Kind_Unsigned_Block_Compressed7_SRGB) with
     Convention => C;

   type Channel_Format_Desc is record
      X : int;
      Y : int;
      Z : int;
      W : int;
      F : Channel_Format_Kind;

   end record with
     Convention => C;

   type CUDA_Array is null record;

   type CUDA_Array_t is access CUDA_Array;

   type CUDA_Array_const_t is access CUDA_Array;

   type Mipmapped_Array is null record;

   type Mipmapped_Array_T is access Mipmapped_Array;

   type Mipmapped_Array_Const_T is access Mipmapped_Array;

   type Anon_Struct965 is record
      Width  : unsigned;
      Height : unsigned;
      Depth  : unsigned;

   end record with
     Convention => C;

   type Anon_Array967 is array (0 .. 3) of unsigned;

   type CUDA_ArraySparseProperties is record
      Tile_Extent         : Anon_Struct965;
      Miptail_First_Level : unsigned;
      Miptail_Size        : Extensions.unsigned_long_long;
      Flags               : unsigned;
      Reserved            : Anon_Array967;

   end record with
     Convention => C;

   type Memory_Type_T is
     (Memory_Type_Unregistered, Memory_Type_Host, Memory_Type_Device,
      Memory_Type_Managed) with
     Convention => C;

   type Memcpy_Kind is
     (Memcpy_Host_To_Host, Memcpy_Host_To_Device, Memcpy_Device_To_Host,
      Memcpy_Device_To_Device, Memcpy_Default) with
     Convention => C;

   type Pitched_Ptr is record
      Ptr   : System.Address;
      Pitch : CUDA.Stddef.Size_T;
      Xsize : CUDA.Stddef.Size_T;
      Ysize : CUDA.Stddef.Size_T;

   end record with
     Convention => C;

   type Extent_T is record
      Width  : CUDA.Stddef.Size_T;
      Height : CUDA.Stddef.Size_T;
      Depth  : CUDA.Stddef.Size_T;

   end record with
     Convention => C;

   type Pos is record
      X : CUDA.Stddef.Size_T;
      Y : CUDA.Stddef.Size_T;
      Z : CUDA.Stddef.Size_T;

   end record with
     Convention => C;

   type Memcpy_3D_Parms is record
      Src_Array : CUDA_Array_t;
      Src_Pos   : Pos;
      Src_Ptr   : Pitched_Ptr;
      Dst_Array : CUDA_Array_t;
      Dst_Pos   : Pos;
      Dst_Ptr   : Pitched_Ptr;
      Extent    : Extent_T;
      Kind      : Memcpy_Kind;

   end record with
     Convention => C;

   type Memcpy_3D_Peer_Parms is record
      Src_Array  : CUDA_Array_t;
      Src_Pos    : Pos;
      Src_Ptr    : Pitched_Ptr;
      Src_Device : int;
      Dst_Array  : CUDA_Array_t;
      Dst_Pos    : Pos;
      Dst_Ptr    : Pitched_Ptr;
      Dst_Device : int;
      Extent     : Extent_T;

   end record with
     Convention => C;

   type Memset_Params is record
      Dst          : System.Address;
      Pitch        : CUDA.Stddef.Size_T;
      Value        : unsigned;
      Element_Size : unsigned;
      Width        : CUDA.Stddef.Size_T;
      Height       : CUDA.Stddef.Size_T;

   end record with
     Convention => C;

   type Access_Property is
     (Access_Property_Normal, Access_Property_Streaming,
      Access_Property_Persisting) with
     Convention => C;

   type Access_Policy_Window_T is record
      Base_Ptr  : System.Address;
      Num_Bytes : CUDA.Stddef.Size_T;
      Hit_Ratio : float;
      Hit_Prop  : Access_Property;
      Miss_Prop : Access_Property;

   end record with
     Convention => C;

   type Host_Fn_T is new udriver_types_h.cudaHostFn_t with
     Convention => C;

   generic
      with procedure Temp_Call_1 (Arg1 : System.Address);
   procedure Host_Fn_T_Gen (Arg1 : System.Address);
   type Host_Node_Params is record
      Fn        : Host_Fn_T;
      User_Data : System.Address;

   end record with
     Convention => C;

   type Stream_Capture_Status is
     (Stream_Capture_Status_None, Stream_Capture_Status_Active,
      Stream_Capture_Status_Invalidated) with
     Convention => C;

   type Stream_Capture_Mode is
     (Stream_Capture_Mode_Global, Stream_Capture_Mode_Thread_Local,
      Stream_Capture_Mode_Relaxed) with
     Convention => C;

   subtype Synchronization_Policy is unsigned;
   Synchronization_Policy_Cuda_Sync_Policy_Auto          :
     Synchronization_Policy renames
     udriver_types_h.cudaSynchronizationPolicy_cudaSyncPolicyAuto;
   Synchronization_Policy_Cuda_Sync_Policy_Spin          :
     Synchronization_Policy renames
     udriver_types_h.cudaSynchronizationPolicy_cudaSyncPolicySpin;
   Synchronization_Policy_Cuda_Sync_Policy_Yield         :
     Synchronization_Policy renames
     udriver_types_h.cudaSynchronizationPolicy_cudaSyncPolicyYield;
   Synchronization_Policy_Cuda_Sync_Policy_Blocking_Sync :
     Synchronization_Policy renames
     udriver_types_h.cudaSynchronizationPolicy_cudaSyncPolicyBlockingSync;
   subtype Stream_Attr_ID is unsigned;
   Stream_Attr_ID_Cuda_Stream_Attribute_Access_Policy_Window   :
     Stream_Attr_ID renames
     udriver_types_h.cudaStreamAttrID_cudaStreamAttributeAccessPolicyWindow;
   Stream_Attr_ID_Cuda_Stream_Attribute_Synchronization_Policy :
     Stream_Attr_ID renames
     udriver_types_h.cudaStreamAttrID_cudaStreamAttributeSynchronizationPolicy;
   type Stream_Attr_Value (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Access_Policy_Window : Access_Policy_Window_T;
         when others =>
            Sync_Policy : Synchronization_Policy;

      end case;
   end record with
     Convention => C, Unchecked_Union => True;

   type Stream_Update_Capture_Dependencies_Flags is
     (Stream_Add_Capture_Dependencies, Stream_Set_Capture_Dependencies) with
     Convention => C;

   subtype User_Object_Flags is unsigned;
   User_Object_Flags_Cuda_User_Object_No_Destructor_Sync :
     User_Object_Flags renames
     udriver_types_h.cudaUserObjectFlags_cudaUserObjectNoDestructorSync;
   subtype User_Object_Retain_Flags is unsigned;
   User_Object_Retain_Flags_Cuda_Graph_User_Object_Move :
     User_Object_Retain_Flags renames
     udriver_types_h.cudaUserObjectRetainFlags_cudaGraphUserObjectMove;
   type Graphics_Resource is null record;

   subtype Graphics_Register_Flags is unsigned;
   Graphics_Register_Flags_Cuda_Graphics_Register_Flags_None               :
     Graphics_Register_Flags renames
     udriver_types_h.cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsNone;
   Graphics_Register_Flags_Cuda_Graphics_Register_Flags_Read_Only          :
     Graphics_Register_Flags renames
     udriver_types_h
       .cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsReadOnly;
   Graphics_Register_Flags_Cuda_Graphics_Register_Flags_Write_Discard      :
     Graphics_Register_Flags renames
     udriver_types_h
       .cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsWriteDiscard;
   Graphics_Register_Flags_Cuda_Graphics_Register_Flags_Surface_Load_Store :
     Graphics_Register_Flags renames
     udriver_types_h
       .cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsSurfaceLoadStore;
   Graphics_Register_Flags_Cuda_Graphics_Register_Flags_Texture_Gather     :
     Graphics_Register_Flags renames
     udriver_types_h
       .cudaGraphicsRegisterFlags_cudaGraphicsRegisterFlagsTextureGather;
   type Graphics_Map_Flags is
     (Graphics_Map_Flags_None, Graphics_Map_Flags_Read_Only,
      Graphics_Map_Flags_Write_Discard) with
     Convention => C;

   type Graphics_Cube_Face is
     (Graphics_Cube_Face_Positive_X, Graphics_Cube_Face_Negative_X,
      Graphics_Cube_Face_Positive_Y, Graphics_Cube_Face_Negative_Y,
      Graphics_Cube_Face_Positive_Z, Graphics_Cube_Face_Negative_Z) with
     Convention => C;

   subtype Kernel_Node_Attr_ID is unsigned;
   Kernel_Node_Attr_ID_Cuda_Kernel_Node_Attribute_Access_Policy_Window :
     Kernel_Node_Attr_ID renames
     udriver_types_h
       .cudaKernelNodeAttrID_cudaKernelNodeAttributeAccessPolicyWindow;
   Kernel_Node_Attr_ID_Cuda_Kernel_Node_Attribute_Cooperative          :
     Kernel_Node_Attr_ID renames
     udriver_types_h.cudaKernelNodeAttrID_cudaKernelNodeAttributeCooperative;
   type Kernel_Node_Attr_Value (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Access_Policy_Window : Access_Policy_Window_T;
         when others =>
            Cooperative : int;

      end case;
   end record with
     Convention => C, Unchecked_Union => True;

   type Resource_Type is
     (Resource_Type_Array, Resource_Type_Mipmapped_Array, Resource_Type_Linear,
      Resource_Type_Pitch_2D) with
     Convention => C;

   type Resource_View_Format is
     (Res_View_Format_None, Res_View_Format_Unsigned_Char1,
      Res_View_Format_Unsigned_Char2, Res_View_Format_Unsigned_Char4,
      Res_View_Format_Signed_Char1, Res_View_Format_Signed_Char2,
      Res_View_Format_Signed_Char4, Res_View_Format_Unsigned_Short1,
      Res_View_Format_Unsigned_Short2, Res_View_Format_Unsigned_Short4,
      Res_View_Format_Signed_Short1, Res_View_Format_Signed_Short2,
      Res_View_Format_Signed_Short4, Res_View_Format_Unsigned_Int1,
      Res_View_Format_Unsigned_Int2, Res_View_Format_Unsigned_Int4,
      Res_View_Format_Signed_Int1, Res_View_Format_Signed_Int2,
      Res_View_Format_Signed_Int4, Res_View_Format_Half1,
      Res_View_Format_Half2, Res_View_Format_Half4, Res_View_Format_Float1,
      Res_View_Format_Float2, Res_View_Format_Float4,
      Res_View_Format_Unsigned_Block_Compressed1,
      Res_View_Format_Unsigned_Block_Compressed2,
      Res_View_Format_Unsigned_Block_Compressed3,
      Res_View_Format_Unsigned_Block_Compressed4,
      Res_View_Format_Signed_Block_Compressed4,
      Res_View_Format_Unsigned_Block_Compressed5,
      Res_View_Format_Signed_Block_Compressed5,
      Res_View_Format_Unsigned_Block_Compressed6_H,
      Res_View_Format_Signed_Block_Compressed6_H,
      Res_View_Format_Unsigned_Block_Compressed7) with
     Convention => C;

   type Anon_Struct998 is record
      C_Array : CUDA_Array_t;

   end record with
     Convention => C;

   type Anon_Struct999 is record
      Mipmap : Mipmapped_Array_T;

   end record with
     Convention => C;

   type Anon_Struct1000 is record
      Dev_Ptr       : System.Address;
      Desc          : Channel_Format_Desc;
      Size_In_Bytes : CUDA.Stddef.Size_T;

   end record with
     Convention => C;

   type Anon_Struct1001 is record
      Dev_Ptr        : System.Address;
      Desc           : Channel_Format_Desc;
      Width          : CUDA.Stddef.Size_T;
      Height         : CUDA.Stddef.Size_T;
      Pitch_In_Bytes : CUDA.Stddef.Size_T;

   end record with
     Convention => C;

   type Anon_Union997 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            C_Array : Anon_Struct998;
         when 1 =>
            Mipmap : Anon_Struct999;
         when 2 =>
            Linear : Anon_Struct1000;
         when others =>
            Pitch2_D : Anon_Struct1001;

      end case;
   end record with
     Convention => C, Unchecked_Union => True;

   type Resource_Desc is record
      Res_Type : Resource_Type;
      Res      : Anon_Union997;

   end record with
     Convention => C;

   type Resource_View_Desc is record
      Format             : Resource_View_Format;
      Width              : CUDA.Stddef.Size_T;
      Height             : CUDA.Stddef.Size_T;
      Depth              : CUDA.Stddef.Size_T;
      First_Mipmap_Level : unsigned;
      Last_Mipmap_Level  : unsigned;
      First_Layer        : unsigned;
      Last_Layer         : unsigned;

   end record with
     Convention => C;

   type Pointer_Attributes is record
      C_Type         : Memory_Type_T;
      Device         : int;
      Device_Pointer : System.Address;
      Host_Pointer   : System.Address;

   end record with
     Convention => C;

   type Func_Attributes is record
      Shared_Size_Bytes             : CUDA.Stddef.Size_T;
      Const_Size_Bytes              : CUDA.Stddef.Size_T;
      Local_Size_Bytes              : CUDA.Stddef.Size_T;
      Max_Threads_Per_Block         : int;
      Num_Regs                      : int;
      Ptx_Version                   : int;
      Binary_Version                : int;
      Cache_Mode_CA                 : int;
      Max_Dynamic_Shared_Size_Bytes : int;
      Preferred_Shmem_Carveout      : int;

   end record with
     Convention => C;

   subtype Func_Attribute is unsigned;
   Func_Attribute_Cuda_Func_Attribute_Max_Dynamic_Shared_Memory_Size   :
     Func_Attribute renames
     udriver_types_h
       .cudaFuncAttribute_cudaFuncAttributeMaxDynamicSharedMemorySize;
   Func_Attribute_Cuda_Func_Attribute_Preferred_Shared_Memory_Carveout :
     Func_Attribute renames
     udriver_types_h
       .cudaFuncAttribute_cudaFuncAttributePreferredSharedMemoryCarveout;
   Func_Attribute_Cuda_Func_Attribute_Max                              :
     Func_Attribute renames
     udriver_types_h.cudaFuncAttribute_cudaFuncAttributeMax;
   type Func_Cache is
     (Func_Cache_Prefer_None, Func_Cache_Prefer_Shared, Func_Cache_Prefer_L1,
      Func_Cache_Prefer_Equal) with
     Convention => C;

   type Shared_Mem_Config is
     (Shared_Mem_Bank_Size_Default, Shared_Mem_Bank_Size_Four_Byte,
      Shared_Mem_Bank_Size_Eight_Byte) with
     Convention => C;

   subtype Shared_Carveout is int;
   Shared_Carveout_Cuda_Sharedmem_Carveout_Default    :
     Shared_Carveout renames
     udriver_types_h.cudaSharedCarveout_cudaSharedmemCarveoutDefault;
   Shared_Carveout_Cuda_Sharedmem_Carveout_Max_Shared :
     Shared_Carveout renames
     udriver_types_h.cudaSharedCarveout_cudaSharedmemCarveoutMaxShared;
   Shared_Carveout_Cuda_Sharedmem_Carveout_Max_L1     :
     Shared_Carveout renames
     udriver_types_h.cudaSharedCarveout_cudaSharedmemCarveoutMaxL1;
   type Compute_Mode is
     (Compute_Mode_Default, Compute_Mode_Exclusive, Compute_Mode_Prohibited,
      Compute_Mode_Exclusive_Process) with
     Convention => C;

   type Limit is
     (Limit_Stack_Size, Limit_Printf_Fifo_Size, Limit_Malloc_Heap_Size,
      Limit_Dev_Runtime_Sync_Depth, Limit_Dev_Runtime_Pending_Launch_Count,
      Limit_Max_L2_Fetch_Granularity, Limit_Persisting_L2_Cache_Size) with
     Convention => C;

   subtype Memory_Advise is unsigned;
   Memory_Advise_Cuda_Mem_Advise_Set_Read_Mostly          :
     Memory_Advise renames
     udriver_types_h.cudaMemoryAdvise_cudaMemAdviseSetReadMostly;
   Memory_Advise_Cuda_Mem_Advise_Unset_Read_Mostly        :
     Memory_Advise renames
     udriver_types_h.cudaMemoryAdvise_cudaMemAdviseUnsetReadMostly;
   Memory_Advise_Cuda_Mem_Advise_Set_Preferred_Location   :
     Memory_Advise renames
     udriver_types_h.cudaMemoryAdvise_cudaMemAdviseSetPreferredLocation;
   Memory_Advise_Cuda_Mem_Advise_Unset_Preferred_Location :
     Memory_Advise renames
     udriver_types_h.cudaMemoryAdvise_cudaMemAdviseUnsetPreferredLocation;
   Memory_Advise_Cuda_Mem_Advise_Set_Accessed_By          :
     Memory_Advise renames
     udriver_types_h.cudaMemoryAdvise_cudaMemAdviseSetAccessedBy;
   Memory_Advise_Cuda_Mem_Advise_Unset_Accessed_By        :
     Memory_Advise renames
     udriver_types_h.cudaMemoryAdvise_cudaMemAdviseUnsetAccessedBy;
   subtype Mem_Range_Attribute is unsigned;
   Mem_Range_Attribute_Cuda_Mem_Range_Attribute_Read_Mostly            :
     Mem_Range_Attribute renames
     udriver_types_h.cudaMemRangeAttribute_cudaMemRangeAttributeReadMostly;
   Mem_Range_Attribute_Cuda_Mem_Range_Attribute_Preferred_Location     :
     Mem_Range_Attribute renames
     udriver_types_h
       .cudaMemRangeAttribute_cudaMemRangeAttributePreferredLocation;
   Mem_Range_Attribute_Cuda_Mem_Range_Attribute_Accessed_By            :
     Mem_Range_Attribute renames
     udriver_types_h.cudaMemRangeAttribute_cudaMemRangeAttributeAccessedBy;
   Mem_Range_Attribute_Cuda_Mem_Range_Attribute_Last_Prefetch_Location :
     Mem_Range_Attribute renames
     udriver_types_h
       .cudaMemRangeAttribute_cudaMemRangeAttributeLastPrefetchLocation;
   type Output_Mode is (Key_Value_Pair, CSV) with
     Convention => C;

   subtype Flush_GPUDirect_RDMAWrites_Options is unsigned;
   Flush_GPUDirect_RDMAWrites_Options_Cuda_Flush_GPUDirect_RDMAWrites_Option_Host :
     Flush_GPUDirect_RDMAWrites_Options renames
     udriver_types_h
       .cudaFlushGPUDirectRDMAWritesOptions_cudaFlushGPUDirectRDMAWritesOptionHost;
   Flush_GPUDirect_RDMAWrites_Options_Cuda_Flush_GPUDirect_RDMAWrites_Option_Mem_Ops :
     Flush_GPUDirect_RDMAWrites_Options renames
     udriver_types_h
       .cudaFlushGPUDirectRDMAWritesOptions_cudaFlushGPUDirectRDMAWritesOptionMemOps;
   subtype GPUDirect_RDMAWrites_Ordering is unsigned;
   GPUDirect_RDMAWrites_Ordering_Cuda_GPUDirect_RDMAWrites_Ordering_None :
     GPUDirect_RDMAWrites_Ordering renames
     udriver_types_h
       .cudaGPUDirectRDMAWritesOrdering_cudaGPUDirectRDMAWritesOrderingNone;
   GPUDirect_RDMAWrites_Ordering_Cuda_GPUDirect_RDMAWrites_Ordering_Owner :
     GPUDirect_RDMAWrites_Ordering renames
     udriver_types_h
       .cudaGPUDirectRDMAWritesOrdering_cudaGPUDirectRDMAWritesOrderingOwner;
   GPUDirect_RDMAWrites_Ordering_Cuda_GPUDirect_RDMAWrites_Ordering_All_Devices :
     GPUDirect_RDMAWrites_Ordering renames
     udriver_types_h
       .cudaGPUDirectRDMAWritesOrdering_cudaGPUDirectRDMAWritesOrderingAllDevices;
   subtype Flush_GPUDirect_RDMAWrites_Scope is unsigned;
   Flush_GPUDirect_RDMAWrites_Scope_Cuda_Flush_GPUDirect_RDMAWrites_To_Owner :
     Flush_GPUDirect_RDMAWrites_Scope renames
     udriver_types_h
       .cudaFlushGPUDirectRDMAWritesScope_cudaFlushGPUDirectRDMAWritesToOwner;
   Flush_GPUDirect_RDMAWrites_Scope_Cuda_Flush_GPUDirect_RDMAWrites_To_All_Devices :
     Flush_GPUDirect_RDMAWrites_Scope renames
     udriver_types_h
       .cudaFlushGPUDirectRDMAWritesScope_cudaFlushGPUDirectRDMAWritesToAllDevices;
   type Flush_GPUDirect_RDMAWrites_Target is
     (Flush_GPUDirect_RDMAWrites_Target_Current_Device) with
     Convention => C;

   subtype Device_Attr is unsigned;
   Device_Attr_Cuda_Dev_Attr_Max_Threads_Per_Block                        :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxThreadsPerBlock;
   Device_Attr_Cuda_Dev_Attr_Max_Block_Dim_X                              :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxBlockDimX;
   Device_Attr_Cuda_Dev_Attr_Max_Block_Dim_Y                              :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxBlockDimY;
   Device_Attr_Cuda_Dev_Attr_Max_Block_Dim_Z                              :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxBlockDimZ;
   Device_Attr_Cuda_Dev_Attr_Max_Grid_Dim_X                               :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxGridDimX;
   Device_Attr_Cuda_Dev_Attr_Max_Grid_Dim_Y                               :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxGridDimY;
   Device_Attr_Cuda_Dev_Attr_Max_Grid_Dim_Z                               :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxGridDimZ;
   Device_Attr_Cuda_Dev_Attr_Max_Shared_Memory_Per_Block                  :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSharedMemoryPerBlock;
   Device_Attr_Cuda_Dev_Attr_Total_Constant_Memory                        :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrTotalConstantMemory;
   Device_Attr_Cuda_Dev_Attr_Warp_Size                                    :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrWarpSize;
   Device_Attr_Cuda_Dev_Attr_Max_Pitch                                    :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxPitch;
   Device_Attr_Cuda_Dev_Attr_Max_Registers_Per_Block                      :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxRegistersPerBlock;
   Device_Attr_Cuda_Dev_Attr_Clock_Rate                                   :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrClockRate;
   Device_Attr_Cuda_Dev_Attr_Texture_Alignment                            :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrTextureAlignment;
   Device_Attr_Cuda_Dev_Attr_Gpu_Overlap                                  :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrGpuOverlap;
   Device_Attr_Cuda_Dev_Attr_Multi_Processor_Count                        :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMultiProcessorCount;
   Device_Attr_Cuda_Dev_Attr_Kernel_Exec_Timeout                          :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrKernelExecTimeout;
   Device_Attr_Cuda_Dev_Attr_Integrated                                   :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrIntegrated;
   Device_Attr_Cuda_Dev_Attr_Can_Map_Host_Memory                          :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrCanMapHostMemory;
   Device_Attr_Cuda_Dev_Attr_Compute_Mode                                 :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrComputeMode;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_1D_Width                         :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture1DWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Width                         :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Height                        :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DHeight;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_3D_Width                         :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture3DWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_3D_Height                        :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture3DHeight;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_3D_Depth                         :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture3DDepth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Layered_Width                 :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DLayeredWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Layered_Height                :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DLayeredHeight;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Layered_Layers                :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DLayeredLayers;
   Device_Attr_Cuda_Dev_Attr_Surface_Alignment                            :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrSurfaceAlignment;
   Device_Attr_Cuda_Dev_Attr_Concurrent_Kernels                           :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrConcurrentKernels;
   Device_Attr_Cuda_Dev_Attr_Ecc_Enabled                                  :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrEccEnabled;
   Device_Attr_Cuda_Dev_Attr_Pci_Bus_Id                                   :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrPciBusId;
   Device_Attr_Cuda_Dev_Attr_Pci_Device_Id                                :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrPciDeviceId;
   Device_Attr_Cuda_Dev_Attr_Tcc_Driver                                   :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrTccDriver;
   Device_Attr_Cuda_Dev_Attr_Memory_Clock_Rate                            :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMemoryClockRate;
   Device_Attr_Cuda_Dev_Attr_Global_Memory_Bus_Width                      :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrGlobalMemoryBusWidth;
   Device_Attr_Cuda_Dev_Attr_L2_Cache_Size                                :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrL2CacheSize;
   Device_Attr_Cuda_Dev_Attr_Max_Threads_Per_Multi_Processor              :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxThreadsPerMultiProcessor;
   Device_Attr_Cuda_Dev_Attr_Async_Engine_Count                           :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrAsyncEngineCount;
   Device_Attr_Cuda_Dev_Attr_Unified_Addressing                           :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrUnifiedAddressing;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_1D_Layered_Width                 :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture1DLayeredWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_1D_Layered_Layers                :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture1DLayeredLayers;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Gather_Width                  :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DGatherWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Gather_Height                 :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DGatherHeight;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_3D_Width_Alt                     :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture3DWidthAlt;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_3D_Height_Alt                    :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture3DHeightAlt;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_3D_Depth_Alt                     :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture3DDepthAlt;
   Device_Attr_Cuda_Dev_Attr_Pci_Domain_Id                                :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrPciDomainId;
   Device_Attr_Cuda_Dev_Attr_Texture_Pitch_Alignment                      :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrTexturePitchAlignment;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_Cubemap_Width                    :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTextureCubemapWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_Cubemap_Layered_Width            :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTextureCubemapLayeredWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_Cubemap_Layered_Layers           :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTextureCubemapLayeredLayers;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_1D_Width                         :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface1DWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_2D_Width                         :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface2DWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_2D_Height                        :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface2DHeight;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_3D_Width                         :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface3DWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_3D_Height                        :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface3DHeight;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_3D_Depth                         :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface3DDepth;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_1D_Layered_Width                 :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface1DLayeredWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_1D_Layered_Layers                :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface1DLayeredLayers;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_2D_Layered_Width                 :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface2DLayeredWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_2D_Layered_Height                :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface2DLayeredHeight;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_2D_Layered_Layers                :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurface2DLayeredLayers;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_Cubemap_Width                    :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurfaceCubemapWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_Cubemap_Layered_Width            :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurfaceCubemapLayeredWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Surface_Cubemap_Layered_Layers           :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSurfaceCubemapLayeredLayers;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_1D_Linear_Width                  :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture1DLinearWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Linear_Width                  :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DLinearWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Linear_Height                 :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DLinearHeight;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Linear_Pitch                  :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DLinearPitch;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Mipmapped_Width               :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DMipmappedWidth;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_2D_Mipmapped_Height              :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture2DMipmappedHeight;
   Device_Attr_Cuda_Dev_Attr_Compute_Capability_Major                     :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrComputeCapabilityMajor;
   Device_Attr_Cuda_Dev_Attr_Compute_Capability_Minor                     :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrComputeCapabilityMinor;
   Device_Attr_Cuda_Dev_Attr_Max_Texture_1D_Mipmapped_Width               :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxTexture1DMipmappedWidth;
   Device_Attr_Cuda_Dev_Attr_Stream_Priorities_Supported                  :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrStreamPrioritiesSupported;
   Device_Attr_Cuda_Dev_Attr_Global_L1_Cache_Supported                    :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrGlobalL1CacheSupported;
   Device_Attr_Cuda_Dev_Attr_Local_L1_Cache_Supported                     :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrLocalL1CacheSupported;
   Device_Attr_Cuda_Dev_Attr_Max_Shared_Memory_Per_Multiprocessor         :
     Device_Attr renames
     udriver_types_h
       .cudaDeviceAttr_cudaDevAttrMaxSharedMemoryPerMultiprocessor;
   Device_Attr_Cuda_Dev_Attr_Max_Registers_Per_Multiprocessor             :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxRegistersPerMultiprocessor;
   Device_Attr_Cuda_Dev_Attr_Managed_Memory                               :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrManagedMemory;
   Device_Attr_Cuda_Dev_Attr_Is_Multi_Gpu_Board                           :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrIsMultiGpuBoard;
   Device_Attr_Cuda_Dev_Attr_Multi_Gpu_Board_Group_ID                     :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMultiGpuBoardGroupID;
   Device_Attr_Cuda_Dev_Attr_Host_Native_Atomic_Supported                 :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrHostNativeAtomicSupported;
   Device_Attr_Cuda_Dev_Attr_Single_To_Double_Precision_Perf_Ratio        :
     Device_Attr renames
     udriver_types_h
       .cudaDeviceAttr_cudaDevAttrSingleToDoublePrecisionPerfRatio;
   Device_Attr_Cuda_Dev_Attr_Pageable_Memory_Access                       :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrPageableMemoryAccess;
   Device_Attr_Cuda_Dev_Attr_Concurrent_Managed_Access                    :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrConcurrentManagedAccess;
   Device_Attr_Cuda_Dev_Attr_Compute_Preemption_Supported                 :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrComputePreemptionSupported;
   Device_Attr_Cuda_Dev_Attr_Can_Use_Host_Pointer_For_Registered_Mem      :
     Device_Attr renames
     udriver_types_h
       .cudaDeviceAttr_cudaDevAttrCanUseHostPointerForRegisteredMem;
   Device_Attr_Cuda_Dev_Attr_Reserved92                                   :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrReserved92;
   Device_Attr_Cuda_Dev_Attr_Reserved93                                   :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrReserved93;
   Device_Attr_Cuda_Dev_Attr_Reserved94                                   :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrReserved94;
   Device_Attr_Cuda_Dev_Attr_Cooperative_Launch                           :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrCooperativeLaunch;
   Device_Attr_Cuda_Dev_Attr_Cooperative_Multi_Device_Launch              :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrCooperativeMultiDeviceLaunch;
   Device_Attr_Cuda_Dev_Attr_Max_Shared_Memory_Per_Block_Optin            :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxSharedMemoryPerBlockOptin;
   Device_Attr_Cuda_Dev_Attr_Can_Flush_Remote_Writes                      :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrCanFlushRemoteWrites;
   Device_Attr_Cuda_Dev_Attr_Host_Register_Supported                      :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrHostRegisterSupported;
   Device_Attr_Cuda_Dev_Attr_Pageable_Memory_Access_Uses_Host_Page_Tables :
     Device_Attr renames
     udriver_types_h
       .cudaDeviceAttr_cudaDevAttrPageableMemoryAccessUsesHostPageTables;
   Device_Attr_Cuda_Dev_Attr_Direct_Managed_Mem_Access_From_Host          :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrDirectManagedMemAccessFromHost;
   Device_Attr_Cuda_Dev_Attr_Max_Blocks_Per_Multiprocessor                :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxBlocksPerMultiprocessor;
   Device_Attr_Cuda_Dev_Attr_Max_Persisting_L2_Cache_Size                 :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxPersistingL2CacheSize;
   Device_Attr_Cuda_Dev_Attr_Max_Access_Policy_Window_Size                :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMaxAccessPolicyWindowSize;
   Device_Attr_Cuda_Dev_Attr_Reserved_Shared_Memory_Per_Block             :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrReservedSharedMemoryPerBlock;
   Device_Attr_Cuda_Dev_Attr_Sparse_Cuda_Array_Supported                  :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrSparseCudaArraySupported;
   Device_Attr_Cuda_Dev_Attr_Host_Register_Read_Only_Supported            :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrHostRegisterReadOnlySupported;
   Device_Attr_Cuda_Dev_Attr_Timeline_Semaphore_Interop_Supported         :
     Device_Attr renames
     udriver_types_h
       .cudaDeviceAttr_cudaDevAttrTimelineSemaphoreInteropSupported;
   Device_Attr_Cuda_Dev_Attr_Max_Timeline_Semaphore_Interop_Supported     :
     Device_Attr renames
     udriver_types_h
       .cudaDeviceAttr_cudaDevAttrMaxTimelineSemaphoreInteropSupported;
   Device_Attr_Cuda_Dev_Attr_Memory_Pools_Supported                       :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMemoryPoolsSupported;
   Device_Attr_Cuda_Dev_Attr_GPUDirect_RDMASupported                      :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrGPUDirectRDMASupported;
   Device_Attr_Cuda_Dev_Attr_GPUDirect_RDMAFlush_Writes_Options           :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrGPUDirectRDMAFlushWritesOptions;
   Device_Attr_Cuda_Dev_Attr_GPUDirect_RDMAWrites_Ordering                :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrGPUDirectRDMAWritesOrdering;
   Device_Attr_Cuda_Dev_Attr_Memory_Pool_Supported_Handle_Types           :
     Device_Attr renames
     udriver_types_h.cudaDeviceAttr_cudaDevAttrMemoryPoolSupportedHandleTypes;
   Device_Attr_Cuda_Dev_Attr_Max                                          :
     Device_Attr renames udriver_types_h.cudaDeviceAttr_cudaDevAttrMax;
   subtype Mem_Pool_Attr is unsigned;
   Mem_Pool_Attr_Cuda_Mem_Pool_Reuse_Follow_Event_Dependencies   :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolAttr_cudaMemPoolReuseFollowEventDependencies;
   Mem_Pool_Attr_Cuda_Mem_Pool_Reuse_Allow_Opportunistic         :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolAttr_cudaMemPoolReuseAllowOpportunistic;
   Mem_Pool_Attr_Cuda_Mem_Pool_Reuse_Allow_Internal_Dependencies :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolAttr_cudaMemPoolReuseAllowInternalDependencies;
   Mem_Pool_Attr_Cuda_Mem_Pool_Attr_Release_Threshold            :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolAttr_cudaMemPoolAttrReleaseThreshold;
   Mem_Pool_Attr_Cuda_Mem_Pool_Attr_Reserved_Mem_Current         :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolAttr_cudaMemPoolAttrReservedMemCurrent;
   Mem_Pool_Attr_Cuda_Mem_Pool_Attr_Reserved_Mem_High            :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolAttr_cudaMemPoolAttrReservedMemHigh;
   Mem_Pool_Attr_Cuda_Mem_Pool_Attr_Used_Mem_Current             :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolAttr_cudaMemPoolAttrUsedMemCurrent;
   Mem_Pool_Attr_Cuda_Mem_Pool_Attr_Used_Mem_High                :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolAttr_cudaMemPoolAttrUsedMemHigh;
   type Mem_Location_Type is
     (Mem_Location_Type_Invalid, Mem_Location_Type_Device) with
     Convention => C;

   type Mem_Location is record
      C_Type : Mem_Location_Type;
      Id     : int;

   end record with
     Convention => C;

   subtype Mem_Access_Flags is unsigned;
   Mem_Access_Flags_Cuda_Mem_Access_Flags_Prot_None       :
     Mem_Access_Flags renames
     udriver_types_h.cudaMemAccessFlags_cudaMemAccessFlagsProtNone;
   Mem_Access_Flags_Cuda_Mem_Access_Flags_Prot_Read       :
     Mem_Access_Flags renames
     udriver_types_h.cudaMemAccessFlags_cudaMemAccessFlagsProtRead;
   Mem_Access_Flags_Cuda_Mem_Access_Flags_Prot_Read_Write :
     Mem_Access_Flags renames
     udriver_types_h.cudaMemAccessFlags_cudaMemAccessFlagsProtReadWrite;
   type Mem_Access_Desc is record
      Location : Mem_Location;
      Flags    : Mem_Access_Flags;

   end record with
     Convention => C;

   subtype Mem_Allocation_Type is unsigned;
   Mem_Allocation_Type_Cuda_Mem_Allocation_Type_Invalid :
     Mem_Allocation_Type renames
     udriver_types_h.cudaMemAllocationType_cudaMemAllocationTypeInvalid;
   Mem_Allocation_Type_Cuda_Mem_Allocation_Type_Pinned  :
     Mem_Allocation_Type renames
     udriver_types_h.cudaMemAllocationType_cudaMemAllocationTypePinned;
   Mem_Allocation_Type_Cuda_Mem_Allocation_Type_Max     :
     Mem_Allocation_Type renames
     udriver_types_h.cudaMemAllocationType_cudaMemAllocationTypeMax;
   subtype Mem_Allocation_Handle_Type is unsigned;
   Mem_Allocation_Handle_Type_Cuda_Mem_Handle_Type_None                  :
     Mem_Allocation_Handle_Type renames
     udriver_types_h.cudaMemAllocationHandleType_cudaMemHandleTypeNone;
   Mem_Allocation_Handle_Type_Cuda_Mem_Handle_Type_Posix_File_Descriptor :
     Mem_Allocation_Handle_Type renames
     udriver_types_h
       .cudaMemAllocationHandleType_cudaMemHandleTypePosixFileDescriptor;
   Mem_Allocation_Handle_Type_Cuda_Mem_Handle_Type_Win32                 :
     Mem_Allocation_Handle_Type renames
     udriver_types_h.cudaMemAllocationHandleType_cudaMemHandleTypeWin32;
   Mem_Allocation_Handle_Type_Cuda_Mem_Handle_Type_Win32_Kmt             :
     Mem_Allocation_Handle_Type renames
     udriver_types_h.cudaMemAllocationHandleType_cudaMemHandleTypeWin32Kmt;
   type Anon_Array1028 is array (0 .. 63) of unsigned_char;

   type Mem_Pool_Props is record
      Alloc_Type                : Mem_Allocation_Type;
      Handle_Types              : Mem_Allocation_Handle_Type;
      Location                  : Mem_Location;
      Win32_Security_Attributes : System.Address;
      Reserved                  : Anon_Array1028;

   end record with
     Convention => C;

   type Mem_Pool_Ptr_Export_Data is record
      Reserved : Anon_Array1028;

   end record with
     Convention => C;

   type Mem_Alloc_Node_Params is record
      Pool_Props        : Mem_Pool_Props;
      Access_Descs      : access Mem_Access_Desc;
      Access_Desc_Count : CUDA.Stddef.Size_T;
      Bytesize          : CUDA.Stddef.Size_T;
      Dptr              : System.Address;

   end record with
     Convention => C;

   subtype Graph_Mem_Attribute_Type is unsigned;
   Graph_Mem_Attribute_Type_Cuda_Graph_Mem_Attr_Used_Mem_Current     :
     Graph_Mem_Attribute_Type renames
     udriver_types_h.cudaGraphMemAttributeType_cudaGraphMemAttrUsedMemCurrent;
   Graph_Mem_Attribute_Type_Cuda_Graph_Mem_Attr_Used_Mem_High        :
     Graph_Mem_Attribute_Type renames
     udriver_types_h.cudaGraphMemAttributeType_cudaGraphMemAttrUsedMemHigh;
   Graph_Mem_Attribute_Type_Cuda_Graph_Mem_Attr_Reserved_Mem_Current :
     Graph_Mem_Attribute_Type renames
     udriver_types_h
       .cudaGraphMemAttributeType_cudaGraphMemAttrReservedMemCurrent;
   Graph_Mem_Attribute_Type_Cuda_Graph_Mem_Attr_Reserved_Mem_High    :
     Graph_Mem_Attribute_Type renames
     udriver_types_h.cudaGraphMemAttributeType_cudaGraphMemAttrReservedMemHigh;
   subtype Device_P2_PAttr is unsigned;
   Device_P2_PAttr_Cuda_Dev_P2_PAttr_Performance_Rank            :
     Device_P2_PAttr renames
     udriver_types_h.cudaDeviceP2PAttr_cudaDevP2PAttrPerformanceRank;
   Device_P2_PAttr_Cuda_Dev_P2_PAttr_Access_Supported            :
     Device_P2_PAttr renames
     udriver_types_h.cudaDeviceP2PAttr_cudaDevP2PAttrAccessSupported;
   Device_P2_PAttr_Cuda_Dev_P2_PAttr_Native_Atomic_Supported     :
     Device_P2_PAttr renames
     udriver_types_h.cudaDeviceP2PAttr_cudaDevP2PAttrNativeAtomicSupported;
   Device_P2_PAttr_Cuda_Dev_P2_PAttr_Cuda_Array_Access_Supported :
     Device_P2_PAttr renames
     udriver_types_h.cudaDeviceP2PAttr_cudaDevP2PAttrCudaArrayAccessSupported;
   subtype Anon_Array1037 is Interfaces.C.char_array (0 .. 15);
   type CUuuid_St is record
      Bytes : Anon_Array1037;

   end record with
     Convention => C;

   subtype CUuuid is CUuuid_St;
   subtype UUID_T is CUuuid_St;
   subtype Anon_Array1042 is Interfaces.C.char_array (0 .. 255);
   subtype Anon_Array1044 is Interfaces.C.char_array (0 .. 7);
   type Anon_Array1046 is array (0 .. 2) of int;

   type Anon_Array1048 is array (0 .. 1) of int;

   type Device_Prop is record
      Name                                         : Anon_Array1042;
      Uuid                                         : UUID_T;
      Luid                                         : Anon_Array1044;
      Luid_Device_Node_Mask                        : unsigned;
      Total_Global_Mem                             : CUDA.Stddef.Size_T;
      Shared_Mem_Per_Block                         : CUDA.Stddef.Size_T;
      Regs_Per_Block                               : int;
      Warp_Size                                    : int;
      Mem_Pitch                                    : CUDA.Stddef.Size_T;
      Max_Threads_Per_Block                        : int;
      Max_Threads_Dim                              : Anon_Array1046;
      Max_Grid_Size                                : Anon_Array1046;
      Clock_Rate                                   : int;
      Total_Const_Mem                              : CUDA.Stddef.Size_T;
      Major                                        : int;
      Minor                                        : int;
      Texture_Alignment                            : CUDA.Stddef.Size_T;
      Texture_Pitch_Alignment                      : CUDA.Stddef.Size_T;
      Device_Overlap                               : int;
      Multi_Processor_Count                        : int;
      Kernel_Exec_Timeout_Enabled                  : int;
      Integrated                                   : int;
      Can_Map_Host_Memory                          : int;
      Compute_Mode                                 : int;
      Max_Texture1_D                               : int;
      Max_Texture1_DMipmap                         : int;
      Max_Texture1_DLinear                         : int;
      Max_Texture2_D                               : Anon_Array1048;
      Max_Texture2_DMipmap                         : Anon_Array1048;
      Max_Texture2_DLinear                         : Anon_Array1046;
      Max_Texture2_DGather                         : Anon_Array1048;
      Max_Texture3_D                               : Anon_Array1046;
      Max_Texture3_DAlt                            : Anon_Array1046;
      Max_Texture_Cubemap                          : int;
      Max_Texture1_DLayered                        : Anon_Array1048;
      Max_Texture2_DLayered                        : Anon_Array1046;
      Max_Texture_Cubemap_Layered                  : Anon_Array1048;
      Max_Surface1_D                               : int;
      Max_Surface2_D                               : Anon_Array1048;
      Max_Surface3_D                               : Anon_Array1046;
      Max_Surface1_DLayered                        : Anon_Array1048;
      Max_Surface2_DLayered                        : Anon_Array1046;
      Max_Surface_Cubemap                          : int;
      Max_Surface_Cubemap_Layered                  : Anon_Array1048;
      Surface_Alignment                            : CUDA.Stddef.Size_T;
      Concurrent_Kernels                           : int;
      ECCEnabled                                   : int;
      Pci_Bus_ID                                   : int;
      Pci_Device_ID                                : int;
      Pci_Domain_ID                                : int;
      Tcc_Driver                                   : int;
      Async_Engine_Count                           : int;
      Unified_Addressing                           : int;
      Memory_Clock_Rate                            : int;
      Memory_Bus_Width                             : int;
      L2_Cache_Size                                : int;
      Persisting_L2_Cache_Max_Size                 : int;
      Max_Threads_Per_Multi_Processor              : int;
      Stream_Priorities_Supported                  : int;
      Global_L1_Cache_Supported                    : int;
      Local_L1_Cache_Supported                     : int;
      Shared_Mem_Per_Multiprocessor                : CUDA.Stddef.Size_T;
      Regs_Per_Multiprocessor                      : int;
      Managed_Memory                               : int;
      Is_Multi_Gpu_Board                           : int;
      Multi_Gpu_Board_Group_ID                     : int;
      Host_Native_Atomic_Supported                 : int;
      Single_To_Double_Precision_Perf_Ratio        : int;
      Pageable_Memory_Access                       : int;
      Concurrent_Managed_Access                    : int;
      Compute_Preemption_Supported                 : int;
      Can_Use_Host_Pointer_For_Registered_Mem      : int;
      Cooperative_Launch                           : int;
      Cooperative_Multi_Device_Launch              : int;
      Shared_Mem_Per_Block_Optin                   : CUDA.Stddef.Size_T;
      Pageable_Memory_Access_Uses_Host_Page_Tables : int;
      Direct_Managed_Mem_Access_From_Host          : int;
      Max_Blocks_Per_Multi_Processor               : int;
      Access_Policy_Max_Window_Size                : int;
      Reserved_Shared_Mem_Per_Block                : CUDA.Stddef.Size_T;

   end record with
     Convention => C;

   subtype Anon_Array1050 is Interfaces.C.char_array (0 .. 63);
   type Ipc_Event_Handle_St is record
      Reserved : Anon_Array1050;

   end record with
     Convention => C;

   subtype Ipc_Event_Handle_T is Ipc_Event_Handle_St;
   type Ipc_Mem_Handle_St is record
      Reserved : Anon_Array1050;

   end record with
     Convention => C;

   subtype Ipc_Mem_Handle_T is Ipc_Mem_Handle_St;
   subtype External_Memory_Handle_Type is unsigned;
   External_Memory_Handle_Type_Cuda_External_Memory_Handle_Type_Opaque_Fd :
     External_Memory_Handle_Type renames
     udriver_types_h
       .cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeOpaqueFd;
   External_Memory_Handle_Type_Cuda_External_Memory_Handle_Type_Opaque_Win32 :
     External_Memory_Handle_Type renames
     udriver_types_h
       .cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeOpaqueWin32;
   External_Memory_Handle_Type_Cuda_External_Memory_Handle_Type_Opaque_Win32_Kmt :
     External_Memory_Handle_Type renames
     udriver_types_h
       .cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeOpaqueWin32Kmt;
   External_Memory_Handle_Type_Cuda_External_Memory_Handle_Type_D_3D_12_Heap :
     External_Memory_Handle_Type renames
     udriver_types_h
       .cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeD3D12Heap;
   External_Memory_Handle_Type_Cuda_External_Memory_Handle_Type_D_3D_12_Resource :
     External_Memory_Handle_Type renames
     udriver_types_h
       .cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeD3D12Resource;
   External_Memory_Handle_Type_Cuda_External_Memory_Handle_Type_D_3D_11_Resource :
     External_Memory_Handle_Type renames
     udriver_types_h
       .cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeD3D11Resource;
   External_Memory_Handle_Type_Cuda_External_Memory_Handle_Type_D_3D_11_Resource_Kmt :
     External_Memory_Handle_Type renames
     udriver_types_h
       .cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeD3D11ResourceKmt;
   External_Memory_Handle_Type_Cuda_External_Memory_Handle_Type_Nv_Sci_Buf :
     External_Memory_Handle_Type renames
     udriver_types_h
       .cudaExternalMemoryHandleType_cudaExternalMemoryHandleTypeNvSciBuf;
   type Anon_Struct1057 is record
      Handle : System.Address;
      Name   : System.Address;

   end record with
     Convention => C;

   type Anon_Union1056 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fd : int;
         when 1 =>
            Win32 : Anon_Struct1057;
         when others =>
            Nv_Sci_Buf_Object : System.Address;

      end case;
   end record with
     Convention => C, Unchecked_Union => True;

   type External_Memory_Handle_Desc is record
      C_Type : External_Memory_Handle_Type;
      Handle : Anon_Union1056;
      Size   : Extensions.unsigned_long_long;
      Flags  : unsigned;

   end record with
     Convention => C;

   type External_Memory_Buffer_Desc is record
      Offset : Extensions.unsigned_long_long;
      Size   : Extensions.unsigned_long_long;
      Flags  : unsigned;

   end record with
     Convention => C;

   type External_Memory_Mipmapped_Array_Desc is record
      Offset      : Extensions.unsigned_long_long;
      Format_Desc : Channel_Format_Desc;
      Extent      : Extent_T;
      Flags       : unsigned;
      Num_Levels  : unsigned;

   end record with
     Convention => C;

   subtype External_Semaphore_Handle_Type is unsigned;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_Opaque_Fd :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeOpaqueFd;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_Opaque_Win32 :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeOpaqueWin32;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_Opaque_Win32_Kmt :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeOpaqueWin32Kmt;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_D_3D_12_Fence :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeD3D12Fence;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_D_3D_11_Fence :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeD3D11Fence;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_Nv_Sci_Sync :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeNvSciSync;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_Keyed_Mutex :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeKeyedMutex;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_Keyed_Mutex_Kmt :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeKeyedMutexKmt;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_Timeline_Semaphore_Fd :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeTimelineSemaphoreFd;
   External_Semaphore_Handle_Type_Cuda_External_Semaphore_Handle_Type_Timeline_Semaphore_Win32 :
     External_Semaphore_Handle_Type renames
     udriver_types_h
       .cudaExternalSemaphoreHandleType_cudaExternalSemaphoreHandleTypeTimelineSemaphoreWin32;
   type Anon_Struct1063 is record
      Handle : System.Address;
      Name   : System.Address;

   end record with
     Convention => C;

   type Anon_Union1062 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fd : int;
         when 1 =>
            Win32 : Anon_Struct1063;
         when others =>
            Nv_Sci_Sync_Obj : System.Address;

      end case;
   end record with
     Convention => C, Unchecked_Union => True;

   type External_Semaphore_Handle_Desc is record
      C_Type : External_Semaphore_Handle_Type;
      Handle : Anon_Union1062;
      Flags  : unsigned;

   end record with
     Convention => C;

   type Anon_Struct1066 is record
      Value : Extensions.unsigned_long_long;

   end record with
     Convention => C;

   type Anon_Union1067 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fence : System.Address;
         when others =>
            Reserved : Extensions.unsigned_long_long;

      end case;
   end record with
     Convention => C, Unchecked_Union => True;

   type Anon_Struct1068 is record
      Key : Extensions.unsigned_long_long;

   end record with
     Convention => C;

   type Anon_Struct1065 is record
      Fence       : Anon_Struct1066;
      Nv_Sci_Sync : Anon_Union1067;
      Keyed_Mutex : Anon_Struct1068;

   end record with
     Convention => C;

   type External_Semaphore_Signal_Params_V1 is record
      Params : Anon_Struct1065;
      Flags  : unsigned;

   end record with
     Convention => C;

   type Anon_Struct1071 is record
      Value : Extensions.unsigned_long_long;

   end record with
     Convention => C;

   type Anon_Union1072 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fence : System.Address;
         when others =>
            Reserved : Extensions.unsigned_long_long;

      end case;
   end record with
     Convention => C, Unchecked_Union => True;

   type Anon_Struct1073 is record
      Key        : Extensions.unsigned_long_long;
      Timeout_Ms : unsigned;

   end record with
     Convention => C;

   type Anon_Struct1070 is record
      Fence       : Anon_Struct1071;
      Nv_Sci_Sync : Anon_Union1072;
      Keyed_Mutex : Anon_Struct1073;

   end record with
     Convention => C;

   type External_Semaphore_Wait_Params_V1 is record
      Params : Anon_Struct1070;
      Flags  : unsigned;

   end record with
     Convention => C;

   type Anon_Struct1076 is record
      Value : Extensions.unsigned_long_long;

   end record with
     Convention => C;

   type Anon_Union1077 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fence : System.Address;
         when others =>
            Reserved : Extensions.unsigned_long_long;

      end case;
   end record with
     Convention => C, Unchecked_Union => True;

   type Anon_Struct1078 is record
      Key : Extensions.unsigned_long_long;

   end record with
     Convention => C;

   type Anon_Array1080 is array (0 .. 11) of unsigned;

   type Anon_Struct1075 is record
      Fence       : Anon_Struct1076;
      Nv_Sci_Sync : Anon_Union1077;
      Keyed_Mutex : Anon_Struct1078;
      Reserved    : Anon_Array1080;

   end record with
     Convention => C;

   type Anon_Array1081 is array (0 .. 15) of unsigned;

   type External_Semaphore_Signal_Params is record
      Params   : Anon_Struct1075;
      Flags    : unsigned;
      Reserved : Anon_Array1081;

   end record with
     Convention => C;

   type Anon_Struct1084 is record
      Value : Extensions.unsigned_long_long;

   end record with
     Convention => C;

   type Anon_Union1085 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fence : System.Address;
         when others =>
            Reserved : Extensions.unsigned_long_long;

      end case;
   end record with
     Convention => C, Unchecked_Union => True;

   type Anon_Struct1086 is record
      Key        : Extensions.unsigned_long_long;
      Timeout_Ms : unsigned;

   end record with
     Convention => C;

   type Anon_Array1088 is array (0 .. 9) of unsigned;

   type Anon_Struct1083 is record
      Fence       : Anon_Struct1084;
      Nv_Sci_Sync : Anon_Union1085;
      Keyed_Mutex : Anon_Struct1086;
      Reserved    : Anon_Array1088;

   end record with
     Convention => C;

   type External_Semaphore_Wait_Params is record
      Params   : Anon_Struct1083;
      Flags    : unsigned;
      Reserved : Anon_Array1081;

   end record with
     Convention => C;

   subtype Error_T is Error;
   type CUstream_St is null record;

   type Stream_T is access CUstream_St;

   type CUevent_St is null record;

   type Event_T is access CUevent_St;

   type Graphics_Resource_T is access Graphics_Resource;

   subtype Output_Mode_T is Output_Mode;
   type CUexternal_Memory_St is null record;

   type External_Memory_T is access CUexternal_Memory_St;

   type CUexternal_Semaphore_St is null record;

   type External_Semaphore_T is access CUexternal_Semaphore_St;

   type CUgraph_St is null record;

   type Graph_T is access CUgraph_St;

   type CUgraph_Node_St is null record;

   type Graph_Node_T is access CUgraph_Node_St;

   type CUuser_Object_St is null record;

   type User_Object_T is access CUuser_Object_St;

   type CUfunc_St is null record;

   type Function_T is access CUfunc_St;

   type CUmem_Pool_Handle_St is null record;

   type Mem_Pool_T is access CUmem_Pool_Handle_St;

   type CGScope is (CGScope_Invalid, CGScope_Grid, CGScope_Multi_Grid) with
     Convention => C;

   type Launch_Params is record
      Func       : System.Address;
      Grid_Dim   : CUDA.Vector_Types.Dim3;
      Block_Dim  : CUDA.Vector_Types.Dim3;
      Args       : System.Address;
      Shared_Mem : CUDA.Stddef.Size_T;
      Stream     : Stream_T;

   end record with
     Convention => C;

   type Kernel_Node_Params is record
      Func             : System.Address;
      Grid_Dim         : CUDA.Vector_Types.Dim3;
      Block_Dim        : CUDA.Vector_Types.Dim3;
      Shared_Mem_Bytes : unsigned;
      Kernel_Params    : System.Address;
      Extra            : System.Address;

   end record with
     Convention => C;

   type External_Semaphore_Signal_Node_Params is record
      Ext_Sem_Array : System.Address;
      Params_Array  : access External_Semaphore_Signal_Params;
      Num_Ext_Sems  : unsigned;

   end record with
     Convention => C;

   type External_Semaphore_Wait_Node_Params is record
      Ext_Sem_Array : System.Address;
      Params_Array  : access External_Semaphore_Wait_Params;
      Num_Ext_Sems  : unsigned;

   end record with
     Convention => C;

   type Graph_Node_Type is
     (Graph_Node_Type_Kernel, Graph_Node_Type_Memcpy, Graph_Node_Type_Memset,
      Graph_Node_Type_Host, Graph_Node_Type_Graph, Graph_Node_Type_Empty,
      Graph_Node_Type_Wait_Event, Graph_Node_Type_Event_Record,
      Graph_Node_Type_Ext_Semaphore_Signal, Graph_Node_Type_Ext_Semaphore_Wait,
      Graph_Node_Type_Mem_Alloc, Graph_Node_Type_Mem_Free,
      Graph_Node_Type_Count) with
     Convention => C;

   type CUgraph_Exec_St is null record;

   type Graph_Exec_T is access CUgraph_Exec_St;

   type Graph_Exec_Update_Result is
     (Graph_Exec_Update_Success, Graph_Exec_Update_Error,
      Graph_Exec_Update_Error_Topology_Changed,
      Graph_Exec_Update_Error_Node_Type_Changed,
      Graph_Exec_Update_Error_Function_Changed,
      Graph_Exec_Update_Error_Parameters_Changed,
      Graph_Exec_Update_Error_Not_Supported,
      Graph_Exec_Update_Error_Unsupported_Function_Change) with
     Convention => C;

   type Get_Driver_Entry_Point_Flags is
     (Enable_Default, Enable_Legacy_Stream,
      Enable_Per_Thread_Default_Stream) with
     Convention => C;

   subtype Graph_Debug_Dot_Flags is unsigned;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Verbose :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsVerbose;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Kernel_Node_Params :
     Graph_Debug_Dot_Flags renames
     udriver_types_h
       .cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsKernelNodeParams;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Memcpy_Node_Params :
     Graph_Debug_Dot_Flags renames
     udriver_types_h
       .cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsMemcpyNodeParams;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Memset_Node_Params :
     Graph_Debug_Dot_Flags renames
     udriver_types_h
       .cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsMemsetNodeParams;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Host_Node_Params :
     Graph_Debug_Dot_Flags renames
     udriver_types_h
       .cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsHostNodeParams;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Event_Node_Params :
     Graph_Debug_Dot_Flags renames
     udriver_types_h
       .cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsEventNodeParams;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Ext_Semas_Signal_Node_Params :
     Graph_Debug_Dot_Flags renames
     udriver_types_h
       .cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsExtSemasSignalNodeParams;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Ext_Semas_Wait_Node_Params :
     Graph_Debug_Dot_Flags renames
     udriver_types_h
       .cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsExtSemasWaitNodeParams;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Kernel_Node_Attributes :
     Graph_Debug_Dot_Flags renames
     udriver_types_h
       .cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsKernelNodeAttributes;
   Graph_Debug_Dot_Flags_Cuda_Graph_Debug_Dot_Flags_Handles :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlags_cudaGraphDebugDotFlagsHandles;
   subtype Graph_Instantiate_Flags is unsigned;
   Graph_Instantiate_Flags_Cuda_Graph_Instantiate_Flag_Auto_Free_On_Launch :
     Graph_Instantiate_Flags renames
     udriver_types_h
       .cudaGraphInstantiateFlags_cudaGraphInstantiateFlagAutoFreeOnLaunch;

end CUDA.Driver_Types;
