with Ada.Containers.Ordered_Maps;
with Ada.Exceptions;

package CUDA.Exceptions is
   pragma Elaborate_Body;

   package Temp_registry_map_1 is new Ada.Containers.Ordered_Maps
     (Integer, Ada.Exceptions.Exception_Id, "<", Ada.Exceptions."=");
   Exception_Registry : Temp_registry_map_1.Map;
   Error_cudaSuccess                             : exception;
   Error_cudaErrorInvalidValue                   : exception;
   Error_cudaErrorMemoryAllocation               : exception;
   Error_cudaErrorInitializationError            : exception;
   Error_cudaErrorCudartUnloading                : exception;
   Error_cudaErrorProfilerDisabled               : exception;
   Error_cudaErrorProfilerNotInitialized         : exception;
   Error_cudaErrorProfilerAlreadyStarted         : exception;
   Error_cudaErrorProfilerAlreadyStopped         : exception;
   Error_cudaErrorInvalidConfiguration           : exception;
   Error_cudaErrorInvalidPitchValue              : exception;
   Error_cudaErrorInvalidSymbol                  : exception;
   Error_cudaErrorInvalidHostPointer             : exception;
   Error_cudaErrorInvalidDevicePointer           : exception;
   Error_cudaErrorInvalidTexture                 : exception;
   Error_cudaErrorInvalidTextureBinding          : exception;
   Error_cudaErrorInvalidChannelDescriptor       : exception;
   Error_cudaErrorInvalidMemcpyDirection         : exception;
   Error_cudaErrorAddressOfConstant              : exception;
   Error_cudaErrorTextureFetchFailed             : exception;
   Error_cudaErrorTextureNotBound                : exception;
   Error_cudaErrorSynchronizationError           : exception;
   Error_cudaErrorInvalidFilterSetting           : exception;
   Error_cudaErrorInvalidNormSetting             : exception;
   Error_cudaErrorMixedDeviceExecution           : exception;
   Error_cudaErrorNotYetImplemented              : exception;
   Error_cudaErrorMemoryValueTooLarge            : exception;
   Error_cudaErrorStubLibrary                    : exception;
   Error_cudaErrorInsufficientDriver             : exception;
   Error_cudaErrorCallRequiresNewerDriver        : exception;
   Error_cudaErrorInvalidSurface                 : exception;
   Error_cudaErrorDuplicateVariableName          : exception;
   Error_cudaErrorDuplicateTextureName           : exception;
   Error_cudaErrorDuplicateSurfaceName           : exception;
   Error_cudaErrorDevicesUnavailable             : exception;
   Error_cudaErrorIncompatibleDriverContext      : exception;
   Error_cudaErrorMissingConfiguration           : exception;
   Error_cudaErrorPriorLaunchFailure             : exception;
   Error_cudaErrorLaunchMaxDepthExceeded         : exception;
   Error_cudaErrorLaunchFileScopedTex            : exception;
   Error_cudaErrorLaunchFileScopedSurf           : exception;
   Error_cudaErrorSyncDepthExceeded              : exception;
   Error_cudaErrorLaunchPendingCountExceeded     : exception;
   Error_cudaErrorInvalidDeviceFunction          : exception;
   Error_cudaErrorNoDevice                       : exception;
   Error_cudaErrorInvalidDevice                  : exception;
   Error_cudaErrorDeviceNotLicensed              : exception;
   Error_cudaErrorSoftwareValidityNotEstablished : exception;
   Error_cudaErrorStartupFailure                 : exception;
   Error_cudaErrorInvalidKernelImage             : exception;
   Error_cudaErrorDeviceUninitialized            : exception;
   Error_cudaErrorMapBufferObjectFailed          : exception;
   Error_cudaErrorUnmapBufferObjectFailed        : exception;
   Error_cudaErrorArrayIsMapped                  : exception;
   Error_cudaErrorAlreadyMapped                  : exception;
   Error_cudaErrorNoKernelImageForDevice         : exception;
   Error_cudaErrorAlreadyAcquired                : exception;
   Error_cudaErrorNotMapped                      : exception;
   Error_cudaErrorNotMappedAsArray               : exception;
   Error_cudaErrorNotMappedAsPointer             : exception;
   Error_cudaErrorECCUncorrectable               : exception;
   Error_cudaErrorUnsupportedLimit               : exception;
   Error_cudaErrorDeviceAlreadyInUse             : exception;
   Error_cudaErrorPeerAccessUnsupported          : exception;
   Error_cudaErrorInvalidPtx                     : exception;
   Error_cudaErrorInvalidGraphicsContext         : exception;
   Error_cudaErrorNvlinkUncorrectable            : exception;
   Error_cudaErrorJitCompilerNotFound            : exception;
   Error_cudaErrorUnsupportedPtxVersion          : exception;
   Error_cudaErrorJitCompilationDisabled         : exception;
   Error_cudaErrorUnsupportedExecAffinity        : exception;
   Error_cudaErrorInvalidSource                  : exception;
   Error_cudaErrorFileNotFound                   : exception;
   Error_cudaErrorSharedObjectSymbolNotFound     : exception;
   Error_cudaErrorSharedObjectInitFailed         : exception;
   Error_cudaErrorOperatingSystem                : exception;
   Error_cudaErrorInvalidResourceHandle          : exception;
   Error_cudaErrorIllegalState                   : exception;
   Error_cudaErrorSymbolNotFound                 : exception;
   Error_cudaErrorNotReady                       : exception;
   Error_cudaErrorIllegalAddress                 : exception;
   Error_cudaErrorLaunchOutOfResources           : exception;
   Error_cudaErrorLaunchTimeout                  : exception;
   Error_cudaErrorLaunchIncompatibleTexturing    : exception;
   Error_cudaErrorPeerAccessAlreadyEnabled       : exception;
   Error_cudaErrorPeerAccessNotEnabled           : exception;
   Error_cudaErrorSetOnActiveProcess             : exception;
   Error_cudaErrorContextIsDestroyed             : exception;
   Error_cudaErrorAssert                         : exception;
   Error_cudaErrorTooManyPeers                   : exception;
   Error_cudaErrorHostMemoryAlreadyRegistered    : exception;
   Error_cudaErrorHostMemoryNotRegistered        : exception;
   Error_cudaErrorHardwareStackError             : exception;
   Error_cudaErrorIllegalInstruction             : exception;
   Error_cudaErrorMisalignedAddress              : exception;
   Error_cudaErrorInvalidAddressSpace            : exception;
   Error_cudaErrorInvalidPc                      : exception;
   Error_cudaErrorLaunchFailure                  : exception;
   Error_cudaErrorCooperativeLaunchTooLarge      : exception;
   Error_cudaErrorNotPermitted                   : exception;
   Error_cudaErrorNotSupported                   : exception;
   Error_cudaErrorSystemNotReady                 : exception;
   Error_cudaErrorSystemDriverMismatch           : exception;
   Error_cudaErrorCompatNotSupportedOnDevice     : exception;
   Error_cudaErrorMpsConnectionFailed            : exception;
   Error_cudaErrorMpsRpcFailure                  : exception;
   Error_cudaErrorMpsServerNotReady              : exception;
   Error_cudaErrorMpsMaxClientsReached           : exception;
   Error_cudaErrorMpsMaxConnectionsReached       : exception;
   Error_cudaErrorStreamCaptureUnsupported       : exception;
   Error_cudaErrorStreamCaptureInvalidated       : exception;
   Error_cudaErrorStreamCaptureMerge             : exception;
   Error_cudaErrorStreamCaptureUnmatched         : exception;
   Error_cudaErrorStreamCaptureUnjoined          : exception;
   Error_cudaErrorStreamCaptureIsolation         : exception;
   Error_cudaErrorStreamCaptureImplicit          : exception;
   Error_cudaErrorCapturedEvent                  : exception;
   Error_cudaErrorStreamCaptureWrongThread       : exception;
   Error_cudaErrorTimeout                        : exception;
   Error_cudaErrorGraphExecUpdateFailure         : exception;
   Error_cudaErrorExternalDevice                 : exception;
   Error_cudaErrorUnknown                        : exception;
   Error_cudaErrorApiFailureBase                 : exception;

end CUDA.Exceptions;
