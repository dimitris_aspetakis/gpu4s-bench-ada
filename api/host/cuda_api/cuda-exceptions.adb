package body CUDA.Exceptions is

begin
   null;
   Exception_Registry.Insert (Integer (0), Error_cudaSuccess'Identity);
   Exception_Registry.Insert
     (Integer (1), Error_cudaErrorInvalidValue'Identity);
   Exception_Registry.Insert
     (Integer (2), Error_cudaErrorMemoryAllocation'Identity);
   Exception_Registry.Insert
     (Integer (3), Error_cudaErrorInitializationError'Identity);
   Exception_Registry.Insert
     (Integer (4), Error_cudaErrorCudartUnloading'Identity);
   Exception_Registry.Insert
     (Integer (5), Error_cudaErrorProfilerDisabled'Identity);
   Exception_Registry.Insert
     (Integer (6), Error_cudaErrorProfilerNotInitialized'Identity);
   Exception_Registry.Insert
     (Integer (7), Error_cudaErrorProfilerAlreadyStarted'Identity);
   Exception_Registry.Insert
     (Integer (8), Error_cudaErrorProfilerAlreadyStopped'Identity);
   Exception_Registry.Insert
     (Integer (9), Error_cudaErrorInvalidConfiguration'Identity);
   Exception_Registry.Insert
     (Integer (12), Error_cudaErrorInvalidPitchValue'Identity);
   Exception_Registry.Insert
     (Integer (13), Error_cudaErrorInvalidSymbol'Identity);
   Exception_Registry.Insert
     (Integer (16), Error_cudaErrorInvalidHostPointer'Identity);
   Exception_Registry.Insert
     (Integer (17), Error_cudaErrorInvalidDevicePointer'Identity);
   Exception_Registry.Insert
     (Integer (18), Error_cudaErrorInvalidTexture'Identity);
   Exception_Registry.Insert
     (Integer (19), Error_cudaErrorInvalidTextureBinding'Identity);
   Exception_Registry.Insert
     (Integer (20), Error_cudaErrorInvalidChannelDescriptor'Identity);
   Exception_Registry.Insert
     (Integer (21), Error_cudaErrorInvalidMemcpyDirection'Identity);
   Exception_Registry.Insert
     (Integer (22), Error_cudaErrorAddressOfConstant'Identity);
   Exception_Registry.Insert
     (Integer (23), Error_cudaErrorTextureFetchFailed'Identity);
   Exception_Registry.Insert
     (Integer (24), Error_cudaErrorTextureNotBound'Identity);
   Exception_Registry.Insert
     (Integer (25), Error_cudaErrorSynchronizationError'Identity);
   Exception_Registry.Insert
     (Integer (26), Error_cudaErrorInvalidFilterSetting'Identity);
   Exception_Registry.Insert
     (Integer (27), Error_cudaErrorInvalidNormSetting'Identity);
   Exception_Registry.Insert
     (Integer (28), Error_cudaErrorMixedDeviceExecution'Identity);
   Exception_Registry.Insert
     (Integer (31), Error_cudaErrorNotYetImplemented'Identity);
   Exception_Registry.Insert
     (Integer (32), Error_cudaErrorMemoryValueTooLarge'Identity);
   Exception_Registry.Insert
     (Integer (34), Error_cudaErrorStubLibrary'Identity);
   Exception_Registry.Insert
     (Integer (35), Error_cudaErrorInsufficientDriver'Identity);
   Exception_Registry.Insert
     (Integer (36), Error_cudaErrorCallRequiresNewerDriver'Identity);
   Exception_Registry.Insert
     (Integer (37), Error_cudaErrorInvalidSurface'Identity);
   Exception_Registry.Insert
     (Integer (43), Error_cudaErrorDuplicateVariableName'Identity);
   Exception_Registry.Insert
     (Integer (44), Error_cudaErrorDuplicateTextureName'Identity);
   Exception_Registry.Insert
     (Integer (45), Error_cudaErrorDuplicateSurfaceName'Identity);
   Exception_Registry.Insert
     (Integer (46), Error_cudaErrorDevicesUnavailable'Identity);
   Exception_Registry.Insert
     (Integer (49), Error_cudaErrorIncompatibleDriverContext'Identity);
   Exception_Registry.Insert
     (Integer (52), Error_cudaErrorMissingConfiguration'Identity);
   Exception_Registry.Insert
     (Integer (53), Error_cudaErrorPriorLaunchFailure'Identity);
   Exception_Registry.Insert
     (Integer (65), Error_cudaErrorLaunchMaxDepthExceeded'Identity);
   Exception_Registry.Insert
     (Integer (66), Error_cudaErrorLaunchFileScopedTex'Identity);
   Exception_Registry.Insert
     (Integer (67), Error_cudaErrorLaunchFileScopedSurf'Identity);
   Exception_Registry.Insert
     (Integer (68), Error_cudaErrorSyncDepthExceeded'Identity);
   Exception_Registry.Insert
     (Integer (69), Error_cudaErrorLaunchPendingCountExceeded'Identity);
   Exception_Registry.Insert
     (Integer (98), Error_cudaErrorInvalidDeviceFunction'Identity);
   Exception_Registry.Insert (Integer (100), Error_cudaErrorNoDevice'Identity);
   Exception_Registry.Insert
     (Integer (101), Error_cudaErrorInvalidDevice'Identity);
   Exception_Registry.Insert
     (Integer (102), Error_cudaErrorDeviceNotLicensed'Identity);
   Exception_Registry.Insert
     (Integer (103), Error_cudaErrorSoftwareValidityNotEstablished'Identity);
   Exception_Registry.Insert
     (Integer (127), Error_cudaErrorStartupFailure'Identity);
   Exception_Registry.Insert
     (Integer (200), Error_cudaErrorInvalidKernelImage'Identity);
   Exception_Registry.Insert
     (Integer (201), Error_cudaErrorDeviceUninitialized'Identity);
   Exception_Registry.Insert
     (Integer (205), Error_cudaErrorMapBufferObjectFailed'Identity);
   Exception_Registry.Insert
     (Integer (206), Error_cudaErrorUnmapBufferObjectFailed'Identity);
   Exception_Registry.Insert
     (Integer (207), Error_cudaErrorArrayIsMapped'Identity);
   Exception_Registry.Insert
     (Integer (208), Error_cudaErrorAlreadyMapped'Identity);
   Exception_Registry.Insert
     (Integer (209), Error_cudaErrorNoKernelImageForDevice'Identity);
   Exception_Registry.Insert
     (Integer (210), Error_cudaErrorAlreadyAcquired'Identity);
   Exception_Registry.Insert
     (Integer (211), Error_cudaErrorNotMapped'Identity);
   Exception_Registry.Insert
     (Integer (212), Error_cudaErrorNotMappedAsArray'Identity);
   Exception_Registry.Insert
     (Integer (213), Error_cudaErrorNotMappedAsPointer'Identity);
   Exception_Registry.Insert
     (Integer (214), Error_cudaErrorECCUncorrectable'Identity);
   Exception_Registry.Insert
     (Integer (215), Error_cudaErrorUnsupportedLimit'Identity);
   Exception_Registry.Insert
     (Integer (216), Error_cudaErrorDeviceAlreadyInUse'Identity);
   Exception_Registry.Insert
     (Integer (217), Error_cudaErrorPeerAccessUnsupported'Identity);
   Exception_Registry.Insert
     (Integer (218), Error_cudaErrorInvalidPtx'Identity);
   Exception_Registry.Insert
     (Integer (219), Error_cudaErrorInvalidGraphicsContext'Identity);
   Exception_Registry.Insert
     (Integer (220), Error_cudaErrorNvlinkUncorrectable'Identity);
   Exception_Registry.Insert
     (Integer (221), Error_cudaErrorJitCompilerNotFound'Identity);
   Exception_Registry.Insert
     (Integer (222), Error_cudaErrorUnsupportedPtxVersion'Identity);
   Exception_Registry.Insert
     (Integer (223), Error_cudaErrorJitCompilationDisabled'Identity);
   Exception_Registry.Insert
     (Integer (224), Error_cudaErrorUnsupportedExecAffinity'Identity);
   Exception_Registry.Insert
     (Integer (300), Error_cudaErrorInvalidSource'Identity);
   Exception_Registry.Insert
     (Integer (301), Error_cudaErrorFileNotFound'Identity);
   Exception_Registry.Insert
     (Integer (302), Error_cudaErrorSharedObjectSymbolNotFound'Identity);
   Exception_Registry.Insert
     (Integer (303), Error_cudaErrorSharedObjectInitFailed'Identity);
   Exception_Registry.Insert
     (Integer (304), Error_cudaErrorOperatingSystem'Identity);
   Exception_Registry.Insert
     (Integer (400), Error_cudaErrorInvalidResourceHandle'Identity);
   Exception_Registry.Insert
     (Integer (401), Error_cudaErrorIllegalState'Identity);
   Exception_Registry.Insert
     (Integer (500), Error_cudaErrorSymbolNotFound'Identity);
   Exception_Registry.Insert (Integer (600), Error_cudaErrorNotReady'Identity);
   Exception_Registry.Insert
     (Integer (700), Error_cudaErrorIllegalAddress'Identity);
   Exception_Registry.Insert
     (Integer (701), Error_cudaErrorLaunchOutOfResources'Identity);
   Exception_Registry.Insert
     (Integer (702), Error_cudaErrorLaunchTimeout'Identity);
   Exception_Registry.Insert
     (Integer (703), Error_cudaErrorLaunchIncompatibleTexturing'Identity);
   Exception_Registry.Insert
     (Integer (704), Error_cudaErrorPeerAccessAlreadyEnabled'Identity);
   Exception_Registry.Insert
     (Integer (705), Error_cudaErrorPeerAccessNotEnabled'Identity);
   Exception_Registry.Insert
     (Integer (708), Error_cudaErrorSetOnActiveProcess'Identity);
   Exception_Registry.Insert
     (Integer (709), Error_cudaErrorContextIsDestroyed'Identity);
   Exception_Registry.Insert (Integer (710), Error_cudaErrorAssert'Identity);
   Exception_Registry.Insert
     (Integer (711), Error_cudaErrorTooManyPeers'Identity);
   Exception_Registry.Insert
     (Integer (712), Error_cudaErrorHostMemoryAlreadyRegistered'Identity);
   Exception_Registry.Insert
     (Integer (713), Error_cudaErrorHostMemoryNotRegistered'Identity);
   Exception_Registry.Insert
     (Integer (714), Error_cudaErrorHardwareStackError'Identity);
   Exception_Registry.Insert
     (Integer (715), Error_cudaErrorIllegalInstruction'Identity);
   Exception_Registry.Insert
     (Integer (716), Error_cudaErrorMisalignedAddress'Identity);
   Exception_Registry.Insert
     (Integer (717), Error_cudaErrorInvalidAddressSpace'Identity);
   Exception_Registry.Insert
     (Integer (718), Error_cudaErrorInvalidPc'Identity);
   Exception_Registry.Insert
     (Integer (719), Error_cudaErrorLaunchFailure'Identity);
   Exception_Registry.Insert
     (Integer (720), Error_cudaErrorCooperativeLaunchTooLarge'Identity);
   Exception_Registry.Insert
     (Integer (800), Error_cudaErrorNotPermitted'Identity);
   Exception_Registry.Insert
     (Integer (801), Error_cudaErrorNotSupported'Identity);
   Exception_Registry.Insert
     (Integer (802), Error_cudaErrorSystemNotReady'Identity);
   Exception_Registry.Insert
     (Integer (803), Error_cudaErrorSystemDriverMismatch'Identity);
   Exception_Registry.Insert
     (Integer (804), Error_cudaErrorCompatNotSupportedOnDevice'Identity);
   Exception_Registry.Insert
     (Integer (805), Error_cudaErrorMpsConnectionFailed'Identity);
   Exception_Registry.Insert
     (Integer (806), Error_cudaErrorMpsRpcFailure'Identity);
   Exception_Registry.Insert
     (Integer (807), Error_cudaErrorMpsServerNotReady'Identity);
   Exception_Registry.Insert
     (Integer (808), Error_cudaErrorMpsMaxClientsReached'Identity);
   Exception_Registry.Insert
     (Integer (809), Error_cudaErrorMpsMaxConnectionsReached'Identity);
   Exception_Registry.Insert
     (Integer (900), Error_cudaErrorStreamCaptureUnsupported'Identity);
   Exception_Registry.Insert
     (Integer (901), Error_cudaErrorStreamCaptureInvalidated'Identity);
   Exception_Registry.Insert
     (Integer (902), Error_cudaErrorStreamCaptureMerge'Identity);
   Exception_Registry.Insert
     (Integer (903), Error_cudaErrorStreamCaptureUnmatched'Identity);
   Exception_Registry.Insert
     (Integer (904), Error_cudaErrorStreamCaptureUnjoined'Identity);
   Exception_Registry.Insert
     (Integer (905), Error_cudaErrorStreamCaptureIsolation'Identity);
   Exception_Registry.Insert
     (Integer (906), Error_cudaErrorStreamCaptureImplicit'Identity);
   Exception_Registry.Insert
     (Integer (907), Error_cudaErrorCapturedEvent'Identity);
   Exception_Registry.Insert
     (Integer (908), Error_cudaErrorStreamCaptureWrongThread'Identity);
   Exception_Registry.Insert (Integer (909), Error_cudaErrorTimeout'Identity);
   Exception_Registry.Insert
     (Integer (910), Error_cudaErrorGraphExecUpdateFailure'Identity);
   Exception_Registry.Insert
     (Integer (911), Error_cudaErrorExternalDevice'Identity);
   Exception_Registry.Insert (Integer (999), Error_cudaErrorUnknown'Identity);
   Exception_Registry.Insert
     (Integer (10_000), Error_cudaErrorApiFailureBase'Identity);

end CUDA.Exceptions;
