-- Generated from /usr/local/cuda-11.5/nvvm/libdevice/libdevice.10.bc
with Interfaces.C;
package libdevice is
   subtype i1 is Interfaces.C.C_bool;
   subtype i8 is Interfaces.C.char;
   subtype u8 is Interfaces.C.unsigned_char;
   subtype i16 is Interfaces.C.short;
   subtype u16 is Interfaces.C.unsigned_short;
   subtype i32 is Interfaces.C.int;
   subtype u32 is Interfaces.C.unsigned;
   subtype i64 is Interfaces.C.long;
   subtype u64 is Interfaces.C.unsigned_long;
   subtype double is Interfaces.C.double;
   type struct_uint2 is record
      e0 : i32;
      e1 : i32;
   end record;
   type struct_float2 is record
      e0 : Float;
      e1 : Float;
   end record;
   type struct_ulonglong2 is record
      e0 : i64;
      e1 : i64;
   end record;
   type struct_double2 is record
      e0 : double;
      e1 : double;
   end record;

-- functions
   function nv_clz (x : i32) return i32 with
     Import, External_Name => "__nv_clz";
   function nv_clzll (x : i64) return i32 with
     Import, External_Name => "__nv_clzll";
   function nv_popc (x : i32) return i32 with
     Import, External_Name => "__nv_popc";
   function nv_popcll (x : i64) return i32 with
     Import, External_Name => "__nv_popcll";
   function nv_byte_perm (a : i32; b : i32; c : i32) return i32 with
     Import, External_Name => "__nv_byte_perm";
   function llvm_nvvm_prmt (a0 : i32; a1 : i32; a2 : i32) return i32 with
     Import, External_Name => "llvm.nvvm.prmt";
   function nv_min (x : i32; y : i32) return i32 with
     Import, External_Name => "__nv_min";
   function nv_umin (x : i32; y : i32) return i32 with
     Import, External_Name => "__nv_umin";
   function nv_llmin (x : i64; y : i64) return i64 with
     Import, External_Name => "__nv_llmin";
   function nv_ullmin (x : i64; y : i64) return i64 with
     Import, External_Name => "__nv_ullmin";
   function nv_max (x : i32; y : i32) return i32 with
     Import, External_Name => "__nv_max";
   function nv_umax (x : i32; y : i32) return i32 with
     Import, External_Name => "__nv_umax";
   function nv_llmax (x : i64; y : i64) return i64 with
     Import, External_Name => "__nv_llmax";
   function nv_ullmax (x : i64; y : i64) return i64 with
     Import, External_Name => "__nv_ullmax";
   function nv_mulhi (x : i32; y : i32) return i32 with
     Import, External_Name => "__nv_mulhi";
   function llvm_nvvm_mulhi_i (a0 : i32; a1 : i32) return i32 with
     Import, External_Name => "llvm.nvvm.mulhi.i";
   function nv_umulhi (x : i32; y : i32) return i32 with
     Import, External_Name => "__nv_umulhi";
   function llvm_nvvm_mulhi_ui (a0 : i32; a1 : i32) return i32 with
     Import, External_Name => "llvm.nvvm.mulhi.ui";
   function nv_mul64hi (x : i64; y : i64) return i64 with
     Import, External_Name => "__nv_mul64hi";
   function llvm_nvvm_mulhi_ll (a0 : i64; a1 : i64) return i64 with
     Import, External_Name => "llvm.nvvm.mulhi.ll";
   function nv_umul64hi (x : i64; y : i64) return i64 with
     Import, External_Name => "__nv_umul64hi";
   function llvm_nvvm_mulhi_ull (a0 : i64; a1 : i64) return i64 with
     Import, External_Name => "llvm.nvvm.mulhi.ull";
   function nv_mul24 (x : i32; y : i32) return i32 with
     Import, External_Name => "__nv_mul24";
   function nv_umul24 (x : i32; y : i32) return i32 with
     Import, External_Name => "__nv_umul24";
   function nv_brev (x : i32) return i32 with
     Import, External_Name => "__nv_brev";
   function nv_brevll (x : i64) return i64 with
     Import, External_Name => "__nv_brevll";
   function nv_sad (x : i32; y : i32; z : i32) return i32 with
     Import, External_Name => "__nv_sad";
   function llvm_nvvm_sad_i (a0 : i32; a1 : i32; a2 : i32) return i32 with
     Import, External_Name => "llvm.nvvm.sad.i";
   function nv_usad (x : i32; y : i32; z : i32) return i32 with
     Import, External_Name => "__nv_usad";
   function llvm_nvvm_sad_ui (a0 : i32; a1 : i32; a2 : i32) return i32 with
     Import, External_Name => "llvm.nvvm.sad.ui";
   function nv_abs (x : i32) return i32 with
     Import, External_Name => "__nv_abs";
   function nv_llabs (x : i64) return i64 with
     Import, External_Name => "__nv_llabs";
   function nv_floorf (f : Float) return Float with
     Import, External_Name => "__nv_floorf";
   function nvvm_reflect (a0 : access i8) return i32 with
     Import, External_Name => "__nvvm_reflect";
   function llvm_nvvm_floor_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.floor.ftz.f";
   function llvm_nvvm_floor_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.floor.f";
   function nv_floor (f : double) return double with
     Import, External_Name => "__nv_floor";
   function llvm_nvvm_floor_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.floor.d";
   function nv_fabsf (f : Float) return Float with
     Import, External_Name => "__nv_fabsf";
   function llvm_nvvm_fabs_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fabs.ftz.f";
   function llvm_nvvm_fabs_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fabs.f";
   function nv_fabs (f : double) return double with
     Import, External_Name => "__nv_fabs";
   function llvm_nvvm_fabs_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.fabs.d";
   function nv_rcp64h (d : double) return double with
     Import, External_Name => "__nv_rcp64h";
   function llvm_nvvm_rcp_approx_ftz_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.rcp.approx.ftz.d";
   function nv_fminf (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fminf";
   function llvm_nvvm_fmin_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fmin.ftz.f";
   function llvm_nvvm_fmin_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fmin.f";
   function nv_fmaxf (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fmaxf";
   function llvm_nvvm_fmax_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fmax.ftz.f";
   function llvm_nvvm_fmax_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fmax.f";
   function nv_rsqrtf (x : Float) return Float with
     Import, External_Name => "__nv_rsqrtf";
   function llvm_nvvm_rsqrt_approx_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rsqrt.approx.ftz.f";
   function llvm_nvvm_rsqrt_approx_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rsqrt.approx.f";
   function nv_fmin (x : double; y : double) return double with
     Import, External_Name => "__nv_fmin";
   function llvm_nvvm_fmin_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.fmin.d";
   function nv_fmax (x : double; y : double) return double with
     Import, External_Name => "__nv_fmax";
   function llvm_nvvm_fmax_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.fmax.d";
   function nv_rsqrt (x : double) return double with
     Import, External_Name => "__nv_rsqrt";
   function llvm_nvvm_rsqrt_approx_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.rsqrt.approx.d";
   function nv_ceil (x : double) return double with
     Import, External_Name => "__nv_ceil";
   function llvm_nvvm_ceil_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.ceil.d";
   function nv_trunc (x : double) return double with
     Import, External_Name => "__nv_trunc";
   function llvm_nvvm_trunc_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.trunc.d";
   function nv_exp2f (x : Float) return Float with
     Import, External_Name => "__nv_exp2f";
   function llvm_nvvm_ex2_approx_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.ex2.approx.ftz.f";
   function llvm_nvvm_ex2_approx_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.ex2.approx.f";
   function nv_truncf (x : Float) return Float with
     Import, External_Name => "__nv_truncf";
   function llvm_nvvm_trunc_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.trunc.f";
   function nv_ceilf (x : Float) return Float with
     Import, External_Name => "__nv_ceilf";
   function llvm_nvvm_ceil_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.ceil.ftz.f";
   function llvm_nvvm_ceil_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.ceil.f";
   function nv_saturatef (x : Float) return Float with
     Import, External_Name => "__nv_saturatef";
   function llvm_nvvm_saturate_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.saturate.ftz.f";
   function llvm_nvvm_saturate_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.saturate.f";
   function nv_fmaf_rn (x : Float; y : Float; z : Float) return Float with
     Import, External_Name => "__nv_fmaf_rn";
   function llvm_nvvm_fma_rn_ftz_f
     (a0 : Float; a1 : Float; a2 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fma.rn.ftz.f";
   function llvm_nvvm_fma_rn_f
     (a0 : Float; a1 : Float; a2 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fma.rn.f";
   function nv_fmaf_rz (x : Float; y : Float; z : Float) return Float with
     Import, External_Name => "__nv_fmaf_rz";
   function llvm_nvvm_fma_rz_ftz_f
     (a0 : Float; a1 : Float; a2 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fma.rz.ftz.f";
   function llvm_nvvm_fma_rz_f
     (a0 : Float; a1 : Float; a2 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fma.rz.f";
   function nv_fmaf_rd (x : Float; y : Float; z : Float) return Float with
     Import, External_Name => "__nv_fmaf_rd";
   function llvm_nvvm_fma_rm_ftz_f
     (a0 : Float; a1 : Float; a2 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fma.rm.ftz.f";
   function llvm_nvvm_fma_rm_f
     (a0 : Float; a1 : Float; a2 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fma.rm.f";
   function nv_fmaf_ru (x : Float; y : Float; z : Float) return Float with
     Import, External_Name => "__nv_fmaf_ru";
   function llvm_nvvm_fma_rp_ftz_f
     (a0 : Float; a1 : Float; a2 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fma.rp.ftz.f";
   function llvm_nvvm_fma_rp_f
     (a0 : Float; a1 : Float; a2 : Float) return Float with
     Import, External_Name => "llvm.nvvm.fma.rp.f";
   function nv_fmaf_ieee_rn (x : Float; y : Float; z : Float) return Float with
     Import, External_Name => "__nv_fmaf_ieee_rn";
   function nv_fmaf_ieee_rz (x : Float; y : Float; z : Float) return Float with
     Import, External_Name => "__nv_fmaf_ieee_rz";
   function nv_fmaf_ieee_rd (x : Float; y : Float; z : Float) return Float with
     Import, External_Name => "__nv_fmaf_ieee_rd";
   function nv_fmaf_ieee_ru (x : Float; y : Float; z : Float) return Float with
     Import, External_Name => "__nv_fmaf_ieee_ru";
   function nv_fma_rn (x : double; y : double; z : double) return double with
     Import, External_Name => "__nv_fma_rn";
   function llvm_nvvm_fma_rn_d
     (a0 : double; a1 : double; a2 : double) return double with
     Import, External_Name => "llvm.nvvm.fma.rn.d";
   function nv_fma_rz (x : double; y : double; z : double) return double with
     Import, External_Name => "__nv_fma_rz";
   function llvm_nvvm_fma_rz_d
     (a0 : double; a1 : double; a2 : double) return double with
     Import, External_Name => "llvm.nvvm.fma.rz.d";
   function nv_fma_rd (x : double; y : double; z : double) return double with
     Import, External_Name => "__nv_fma_rd";
   function llvm_nvvm_fma_rm_d
     (a0 : double; a1 : double; a2 : double) return double with
     Import, External_Name => "llvm.nvvm.fma.rm.d";
   function nv_fma_ru (x : double; y : double; z : double) return double with
     Import, External_Name => "__nv_fma_ru";
   function llvm_nvvm_fma_rp_d
     (a0 : double; a1 : double; a2 : double) return double with
     Import, External_Name => "llvm.nvvm.fma.rp.d";
   function nv_fast_fdividef (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fast_fdividef";
   function llvm_nvvm_div_approx_ftz_f
     (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.approx.ftz.f";
   function llvm_nvvm_div_approx_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.approx.f";
   function nv_fdiv_rn (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fdiv_rn";
   function llvm_nvvm_div_rn_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.rn.ftz.f";
   function llvm_nvvm_div_rn_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.rn.f";
   function nv_fdiv_rz (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fdiv_rz";
   function llvm_nvvm_div_rz_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.rz.ftz.f";
   function llvm_nvvm_div_rz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.rz.f";
   function nv_fdiv_rd (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fdiv_rd";
   function llvm_nvvm_div_rm_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.rm.ftz.f";
   function llvm_nvvm_div_rm_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.rm.f";
   function nv_fdiv_ru (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fdiv_ru";
   function llvm_nvvm_div_rp_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.rp.ftz.f";
   function llvm_nvvm_div_rp_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.div.rp.f";
   function nv_frcp_rn (x : Float) return Float with
     Import, External_Name => "__nv_frcp_rn";
   function llvm_nvvm_rcp_rn_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rcp.rn.ftz.f";
   function llvm_nvvm_rcp_rn_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rcp.rn.f";
   function nv_frcp_rz (x : Float) return Float with
     Import, External_Name => "__nv_frcp_rz";
   function llvm_nvvm_rcp_rz_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rcp.rz.ftz.f";
   function llvm_nvvm_rcp_rz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rcp.rz.f";
   function nv_frcp_rd (x : Float) return Float with
     Import, External_Name => "__nv_frcp_rd";
   function llvm_nvvm_rcp_rm_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rcp.rm.ftz.f";
   function llvm_nvvm_rcp_rm_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rcp.rm.f";
   function nv_frcp_ru (x : Float) return Float with
     Import, External_Name => "__nv_frcp_ru";
   function llvm_nvvm_rcp_rp_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rcp.rp.ftz.f";
   function llvm_nvvm_rcp_rp_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.rcp.rp.f";
   function nv_fsqrt_rn (x : Float) return Float with
     Import, External_Name => "__nv_fsqrt_rn";
   function llvm_nvvm_sqrt_rn_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.rn.ftz.f";
   function llvm_nvvm_sqrt_rn_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.rn.f";
   function nv_fsqrt_rz (x : Float) return Float with
     Import, External_Name => "__nv_fsqrt_rz";
   function llvm_nvvm_sqrt_rz_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.rz.ftz.f";
   function llvm_nvvm_sqrt_rz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.rz.f";
   function nv_fsqrt_rd (x : Float) return Float with
     Import, External_Name => "__nv_fsqrt_rd";
   function llvm_nvvm_sqrt_rm_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.rm.ftz.f";
   function llvm_nvvm_sqrt_rm_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.rm.f";
   function nv_fsqrt_ru (x : Float) return Float with
     Import, External_Name => "__nv_fsqrt_ru";
   function llvm_nvvm_sqrt_rp_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.rp.ftz.f";
   function llvm_nvvm_sqrt_rp_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.rp.f";
   function nv_ddiv_rn (x : double; y : double) return double with
     Import, External_Name => "__nv_ddiv_rn";
   function llvm_nvvm_div_rn_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.div.rn.d";
   function nv_ddiv_rz (x : double; y : double) return double with
     Import, External_Name => "__nv_ddiv_rz";
   function llvm_nvvm_div_rz_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.div.rz.d";
   function nv_ddiv_rd (x : double; y : double) return double with
     Import, External_Name => "__nv_ddiv_rd";
   function llvm_nvvm_div_rm_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.div.rm.d";
   function nv_ddiv_ru (x : double; y : double) return double with
     Import, External_Name => "__nv_ddiv_ru";
   function llvm_nvvm_div_rp_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.div.rp.d";
   function nv_drcp_rn (x : double) return double with
     Import, External_Name => "__nv_drcp_rn";
   function llvm_nvvm_rcp_rn_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.rcp.rn.d";
   function nv_drcp_rz (x : double) return double with
     Import, External_Name => "__nv_drcp_rz";
   function llvm_nvvm_rcp_rz_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.rcp.rz.d";
   function nv_drcp_rd (x : double) return double with
     Import, External_Name => "__nv_drcp_rd";
   function llvm_nvvm_rcp_rm_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.rcp.rm.d";
   function nv_drcp_ru (x : double) return double with
     Import, External_Name => "__nv_drcp_ru";
   function llvm_nvvm_rcp_rp_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.rcp.rp.d";
   function nv_dsqrt_rn (x : double) return double with
     Import, External_Name => "__nv_dsqrt_rn";
   function llvm_nvvm_sqrt_rn_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.sqrt.rn.d";
   function nv_dsqrt_rz (x : double) return double with
     Import, External_Name => "__nv_dsqrt_rz";
   function llvm_nvvm_sqrt_rz_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.sqrt.rz.d";
   function nv_dsqrt_rd (x : double) return double with
     Import, External_Name => "__nv_dsqrt_rd";
   function llvm_nvvm_sqrt_rm_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.sqrt.rm.d";
   function nv_dsqrt_ru (x : double) return double with
     Import, External_Name => "__nv_dsqrt_ru";
   function llvm_nvvm_sqrt_rp_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.sqrt.rp.d";
   function nv_sqrtf (x : Float) return Float with
     Import, External_Name => "__nv_sqrtf";
   function llvm_nvvm_sqrt_approx_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.approx.ftz.f";
   function llvm_nvvm_sqrt_approx_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sqrt.approx.f";
   function nv_sqrt (x : double) return double with
     Import, External_Name => "__nv_sqrt";
   function nv_dadd_rn (x : double; y : double) return double with
     Import, External_Name => "__nv_dadd_rn";
   function llvm_nvvm_add_rn_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.add.rn.d";
   function nv_dadd_rz (x : double; y : double) return double with
     Import, External_Name => "__nv_dadd_rz";
   function llvm_nvvm_add_rz_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.add.rz.d";
   function nv_dadd_rd (x : double; y : double) return double with
     Import, External_Name => "__nv_dadd_rd";
   function llvm_nvvm_add_rm_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.add.rm.d";
   function nv_dadd_ru (x : double; y : double) return double with
     Import, External_Name => "__nv_dadd_ru";
   function llvm_nvvm_add_rp_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.add.rp.d";
   function nv_dmul_rn (x : double; y : double) return double with
     Import, External_Name => "__nv_dmul_rn";
   function llvm_nvvm_mul_rn_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.mul.rn.d";
   function nv_dmul_rz (x : double; y : double) return double with
     Import, External_Name => "__nv_dmul_rz";
   function llvm_nvvm_mul_rz_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.mul.rz.d";
   function nv_dmul_rd (x : double; y : double) return double with
     Import, External_Name => "__nv_dmul_rd";
   function llvm_nvvm_mul_rm_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.mul.rm.d";
   function nv_dmul_ru (x : double; y : double) return double with
     Import, External_Name => "__nv_dmul_ru";
   function llvm_nvvm_mul_rp_d (a0 : double; a1 : double) return double with
     Import, External_Name => "llvm.nvvm.mul.rp.d";
   function nv_fadd_rd (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fadd_rd";
   function llvm_nvvm_add_rm_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.add.rm.ftz.f";
   function llvm_nvvm_add_rm_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.add.rm.f";
   function nv_fadd_ru (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fadd_ru";
   function llvm_nvvm_add_rp_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.add.rp.ftz.f";
   function llvm_nvvm_add_rp_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.add.rp.f";
   function nv_fmul_rd (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fmul_rd";
   function llvm_nvvm_mul_rm_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.mul.rm.ftz.f";
   function llvm_nvvm_mul_rm_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.mul.rm.f";
   function nv_fmul_ru (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fmul_ru";
   function llvm_nvvm_mul_rp_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.mul.rp.ftz.f";
   function llvm_nvvm_mul_rp_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.mul.rp.f";
   function nv_fadd_rn (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fadd_rn";
   function llvm_nvvm_add_rn_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.add.rn.ftz.f";
   function llvm_nvvm_add_rn_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.add.rn.f";
   function nv_fadd_rz (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fadd_rz";
   function llvm_nvvm_add_rz_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.add.rz.ftz.f";
   function llvm_nvvm_add_rz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.add.rz.f";
   function nv_fmul_rn (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fmul_rn";
   function llvm_nvvm_mul_rn_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.mul.rn.ftz.f";
   function llvm_nvvm_mul_rn_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.mul.rn.f";
   function nv_fmul_rz (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fmul_rz";
   function llvm_nvvm_mul_rz_ftz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.mul.rz.ftz.f";
   function llvm_nvvm_mul_rz_f (a0 : Float; a1 : Float) return Float with
     Import, External_Name => "llvm.nvvm.mul.rz.f";
   function nv_double2float_rn (d : double) return Float with
     Import, External_Name => "__nv_double2float_rn";
   function llvm_nvvm_d2f_rn_ftz (a0 : double) return Float with
     Import, External_Name => "llvm.nvvm.d2f.rn.ftz";
   function llvm_nvvm_d2f_rn (a0 : double) return Float with
     Import, External_Name => "llvm.nvvm.d2f.rn";
   function nv_double2float_rz (d : double) return Float with
     Import, External_Name => "__nv_double2float_rz";
   function llvm_nvvm_d2f_rz_ftz (a0 : double) return Float with
     Import, External_Name => "llvm.nvvm.d2f.rz.ftz";
   function llvm_nvvm_d2f_rz (a0 : double) return Float with
     Import, External_Name => "llvm.nvvm.d2f.rz";
   function nv_double2float_rd (d : double) return Float with
     Import, External_Name => "__nv_double2float_rd";
   function llvm_nvvm_d2f_rm_ftz (a0 : double) return Float with
     Import, External_Name => "llvm.nvvm.d2f.rm.ftz";
   function llvm_nvvm_d2f_rm (a0 : double) return Float with
     Import, External_Name => "llvm.nvvm.d2f.rm";
   function nv_double2float_ru (d : double) return Float with
     Import, External_Name => "__nv_double2float_ru";
   function llvm_nvvm_d2f_rp_ftz (a0 : double) return Float with
     Import, External_Name => "llvm.nvvm.d2f.rp.ftz";
   function llvm_nvvm_d2f_rp (a0 : double) return Float with
     Import, External_Name => "llvm.nvvm.d2f.rp";
   function nv_double2int_rn (d : double) return i32 with
     Import, External_Name => "__nv_double2int_rn";
   function llvm_nvvm_d2i_rn (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2i.rn";
   function nv_double2int_rz (d : double) return i32 with
     Import, External_Name => "__nv_double2int_rz";
   function llvm_nvvm_d2i_rz (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2i.rz";
   function nv_double2int_rd (d : double) return i32 with
     Import, External_Name => "__nv_double2int_rd";
   function llvm_nvvm_d2i_rm (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2i.rm";
   function nv_double2int_ru (d : double) return i32 with
     Import, External_Name => "__nv_double2int_ru";
   function llvm_nvvm_d2i_rp (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2i.rp";
   function nv_double2uint_rn (d : double) return i32 with
     Import, External_Name => "__nv_double2uint_rn";
   function llvm_nvvm_d2ui_rn (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2ui.rn";
   function nv_double2uint_rz (d : double) return i32 with
     Import, External_Name => "__nv_double2uint_rz";
   function llvm_nvvm_d2ui_rz (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2ui.rz";
   function nv_double2uint_rd (d : double) return i32 with
     Import, External_Name => "__nv_double2uint_rd";
   function llvm_nvvm_d2ui_rm (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2ui.rm";
   function nv_double2uint_ru (d : double) return i32 with
     Import, External_Name => "__nv_double2uint_ru";
   function llvm_nvvm_d2ui_rp (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2ui.rp";
   function nv_int2double_rn (i : i32) return double with
     Import, External_Name => "__nv_int2double_rn";
   function llvm_nvvm_i2d_rn (a0 : i32) return double with
     Import, External_Name => "llvm.nvvm.i2d.rn";
   function nv_uint2double_rn (i : i32) return double with
     Import, External_Name => "__nv_uint2double_rn";
   function llvm_nvvm_ui2d_rn (a0 : i32) return double with
     Import, External_Name => "llvm.nvvm.ui2d.rn";
   function nv_float2int_rn (inn : Float) return i32 with
     Import, External_Name => "__nv_float2int_rn";
   function llvm_nvvm_f2i_rn_ftz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2i.rn.ftz";
   function llvm_nvvm_f2i_rn (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2i.rn";
   function nv_float2int_rz (inn : Float) return i32 with
     Import, External_Name => "__nv_float2int_rz";
   function llvm_nvvm_f2i_rz_ftz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2i.rz.ftz";
   function llvm_nvvm_f2i_rz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2i.rz";
   function nv_float2int_rd (inn : Float) return i32 with
     Import, External_Name => "__nv_float2int_rd";
   function llvm_nvvm_f2i_rm_ftz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2i.rm.ftz";
   function llvm_nvvm_f2i_rm (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2i.rm";
   function nv_float2int_ru (inn : Float) return i32 with
     Import, External_Name => "__nv_float2int_ru";
   function llvm_nvvm_f2i_rp_ftz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2i.rp.ftz";
   function llvm_nvvm_f2i_rp (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2i.rp";
   function nv_float2uint_rn (inn : Float) return i32 with
     Import, External_Name => "__nv_float2uint_rn";
   function llvm_nvvm_f2ui_rn_ftz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2ui.rn.ftz";
   function llvm_nvvm_f2ui_rn (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2ui.rn";
   function nv_float2uint_rz (inn : Float) return i32 with
     Import, External_Name => "__nv_float2uint_rz";
   function llvm_nvvm_f2ui_rz_ftz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2ui.rz.ftz";
   function llvm_nvvm_f2ui_rz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2ui.rz";
   function nv_float2uint_rd (inn : Float) return i32 with
     Import, External_Name => "__nv_float2uint_rd";
   function llvm_nvvm_f2ui_rm_ftz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2ui.rm.ftz";
   function llvm_nvvm_f2ui_rm (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2ui.rm";
   function nv_float2uint_ru (inn : Float) return i32 with
     Import, External_Name => "__nv_float2uint_ru";
   function llvm_nvvm_f2ui_rp_ftz (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2ui.rp.ftz";
   function llvm_nvvm_f2ui_rp (a0 : Float) return i32 with
     Import, External_Name => "llvm.nvvm.f2ui.rp";
   function nv_int2float_rn (inn : i32) return Float with
     Import, External_Name => "__nv_int2float_rn";
   function llvm_nvvm_i2f_rn (a0 : i32) return Float with
     Import, External_Name => "llvm.nvvm.i2f.rn";
   function nv_int2float_rz (inn : i32) return Float with
     Import, External_Name => "__nv_int2float_rz";
   function llvm_nvvm_i2f_rz (a0 : i32) return Float with
     Import, External_Name => "llvm.nvvm.i2f.rz";
   function nv_int2float_rd (inn : i32) return Float with
     Import, External_Name => "__nv_int2float_rd";
   function llvm_nvvm_i2f_rm (a0 : i32) return Float with
     Import, External_Name => "llvm.nvvm.i2f.rm";
   function nv_int2float_ru (inn : i32) return Float with
     Import, External_Name => "__nv_int2float_ru";
   function llvm_nvvm_i2f_rp (a0 : i32) return Float with
     Import, External_Name => "llvm.nvvm.i2f.rp";
   function nv_uint2float_rn (inn : i32) return Float with
     Import, External_Name => "__nv_uint2float_rn";
   function llvm_nvvm_ui2f_rn (a0 : i32) return Float with
     Import, External_Name => "llvm.nvvm.ui2f.rn";
   function nv_uint2float_rz (inn : i32) return Float with
     Import, External_Name => "__nv_uint2float_rz";
   function llvm_nvvm_ui2f_rz (a0 : i32) return Float with
     Import, External_Name => "llvm.nvvm.ui2f.rz";
   function nv_uint2float_rd (inn : i32) return Float with
     Import, External_Name => "__nv_uint2float_rd";
   function llvm_nvvm_ui2f_rm (a0 : i32) return Float with
     Import, External_Name => "llvm.nvvm.ui2f.rm";
   function nv_uint2float_ru (inn : i32) return Float with
     Import, External_Name => "__nv_uint2float_ru";
   function llvm_nvvm_ui2f_rp (a0 : i32) return Float with
     Import, External_Name => "llvm.nvvm.ui2f.rp";
   function nv_hiloint2double (a : i32; b : i32) return double with
     Import, External_Name => "__nv_hiloint2double";
   function llvm_nvvm_lohi_i2d (a0 : i32; a1 : i32) return double with
     Import, External_Name => "llvm.nvvm.lohi.i2d";
   function nv_double2loint (d : double) return i32 with
     Import, External_Name => "__nv_double2loint";
   function llvm_nvvm_d2i_lo (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2i.lo";
   function nv_double2hiint (d : double) return i32 with
     Import, External_Name => "__nv_double2hiint";
   function llvm_nvvm_d2i_hi (a0 : double) return i32 with
     Import, External_Name => "llvm.nvvm.d2i.hi";
   function nv_float2ll_rn (f : Float) return i64 with
     Import, External_Name => "__nv_float2ll_rn";
   function llvm_nvvm_f2ll_rn_ftz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ll.rn.ftz";
   function llvm_nvvm_f2ll_rn (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ll.rn";
   function nv_float2ll_rz (f : Float) return i64 with
     Import, External_Name => "__nv_float2ll_rz";
   function llvm_nvvm_f2ll_rz_ftz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ll.rz.ftz";
   function llvm_nvvm_f2ll_rz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ll.rz";
   function nv_float2ll_rd (f : Float) return i64 with
     Import, External_Name => "__nv_float2ll_rd";
   function llvm_nvvm_f2ll_rm_ftz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ll.rm.ftz";
   function llvm_nvvm_f2ll_rm (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ll.rm";
   function nv_float2ll_ru (f : Float) return i64 with
     Import, External_Name => "__nv_float2ll_ru";
   function llvm_nvvm_f2ll_rp_ftz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ll.rp.ftz";
   function llvm_nvvm_f2ll_rp (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ll.rp";
   function nv_float2ull_rn (f : Float) return i64 with
     Import, External_Name => "__nv_float2ull_rn";
   function llvm_nvvm_f2ull_rn_ftz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ull.rn.ftz";
   function llvm_nvvm_f2ull_rn (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ull.rn";
   function nv_float2ull_rz (f : Float) return i64 with
     Import, External_Name => "__nv_float2ull_rz";
   function llvm_nvvm_f2ull_rz_ftz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ull.rz.ftz";
   function llvm_nvvm_f2ull_rz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ull.rz";
   function nv_float2ull_rd (f : Float) return i64 with
     Import, External_Name => "__nv_float2ull_rd";
   function llvm_nvvm_f2ull_rm_ftz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ull.rm.ftz";
   function llvm_nvvm_f2ull_rm (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ull.rm";
   function nv_float2ull_ru (f : Float) return i64 with
     Import, External_Name => "__nv_float2ull_ru";
   function llvm_nvvm_f2ull_rp_ftz (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ull.rp.ftz";
   function llvm_nvvm_f2ull_rp (a0 : Float) return i64 with
     Import, External_Name => "llvm.nvvm.f2ull.rp";
   function nv_double2ll_rn (f : double) return i64 with
     Import, External_Name => "__nv_double2ll_rn";
   function llvm_nvvm_d2ll_rn (a0 : double) return i64 with
     Import, External_Name => "llvm.nvvm.d2ll.rn";
   function nv_double2ll_rz (f : double) return i64 with
     Import, External_Name => "__nv_double2ll_rz";
   function llvm_nvvm_d2ll_rz (a0 : double) return i64 with
     Import, External_Name => "llvm.nvvm.d2ll.rz";
   function nv_double2ll_rd (f : double) return i64 with
     Import, External_Name => "__nv_double2ll_rd";
   function llvm_nvvm_d2ll_rm (a0 : double) return i64 with
     Import, External_Name => "llvm.nvvm.d2ll.rm";
   function nv_double2ll_ru (f : double) return i64 with
     Import, External_Name => "__nv_double2ll_ru";
   function llvm_nvvm_d2ll_rp (a0 : double) return i64 with
     Import, External_Name => "llvm.nvvm.d2ll.rp";
   function nv_double2ull_rn (f : double) return i64 with
     Import, External_Name => "__nv_double2ull_rn";
   function llvm_nvvm_d2ull_rn (a0 : double) return i64 with
     Import, External_Name => "llvm.nvvm.d2ull.rn";
   function nv_double2ull_rz (f : double) return i64 with
     Import, External_Name => "__nv_double2ull_rz";
   function llvm_nvvm_d2ull_rz (a0 : double) return i64 with
     Import, External_Name => "llvm.nvvm.d2ull.rz";
   function nv_double2ull_rd (f : double) return i64 with
     Import, External_Name => "__nv_double2ull_rd";
   function llvm_nvvm_d2ull_rm (a0 : double) return i64 with
     Import, External_Name => "llvm.nvvm.d2ull.rm";
   function nv_double2ull_ru (f : double) return i64 with
     Import, External_Name => "__nv_double2ull_ru";
   function llvm_nvvm_d2ull_rp (a0 : double) return i64 with
     Import, External_Name => "llvm.nvvm.d2ull.rp";
   function nv_ll2float_rn (l : i64) return Float with
     Import, External_Name => "__nv_ll2float_rn";
   function llvm_nvvm_ll2f_rn (a0 : i64) return Float with
     Import, External_Name => "llvm.nvvm.ll2f.rn";
   function nv_ll2float_rz (l : i64) return Float with
     Import, External_Name => "__nv_ll2float_rz";
   function llvm_nvvm_ll2f_rz (a0 : i64) return Float with
     Import, External_Name => "llvm.nvvm.ll2f.rz";
   function nv_ll2float_rd (l : i64) return Float with
     Import, External_Name => "__nv_ll2float_rd";
   function llvm_nvvm_ll2f_rm (a0 : i64) return Float with
     Import, External_Name => "llvm.nvvm.ll2f.rm";
   function nv_ll2float_ru (l : i64) return Float with
     Import, External_Name => "__nv_ll2float_ru";
   function llvm_nvvm_ll2f_rp (a0 : i64) return Float with
     Import, External_Name => "llvm.nvvm.ll2f.rp";
   function nv_ull2float_rn (l : i64) return Float with
     Import, External_Name => "__nv_ull2float_rn";
   function llvm_nvvm_ull2f_rn (a0 : i64) return Float with
     Import, External_Name => "llvm.nvvm.ull2f.rn";
   function nv_ull2float_rz (l : i64) return Float with
     Import, External_Name => "__nv_ull2float_rz";
   function llvm_nvvm_ull2f_rz (a0 : i64) return Float with
     Import, External_Name => "llvm.nvvm.ull2f.rz";
   function nv_ull2float_rd (l : i64) return Float with
     Import, External_Name => "__nv_ull2float_rd";
   function llvm_nvvm_ull2f_rm (a0 : i64) return Float with
     Import, External_Name => "llvm.nvvm.ull2f.rm";
   function nv_ull2float_ru (l : i64) return Float with
     Import, External_Name => "__nv_ull2float_ru";
   function llvm_nvvm_ull2f_rp (a0 : i64) return Float with
     Import, External_Name => "llvm.nvvm.ull2f.rp";
   function nv_ll2double_rn (l : i64) return double with
     Import, External_Name => "__nv_ll2double_rn";
   function llvm_nvvm_ll2d_rn (a0 : i64) return double with
     Import, External_Name => "llvm.nvvm.ll2d.rn";
   function nv_ll2double_rz (l : i64) return double with
     Import, External_Name => "__nv_ll2double_rz";
   function llvm_nvvm_ll2d_rz (a0 : i64) return double with
     Import, External_Name => "llvm.nvvm.ll2d.rz";
   function nv_ll2double_rd (l : i64) return double with
     Import, External_Name => "__nv_ll2double_rd";
   function llvm_nvvm_ll2d_rm (a0 : i64) return double with
     Import, External_Name => "llvm.nvvm.ll2d.rm";
   function nv_ll2double_ru (l : i64) return double with
     Import, External_Name => "__nv_ll2double_ru";
   function llvm_nvvm_ll2d_rp (a0 : i64) return double with
     Import, External_Name => "llvm.nvvm.ll2d.rp";
   function nv_ull2double_rn (l : i64) return double with
     Import, External_Name => "__nv_ull2double_rn";
   function llvm_nvvm_ull2d_rn (a0 : i64) return double with
     Import, External_Name => "llvm.nvvm.ull2d.rn";
   function nv_ull2double_rz (l : i64) return double with
     Import, External_Name => "__nv_ull2double_rz";
   function llvm_nvvm_ull2d_rz (a0 : i64) return double with
     Import, External_Name => "llvm.nvvm.ull2d.rz";
   function nv_ull2double_rd (l : i64) return double with
     Import, External_Name => "__nv_ull2double_rd";
   function llvm_nvvm_ull2d_rm (a0 : i64) return double with
     Import, External_Name => "llvm.nvvm.ull2d.rm";
   function nv_ull2double_ru (l : i64) return double with
     Import, External_Name => "__nv_ull2double_ru";
   function llvm_nvvm_ull2d_rp (a0 : i64) return double with
     Import, External_Name => "llvm.nvvm.ull2d.rp";
   function nv_float2half_rn (f : Float) return i16 with
     Import, External_Name => "__nv_float2half_rn";
   function llvm_nvvm_f2h_rn_ftz (a0 : Float) return i16 with
     Import, External_Name => "llvm.nvvm.f2h.rn.ftz";
   function llvm_nvvm_f2h_rn (a0 : Float) return i16 with
     Import, External_Name => "llvm.nvvm.f2h.rn";
   function nv_half2float (h : i16) return Float with
     Import, External_Name => "__nv_half2float";
   function nv_int_as_float (x : i32) return Float with
     Import, External_Name => "__nv_int_as_float";
   function nv_float_as_int (x : Float) return i32 with
     Import, External_Name => "__nv_float_as_int";
   function nv_uint_as_float (x : i32) return Float with
     Import, External_Name => "__nv_uint_as_float";
   function nv_float_as_uint (x : Float) return i32 with
     Import, External_Name => "__nv_float_as_uint";
   function nv_longlong_as_double (x : i64) return double with
     Import, External_Name => "__nv_longlong_as_double";
   function nv_double_as_longlong (x : double) return i64 with
     Import, External_Name => "__nv_double_as_longlong";
   function nv_fast_sinf (a : Float) return Float with
     Import, External_Name => "__nv_fast_sinf";
   function llvm_nvvm_sin_approx_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sin.approx.ftz.f";
   function llvm_nvvm_sin_approx_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.sin.approx.f";
   function nv_fast_cosf (a : Float) return Float with
     Import, External_Name => "__nv_fast_cosf";
   function llvm_nvvm_cos_approx_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.cos.approx.ftz.f";
   function llvm_nvvm_cos_approx_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.cos.approx.f";
   function nv_fast_log2f (a : Float) return Float with
     Import, External_Name => "__nv_fast_log2f";
   function llvm_nvvm_lg2_approx_ftz_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.lg2.approx.ftz.f";
   function llvm_nvvm_lg2_approx_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.lg2.approx.f";
   function nv_fast_logf (a : Float) return Float with
     Import, External_Name => "__nv_fast_logf";
   function nv_fast_expf (a : Float) return Float with
     Import, External_Name => "__nv_fast_expf";
   function nv_fast_tanf (a : Float) return Float with
     Import, External_Name => "__nv_fast_tanf";
   procedure nv_fast_sincosf
     (a : Float; sptr : access Float; cptr : access Float) with
     Import, External_Name => "__nv_fast_sincosf";
   function nv_fast_exp10f (a : Float) return Float with
     Import, External_Name => "__nv_fast_exp10f";
   function nv_fast_log10f (a : Float) return Float with
     Import, External_Name => "__nv_fast_log10f";
   function nv_fast_powf (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_fast_powf";
   function nv_hadd (a : i32; b : i32) return i32 with
     Import, External_Name => "__nv_hadd";
   function nv_rhadd (a : i32; b : i32) return i32 with
     Import, External_Name => "__nv_rhadd";
   function nv_uhadd (a : i32; b : i32) return i32 with
     Import, External_Name => "__nv_uhadd";
   function nv_urhadd (a : i32; b : i32) return i32 with
     Import, External_Name => "__nv_urhadd";
   function nv_fsub_rn (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_fsub_rn";
   function nv_fsub_rz (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_fsub_rz";
   function nv_fsub_rd (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_fsub_rd";
   function nv_fsub_ru (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_fsub_ru";
   function nv_frsqrt_rn (a : Float) return Float with
     Import, External_Name => "__nv_frsqrt_rn";
   function nv_ffs (v : i32) return i32 with
     Import, External_Name => "__nv_ffs";
   function nv_ffsll (a : i64) return i32 with
     Import, External_Name => "__nv_ffsll";
   function nv_rintf (a : Float) return Float with
     Import, External_Name => "__nv_rintf";
   function llvm_nvvm_round_f (a0 : Float) return Float with
     Import, External_Name => "llvm.nvvm.round.f";
   function nv_llrintf (a : Float) return i64 with
     Import, External_Name => "__nv_llrintf";
   function nv_nearbyintf (a : Float) return Float with
     Import, External_Name => "__nv_nearbyintf";
   function nv_isnanf (a : Float) return i32 with
     Import, External_Name => "__nv_isnanf";
   function nv_signbitf (a : Float) return i32 with
     Import, External_Name => "__nv_signbitf";
   function nv_copysignf (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_copysignf";
   function nv_finitef (a : Float) return i32 with
     Import, External_Name => "__nv_finitef";
   function nv_isinff (a : Float) return i32 with
     Import, External_Name => "__nv_isinff";
   function nv_nextafterf (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_nextafterf";
   function nv_nanf (tagp : access i8) return Float with
     Import, External_Name => "__nv_nanf";
   function nv_sinf (a : Float) return Float with
     Import, External_Name => "__nv_sinf";
   function nv_cosf (a : Float) return Float with
     Import, External_Name => "__nv_cosf";
   procedure nv_sincosf
     (a : Float; sptr : access Float; cptr : access Float) with
     Import, External_Name => "__nv_sincosf";
   function nv_sinpif (a : Float) return Float with
     Import, External_Name => "__nv_sinpif";
   function nv_cospif (a : Float) return Float with
     Import, External_Name => "__nv_cospif";
   procedure nv_sincospif
     (a : Float; sptr : access Float; cptr : access Float) with
     Import, External_Name => "__nv_sincospif";
   function nv_tanf (a : Float) return Float with
     Import, External_Name => "__nv_tanf";
   function nv_log2f (a : Float) return Float with
     Import, External_Name => "__nv_log2f";
   function nv_expf (a : Float) return Float with
     Import, External_Name => "__nv_expf";
   function nv_exp10f (a : Float) return Float with
     Import, External_Name => "__nv_exp10f";
   function nv_coshf (a : Float) return Float with
     Import, External_Name => "__nv_coshf";
   function nv_sinhf (a : Float) return Float with
     Import, External_Name => "__nv_sinhf";
   function nv_tanhf (a : Float) return Float with
     Import, External_Name => "__nv_tanhf";
   function nv_atan2f (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_atan2f";
   function nv_atanf (a : Float) return Float with
     Import, External_Name => "__nv_atanf";
   function nv_asinf (a : Float) return Float with
     Import, External_Name => "__nv_asinf";
   function nv_acosf (a : Float) return Float with
     Import, External_Name => "__nv_acosf";
   function nv_logf (a : Float) return Float with
     Import, External_Name => "__nv_logf";
   function nv_log10f (a : Float) return Float with
     Import, External_Name => "__nv_log10f";
   function nv_log1pf (a : Float) return Float with
     Import, External_Name => "__nv_log1pf";
   function nv_acoshf (a : Float) return Float with
     Import, External_Name => "__nv_acoshf";
   function nv_asinhf (a : Float) return Float with
     Import, External_Name => "__nv_asinhf";
   function nv_atanhf (a : Float) return Float with
     Import, External_Name => "__nv_atanhf";
   function nv_expm1f (a : Float) return Float with
     Import, External_Name => "__nv_expm1f";
   function nv_hypotf (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_hypotf";
   function nv_rhypotf (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_rhypotf";
   function nv_norm3df (a : Float; b : Float; c : Float) return Float with
     Import, External_Name => "__nv_norm3df";
   function nv_rnorm3df (a : Float; b : Float; c : Float) return Float with
     Import, External_Name => "__nv_rnorm3df";
   function nv_norm4df
     (a : Float; b : Float; c : Float; d : Float) return Float with
     Import, External_Name => "__nv_norm4df";
   function nv_rnorm4df
     (a : Float; b : Float; c : Float; d : Float) return Float with
     Import, External_Name => "__nv_rnorm4df";
   function nv_normf (dim : i32; t : access Float) return Float with
     Import, External_Name => "__nv_normf";
   function nv_rnormf (dim : i32; t : access Float) return Float with
     Import, External_Name => "__nv_rnormf";
   function nv_cbrtf (a : Float) return Float with
     Import, External_Name => "__nv_cbrtf";
   function nv_rcbrtf (a : Float) return Float with
     Import, External_Name => "__nv_rcbrtf";
   function nv_j0f (a : Float) return Float with
     Import, External_Name => "__nv_j0f";
   function nv_j1f (a : Float) return Float with
     Import, External_Name => "__nv_j1f";
   function nv_y0f (a : Float) return Float with
     Import, External_Name => "__nv_y0f";
   function nv_y1f (a : Float) return Float with
     Import, External_Name => "__nv_y1f";
   function nv_ynf (n : i32; a : Float) return Float with
     Import, External_Name => "__nv_ynf";
   function nv_jnf (n : i32; a : Float) return Float with
     Import, External_Name => "__nv_jnf";
   function nv_cyl_bessel_i0f (a : Float) return Float with
     Import, External_Name => "__nv_cyl_bessel_i0f";
   function nv_cyl_bessel_i1f (a : Float) return Float with
     Import, External_Name => "__nv_cyl_bessel_i1f";
   function nv_erff (a : Float) return Float with
     Import, External_Name => "__nv_erff";
   function nv_erfinvf (a : Float) return Float with
     Import, External_Name => "__nv_erfinvf";
   function nv_erfcf (a : Float) return Float with
     Import, External_Name => "__nv_erfcf";
   function nv_erfcxf (a : Float) return Float with
     Import, External_Name => "__nv_erfcxf";
   function nv_erfcinvf (a : Float) return Float with
     Import, External_Name => "__nv_erfcinvf";
   function nv_normcdfinvf (a : Float) return Float with
     Import, External_Name => "__nv_normcdfinvf";
   function nv_normcdff (a : Float) return Float with
     Import, External_Name => "__nv_normcdff";
   function nv_lgammaf (a : Float) return Float with
     Import, External_Name => "__nv_lgammaf";
   function nv_ldexpf (a : Float; b : i32) return Float with
     Import, External_Name => "__nv_ldexpf";
   function nv_scalbnf (a : Float; b : i32) return Float with
     Import, External_Name => "__nv_scalbnf";
   function nv_frexpf (a : Float; b : access i32) return Float with
     Import, External_Name => "__nv_frexpf";
   function nv_modff (a : Float; b : access Float) return Float with
     Import, External_Name => "__nv_modff";
   function nv_fmodf (x : Float; y : Float) return Float with
     Import, External_Name => "__nv_fmodf";
   function nv_remainderf (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_remainderf";
   function nv_remquof
     (a : Float; b : Float; quo : access i32) return Float with
     Import, External_Name => "__nv_remquof";
   function nv_fmaf (a : Float; b : Float; c : Float) return Float with
     Import, External_Name => "__nv_fmaf";
   function nv_powif (a : Float; b : i32) return Float with
     Import, External_Name => "__nv_powif";
   function nv_powi (a : double; b : i32) return double with
     Import, External_Name => "__nv_powi";
   function nv_powf (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_powf";
   function nv_tgammaf (a : Float) return Float with
     Import, External_Name => "__nv_tgammaf";
   function nv_roundf (a : Float) return Float with
     Import, External_Name => "__nv_roundf";
   function nv_llroundf (a : Float) return i64 with
     Import, External_Name => "__nv_llroundf";
   function nv_fdimf (a : Float; b : Float) return Float with
     Import, External_Name => "__nv_fdimf";
   function nv_ilogbf (a : Float) return i32 with
     Import, External_Name => "__nv_ilogbf";
   function nv_logbf (a : Float) return Float with
     Import, External_Name => "__nv_logbf";
   function nv_rint (a : double) return double with
     Import, External_Name => "__nv_rint";
   function llvm_nvvm_round_d (a0 : double) return double with
     Import, External_Name => "llvm.nvvm.round.d";
   function nv_llrint (a : double) return i64 with
     Import, External_Name => "__nv_llrint";
   function nv_nearbyint (a : double) return double with
     Import, External_Name => "__nv_nearbyint";
   function nv_signbitd (a : double) return i32 with
     Import, External_Name => "__nv_signbitd";
   function nv_isfinited (a : double) return i32 with
     Import, External_Name => "__nv_isfinited";
   function nv_isinfd (a : double) return i32 with
     Import, External_Name => "__nv_isinfd";
   function nv_isnand (a : double) return i32 with
     Import, External_Name => "__nv_isnand";
   function nv_copysign (a : double; b : double) return double with
     Import, External_Name => "__nv_copysign";
   procedure nv_sincos
     (a : double; sptr : access double; cptr : access double) with
     Import, External_Name => "__nv_sincos";
   function internal_trig_reduction_slowpathd
     (a : double; quadrant : access i32) return double with
     Import, External_Name => "__internal_trig_reduction_slowpathd";
   procedure nv_sincospi
     (a : double; sptr : access double; cptr : access double) with
     Import, External_Name => "__nv_sincospi";
   function nv_sin (a : double) return double with
     Import, External_Name => "__nv_sin";
   function nv_cos (a : double) return double with
     Import, External_Name => "__nv_cos";
   function nv_sinpi (a : double) return double with
     Import, External_Name => "__nv_sinpi";
   function nv_cospi (a : double) return double with
     Import, External_Name => "__nv_cospi";
   function nv_tan (a : double) return double with
     Import, External_Name => "__nv_tan";
   function nv_log (a : double) return double with
     Import, External_Name => "__nv_log";
   function nv_log2 (a : double) return double with
     Import, External_Name => "__nv_log2";
   function nv_log10 (a : double) return double with
     Import, External_Name => "__nv_log10";
   function nv_log1p (a : double) return double with
     Import, External_Name => "__nv_log1p";
   function nv_exp (a : double) return double with
     Import, External_Name => "__nv_exp";
   function nv_exp2 (a : double) return double with
     Import, External_Name => "__nv_exp2";
   function nv_exp10 (a : double) return double with
     Import, External_Name => "__nv_exp10";
   function nv_expm1 (a : double) return double with
     Import, External_Name => "__nv_expm1";
   function nv_cosh (a : double) return double with
     Import, External_Name => "__nv_cosh";
   function nv_sinh (a : double) return double with
     Import, External_Name => "__nv_sinh";
   function nv_tanh (a : double) return double with
     Import, External_Name => "__nv_tanh";
   function nv_atan2 (a : double; b : double) return double with
     Import, External_Name => "__nv_atan2";
   function nv_atan (a : double) return double with
     Import, External_Name => "__nv_atan";
   function nv_asin (a : double) return double with
     Import, External_Name => "__nv_asin";
   function nv_acos (a : double) return double with
     Import, External_Name => "__nv_acos";
   function nv_acosh (a : double) return double with
     Import, External_Name => "__nv_acosh";
   function nv_asinh (a : double) return double with
     Import, External_Name => "__nv_asinh";
   function nv_atanh (a : double) return double with
     Import, External_Name => "__nv_atanh";
   function nv_hypot (a : double; b : double) return double with
     Import, External_Name => "__nv_hypot";
   function nv_rhypot (a : double; b : double) return double with
     Import, External_Name => "__nv_rhypot";
   function nv_norm3d (a : double; b : double; c : double) return double with
     Import, External_Name => "__nv_norm3d";
   function nv_rnorm3d (a : double; b : double; c : double) return double with
     Import, External_Name => "__nv_rnorm3d";
   function nv_norm4d
     (a : double; b : double; c : double; d : double) return double with
     Import, External_Name => "__nv_norm4d";
   function nv_rnorm4d
     (a : double; b : double; c : double; d : double) return double with
     Import, External_Name => "__nv_rnorm4d";
   function nv_norm (dim : i32; t : access double) return double with
     Import, External_Name => "__nv_norm";
   function nv_rnorm (dim : i32; t : access double) return double with
     Import, External_Name => "__nv_rnorm";
   function nv_cbrt (a : double) return double with
     Import, External_Name => "__nv_cbrt";
   function nv_rcbrt (a : double) return double with
     Import, External_Name => "__nv_rcbrt";
   function nv_pow (a : double; b : double) return double with
     Import, External_Name => "__nv_pow";
   function internal_accurate_pow (a : double; b : double) return double with
     Import, External_Name => "__internal_accurate_pow";
   function nv_j0 (a : double) return double with
     Import, External_Name => "__nv_j0";
   function nv_j1 (a : double) return double with
     Import, External_Name => "__nv_j1";
   function nv_y0 (a : double) return double with
     Import, External_Name => "__nv_y0";
   function nv_y1 (a : double) return double with
     Import, External_Name => "__nv_y1";
   function nv_yn (n : i32; a : double) return double with
     Import, External_Name => "__nv_yn";
   function nv_jn (n : i32; a : double) return double with
     Import, External_Name => "__nv_jn";
   function nv_cyl_bessel_i0 (a : double) return double with
     Import, External_Name => "__nv_cyl_bessel_i0";
   function nv_cyl_bessel_i1 (a : double) return double with
     Import, External_Name => "__nv_cyl_bessel_i1";
   function nv_erf (a : double) return double with
     Import, External_Name => "__nv_erf";
   function nv_erfinv (a : double) return double with
     Import, External_Name => "__nv_erfinv";
   function nv_erfcinv (a : double) return double with
     Import, External_Name => "__nv_erfcinv";
   function nv_normcdfinv (a : double) return double with
     Import, External_Name => "__nv_normcdfinv";
   function nv_erfc (a : double) return double with
     Import, External_Name => "__nv_erfc";
   function nv_erfcx (a : double) return double with
     Import, External_Name => "__nv_erfcx";
   function nv_normcdf (a : double) return double with
     Import, External_Name => "__nv_normcdf";
   function nv_tgamma (a : double) return double with
     Import, External_Name => "__nv_tgamma";
   function nv_lgamma (a : double) return double with
     Import, External_Name => "__nv_lgamma";
   function internal_lgamma_pos (a : double) return double with
     Import, External_Name => "__internal_lgamma_pos";
   function nv_ldexp (a : double; b : i32) return double with
     Import, External_Name => "__nv_ldexp";
   function nv_scalbn (a : double; b : i32) return double with
     Import, External_Name => "__nv_scalbn";
   function nv_frexp (a : double; b : access i32) return double with
     Import, External_Name => "__nv_frexp";
   function nv_modf (a : double; b : access double) return double with
     Import, External_Name => "__nv_modf";
   function nv_fmod (a : double; b : double) return double with
     Import, External_Name => "__nv_fmod";
   function nv_remainder (a : double; b : double) return double with
     Import, External_Name => "__nv_remainder";
   function nv_remquo
     (a : double; b : double; c : access i32) return double with
     Import, External_Name => "__nv_remquo";
   function nv_nextafter (a : double; b : double) return double with
     Import, External_Name => "__nv_nextafter";
   function nv_nan (tagp : access i8) return double with
     Import, External_Name => "__nv_nan";
   function nv_round (a : double) return double with
     Import, External_Name => "__nv_round";
   function nv_llround (a : double) return i64 with
     Import, External_Name => "__nv_llround";
   function nv_fdim (a : double; b : double) return double with
     Import, External_Name => "__nv_fdim";
   function nv_ilogb (a : double) return i32 with
     Import, External_Name => "__nv_ilogb";
   function nv_logb (a : double) return double with
     Import, External_Name => "__nv_logb";
   function nv_fma (a : double; b : double; c : double) return double with
     Import, External_Name => "__nv_fma";
   function nv_dsub_rn (a : double; b : double) return double with
     Import, External_Name => "__nv_dsub_rn";
   function nv_dsub_rz (a : double; b : double) return double with
     Import, External_Name => "__nv_dsub_rz";
   function nv_dsub_ru (a : double; b : double) return double with
     Import, External_Name => "__nv_dsub_ru";
   function nv_dsub_rd (a : double; b : double) return double with
     Import, External_Name => "__nv_dsub_rd";
   function llvm_ctlz_i32 (a0 : i32; a1 : i1) return i32 with
     Import, External_Name => "llvm.ctlz.i32";
   function llvm_ctpop_i32 (a0 : i32) return i32 with
     Import, External_Name => "llvm.ctpop.i32";
   function llvm_bitreverse_i32 (a0 : i32) return i32 with
     Import, External_Name => "llvm.bitreverse.i32";
   function llvm_bitreverse_i64 (a0 : i64) return i64 with
     Import, External_Name => "llvm.bitreverse.i64";
   procedure llvm_memcpy_p0i8_p0i8_i64
     (a0 : access i8; a1 : access i8; a2 : i64; a3 : i1) with
     Import, External_Name => "llvm.memcpy.p0i8.p0i8.i64";
   function llvm_ctlz_i64 (a0 : i64; a1 : i1) return i64 with
     Import, External_Name => "llvm.ctlz.i64";
   function llvm_ctpop_i64 (a0 : i64) return i64 with
     Import, External_Name => "llvm.ctpop.i64";
   function llvm_convert_from_fp16_f32 (a0 : i16) return Float with
     Import, External_Name => "llvm.convert.from.fp16.f32";
end libdevice;
