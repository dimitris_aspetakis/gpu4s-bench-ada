with Interfaces.C; use Interfaces.C;
with System;
with Interfaces.C.Extensions;
with CUDA.Stddef;
with stddef_h;
with CUDA.Vector_Types;
with uvector_types_h;
with udriver_types_h;
use Launch_Params;
use Class_cudaLaunchParams;
use Kernel_Node_Params;
use Class_cudaKernelNodeParams;

package CUDA.Driver_Types is

   subtype Error is unsigned;
   Success : Error renames udriver_types_h.cudaSuccess;
   type Channel_Format_Kind is
     (Channel_Format_Kind_Signed, Channel_Format_Kind_Unsigned,
      Channel_Format_Kind_Float, Channel_Format_Kind_None,
      Channel_Format_Kind_NV12, Channel_Format_Kind_Unsigned_Normalized8_X1,
      Channel_Format_Kind_Unsigned_Normalized8_X2,
      Channel_Format_Kind_Unsigned_Normalized8_X4,
      Channel_Format_Kind_Unsigned_Normalized16_X1,
      Channel_Format_Kind_Unsigned_Normalized16_X2,
      Channel_Format_Kind_Unsigned_Normalized16_X4,
      Channel_Format_Kind_Signed_Normalized8_X1,
      Channel_Format_Kind_Signed_Normalized8_X2,
      Channel_Format_Kind_Signed_Normalized8_X4,
      Channel_Format_Kind_Signed_Normalized16_X1,
      Channel_Format_Kind_Signed_Normalized16_X2,
      Channel_Format_Kind_Signed_Normalized16_X4,
      Channel_Format_Kind_Unsigned_Block_Compressed1,
      Channel_Format_Kind_Unsigned_Block_Compressed1_SRGB,
      Channel_Format_Kind_Unsigned_Block_Compressed2,
      Channel_Format_Kind_Unsigned_Block_Compressed2_SRGB,
      Channel_Format_Kind_Unsigned_Block_Compressed3,
      Channel_Format_Kind_Unsigned_Block_Compressed3_SRGB,
      Channel_Format_Kind_Unsigned_Block_Compressed4,
      Channel_Format_Kind_Signed_Block_Compressed4,
      Channel_Format_Kind_Unsigned_Block_Compressed5,
      Channel_Format_Kind_Signed_Block_Compressed5,
      Channel_Format_Kind_Unsigned_Block_Compressed6_H,
      Channel_Format_Kind_Signed_Block_Compressed6_H,
      Channel_Format_Kind_Unsigned_Block_Compressed7,
      Channel_Format_Kind_Unsigned_Block_Compressed7_SRGB);

   type Channel_Format_Desc is record
      X : int;
      Y : int;
      Z : int;
      W : int;
      F : Channel_Format_Kind;

   end record;

   type CUDA_Array_t is new System.Address;

   type CUDA_Array_const_t is new System.Address;

   type Mipmapped_Array_T is new System.Address;

   type Mipmapped_Array_Const_T is new System.Address;

   type CUDA_ArraySparseProperties;
   type Anon_1 is record
      Width  : unsigned;
      Height : unsigned;
      Depth  : unsigned;

   end record;

   type CUDA_ArraySparseProperties_reserved_array is
     array (0 .. 3) of unsigned;

   type CUDA_ArraySparseProperties is record
      Tile_Extent         : Anon_1;
      Miptail_First_Level : unsigned;
      Miptail_Size        : Extensions.unsigned_long_long;
      Flags               : unsigned;
      Reserved            : CUDA_ArraySparseProperties_reserved_array;

   end record;

   type Memory_Type_T is
     (Memory_Type_Unregistered, Memory_Type_Host, Memory_Type_Device,
      Memory_Type_Managed);

   type Memcpy_Kind is
     (Memcpy_Host_To_Host, Memcpy_Host_To_Device, Memcpy_Device_To_Host,
      Memcpy_Device_To_Device, Memcpy_Default);

   type Pitched_Ptr is record
      Ptr   : System.Address;
      Pitch : CUDA.Stddef.Size_T;
      Xsize : CUDA.Stddef.Size_T;
      Ysize : CUDA.Stddef.Size_T;

   end record;

   type Extent_T is record
      Width  : CUDA.Stddef.Size_T;
      Height : CUDA.Stddef.Size_T;
      Depth  : CUDA.Stddef.Size_T;

   end record;

   type Pos is record
      X : CUDA.Stddef.Size_T;
      Y : CUDA.Stddef.Size_T;
      Z : CUDA.Stddef.Size_T;

   end record;

   type Memcpy_3D_Parms is record
      Src_Array : CUDA_Array_t;
      Src_Pos   : Pos;
      Src_Ptr   : Pitched_Ptr;
      Dst_Array : CUDA_Array_t;
      Dst_Pos   : Pos;
      Dst_Ptr   : Pitched_Ptr;
      Extent    : Extent_T;
      Kind      : Memcpy_Kind;

   end record;

   type Memcpy_3D_Peer_Parms is record
      Src_Array  : CUDA_Array_t;
      Src_Pos    : Pos;
      Src_Ptr    : Pitched_Ptr;
      Src_Device : int;
      Dst_Array  : CUDA_Array_t;
      Dst_Pos    : Pos;
      Dst_Ptr    : Pitched_Ptr;
      Dst_Device : int;
      Extent     : Extent_T;

   end record;

   type Memset_Params is record
      Dst          : System.Address;
      Pitch        : CUDA.Stddef.Size_T;
      Value        : unsigned;
      Element_Size : unsigned;
      Width        : CUDA.Stddef.Size_T;
      Height       : CUDA.Stddef.Size_T;

   end record;

   type Access_Property is
     (Access_Property_Normal, Access_Property_Streaming,
      Access_Property_Persisting);

   type Access_Policy_Window_T is record
      Base_Ptr  : System.Address;
      Num_Bytes : CUDA.Stddef.Size_T;
      Hit_Ratio : float;
      Hit_Prop  : Access_Property;
      Miss_Prop : Access_Property;

   end record;

   type Host_Fn_T is new udriver_types_h.cudaHostFn_t;

   generic
      with procedure Temp_Call_1 (Arg1 : System.Address);
   procedure Host_Fn_T_Gen (Arg1 : System.Address);
   type Host_Node_Params is record
      Fn        : Host_Fn_T;
      User_Data : System.Address;

   end record;

   type Stream_Capture_Status is
     (Stream_Capture_Status_None, Stream_Capture_Status_Active,
      Stream_Capture_Status_Invalidated);

   type Stream_Capture_Mode is
     (Stream_Capture_Mode_Global, Stream_Capture_Mode_Thread_Local,
      Stream_Capture_Mode_Relaxed);

   subtype Synchronization_Policy is unsigned;
   Sync_Policy_Auto          :
     Synchronization_Policy renames udriver_types_h.cudaSyncPolicyAuto;
   Sync_Policy_Spin          :
     Synchronization_Policy renames udriver_types_h.cudaSyncPolicySpin;
   Sync_Policy_Yield         :
     Synchronization_Policy renames udriver_types_h.cudaSyncPolicyYield;
   Sync_Policy_Blocking_Sync :
     Synchronization_Policy renames udriver_types_h.cudaSyncPolicyBlockingSync;
   subtype Stream_Attr_ID is unsigned;
   Stream_Attribute_Access_Policy_Window   :
     Stream_Attr_ID renames
     udriver_types_h.cudaStreamAttributeAccessPolicyWindow;
   Stream_Attribute_Synchronization_Policy :
     Stream_Attr_ID renames
     udriver_types_h.cudaStreamAttributeSynchronizationPolicy;
   type Stream_Attr_Value (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Access_Policy_Window : Access_Policy_Window_T;
         when others =>
            Sync_Policy : Synchronization_Policy;

      end case;
   end record;

   type Stream_Update_Capture_Dependencies_Flags is
     (Stream_Add_Capture_Dependencies, Stream_Set_Capture_Dependencies);

   subtype User_Object_Flags is unsigned;
   User_Object_No_Destructor_Sync :
     User_Object_Flags renames udriver_types_h.cudaUserObjectNoDestructorSync;
   subtype User_Object_Retain_Flags is unsigned;
   Graph_User_Object_Move :
     User_Object_Retain_Flags renames udriver_types_h.cudaGraphUserObjectMove;
   subtype Graphics_Register_Flags is unsigned;
   Graphics_Register_Flags_None               :
     Graphics_Register_Flags renames
     udriver_types_h.cudaGraphicsRegisterFlagsNone;
   Graphics_Register_Flags_Read_Only          :
     Graphics_Register_Flags renames
     udriver_types_h.cudaGraphicsRegisterFlagsReadOnly;
   Graphics_Register_Flags_Write_Discard      :
     Graphics_Register_Flags renames
     udriver_types_h.cudaGraphicsRegisterFlagsWriteDiscard;
   Graphics_Register_Flags_Surface_Load_Store :
     Graphics_Register_Flags renames
     udriver_types_h.cudaGraphicsRegisterFlagsSurfaceLoadStore;
   Graphics_Register_Flags_Texture_Gather     :
     Graphics_Register_Flags renames
     udriver_types_h.cudaGraphicsRegisterFlagsTextureGather;
   type Graphics_Map_Flags is
     (Graphics_Map_Flags_None, Graphics_Map_Flags_Read_Only,
      Graphics_Map_Flags_Write_Discard);

   type Graphics_Cube_Face is
     (Graphics_Cube_Face_Positive_X, Graphics_Cube_Face_Negative_X,
      Graphics_Cube_Face_Positive_Y, Graphics_Cube_Face_Negative_Y,
      Graphics_Cube_Face_Positive_Z, Graphics_Cube_Face_Negative_Z);

   subtype Kernel_Node_Attr_ID is unsigned;
   Kernel_Node_Attribute_Access_Policy_Window :
     Kernel_Node_Attr_ID renames
     udriver_types_h.cudaKernelNodeAttributeAccessPolicyWindow;
   Kernel_Node_Attribute_Cooperative          :
     Kernel_Node_Attr_ID renames
     udriver_types_h.cudaKernelNodeAttributeCooperative;
   type Kernel_Node_Attr_Value (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Access_Policy_Window : Access_Policy_Window_T;
         when others =>
            Cooperative : int;

      end case;
   end record;

   type Resource_Type is
     (Resource_Type_Array, Resource_Type_Mipmapped_Array, Resource_Type_Linear,
      Resource_Type_Pitch_2D);

   type Resource_View_Format is
     (Res_View_Format_None, Res_View_Format_Unsigned_Char1,
      Res_View_Format_Unsigned_Char2, Res_View_Format_Unsigned_Char4,
      Res_View_Format_Signed_Char1, Res_View_Format_Signed_Char2,
      Res_View_Format_Signed_Char4, Res_View_Format_Unsigned_Short1,
      Res_View_Format_Unsigned_Short2, Res_View_Format_Unsigned_Short4,
      Res_View_Format_Signed_Short1, Res_View_Format_Signed_Short2,
      Res_View_Format_Signed_Short4, Res_View_Format_Unsigned_Int1,
      Res_View_Format_Unsigned_Int2, Res_View_Format_Unsigned_Int4,
      Res_View_Format_Signed_Int1, Res_View_Format_Signed_Int2,
      Res_View_Format_Signed_Int4, Res_View_Format_Half1,
      Res_View_Format_Half2, Res_View_Format_Half4, Res_View_Format_Float1,
      Res_View_Format_Float2, Res_View_Format_Float4,
      Res_View_Format_Unsigned_Block_Compressed1,
      Res_View_Format_Unsigned_Block_Compressed2,
      Res_View_Format_Unsigned_Block_Compressed3,
      Res_View_Format_Unsigned_Block_Compressed4,
      Res_View_Format_Signed_Block_Compressed4,
      Res_View_Format_Unsigned_Block_Compressed5,
      Res_View_Format_Signed_Block_Compressed5,
      Res_View_Format_Unsigned_Block_Compressed6_H,
      Res_View_Format_Signed_Block_Compressed6_H,
      Res_View_Format_Unsigned_Block_Compressed7);

   type Resource_Desc;
   type Anon_2;
   type Anon_3 is record
      C_Array : CUDA_Array_t;

   end record;

   type Anon_4 is record
      Mipmap : Mipmapped_Array_T;

   end record;

   type Anon_5 is record
      Dev_Ptr       : System.Address;
      Desc          : Channel_Format_Desc;
      Size_In_Bytes : CUDA.Stddef.Size_T;

   end record;

   type Anon_6 is record
      Dev_Ptr        : System.Address;
      Desc           : Channel_Format_Desc;
      Width          : CUDA.Stddef.Size_T;
      Height         : CUDA.Stddef.Size_T;
      Pitch_In_Bytes : CUDA.Stddef.Size_T;

   end record;

   type Anon_2 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            C_Array : Anon_3;
         when 1 =>
            Mipmap : Anon_4;
         when 2 =>
            Linear : Anon_5;
         when others =>
            Pitch2_D : Anon_6;

      end case;
   end record;

   type Resource_Desc is record
      Res_Type : Resource_Type;
      Res      : Anon_2;

   end record;

   type Resource_View_Desc is record
      Format             : Resource_View_Format;
      Width              : CUDA.Stddef.Size_T;
      Height             : CUDA.Stddef.Size_T;
      Depth              : CUDA.Stddef.Size_T;
      First_Mipmap_Level : unsigned;
      Last_Mipmap_Level  : unsigned;
      First_Layer        : unsigned;
      Last_Layer         : unsigned;

   end record;

   type Pointer_Attributes is record
      C_Type         : Memory_Type_T;
      Device         : int;
      Device_Pointer : System.Address;
      Host_Pointer   : System.Address;

   end record;

   type Func_Attributes is record
      Shared_Size_Bytes             : CUDA.Stddef.Size_T;
      Const_Size_Bytes              : CUDA.Stddef.Size_T;
      Local_Size_Bytes              : CUDA.Stddef.Size_T;
      Max_Threads_Per_Block         : int;
      Num_Regs                      : int;
      Ptx_Version                   : int;
      Binary_Version                : int;
      Cache_Mode_CA                 : int;
      Max_Dynamic_Shared_Size_Bytes : int;
      Preferred_Shmem_Carveout      : int;

   end record;

   subtype Func_Attribute is unsigned;
   Func_Attribute_Max_Dynamic_Shared_Memory_Size   :
     Func_Attribute renames
     udriver_types_h.cudaFuncAttributeMaxDynamicSharedMemorySize;
   Func_Attribute_Preferred_Shared_Memory_Carveout :
     Func_Attribute renames
     udriver_types_h.cudaFuncAttributePreferredSharedMemoryCarveout;
   Func_Attribute_Max                              :
     Func_Attribute renames udriver_types_h.cudaFuncAttributeMax;
   type Func_Cache is
     (Func_Cache_Prefer_None, Func_Cache_Prefer_Shared, Func_Cache_Prefer_L1,
      Func_Cache_Prefer_Equal);

   type Shared_Mem_Config is
     (Shared_Mem_Bank_Size_Default, Shared_Mem_Bank_Size_Four_Byte,
      Shared_Mem_Bank_Size_Eight_Byte);

   subtype Shared_Carveout is unsigned;
   Sharedmem_Carveout_Default    :
     Shared_Carveout renames udriver_types_h.cudaSharedmemCarveoutDefault;
   Sharedmem_Carveout_Max_Shared :
     Shared_Carveout renames udriver_types_h.cudaSharedmemCarveoutMaxShared;
   Sharedmem_Carveout_Max_L1     :
     Shared_Carveout renames udriver_types_h.cudaSharedmemCarveoutMaxL1;
   type Compute_Mode is
     (Compute_Mode_Default, Compute_Mode_Exclusive, Compute_Mode_Prohibited,
      Compute_Mode_Exclusive_Process);

   type Limit is
     (Limit_Stack_Size, Limit_Printf_Fifo_Size, Limit_Malloc_Heap_Size,
      Limit_Dev_Runtime_Sync_Depth, Limit_Dev_Runtime_Pending_Launch_Count,
      Limit_Max_L2_Fetch_Granularity, Limit_Persisting_L2_Cache_Size);

   subtype Memory_Advise is unsigned;
   Mem_Advise_Set_Read_Mostly          :
     Memory_Advise renames udriver_types_h.cudaMemAdviseSetReadMostly;
   Mem_Advise_Unset_Read_Mostly        :
     Memory_Advise renames udriver_types_h.cudaMemAdviseUnsetReadMostly;
   Mem_Advise_Set_Preferred_Location   :
     Memory_Advise renames udriver_types_h.cudaMemAdviseSetPreferredLocation;
   Mem_Advise_Unset_Preferred_Location :
     Memory_Advise renames udriver_types_h.cudaMemAdviseUnsetPreferredLocation;
   Mem_Advise_Set_Accessed_By          :
     Memory_Advise renames udriver_types_h.cudaMemAdviseSetAccessedBy;
   Mem_Advise_Unset_Accessed_By        :
     Memory_Advise renames udriver_types_h.cudaMemAdviseUnsetAccessedBy;
   subtype Mem_Range_Attribute is unsigned;
   Mem_Range_Attribute_Read_Mostly            :
     Mem_Range_Attribute renames
     udriver_types_h.cudaMemRangeAttributeReadMostly;
   Mem_Range_Attribute_Preferred_Location     :
     Mem_Range_Attribute renames
     udriver_types_h.cudaMemRangeAttributePreferredLocation;
   Mem_Range_Attribute_Accessed_By            :
     Mem_Range_Attribute renames
     udriver_types_h.cudaMemRangeAttributeAccessedBy;
   Mem_Range_Attribute_Last_Prefetch_Location :
     Mem_Range_Attribute renames
     udriver_types_h.cudaMemRangeAttributeLastPrefetchLocation;
   type Output_Mode is (Key_Value_Pair, CSV);

   subtype Flush_GPUDirect_RDMAWrites_Options is unsigned;
   Flush_GPUDirect_RDMAWrites_Option_Host    :
     Flush_GPUDirect_RDMAWrites_Options renames
     udriver_types_h.cudaFlushGPUDirectRDMAWritesOptionHost;
   Flush_GPUDirect_RDMAWrites_Option_Mem_Ops :
     Flush_GPUDirect_RDMAWrites_Options renames
     udriver_types_h.cudaFlushGPUDirectRDMAWritesOptionMemOps;
   subtype GPUDirect_RDMAWrites_Ordering is unsigned;
   GPUDirect_RDMAWrites_Ordering_None        :
     GPUDirect_RDMAWrites_Ordering renames
     udriver_types_h.cudaGPUDirectRDMAWritesOrderingNone;
   GPUDirect_RDMAWrites_Ordering_Owner       :
     GPUDirect_RDMAWrites_Ordering renames
     udriver_types_h.cudaGPUDirectRDMAWritesOrderingOwner;
   GPUDirect_RDMAWrites_Ordering_All_Devices :
     GPUDirect_RDMAWrites_Ordering renames
     udriver_types_h.cudaGPUDirectRDMAWritesOrderingAllDevices;
   subtype Flush_GPUDirect_RDMAWrites_Scope is unsigned;
   Flush_GPUDirect_RDMAWrites_To_Owner       :
     Flush_GPUDirect_RDMAWrites_Scope renames
     udriver_types_h.cudaFlushGPUDirectRDMAWritesToOwner;
   Flush_GPUDirect_RDMAWrites_To_All_Devices :
     Flush_GPUDirect_RDMAWrites_Scope renames
     udriver_types_h.cudaFlushGPUDirectRDMAWritesToAllDevices;
   type Flush_GPUDirect_RDMAWrites_Target is
     (Flush_GPUDirect_RDMAWrites_Target_Current_Device);

   subtype Device_Attr is unsigned;
   Dev_Attr_Max_Threads_Per_Block                        :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxThreadsPerBlock;
   Dev_Attr_Max_Block_Dim_X                              :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxBlockDimX;
   Dev_Attr_Max_Block_Dim_Y                              :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxBlockDimY;
   Dev_Attr_Max_Block_Dim_Z                              :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxBlockDimZ;
   Dev_Attr_Max_Grid_Dim_X                               :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxGridDimX;
   Dev_Attr_Max_Grid_Dim_Y                               :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxGridDimY;
   Dev_Attr_Max_Grid_Dim_Z                               :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxGridDimZ;
   Dev_Attr_Max_Shared_Memory_Per_Block                  :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSharedMemoryPerBlock;
   Dev_Attr_Total_Constant_Memory                        :
     Device_Attr renames udriver_types_h.cudaDevAttrTotalConstantMemory;
   Dev_Attr_Warp_Size                                    :
     Device_Attr renames udriver_types_h.cudaDevAttrWarpSize;
   Dev_Attr_Max_Pitch                                    :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxPitch;
   Dev_Attr_Max_Registers_Per_Block                      :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxRegistersPerBlock;
   Dev_Attr_Clock_Rate                                   :
     Device_Attr renames udriver_types_h.cudaDevAttrClockRate;
   Dev_Attr_Texture_Alignment                            :
     Device_Attr renames udriver_types_h.cudaDevAttrTextureAlignment;
   Dev_Attr_Gpu_Overlap                                  :
     Device_Attr renames udriver_types_h.cudaDevAttrGpuOverlap;
   Dev_Attr_Multi_Processor_Count                        :
     Device_Attr renames udriver_types_h.cudaDevAttrMultiProcessorCount;
   Dev_Attr_Kernel_Exec_Timeout                          :
     Device_Attr renames udriver_types_h.cudaDevAttrKernelExecTimeout;
   Dev_Attr_Integrated                                   :
     Device_Attr renames udriver_types_h.cudaDevAttrIntegrated;
   Dev_Attr_Can_Map_Host_Memory                          :
     Device_Attr renames udriver_types_h.cudaDevAttrCanMapHostMemory;
   Dev_Attr_Compute_Mode                                 :
     Device_Attr renames udriver_types_h.cudaDevAttrComputeMode;
   Dev_Attr_Max_Texture_1D_Width                         :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture1DWidth;
   Dev_Attr_Max_Texture_2D_Width                         :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DWidth;
   Dev_Attr_Max_Texture_2D_Height                        :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DHeight;
   Dev_Attr_Max_Texture_3D_Width                         :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture3DWidth;
   Dev_Attr_Max_Texture_3D_Height                        :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture3DHeight;
   Dev_Attr_Max_Texture_3D_Depth                         :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture3DDepth;
   Dev_Attr_Max_Texture_2D_Layered_Width                 :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DLayeredWidth;
   Dev_Attr_Max_Texture_2D_Layered_Height                :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DLayeredHeight;
   Dev_Attr_Max_Texture_2D_Layered_Layers                :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DLayeredLayers;
   Dev_Attr_Surface_Alignment                            :
     Device_Attr renames udriver_types_h.cudaDevAttrSurfaceAlignment;
   Dev_Attr_Concurrent_Kernels                           :
     Device_Attr renames udriver_types_h.cudaDevAttrConcurrentKernels;
   Dev_Attr_Ecc_Enabled                                  :
     Device_Attr renames udriver_types_h.cudaDevAttrEccEnabled;
   Dev_Attr_Pci_Bus_Id                                   :
     Device_Attr renames udriver_types_h.cudaDevAttrPciBusId;
   Dev_Attr_Pci_Device_Id                                :
     Device_Attr renames udriver_types_h.cudaDevAttrPciDeviceId;
   Dev_Attr_Tcc_Driver                                   :
     Device_Attr renames udriver_types_h.cudaDevAttrTccDriver;
   Dev_Attr_Memory_Clock_Rate                            :
     Device_Attr renames udriver_types_h.cudaDevAttrMemoryClockRate;
   Dev_Attr_Global_Memory_Bus_Width                      :
     Device_Attr renames udriver_types_h.cudaDevAttrGlobalMemoryBusWidth;
   Dev_Attr_L2_Cache_Size                                :
     Device_Attr renames udriver_types_h.cudaDevAttrL2CacheSize;
   Dev_Attr_Max_Threads_Per_Multi_Processor              :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxThreadsPerMultiProcessor;
   Dev_Attr_Async_Engine_Count                           :
     Device_Attr renames udriver_types_h.cudaDevAttrAsyncEngineCount;
   Dev_Attr_Unified_Addressing                           :
     Device_Attr renames udriver_types_h.cudaDevAttrUnifiedAddressing;
   Dev_Attr_Max_Texture_1D_Layered_Width                 :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture1DLayeredWidth;
   Dev_Attr_Max_Texture_1D_Layered_Layers                :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture1DLayeredLayers;
   Dev_Attr_Max_Texture_2D_Gather_Width                  :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DGatherWidth;
   Dev_Attr_Max_Texture_2D_Gather_Height                 :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DGatherHeight;
   Dev_Attr_Max_Texture_3D_Width_Alt                     :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture3DWidthAlt;
   Dev_Attr_Max_Texture_3D_Height_Alt                    :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture3DHeightAlt;
   Dev_Attr_Max_Texture_3D_Depth_Alt                     :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture3DDepthAlt;
   Dev_Attr_Pci_Domain_Id                                :
     Device_Attr renames udriver_types_h.cudaDevAttrPciDomainId;
   Dev_Attr_Texture_Pitch_Alignment                      :
     Device_Attr renames udriver_types_h.cudaDevAttrTexturePitchAlignment;
   Dev_Attr_Max_Texture_Cubemap_Width                    :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTextureCubemapWidth;
   Dev_Attr_Max_Texture_Cubemap_Layered_Width            :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxTextureCubemapLayeredWidth;
   Dev_Attr_Max_Texture_Cubemap_Layered_Layers           :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxTextureCubemapLayeredLayers;
   Dev_Attr_Max_Surface_1D_Width                         :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface1DWidth;
   Dev_Attr_Max_Surface_2D_Width                         :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface2DWidth;
   Dev_Attr_Max_Surface_2D_Height                        :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface2DHeight;
   Dev_Attr_Max_Surface_3D_Width                         :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface3DWidth;
   Dev_Attr_Max_Surface_3D_Height                        :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface3DHeight;
   Dev_Attr_Max_Surface_3D_Depth                         :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface3DDepth;
   Dev_Attr_Max_Surface_1D_Layered_Width                 :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface1DLayeredWidth;
   Dev_Attr_Max_Surface_1D_Layered_Layers                :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface1DLayeredLayers;
   Dev_Attr_Max_Surface_2D_Layered_Width                 :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface2DLayeredWidth;
   Dev_Attr_Max_Surface_2D_Layered_Height                :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface2DLayeredHeight;
   Dev_Attr_Max_Surface_2D_Layered_Layers                :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurface2DLayeredLayers;
   Dev_Attr_Max_Surface_Cubemap_Width                    :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxSurfaceCubemapWidth;
   Dev_Attr_Max_Surface_Cubemap_Layered_Width            :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxSurfaceCubemapLayeredWidth;
   Dev_Attr_Max_Surface_Cubemap_Layered_Layers           :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxSurfaceCubemapLayeredLayers;
   Dev_Attr_Max_Texture_1D_Linear_Width                  :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture1DLinearWidth;
   Dev_Attr_Max_Texture_2D_Linear_Width                  :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DLinearWidth;
   Dev_Attr_Max_Texture_2D_Linear_Height                 :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DLinearHeight;
   Dev_Attr_Max_Texture_2D_Linear_Pitch                  :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DLinearPitch;
   Dev_Attr_Max_Texture_2D_Mipmapped_Width               :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture2DMipmappedWidth;
   Dev_Attr_Max_Texture_2D_Mipmapped_Height              :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxTexture2DMipmappedHeight;
   Dev_Attr_Compute_Capability_Major                     :
     Device_Attr renames udriver_types_h.cudaDevAttrComputeCapabilityMajor;
   Dev_Attr_Compute_Capability_Minor                     :
     Device_Attr renames udriver_types_h.cudaDevAttrComputeCapabilityMinor;
   Dev_Attr_Max_Texture_1D_Mipmapped_Width               :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxTexture1DMipmappedWidth;
   Dev_Attr_Stream_Priorities_Supported                  :
     Device_Attr renames udriver_types_h.cudaDevAttrStreamPrioritiesSupported;
   Dev_Attr_Global_L1_Cache_Supported                    :
     Device_Attr renames udriver_types_h.cudaDevAttrGlobalL1CacheSupported;
   Dev_Attr_Local_L1_Cache_Supported                     :
     Device_Attr renames udriver_types_h.cudaDevAttrLocalL1CacheSupported;
   Dev_Attr_Max_Shared_Memory_Per_Multiprocessor         :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxSharedMemoryPerMultiprocessor;
   Dev_Attr_Max_Registers_Per_Multiprocessor             :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxRegistersPerMultiprocessor;
   Dev_Attr_Managed_Memory                               :
     Device_Attr renames udriver_types_h.cudaDevAttrManagedMemory;
   Dev_Attr_Is_Multi_Gpu_Board                           :
     Device_Attr renames udriver_types_h.cudaDevAttrIsMultiGpuBoard;
   Dev_Attr_Multi_Gpu_Board_Group_ID                     :
     Device_Attr renames udriver_types_h.cudaDevAttrMultiGpuBoardGroupID;
   Dev_Attr_Host_Native_Atomic_Supported                 :
     Device_Attr renames udriver_types_h.cudaDevAttrHostNativeAtomicSupported;
   Dev_Attr_Single_To_Double_Precision_Perf_Ratio        :
     Device_Attr renames
     udriver_types_h.cudaDevAttrSingleToDoublePrecisionPerfRatio;
   Dev_Attr_Pageable_Memory_Access                       :
     Device_Attr renames udriver_types_h.cudaDevAttrPageableMemoryAccess;
   Dev_Attr_Concurrent_Managed_Access                    :
     Device_Attr renames udriver_types_h.cudaDevAttrConcurrentManagedAccess;
   Dev_Attr_Compute_Preemption_Supported                 :
     Device_Attr renames udriver_types_h.cudaDevAttrComputePreemptionSupported;
   Dev_Attr_Can_Use_Host_Pointer_For_Registered_Mem      :
     Device_Attr renames
     udriver_types_h.cudaDevAttrCanUseHostPointerForRegisteredMem;
   Dev_Attr_Reserved92                                   :
     Device_Attr renames udriver_types_h.cudaDevAttrReserved92;
   Dev_Attr_Reserved93                                   :
     Device_Attr renames udriver_types_h.cudaDevAttrReserved93;
   Dev_Attr_Reserved94                                   :
     Device_Attr renames udriver_types_h.cudaDevAttrReserved94;
   Dev_Attr_Cooperative_Launch                           :
     Device_Attr renames udriver_types_h.cudaDevAttrCooperativeLaunch;
   Dev_Attr_Cooperative_Multi_Device_Launch              :
     Device_Attr renames
     udriver_types_h.cudaDevAttrCooperativeMultiDeviceLaunch;
   Dev_Attr_Max_Shared_Memory_Per_Block_Optin            :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxSharedMemoryPerBlockOptin;
   Dev_Attr_Can_Flush_Remote_Writes                      :
     Device_Attr renames udriver_types_h.cudaDevAttrCanFlushRemoteWrites;
   Dev_Attr_Host_Register_Supported                      :
     Device_Attr renames udriver_types_h.cudaDevAttrHostRegisterSupported;
   Dev_Attr_Pageable_Memory_Access_Uses_Host_Page_Tables :
     Device_Attr renames
     udriver_types_h.cudaDevAttrPageableMemoryAccessUsesHostPageTables;
   Dev_Attr_Direct_Managed_Mem_Access_From_Host          :
     Device_Attr renames
     udriver_types_h.cudaDevAttrDirectManagedMemAccessFromHost;
   Dev_Attr_Max_Blocks_Per_Multiprocessor                :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxBlocksPerMultiprocessor;
   Dev_Attr_Max_Persisting_L2_Cache_Size                 :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxPersistingL2CacheSize;
   Dev_Attr_Max_Access_Policy_Window_Size                :
     Device_Attr renames udriver_types_h.cudaDevAttrMaxAccessPolicyWindowSize;
   Dev_Attr_Reserved_Shared_Memory_Per_Block             :
     Device_Attr renames
     udriver_types_h.cudaDevAttrReservedSharedMemoryPerBlock;
   Dev_Attr_Sparse_Cuda_Array_Supported                  :
     Device_Attr renames udriver_types_h.cudaDevAttrSparseCudaArraySupported;
   Dev_Attr_Host_Register_Read_Only_Supported            :
     Device_Attr renames
     udriver_types_h.cudaDevAttrHostRegisterReadOnlySupported;
   Dev_Attr_Timeline_Semaphore_Interop_Supported         :
     Device_Attr renames
     udriver_types_h.cudaDevAttrTimelineSemaphoreInteropSupported;
   Dev_Attr_Max_Timeline_Semaphore_Interop_Supported     :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMaxTimelineSemaphoreInteropSupported;
   Dev_Attr_Memory_Pools_Supported                       :
     Device_Attr renames udriver_types_h.cudaDevAttrMemoryPoolsSupported;
   Dev_Attr_GPUDirect_RDMASupported                      :
     Device_Attr renames udriver_types_h.cudaDevAttrGPUDirectRDMASupported;
   Dev_Attr_GPUDirect_RDMAFlush_Writes_Options           :
     Device_Attr renames
     udriver_types_h.cudaDevAttrGPUDirectRDMAFlushWritesOptions;
   Dev_Attr_GPUDirect_RDMAWrites_Ordering                :
     Device_Attr renames
     udriver_types_h.cudaDevAttrGPUDirectRDMAWritesOrdering;
   Dev_Attr_Memory_Pool_Supported_Handle_Types           :
     Device_Attr renames
     udriver_types_h.cudaDevAttrMemoryPoolSupportedHandleTypes;
   Dev_Attr_Max : Device_Attr renames udriver_types_h.cudaDevAttrMax;
   subtype Mem_Pool_Attr is unsigned;
   Mem_Pool_Reuse_Follow_Event_Dependencies   :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolReuseFollowEventDependencies;
   Mem_Pool_Reuse_Allow_Opportunistic         :
     Mem_Pool_Attr renames udriver_types_h.cudaMemPoolReuseAllowOpportunistic;
   Mem_Pool_Reuse_Allow_Internal_Dependencies :
     Mem_Pool_Attr renames
     udriver_types_h.cudaMemPoolReuseAllowInternalDependencies;
   Mem_Pool_Attr_Release_Threshold            :
     Mem_Pool_Attr renames udriver_types_h.cudaMemPoolAttrReleaseThreshold;
   Mem_Pool_Attr_Reserved_Mem_Current         :
     Mem_Pool_Attr renames udriver_types_h.cudaMemPoolAttrReservedMemCurrent;
   Mem_Pool_Attr_Reserved_Mem_High            :
     Mem_Pool_Attr renames udriver_types_h.cudaMemPoolAttrReservedMemHigh;
   Mem_Pool_Attr_Used_Mem_Current             :
     Mem_Pool_Attr renames udriver_types_h.cudaMemPoolAttrUsedMemCurrent;
   Mem_Pool_Attr_Used_Mem_High                :
     Mem_Pool_Attr renames udriver_types_h.cudaMemPoolAttrUsedMemHigh;
   type Mem_Location_Type is
     (Mem_Location_Type_Invalid, Mem_Location_Type_Device);

   type Mem_Location is record
      C_Type : Mem_Location_Type;
      Id     : int;

   end record;

   subtype Mem_Access_Flags is unsigned;
   Mem_Access_Flags_Prot_None       :
     Mem_Access_Flags renames udriver_types_h.cudaMemAccessFlagsProtNone;
   Mem_Access_Flags_Prot_Read       :
     Mem_Access_Flags renames udriver_types_h.cudaMemAccessFlagsProtRead;
   Mem_Access_Flags_Prot_Read_Write :
     Mem_Access_Flags renames udriver_types_h.cudaMemAccessFlagsProtReadWrite;
   type Mem_Access_Desc is record
      Location : Mem_Location;
      Flags    : Mem_Access_Flags;

   end record;

   subtype Mem_Allocation_Type is unsigned;
   Mem_Allocation_Type_Invalid :
     Mem_Allocation_Type renames udriver_types_h.cudaMemAllocationTypeInvalid;
   Mem_Allocation_Type_Pinned  :
     Mem_Allocation_Type renames udriver_types_h.cudaMemAllocationTypePinned;
   Mem_Allocation_Type_Max     :
     Mem_Allocation_Type renames udriver_types_h.cudaMemAllocationTypeMax;
   subtype Mem_Allocation_Handle_Type is unsigned;
   Mem_Handle_Type_None                  :
     Mem_Allocation_Handle_Type renames udriver_types_h.cudaMemHandleTypeNone;
   Mem_Handle_Type_Posix_File_Descriptor :
     Mem_Allocation_Handle_Type renames
     udriver_types_h.cudaMemHandleTypePosixFileDescriptor;
   Mem_Handle_Type_Win32                 :
     Mem_Allocation_Handle_Type renames udriver_types_h.cudaMemHandleTypeWin32;
   Mem_Handle_Type_Win32_Kmt             :
     Mem_Allocation_Handle_Type renames
     udriver_types_h.cudaMemHandleTypeWin32Kmt;
   type Mem_Pool_Props_Reserved_Array is array (0 .. 63) of unsigned_char;

   type Mem_Pool_Props is record
      Alloc_Type                : Mem_Allocation_Type;
      Handle_Types              : Mem_Allocation_Handle_Type;
      Location                  : Mem_Location;
      Win32_Security_Attributes : System.Address;
      Reserved                  : Mem_Pool_Props_Reserved_Array;

   end record;

   type Mem_Pool_Ptr_Export_Data_Reserved_Array is
     array (0 .. 63) of unsigned_char;

   type Mem_Pool_Ptr_Export_Data is record
      Reserved : Mem_Pool_Ptr_Export_Data_Reserved_Array;

   end record;

   type Mem_Alloc_Node_Params is record
      Pool_Props        : Mem_Pool_Props;
      Access_Descs      : access Mem_Access_Desc;
      Access_Desc_Count : CUDA.Stddef.Size_T;
      Bytesize          : CUDA.Stddef.Size_T;
      Dptr              : System.Address;

   end record;

   subtype Graph_Mem_Attribute_Type is unsigned;
   Graph_Mem_Attr_Used_Mem_Current     :
     Graph_Mem_Attribute_Type renames
     udriver_types_h.cudaGraphMemAttrUsedMemCurrent;
   Graph_Mem_Attr_Used_Mem_High        :
     Graph_Mem_Attribute_Type renames
     udriver_types_h.cudaGraphMemAttrUsedMemHigh;
   Graph_Mem_Attr_Reserved_Mem_Current :
     Graph_Mem_Attribute_Type renames
     udriver_types_h.cudaGraphMemAttrReservedMemCurrent;
   Graph_Mem_Attr_Reserved_Mem_High    :
     Graph_Mem_Attribute_Type renames
     udriver_types_h.cudaGraphMemAttrReservedMemHigh;
   subtype Device_P2_PAttr is unsigned;
   Dev_P2_PAttr_Performance_Rank            :
     Device_P2_PAttr renames udriver_types_h.cudaDevP2PAttrPerformanceRank;
   Dev_P2_PAttr_Access_Supported            :
     Device_P2_PAttr renames udriver_types_h.cudaDevP2PAttrAccessSupported;
   Dev_P2_PAttr_Native_Atomic_Supported     :
     Device_P2_PAttr renames
     udriver_types_h.cudaDevP2PAttrNativeAtomicSupported;
   Dev_P2_PAttr_Cuda_Array_Access_Supported :
     Device_P2_PAttr renames
     udriver_types_h.cudaDevP2PAttrCudaArrayAccessSupported;
   subtype CUuuid_St_Bytes_Array is Interfaces.C.char_array (0 .. 15);
   type CUuuid_St is record
      Bytes : CUuuid_St_Bytes_Array;

   end record;

   subtype CUuuid is CUuuid_St;
   subtype UUID_T is CUuuid_St;
   subtype Device_Prop_Name_Array is Interfaces.C.char_array (0 .. 255);
   subtype Device_Prop_Luid_Array is Interfaces.C.char_array (0 .. 7);
   type Device_Prop_Max_Threads_Dim_Array is array (0 .. 2) of int;

   type Device_Prop_Max_Grid_Size_Array is array (0 .. 2) of int;

   type Device_Prop_Max_Texture_2D__Array is array (0 .. 1) of int;

   type Device_Prop_Max_Texture_2D_Mipmap_Array is array (0 .. 1) of int;

   type Device_Prop_Max_Texture_2D_Linear_Array is array (0 .. 2) of int;

   type Device_Prop_Max_Texture_2D_Gather_Array is array (0 .. 1) of int;

   type Device_Prop_Max_Texture_3D__Array is array (0 .. 2) of int;

   type Device_Prop_Max_Texture_3D_Alt_Array is array (0 .. 2) of int;

   type Device_Prop_Max_Texture_1D_Layered_Array is array (0 .. 1) of int;

   type Device_Prop_Max_Texture_2D_Layered_Array is array (0 .. 2) of int;

   type Device_Prop_Max_Texture_Cubemap_Layered_Array is array (0 .. 1) of int;

   type Device_Prop_Max_Surface_2D__Array is array (0 .. 1) of int;

   type Device_Prop_Max_Surface_3D__Array is array (0 .. 2) of int;

   type Device_Prop_Max_Surface_1D_Layered_Array is array (0 .. 1) of int;

   type Device_Prop_Max_Surface_2D_Layered_Array is array (0 .. 2) of int;

   type Device_Prop_Max_Surface_Cubemap_Layered_Array is array (0 .. 1) of int;

   type Device_Prop is record
      Name                                         : Device_Prop_Name_Array;
      Uuid                                         : UUID_T;
      Luid                                         : Device_Prop_Luid_Array;
      Luid_Device_Node_Mask                        : unsigned;
      Total_Global_Mem                             : CUDA.Stddef.Size_T;
      Shared_Mem_Per_Block                         : CUDA.Stddef.Size_T;
      Regs_Per_Block                               : int;
      Warp_Size                                    : int;
      Mem_Pitch                                    : CUDA.Stddef.Size_T;
      Max_Threads_Per_Block                        : int;
      Max_Threads_Dim : Device_Prop_Max_Threads_Dim_Array;
      Max_Grid_Size : Device_Prop_Max_Grid_Size_Array;
      Clock_Rate                                   : int;
      Total_Const_Mem                              : CUDA.Stddef.Size_T;
      Major                                        : int;
      Minor                                        : int;
      Texture_Alignment                            : CUDA.Stddef.Size_T;
      Texture_Pitch_Alignment                      : CUDA.Stddef.Size_T;
      Device_Overlap                               : int;
      Multi_Processor_Count                        : int;
      Kernel_Exec_Timeout_Enabled                  : int;
      Integrated                                   : int;
      Can_Map_Host_Memory                          : int;
      Compute_Mode                                 : int;
      Max_Texture1_D                               : int;
      Max_Texture1_DMipmap                         : int;
      Max_Texture1_DLinear                         : int;
      Max_Texture2_D : Device_Prop_Max_Texture_2D__Array;
      Max_Texture2_DMipmap : Device_Prop_Max_Texture_2D_Mipmap_Array;
      Max_Texture2_DLinear : Device_Prop_Max_Texture_2D_Linear_Array;
      Max_Texture2_DGather : Device_Prop_Max_Texture_2D_Gather_Array;
      Max_Texture3_D : Device_Prop_Max_Texture_3D__Array;
      Max_Texture3_DAlt : Device_Prop_Max_Texture_3D_Alt_Array;
      Max_Texture_Cubemap                          : int;
      Max_Texture1_DLayered : Device_Prop_Max_Texture_1D_Layered_Array;
      Max_Texture2_DLayered : Device_Prop_Max_Texture_2D_Layered_Array;
      Max_Texture_Cubemap_Layered : Device_Prop_Max_Texture_Cubemap_Layered_Array;
      Max_Surface1_D                               : int;
      Max_Surface2_D : Device_Prop_Max_Surface_2D__Array;
      Max_Surface3_D : Device_Prop_Max_Surface_3D__Array;
      Max_Surface1_DLayered : Device_Prop_Max_Surface_1D_Layered_Array;
      Max_Surface2_DLayered : Device_Prop_Max_Surface_2D_Layered_Array;
      Max_Surface_Cubemap                          : int;
      Max_Surface_Cubemap_Layered : Device_Prop_Max_Surface_Cubemap_Layered_Array;
      Surface_Alignment                            : CUDA.Stddef.Size_T;
      Concurrent_Kernels                           : int;
      ECCEnabled                                   : int;
      Pci_Bus_ID                                   : int;
      Pci_Device_ID                                : int;
      Pci_Domain_ID                                : int;
      Tcc_Driver                                   : int;
      Async_Engine_Count                           : int;
      Unified_Addressing                           : int;
      Memory_Clock_Rate                            : int;
      Memory_Bus_Width                             : int;
      L2_Cache_Size                                : int;
      Persisting_L2_Cache_Max_Size                 : int;
      Max_Threads_Per_Multi_Processor              : int;
      Stream_Priorities_Supported                  : int;
      Global_L1_Cache_Supported                    : int;
      Local_L1_Cache_Supported                     : int;
      Shared_Mem_Per_Multiprocessor                : CUDA.Stddef.Size_T;
      Regs_Per_Multiprocessor                      : int;
      Managed_Memory                               : int;
      Is_Multi_Gpu_Board                           : int;
      Multi_Gpu_Board_Group_ID                     : int;
      Host_Native_Atomic_Supported                 : int;
      Single_To_Double_Precision_Perf_Ratio        : int;
      Pageable_Memory_Access                       : int;
      Concurrent_Managed_Access                    : int;
      Compute_Preemption_Supported                 : int;
      Can_Use_Host_Pointer_For_Registered_Mem      : int;
      Cooperative_Launch                           : int;
      Cooperative_Multi_Device_Launch              : int;
      Shared_Mem_Per_Block_Optin                   : CUDA.Stddef.Size_T;
      Pageable_Memory_Access_Uses_Host_Page_Tables : int;
      Direct_Managed_Mem_Access_From_Host          : int;
      Max_Blocks_Per_Multi_Processor               : int;
      Access_Policy_Max_Window_Size                : int;
      Reserved_Shared_Mem_Per_Block                : CUDA.Stddef.Size_T;

   end record;

   subtype Ipc_Event_Handle_St_Reserved_Array is
     Interfaces.C.char_array (0 .. 63);
   type Ipc_Event_Handle_St is record
      Reserved : Ipc_Event_Handle_St_Reserved_Array;

   end record;

   subtype Ipc_Event_Handle_T is Ipc_Event_Handle_St;
   subtype Ipc_Mem_Handle_St_Reserved_Array is
     Interfaces.C.char_array (0 .. 63);
   type Ipc_Mem_Handle_St is record
      Reserved : Ipc_Mem_Handle_St_Reserved_Array;

   end record;

   subtype Ipc_Mem_Handle_T is Ipc_Mem_Handle_St;
   subtype External_Memory_Handle_Type is unsigned;
   External_Memory_Handle_Type_Opaque_Fd            :
     External_Memory_Handle_Type renames
     udriver_types_h.cudaExternalMemoryHandleTypeOpaqueFd;
   External_Memory_Handle_Type_Opaque_Win32         :
     External_Memory_Handle_Type renames
     udriver_types_h.cudaExternalMemoryHandleTypeOpaqueWin32;
   External_Memory_Handle_Type_Opaque_Win32_Kmt     :
     External_Memory_Handle_Type renames
     udriver_types_h.cudaExternalMemoryHandleTypeOpaqueWin32Kmt;
   External_Memory_Handle_Type_D_3D_12_Heap         :
     External_Memory_Handle_Type renames
     udriver_types_h.cudaExternalMemoryHandleTypeD3D12Heap;
   External_Memory_Handle_Type_D_3D_12_Resource     :
     External_Memory_Handle_Type renames
     udriver_types_h.cudaExternalMemoryHandleTypeD3D12Resource;
   External_Memory_Handle_Type_D_3D_11_Resource     :
     External_Memory_Handle_Type renames
     udriver_types_h.cudaExternalMemoryHandleTypeD3D11Resource;
   External_Memory_Handle_Type_D_3D_11_Resource_Kmt :
     External_Memory_Handle_Type renames
     udriver_types_h.cudaExternalMemoryHandleTypeD3D11ResourceKmt;
   External_Memory_Handle_Type_Nv_Sci_Buf           :
     External_Memory_Handle_Type renames
     udriver_types_h.cudaExternalMemoryHandleTypeNvSciBuf;
   type External_Memory_Handle_Desc;
   type Anon_7;
   type Anon_8 is record
      Handle : System.Address;
      Name   : System.Address;

   end record;

   type Anon_7 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fd : int;
         when 1 =>
            Win32 : Anon_8;
         when others =>
            Nv_Sci_Buf_Object : System.Address;

      end case;
   end record;

   type External_Memory_Handle_Desc is record
      C_Type : External_Memory_Handle_Type;
      Handle : Anon_7;
      Size   : Extensions.unsigned_long_long;
      Flags  : unsigned;

   end record;

   type External_Memory_Buffer_Desc is record
      Offset : Extensions.unsigned_long_long;
      Size   : Extensions.unsigned_long_long;
      Flags  : unsigned;

   end record;

   type External_Memory_Mipmapped_Array_Desc is record
      Offset      : Extensions.unsigned_long_long;
      Format_Desc : Channel_Format_Desc;
      Extent      : Extent_T;
      Flags       : unsigned;
      Num_Levels  : unsigned;

   end record;

   subtype External_Semaphore_Handle_Type is unsigned;
   External_Semaphore_Handle_Type_Opaque_Fd                :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeOpaqueFd;
   External_Semaphore_Handle_Type_Opaque_Win32             :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeOpaqueWin32;
   External_Semaphore_Handle_Type_Opaque_Win32_Kmt         :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeOpaqueWin32Kmt;
   External_Semaphore_Handle_Type_D_3D_12_Fence            :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeD3D12Fence;
   External_Semaphore_Handle_Type_D_3D_11_Fence            :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeD3D11Fence;
   External_Semaphore_Handle_Type_Nv_Sci_Sync              :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeNvSciSync;
   External_Semaphore_Handle_Type_Keyed_Mutex              :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeKeyedMutex;
   External_Semaphore_Handle_Type_Keyed_Mutex_Kmt          :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeKeyedMutexKmt;
   External_Semaphore_Handle_Type_Timeline_Semaphore_Fd    :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeTimelineSemaphoreFd;
   External_Semaphore_Handle_Type_Timeline_Semaphore_Win32 :
     External_Semaphore_Handle_Type renames
     udriver_types_h.cudaExternalSemaphoreHandleTypeTimelineSemaphoreWin32;
   type External_Semaphore_Handle_Desc;
   type Anon_9;
   type Anon_10 is record
      Handle : System.Address;
      Name   : System.Address;

   end record;

   type Anon_9 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fd : int;
         when 1 =>
            Win32 : Anon_10;
         when others =>
            Nv_Sci_Sync_Obj : System.Address;

      end case;
   end record;

   type External_Semaphore_Handle_Desc is record
      C_Type : External_Semaphore_Handle_Type;
      Handle : Anon_9;
      Flags  : unsigned;

   end record;

   type External_Semaphore_Signal_Params_V1;
   type Anon_11;
   type Anon_12 is record
      Value : Extensions.unsigned_long_long;

   end record;

   type Anon_13 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fence : System.Address;
         when others =>
            Reserved : Extensions.unsigned_long_long;

      end case;
   end record;

   type Anon_14 is record
      Key : Extensions.unsigned_long_long;

   end record;

   type Anon_11 is record
      Fence       : Anon_12;
      Nv_Sci_Sync : Anon_13;
      Keyed_Mutex : Anon_14;

   end record;

   type External_Semaphore_Signal_Params_V1 is record
      Params : Anon_11;
      Flags  : unsigned;

   end record;

   type External_Semaphore_Wait_Params_V1;
   type Anon_15;
   type Anon_16 is record
      Value : Extensions.unsigned_long_long;

   end record;

   type Anon_17 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fence : System.Address;
         when others =>
            Reserved : Extensions.unsigned_long_long;

      end case;
   end record;

   type Anon_18 is record
      Key        : Extensions.unsigned_long_long;
      Timeout_Ms : unsigned;

   end record;

   type Anon_15 is record
      Fence       : Anon_16;
      Nv_Sci_Sync : Anon_17;
      Keyed_Mutex : Anon_18;

   end record;

   type External_Semaphore_Wait_Params_V1 is record
      Params : Anon_15;
      Flags  : unsigned;

   end record;

   type External_Semaphore_Signal_Params;
   type Anon_19;
   type Anon_20 is record
      Value : Extensions.unsigned_long_long;

   end record;

   type Anon_21 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fence : System.Address;
         when others =>
            Reserved : Extensions.unsigned_long_long;

      end case;
   end record;

   type Anon_22 is record
      Key : Extensions.unsigned_long_long;

   end record;

   type External_Semaphore_Signal_Params_Reserved_Array is
     array (0 .. 11) of unsigned;

   type Anon_19 is record
      Fence       : Anon_20;
      Nv_Sci_Sync : Anon_21;
      Keyed_Mutex : Anon_22;
      Reserved    : External_Semaphore_Signal_Params_Reserved_Array;

   end record;

   type External_Semaphore_Signal_Params_Reserved_Array is
     array (0 .. 15) of unsigned;

   type External_Semaphore_Signal_Params is record
      Params   : Anon_19;
      Flags    : unsigned;
      Reserved : External_Semaphore_Signal_Params_Reserved_Array;

   end record;

   type External_Semaphore_Wait_Params;
   type Anon_23;
   type Anon_24 is record
      Value : Extensions.unsigned_long_long;

   end record;

   type Anon_25 (Discr : unsigned := 0) is record
      case Discr is
         when 0 =>
            Fence : System.Address;
         when others =>
            Reserved : Extensions.unsigned_long_long;

      end case;
   end record;

   type Anon_26 is record
      Key        : Extensions.unsigned_long_long;
      Timeout_Ms : unsigned;

   end record;

   type External_Semaphore_Wait_Params_Reserved_Array is
     array (0 .. 9) of unsigned;

   type Anon_23 is record
      Fence       : Anon_24;
      Nv_Sci_Sync : Anon_25;
      Keyed_Mutex : Anon_26;
      Reserved    : External_Semaphore_Wait_Params_Reserved_Array;

   end record;

   type External_Semaphore_Wait_Params_Reserved_Array is
     array (0 .. 15) of unsigned;

   type External_Semaphore_Wait_Params is record
      Params   : Anon_23;
      Flags    : unsigned;
      Reserved : External_Semaphore_Wait_Params_Reserved_Array;

   end record;

   subtype Error_T is Error;
   type Stream_T is new System.Address;

   type Event_T is new System.Address;

   type Graphics_Resource_T is new System.Address;

   subtype Output_Mode_T is Output_Mode;
   type External_Memory_T is new System.Address;

   type External_Semaphore_T is new System.Address;

   type Graph_T is new System.Address;

   type Graph_Node_T is new System.Address;

   type User_Object_T is new System.Address;

   type Function_T is new System.Address;

   type Mem_Pool_T is new System.Address;

   type CGScope is (CGScope_Invalid, CGScope_Grid, CGScope_Multi_Grid);

   package Launch_Params is

      type Launch_Params is record
         Func       : System.Address;
         Grid_Dim   : CUDA.Vector_Types.Class_Dim3.Dim3;
         Block_Dim  : CUDA.Vector_Types.Class_Dim3.Dim3;
         Args       : System.Address;
         Shared_Mem : CUDA.Stddef.Size_T;
         Stream     : Stream_T;

      end record;

   end Launch_Params;
   package Kernel_Node_Params is

      type Kernel_Node_Params is record
         Func             : System.Address;
         Grid_Dim         : CUDA.Vector_Types.Class_Dim3.Dim3;
         Block_Dim        : CUDA.Vector_Types.Class_Dim3.Dim3;
         Shared_Mem_Bytes : unsigned;
         Kernel_Params    : System.Address;
         Extra            : System.Address;

      end record;

   end Kernel_Node_Params;
   type External_Semaphore_Signal_Node_Params is record
      Ext_Sem_Array : System.Address;
      Params_Array  : access External_Semaphore_Signal_Params;
      Num_Ext_Sems  : unsigned;

   end record;

   type External_Semaphore_Wait_Node_Params is record
      Ext_Sem_Array : System.Address;
      Params_Array  : access External_Semaphore_Wait_Params;
      Num_Ext_Sems  : unsigned;

   end record;

   type Graph_Node_Type is
     (Graph_Node_Type_Kernel, Graph_Node_Type_Memcpy, Graph_Node_Type_Memset,
      Graph_Node_Type_Host, Graph_Node_Type_Graph, Graph_Node_Type_Empty,
      Graph_Node_Type_Wait_Event, Graph_Node_Type_Event_Record,
      Graph_Node_Type_Ext_Semaphore_Signal, Graph_Node_Type_Ext_Semaphore_Wait,
      Graph_Node_Type_Mem_Alloc, Graph_Node_Type_Mem_Free,
      Graph_Node_Type_Count);

   type Graph_Exec_T is new System.Address;

   type Graph_Exec_Update_Result is
     (Graph_Exec_Update_Success, Graph_Exec_Update_Error,
      Graph_Exec_Update_Error_Topology_Changed,
      Graph_Exec_Update_Error_Node_Type_Changed,
      Graph_Exec_Update_Error_Function_Changed,
      Graph_Exec_Update_Error_Parameters_Changed,
      Graph_Exec_Update_Error_Not_Supported,
      Graph_Exec_Update_Error_Unsupported_Function_Change);

   type Get_Driver_Entry_Point_Flags is
     (Enable_Default, Enable_Legacy_Stream, Enable_Per_Thread_Default_Stream);

   subtype Graph_Debug_Dot_Flags is unsigned;
   Graph_Debug_Dot_Flags_Verbose                      :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsVerbose;
   Graph_Debug_Dot_Flags_Kernel_Node_Params           :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsKernelNodeParams;
   Graph_Debug_Dot_Flags_Memcpy_Node_Params           :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsMemcpyNodeParams;
   Graph_Debug_Dot_Flags_Memset_Node_Params           :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsMemsetNodeParams;
   Graph_Debug_Dot_Flags_Host_Node_Params             :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsHostNodeParams;
   Graph_Debug_Dot_Flags_Event_Node_Params            :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsEventNodeParams;
   Graph_Debug_Dot_Flags_Ext_Semas_Signal_Node_Params :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsExtSemasSignalNodeParams;
   Graph_Debug_Dot_Flags_Ext_Semas_Wait_Node_Params   :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsExtSemasWaitNodeParams;
   Graph_Debug_Dot_Flags_Kernel_Node_Attributes       :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsKernelNodeAttributes;
   Graph_Debug_Dot_Flags_Handles                      :
     Graph_Debug_Dot_Flags renames
     udriver_types_h.cudaGraphDebugDotFlagsHandles;
   subtype Graph_Instantiate_Flags is unsigned;
   Graph_Instantiate_Flag_Auto_Free_On_Launch :
     Graph_Instantiate_Flags renames
     udriver_types_h.cudaGraphInstantiateFlagAutoFreeOnLaunch;

end CUDA.Driver_Types;
