with Interfaces.C; use Interfaces.C;
with udevice_types_h;

package CUDA.Device_Types is

   type Round_Mode is
     (Round_Nearest, Round_Zero, Round_Pos_Inf, Round_Min_Inf);

end CUDA.Device_Types;
