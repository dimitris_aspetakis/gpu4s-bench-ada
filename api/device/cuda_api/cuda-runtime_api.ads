with Interfaces.C; use Interfaces.C;
with CUDA.Driver_Types;
with udriver_types_h;
with CUDA.Stddef;
with stddef_h;
with Interfaces.C.Strings;
with System;
with Interfaces.C.Extensions;
with CUDA.Vector_Types;
with uvector_types_h;
with CUDA.Texture_Types;
with utexture_types_h;
with CUDA.Surface_Types;
with usurface_types_h;
with ucuda_runtime_api_h;
with Ada.Exceptions;
with CUDA.Exceptions;
with CUDA.Exceptions;

package CUDA.Runtime_Api is

   type Device_T is new Integer;

   type Stream_Priority_Range_T is record
      Least    : Integer;
      Greatest : Integer;
   end record;

   type Graph_Node_Array_T is
     array (Integer range <>) of CUDA.Driver_Types.Graph_Node_T;

   function Grid_Dim return CUDA.Vector_Types.Dim3 with
     Inline;
   function Block_Idx return CUDA.Vector_Types.Uint3 with
     Inline;
   function Block_Dim return CUDA.Vector_Types.Dim3 with
     Inline;
   function Thread_Idx return CUDA.Vector_Types.Uint3 with
     Inline;
   function Warp_Size return Interfaces.C.Int with
     Inline;
   procedure Device_Reset;

   procedure Device_Synchronize;

   procedure Device_Set_Limit
     (Limit : CUDA.Driver_Types.Limit; Value : CUDA.Stddef.Size_T);

   function Device_Get_Limit
     (Limit : CUDA.Driver_Types.Limit) return CUDA.Stddef.Size_T;

   function Device_Get_Texture_1D_Linear_Max_Width
     (Fmt_Desc : CUDA.Driver_Types.Channel_Format_Desc; Device : Device_T)
      return CUDA.Stddef.Size_T;

   function Device_Get_Cache_Config return CUDA.Driver_Types.Func_Cache;

   function Device_Get_Stream_Priority_Range return Stream_Priority_Range_T;

   procedure Device_Set_Cache_Config
     (Cache_Config : CUDA.Driver_Types.Func_Cache);

   function Device_Get_Shared_Mem_Config
      return CUDA.Driver_Types.Shared_Mem_Config;

   procedure Device_Set_Shared_Mem_Config
     (Config : CUDA.Driver_Types.Shared_Mem_Config);

   function Device_Get_By_PCIBus_Id (Pci_Bus_Id : String) return Device_T;

   procedure Device_Get_PCIBus_Id
     (Pci_Bus_Id : String; Len : int; Device : Device_T);

   function Ipc_Get_Event_Handle
     (Event : CUDA.Driver_Types.Event_T)
      return CUDA.Driver_Types.Ipc_Event_Handle_T;

   function Ipc_Open_Event_Handle
     (Handle : CUDA.Driver_Types.Ipc_Event_Handle_T)
      return CUDA.Driver_Types.Event_T;

   function Ipc_Get_Mem_Handle
     (Dev_Ptr : System.Address) return CUDA.Driver_Types.Ipc_Mem_Handle_T;

   procedure Ipc_Open_Mem_Handle
     (Dev_Ptr : System.Address; Handle : CUDA.Driver_Types.Ipc_Mem_Handle_T;
      Flags   : unsigned);

   procedure Ipc_Close_Mem_Handle (Dev_Ptr : System.Address);

   procedure Device_Flush_GPUDirect_RDMAWrites
     (Target : CUDA.Driver_Types.Flush_GPUDirect_RDMAWrites_Target;
      Scope  : CUDA.Driver_Types.Flush_GPUDirect_RDMAWrites_Scope);

   procedure Thread_Exit;

   procedure Thread_Synchronize;

   procedure Thread_Set_Limit
     (Limit : CUDA.Driver_Types.Limit; Value : CUDA.Stddef.Size_T);

   function Thread_Get_Limit
     (Limit : CUDA.Driver_Types.Limit) return CUDA.Stddef.Size_T;

   function Thread_Get_Cache_Config return CUDA.Driver_Types.Func_Cache;

   procedure Thread_Set_Cache_Config
     (Cache_Config : CUDA.Driver_Types.Func_Cache);

   function Get_Last_Error return CUDA.Driver_Types.Error_T;

   function Peek_At_Last_Error return CUDA.Driver_Types.Error_T;

   function Get_Error_Name (Error : CUDA.Driver_Types.Error_T) return String;

   function Get_Error_String (Error : CUDA.Driver_Types.Error_T) return String;

   function Get_Device_Count return int;

   function Get_Device_Properties
     (Device : Device_T) return CUDA.Driver_Types.Device_Prop;

   function Device_Get_Attribute
     (Attr : CUDA.Driver_Types.Device_Attr; Device : Device_T) return int;

   procedure Device_Get_Default_Mem_Pool
     (Mem_Pool : System.Address; Device : Device_T);

   procedure Device_Set_Mem_Pool
     (Device : Device_T; Mem_Pool : CUDA.Driver_Types.Mem_Pool_T);

   procedure Device_Get_Mem_Pool
     (Mem_Pool : System.Address; Device : Device_T);

   procedure Device_Get_Nv_Sci_Sync_Attributes
     (Nv_Sci_Sync_Attr_List : System.Address; Device : Device_T; Flags : int);

   function Device_Get_P2_PAttribute
     (Attr       : CUDA.Driver_Types.Device_P2_PAttr; Src_Device : Device_T;
      Dst_Device : Device_T) return int;

   function Choose_Device
     (Prop : CUDA.Driver_Types.Device_Prop) return Device_T;

   procedure Set_Device (Device : Device_T);

   function Get_Device return Device_T;

   function Set_Valid_Devices (Len : int) return Device_T;

   procedure Set_Device_Flags (Flags : unsigned);

   function Get_Device_Flags return unsigned;

   function Stream_Create return CUDA.Driver_Types.Stream_T;

   function Stream_Create_With_Flags
     (Flags : unsigned) return CUDA.Driver_Types.Stream_T;

   function Stream_Create_With_Priority
     (Flags : unsigned; Priority : int) return CUDA.Driver_Types.Stream_T;

   procedure Stream_Get_Priority
     (H_Stream : CUDA.Driver_Types.Stream_T; Priority : out int);

   procedure Stream_Get_Flags
     (H_Stream : CUDA.Driver_Types.Stream_T; Flags : out unsigned);

   procedure Ctx_Reset_Persisting_L2_Cache;

   procedure Stream_Copy_Attributes
     (Dst : CUDA.Driver_Types.Stream_T; Src : CUDA.Driver_Types.Stream_T);

   procedure Stream_Get_Attribute
     (H_Stream  :     CUDA.Driver_Types.Stream_T;
      Attr      :     CUDA.Driver_Types.Stream_Attr_ID;
      Value_Out : out CUDA.Driver_Types.Stream_Attr_Value);

   procedure Stream_Set_Attribute
     (H_Stream : CUDA.Driver_Types.Stream_T;
      Attr     : CUDA.Driver_Types.Stream_Attr_ID;
      Value    : CUDA.Driver_Types.Stream_Attr_Value);

   procedure Stream_Destroy (Stream : CUDA.Driver_Types.Stream_T);

   procedure Stream_Wait_Event
     (Stream : CUDA.Driver_Types.Stream_T; Event : CUDA.Driver_Types.Event_T;
      Flags  : unsigned);

   type Stream_Callback_T is new ucuda_runtime_api_h.cudaStreamCallback_t;

   generic
      with procedure Temp_Call_1
        (Arg1 : CUDA.Driver_Types.Stream_T; Arg2 : CUDA.Driver_Types.Error_T;
         Arg3 : System.Address);
   procedure Stream_Callback_T_Gen
     (Arg1 : udriver_types_h.cudaStream_t; Arg2 : udriver_types_h.cudaError_t;
      Arg3 : System.Address);
   procedure Stream_Add_Callback
     (Stream    : CUDA.Driver_Types.Stream_T; Callback : Stream_Callback_T;
      User_Data : System.Address; Flags : unsigned);

   procedure Stream_Synchronize (Stream : CUDA.Driver_Types.Stream_T);

   procedure Stream_Query (Stream : CUDA.Driver_Types.Stream_T);

   procedure Stream_Attach_Mem_Async
     (Stream : CUDA.Driver_Types.Stream_T; Dev_Ptr : System.Address;
      Length : CUDA.Stddef.Size_T; Flags : unsigned);

   procedure Stream_Begin_Capture
     (Stream : CUDA.Driver_Types.Stream_T;
      Mode   : CUDA.Driver_Types.Stream_Capture_Mode);

   procedure Thread_Exchange_Stream_Capture_Mode
     (Mode : out CUDA.Driver_Types.Stream_Capture_Mode);

   procedure Stream_End_Capture
     (Stream : CUDA.Driver_Types.Stream_T; Graph : System.Address);

   procedure Stream_Is_Capturing
     (Stream         :     CUDA.Driver_Types.Stream_T;
      Capture_Status : out CUDA.Driver_Types.Stream_Capture_Status);

   procedure Stream_Get_Capture_Info
     (Stream         :     CUDA.Driver_Types.Stream_T;
      Capture_Status : out CUDA.Driver_Types.Stream_Capture_Status;
      Id             : out Extensions.unsigned_long_long);

   procedure Stream_Get_Capture_Info_V2
     (Stream               :     CUDA.Driver_Types.Stream_T;
      Capture_Status_Out   : out CUDA.Driver_Types.Stream_Capture_Status;
      Id_Out : out Extensions.unsigned_long_long; Graph_Out : System.Address;
      Dependencies_Out     :     System.Address;
      Num_Dependencies_Out : out CUDA.Stddef.Size_T);

   procedure Stream_Update_Capture_Dependencies
     (Stream : CUDA.Driver_Types.Stream_T; Dependencies : System.Address;
      Num_Dependencies : CUDA.Stddef.Size_T; Flags : unsigned);

   function Event_Create return CUDA.Driver_Types.Event_T;

   function Event_Create_With_Flags
     (Flags : unsigned) return CUDA.Driver_Types.Event_T;

   procedure Event_Record
     (Event : CUDA.Driver_Types.Event_T; Stream : CUDA.Driver_Types.Stream_T);

   procedure Event_Record_With_Flags
     (Event : CUDA.Driver_Types.Event_T; Stream : CUDA.Driver_Types.Stream_T;
      Flags : unsigned);

   procedure Event_Query (Event : CUDA.Driver_Types.Event_T);

   procedure Event_Synchronize (Event : CUDA.Driver_Types.Event_T);

   procedure Event_Destroy (Event : CUDA.Driver_Types.Event_T);

   function Event_Elapsed_Time
     (Start : CUDA.Driver_Types.Event_T; C_End : CUDA.Driver_Types.Event_T)
      return float;

   procedure Import_External_Memory
     (Ext_Mem_Out     : System.Address;
      Mem_Handle_Desc : CUDA.Driver_Types.External_Memory_Handle_Desc);

   procedure External_Memory_Get_Mapped_Buffer
     (Dev_Ptr : System.Address; Ext_Mem : CUDA.Driver_Types.External_Memory_T;
      Buffer_Desc : CUDA.Driver_Types.External_Memory_Buffer_Desc);

   procedure External_Memory_Get_Mapped_Mipmapped_Array
     (Mipmap : System.Address; Ext_Mem : CUDA.Driver_Types.External_Memory_T;
      Mipmap_Desc : CUDA.Driver_Types.External_Memory_Mipmapped_Array_Desc);

   procedure Destroy_External_Memory
     (Ext_Mem : CUDA.Driver_Types.External_Memory_T);

   procedure Import_External_Semaphore
     (Ext_Sem_Out     : System.Address;
      Sem_Handle_Desc : CUDA.Driver_Types.External_Semaphore_Handle_Desc);

   procedure Signal_External_Semaphores_Async_V2
     (Ext_Sem_Array : System.Address;
      Params_Array  : CUDA.Driver_Types.External_Semaphore_Signal_Params;
      Num_Ext_Sems  : unsigned; Stream : CUDA.Driver_Types.Stream_T);

   procedure Wait_External_Semaphores_Async_V2
     (Ext_Sem_Array : System.Address;
      Params_Array  : CUDA.Driver_Types.External_Semaphore_Wait_Params;
      Num_Ext_Sems  : unsigned; Stream : CUDA.Driver_Types.Stream_T);

   procedure Destroy_External_Semaphore
     (Ext_Sem : CUDA.Driver_Types.External_Semaphore_T);

   procedure Launch_Kernel
     (Func : System.Address; Grid_Dim : CUDA.Vector_Types.Class_Dim3.Dim3;
      Block_Dim  : CUDA.Vector_Types.Class_Dim3.Dim3; Args : System.Address;
      Shared_Mem : CUDA.Stddef.Size_T; Stream : CUDA.Driver_Types.Stream_T);

   procedure Launch_Cooperative_Kernel
     (Func : System.Address; Grid_Dim : CUDA.Vector_Types.Class_Dim3.Dim3;
      Block_Dim  : CUDA.Vector_Types.Class_Dim3.Dim3; Args : System.Address;
      Shared_Mem : CUDA.Stddef.Size_T; Stream : CUDA.Driver_Types.Stream_T);

   procedure Launch_Cooperative_Kernel_Multi_Device
     (Launch_Params_List : out CUDA.Driver_Types.Launch_Params.Launch_Params;
      Num_Devices        :     unsigned; Flags : unsigned);

   procedure Func_Set_Cache_Config
     (Func : System.Address; Cache_Config : CUDA.Driver_Types.Func_Cache);

   procedure Func_Set_Shared_Mem_Config
     (Func : System.Address; Config : CUDA.Driver_Types.Shared_Mem_Config);

   function Func_Get_Attributes
     (Func : System.Address) return CUDA.Driver_Types.Func_Attributes;

   procedure Func_Set_Attribute
     (Func  : System.Address; Attr : CUDA.Driver_Types.Func_Attribute;
      Value : int);

   procedure Set_Double_For_Device (D : out double);

   procedure Set_Double_For_Host (D : out double);

   procedure Launch_Host_Func
     (Stream    : CUDA.Driver_Types.Stream_T; Fn : CUDA.Driver_Types.Host_Fn_T;
      User_Data : System.Address);

   procedure Occupancy_Max_Active_Blocks_Per_Multiprocessor
     (Num_Blocks        : out int; Func : System.Address; Block_Size : int;
      Dynamic_SMem_Size :     CUDA.Stddef.Size_T);

   procedure Occupancy_Available_Dynamic_SMem_Per_Block
     (Dynamic_Smem_Size : out CUDA.Stddef.Size_T; Func : System.Address;
      Num_Blocks        :     int; Block_Size : int);

   procedure Occupancy_Max_Active_Blocks_Per_Multiprocessor_With_Flags
     (Num_Blocks        : out int; Func : System.Address; Block_Size : int;
      Dynamic_SMem_Size :     CUDA.Stddef.Size_T; Flags : unsigned);

   function Malloc_Managed
     (Size : CUDA.Stddef.Size_T; Flags : unsigned) return System.Address;

   function Malloc (Size : CUDA.Stddef.Size_T) return System.Address;

   function Malloc_Host (Size : CUDA.Stddef.Size_T) return System.Address;

   function Malloc_Pitch
     (Pitch  : out CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height :     CUDA.Stddef.Size_T) return System.Address;

   procedure Malloc_Array
     (C_Array : System.Address; Desc : CUDA.Driver_Types.Channel_Format_Desc;
      Width   : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Flags   : unsigned);

   procedure Free (Dev_Ptr : System.Address);

   procedure Free_Host (Ptr : System.Address);

   procedure Free_Array (C_Array : CUDA.Driver_Types.CUDA_Array_t);

   procedure Free_Mipmapped_Array
     (Mipmapped_Array : CUDA.Driver_Types.Mipmapped_Array_T);

   procedure Host_Alloc
     (Host : System.Address; Size : CUDA.Stddef.Size_T; Flags : unsigned);

   procedure Host_Register
     (Ptr : System.Address; Size : CUDA.Stddef.Size_T; Flags : unsigned);

   procedure Host_Unregister (Ptr : System.Address);

   procedure Host_Get_Device_Pointer
     (Device : System.Address; Host : System.Address; Flags : unsigned);

   function Host_Get_Flags (Host : System.Address) return unsigned;

   procedure Malloc_3D
     (Pitched_Dev_Ptr : out CUDA.Driver_Types.Pitched_Ptr;
      Extent          :     CUDA.Driver_Types.Extent_T);

   procedure Malloc_3D_Array
     (C_Array : System.Address; Desc : CUDA.Driver_Types.Channel_Format_Desc;
      Extent  : CUDA.Driver_Types.Extent_T; Flags : unsigned);

   procedure Malloc_Mipmapped_Array
     (Mipmapped_Array : System.Address;
      Desc            : CUDA.Driver_Types.Channel_Format_Desc;
      Extent          : CUDA.Driver_Types.Extent_T; Num_Levels : unsigned;
      Flags           : unsigned);

   procedure Get_Mipmapped_Array_Level
     (Level_Array     : System.Address;
      Mipmapped_Array : CUDA.Driver_Types.Mipmapped_Array_Const_T;
      Level           : unsigned);

   procedure Memcpy_3D (P : CUDA.Driver_Types.Memcpy_3D_Parms);

   procedure Memcpy_3D_Peer (P : CUDA.Driver_Types.Memcpy_3D_Peer_Parms);

   procedure Memcpy_3D_Async
     (P      : CUDA.Driver_Types.Memcpy_3D_Parms;
      Stream : CUDA.Driver_Types.Stream_T);

   procedure Memcpy_3D_Peer_Async
     (P      : CUDA.Driver_Types.Memcpy_3D_Peer_Parms;
      Stream : CUDA.Driver_Types.Stream_T);

   function Mem_Get_Info
     (Total : out CUDA.Stddef.Size_T) return CUDA.Stddef.Size_T;

   function CUDA_ArrayGetInfo
     (Extent  : out CUDA.Driver_Types.Extent_T; Flags : out unsigned;
      C_Array :     CUDA.Driver_Types.CUDA_Array_t)
      return CUDA.Driver_Types.Channel_Format_Desc;

   procedure CUDA_ArrayGetPlane
     (Plane_Array : System.Address; H_Array : CUDA.Driver_Types.CUDA_Array_t;
      Plane_Idx   : unsigned);

   function CUDA_ArrayGetSparseProperties
     (C_Array : CUDA.Driver_Types.CUDA_Array_t)
      return CUDA.Driver_Types.CUDA_ArraySparseProperties;

   function Mipmapped_Array_Get_Sparse_Properties
     (Mipmap : CUDA.Driver_Types.Mipmapped_Array_T)
      return CUDA.Driver_Types.CUDA_ArraySparseProperties;

   procedure Memcpy
     (Dst  : System.Address; Src : System.Address; Count : CUDA.Stddef.Size_T;
      Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_Peer
     (Dst        : System.Address; Dst_Device : Device_T; Src : System.Address;
      Src_Device : Device_T; Count : CUDA.Stddef.Size_T);

   procedure Memcpy_2D
     (Dst : System.Address; Dpitch : CUDA.Stddef.Size_T; Src : System.Address;
      Spitch : CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_2D_To_Array
     (Dst      : CUDA.Driver_Types.CUDA_Array_t; W_Offset : CUDA.Stddef.Size_T;
      H_Offset : CUDA.Stddef.Size_T; Src : System.Address;
      Spitch   : CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height   : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_2D_From_Array
     (Dst      : System.Address; Dpitch : CUDA.Stddef.Size_T;
      Src      : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset : CUDA.Stddef.Size_T; H_Offset : CUDA.Stddef.Size_T;
      Width    : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Kind     : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_2D_Array_To_Array
     (Dst : CUDA.Driver_Types.CUDA_Array_t; W_Offset_Dst : CUDA.Stddef.Size_T;
      H_Offset_Dst : CUDA.Stddef.Size_T;
      Src          : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset_Src : CUDA.Stddef.Size_T; H_Offset_Src : CUDA.Stddef.Size_T;
      Width        : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Kind         : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_To_Symbol
     (Symbol : System.Address; Src : System.Address;
      Count  : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_From_Symbol
     (Dst   : System.Address; Symbol : System.Address;
      Count : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind  : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_Async
     (Dst : System.Address; Src : System.Address; Count : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind;
      Stream : CUDA.Driver_Types.Stream_T);

   procedure Memcpy_Peer_Async
     (Dst        : System.Address; Dst_Device : Device_T; Src : System.Address;
      Src_Device : Device_T; Count : CUDA.Stddef.Size_T;
      Stream     : CUDA.Driver_Types.Stream_T);

   procedure Memcpy_2D_Async
     (Dst : System.Address; Dpitch : CUDA.Stddef.Size_T; Src : System.Address;
      Spitch : CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind;
      Stream : CUDA.Driver_Types.Stream_T);

   procedure Memcpy_2D_To_Array_Async
     (Dst      : CUDA.Driver_Types.CUDA_Array_t; W_Offset : CUDA.Stddef.Size_T;
      H_Offset : CUDA.Stddef.Size_T; Src : System.Address;
      Spitch   : CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height   : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind;
      Stream   : CUDA.Driver_Types.Stream_T);

   procedure Memcpy_2D_From_Array_Async
     (Dst      : System.Address; Dpitch : CUDA.Stddef.Size_T;
      Src      : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset : CUDA.Stddef.Size_T; H_Offset : CUDA.Stddef.Size_T;
      Width    : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Kind     : CUDA.Driver_Types.Memcpy_Kind;
      Stream   : CUDA.Driver_Types.Stream_T);

   procedure Memcpy_To_Symbol_Async
     (Symbol : System.Address; Src : System.Address;
      Count  : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind;
      Stream : CUDA.Driver_Types.Stream_T);

   procedure Memcpy_From_Symbol_Async
     (Dst    : System.Address; Symbol : System.Address;
      Count  : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind;
      Stream : CUDA.Driver_Types.Stream_T);

   procedure Memset
     (Dev_Ptr : System.Address; Value : int; Count : CUDA.Stddef.Size_T);

   procedure Memset_2D
     (Dev_Ptr : System.Address; Pitch : CUDA.Stddef.Size_T; Value : int;
      Width   : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T);

   procedure Memset_3D
     (Pitched_Dev_Ptr : CUDA.Driver_Types.Pitched_Ptr; Value : int;
      Extent          : CUDA.Driver_Types.Extent_T);

   procedure Memset_Async
     (Dev_Ptr : System.Address; Value : int; Count : CUDA.Stddef.Size_T;
      Stream  : CUDA.Driver_Types.Stream_T);

   procedure Memset_2D_Async
     (Dev_Ptr : System.Address; Pitch : CUDA.Stddef.Size_T; Value : int;
      Width   : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Stream  : CUDA.Driver_Types.Stream_T);

   procedure Memset_3D_Async
     (Pitched_Dev_Ptr : CUDA.Driver_Types.Pitched_Ptr; Value : int;
      Extent          : CUDA.Driver_Types.Extent_T;
      Stream          : CUDA.Driver_Types.Stream_T);

   procedure Get_Symbol_Address
     (Dev_Ptr : System.Address; Symbol : System.Address);

   function Get_Symbol_Size
     (Symbol : System.Address) return CUDA.Stddef.Size_T;

   procedure Mem_Prefetch_Async
     (Dev_Ptr    : System.Address; Count : CUDA.Stddef.Size_T;
      Dst_Device : Device_T; Stream : CUDA.Driver_Types.Stream_T);

   procedure Mem_Advise
     (Dev_Ptr : System.Address; Count : CUDA.Stddef.Size_T;
      Advice  : CUDA.Driver_Types.Memory_Advise; Device : Device_T);

   procedure Mem_Range_Get_Attribute
     (Data      : System.Address; Data_Size : CUDA.Stddef.Size_T;
      Attribute : CUDA.Driver_Types.Mem_Range_Attribute;
      Dev_Ptr   : System.Address; Count : CUDA.Stddef.Size_T);

   procedure Mem_Range_Get_Attributes
     (Data           :     System.Address; Data_Sizes : out CUDA.Stddef.Size_T;
      Attributes     : out CUDA.Driver_Types.Mem_Range_Attribute;
      Num_Attributes :     CUDA.Stddef.Size_T; Dev_Ptr : System.Address;
      Count          :     CUDA.Stddef.Size_T);

   procedure Memcpy_To_Array
     (Dst      : CUDA.Driver_Types.CUDA_Array_t; W_Offset : CUDA.Stddef.Size_T;
      H_Offset : CUDA.Stddef.Size_T; Src : System.Address;
      Count    : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_From_Array
     (Dst      : System.Address; Src : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset : CUDA.Stddef.Size_T; H_Offset : CUDA.Stddef.Size_T;
      Count    : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_Array_To_Array
     (Dst : CUDA.Driver_Types.CUDA_Array_t; W_Offset_Dst : CUDA.Stddef.Size_T;
      H_Offset_Dst : CUDA.Stddef.Size_T;
      Src          : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset_Src : CUDA.Stddef.Size_T; H_Offset_Src : CUDA.Stddef.Size_T;
      Count        : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Memcpy_To_Array_Async
     (Dst      : CUDA.Driver_Types.CUDA_Array_t; W_Offset : CUDA.Stddef.Size_T;
      H_Offset : CUDA.Stddef.Size_T; Src : System.Address;
      Count    : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind;
      Stream   : CUDA.Driver_Types.Stream_T);

   procedure Memcpy_From_Array_Async
     (Dst      : System.Address; Src : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset : CUDA.Stddef.Size_T; H_Offset : CUDA.Stddef.Size_T;
      Count    : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind;
      Stream   : CUDA.Driver_Types.Stream_T);

   function Malloc_Async
     (Size : CUDA.Stddef.Size_T; H_Stream : CUDA.Driver_Types.Stream_T)
      return System.Address;

   procedure Free_Async
     (Dev_Ptr : System.Address; H_Stream : CUDA.Driver_Types.Stream_T);

   procedure Mem_Pool_Trim_To
     (Mem_Pool          : CUDA.Driver_Types.Mem_Pool_T;
      Min_Bytes_To_Keep : CUDA.Stddef.Size_T);

   procedure Mem_Pool_Set_Attribute
     (Mem_Pool : CUDA.Driver_Types.Mem_Pool_T;
      Attr     : CUDA.Driver_Types.Mem_Pool_Attr; Value : System.Address);

   procedure Mem_Pool_Get_Attribute
     (Mem_Pool : CUDA.Driver_Types.Mem_Pool_T;
      Attr     : CUDA.Driver_Types.Mem_Pool_Attr; Value : System.Address);

   procedure Mem_Pool_Set_Access
     (Mem_Pool  : CUDA.Driver_Types.Mem_Pool_T;
      Desc_List : CUDA.Driver_Types.Mem_Access_Desc;
      Count     : CUDA.Stddef.Size_T);

   function Mem_Pool_Get_Access
     (Mem_Pool :     CUDA.Driver_Types.Mem_Pool_T;
      Location : out CUDA.Driver_Types.Mem_Location)
      return CUDA.Driver_Types.Mem_Access_Flags;

   procedure Mem_Pool_Create
     (Mem_Pool   : System.Address;
      Pool_Props : CUDA.Driver_Types.Mem_Pool_Props);

   procedure Mem_Pool_Destroy (Mem_Pool : CUDA.Driver_Types.Mem_Pool_T);

   procedure Malloc_From_Pool_Async
     (Ptr      : System.Address; Size : CUDA.Stddef.Size_T;
      Mem_Pool : CUDA.Driver_Types.Mem_Pool_T;
      Stream   : CUDA.Driver_Types.Stream_T);

   procedure Mem_Pool_Export_To_Shareable_Handle
     (Shareable_Handle : System.Address;
      Mem_Pool         : CUDA.Driver_Types.Mem_Pool_T;
      Handle_Type      : CUDA.Driver_Types.Mem_Allocation_Handle_Type;
      Flags            : unsigned);

   procedure Mem_Pool_Import_From_Shareable_Handle
     (Mem_Pool    : System.Address; Shareable_Handle : System.Address;
      Handle_Type : CUDA.Driver_Types.Mem_Allocation_Handle_Type;
      Flags       : unsigned);

   procedure Mem_Pool_Export_Pointer
     (Export_Data : out CUDA.Driver_Types.Mem_Pool_Ptr_Export_Data;
      Ptr         :     System.Address);

   procedure Mem_Pool_Import_Pointer
     (Ptr         : System.Address; Mem_Pool : CUDA.Driver_Types.Mem_Pool_T;
      Export_Data : out CUDA.Driver_Types.Mem_Pool_Ptr_Export_Data);

   function Pointer_Get_Attributes
     (Ptr : System.Address) return CUDA.Driver_Types.Pointer_Attributes;

   procedure Device_Can_Access_Peer
     (Can_Access_Peer : out int; Device : Device_T; Peer_Device : Device_T);

   procedure Device_Enable_Peer_Access
     (Peer_Device : Device_T; Flags : unsigned);

   procedure Device_Disable_Peer_Access (Peer_Device : Device_T);

   procedure Graphics_Unregister_Resource
     (Resource : CUDA.Driver_Types.Graphics_Resource_T);

   procedure Graphics_Resource_Set_Map_Flags
     (Resource : CUDA.Driver_Types.Graphics_Resource_T; Flags : unsigned);

   procedure Graphics_Map_Resources
     (Count  : int; Resources : System.Address;
      Stream : CUDA.Driver_Types.Stream_T);

   procedure Graphics_Unmap_Resources
     (Count  : int; Resources : System.Address;
      Stream : CUDA.Driver_Types.Stream_T);

   procedure Graphics_Resource_Get_Mapped_Pointer
     (Dev_Ptr  : System.Address; Size : out CUDA.Stddef.Size_T;
      Resource : CUDA.Driver_Types.Graphics_Resource_T);

   procedure Graphics_Sub_Resource_Get_Mapped_Array
     (C_Array   : System.Address;
      Resource : CUDA.Driver_Types.Graphics_Resource_T; Array_Index : unsigned;
      Mip_Level : unsigned);

   procedure Graphics_Resource_Get_Mapped_Mipmapped_Array
     (Mipmapped_Array : System.Address;
      Resource        : CUDA.Driver_Types.Graphics_Resource_T);

   procedure Bind_Texture
     (Offset : out CUDA.Stddef.Size_T;
      Texref : CUDA.Texture_Types.Texture_Reference; Dev_Ptr : System.Address;
      Desc : CUDA.Driver_Types.Channel_Format_Desc; Size : CUDA.Stddef.Size_T);

   procedure Bind_Texture_2D
     (Offset : out CUDA.Stddef.Size_T;
      Texref : CUDA.Texture_Types.Texture_Reference; Dev_Ptr : System.Address;
      Desc : CUDA.Driver_Types.Channel_Format_Desc; Width : CUDA.Stddef.Size_T;
      Height :     CUDA.Stddef.Size_T; Pitch : CUDA.Stddef.Size_T);

   procedure Bind_Texture_To_Array
     (Texref  : CUDA.Texture_Types.Texture_Reference;
      C_Array : CUDA.Driver_Types.CUDA_Array_const_t;
      Desc    : CUDA.Driver_Types.Channel_Format_Desc);

   procedure Bind_Texture_To_Mipmapped_Array
     (Texref          : CUDA.Texture_Types.Texture_Reference;
      Mipmapped_Array : CUDA.Driver_Types.Mipmapped_Array_Const_T;
      Desc            : CUDA.Driver_Types.Channel_Format_Desc);

   procedure Unbind_Texture (Texref : CUDA.Texture_Types.Texture_Reference);

   function Get_Texture_Alignment_Offset
     (Texref : CUDA.Texture_Types.Texture_Reference) return CUDA.Stddef.Size_T;

   procedure Get_Texture_Reference
     (Texref : System.Address; Symbol : System.Address);

   procedure Bind_Surface_To_Array
     (Surfref : CUDA.Surface_Types.Surface_Reference;
      C_Array : CUDA.Driver_Types.CUDA_Array_const_t;
      Desc    : CUDA.Driver_Types.Channel_Format_Desc);

   procedure Get_Surface_Reference
     (Surfref : System.Address; Symbol : System.Address);

   function Get_Channel_Desc
     (C_Array : CUDA.Driver_Types.CUDA_Array_const_t)
      return CUDA.Driver_Types.Channel_Format_Desc;

   function Create_Channel_Desc
     (X : int; Y : int; Z : int; W : int;
      F : CUDA.Driver_Types.Channel_Format_Kind)
      return CUDA.Driver_Types.Channel_Format_Desc;

   procedure Create_Texture_Object
     (Tex_Object    : out CUDA.Texture_Types.Texture_Object_T;
      Res_Desc      :     CUDA.Driver_Types.Resource_Desc;
      Tex_Desc      :     CUDA.Texture_Types.Texture_Desc;
      Res_View_Desc :     CUDA.Driver_Types.Resource_View_Desc);

   procedure Destroy_Texture_Object
     (Tex_Object : CUDA.Texture_Types.Texture_Object_T);

   function Get_Texture_Object_Resource_Desc
     (Tex_Object : CUDA.Texture_Types.Texture_Object_T)
      return CUDA.Driver_Types.Resource_Desc;

   function Get_Texture_Object_Texture_Desc
     (Tex_Object : CUDA.Texture_Types.Texture_Object_T)
      return CUDA.Texture_Types.Texture_Desc;

   function Get_Texture_Object_Resource_View_Desc
     (Tex_Object : CUDA.Texture_Types.Texture_Object_T)
      return CUDA.Driver_Types.Resource_View_Desc;

   procedure Create_Surface_Object
     (Surf_Object : out CUDA.Surface_Types.Surface_Object_T;
      Res_Desc    :     CUDA.Driver_Types.Resource_Desc);

   procedure Destroy_Surface_Object
     (Surf_Object : CUDA.Surface_Types.Surface_Object_T);

   function Get_Surface_Object_Resource_Desc
     (Surf_Object : CUDA.Surface_Types.Surface_Object_T)
      return CUDA.Driver_Types.Resource_Desc;

   function Driver_Get_Version return int;

   function Runtime_Get_Version return int;

   procedure Graph_Create (Graph : System.Address; Flags : unsigned);

   function Graph_Add_Kernel_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : CUDA.Driver_Types.Kernel_Node_Params.Kernel_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T;

   procedure Graph_Kernel_Node_Get_Params
     (Node        :     CUDA.Driver_Types.Graph_Node_T;
      Node_Params : out CUDA.Driver_Types.Kernel_Node_Params
        .Kernel_Node_Params);

   procedure Graph_Kernel_Node_Set_Params
     (Node        : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.Kernel_Node_Params.Kernel_Node_Params);

   procedure Graph_Kernel_Node_Copy_Attributes
     (H_Src : CUDA.Driver_Types.Graph_Node_T;
      H_Dst : CUDA.Driver_Types.Graph_Node_T);

   procedure Graph_Kernel_Node_Get_Attribute
     (H_Node    :     CUDA.Driver_Types.Graph_Node_T;
      Attr      :     CUDA.Driver_Types.Kernel_Node_Attr_ID;
      Value_Out : out CUDA.Driver_Types.Kernel_Node_Attr_Value);

   procedure Graph_Kernel_Node_Set_Attribute
     (H_Node : CUDA.Driver_Types.Graph_Node_T;
      Attr   : CUDA.Driver_Types.Kernel_Node_Attr_ID;
      Value  : CUDA.Driver_Types.Kernel_Node_Attr_Value);

   function Graph_Add_Memcpy_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Copy_Params : CUDA.Driver_Types.Memcpy_3D_Parms)
      return CUDA.Driver_Types.Graph_Node_T;

   function Graph_Add_Memcpy_Node_To_Symbol
     (Graph  : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Symbol : System.Address; Src : System.Address;
      Count  : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind)
      return CUDA.Driver_Types.Graph_Node_T;

   function Graph_Add_Memcpy_Node_From_Symbol
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Dst   : System.Address; Symbol : System.Address;
      Count : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind  : CUDA.Driver_Types.Memcpy_Kind)
      return CUDA.Driver_Types.Graph_Node_T;

   function Graph_Add_Memcpy_Node_1D
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Dst   : System.Address; Src : System.Address; Count : CUDA.Stddef.Size_T;
      Kind  : CUDA.Driver_Types.Memcpy_Kind)
      return CUDA.Driver_Types.Graph_Node_T;

   procedure Graph_Memcpy_Node_Get_Params
     (Node        :     CUDA.Driver_Types.Graph_Node_T;
      Node_Params : out CUDA.Driver_Types.Memcpy_3D_Parms);

   procedure Graph_Memcpy_Node_Set_Params
     (Node        : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.Memcpy_3D_Parms);

   procedure Graph_Memcpy_Node_Set_Params_To_Symbol
     (Node   : CUDA.Driver_Types.Graph_Node_T; Symbol : System.Address;
      Src    : System.Address; Count : CUDA.Stddef.Size_T;
      Offset : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Graph_Memcpy_Node_Set_Params_From_Symbol
     (Node   : CUDA.Driver_Types.Graph_Node_T; Dst : System.Address;
      Symbol : System.Address; Count : CUDA.Stddef.Size_T;
      Offset : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Graph_Memcpy_Node_Set_Params_1D
     (Node : CUDA.Driver_Types.Graph_Node_T; Dst : System.Address;
      Src  : System.Address; Count : CUDA.Stddef.Size_T;
      Kind : CUDA.Driver_Types.Memcpy_Kind);

   function Graph_Add_Memset_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Memset_Params : CUDA.Driver_Types.Memset_Params)
      return CUDA.Driver_Types.Graph_Node_T;

   procedure Graph_Memset_Node_Get_Params
     (Node        :     CUDA.Driver_Types.Graph_Node_T;
      Node_Params : out CUDA.Driver_Types.Memset_Params);

   procedure Graph_Memset_Node_Set_Params
     (Node        : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.Memset_Params);

   function Graph_Add_Host_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : CUDA.Driver_Types.Host_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T;

   procedure Graph_Host_Node_Get_Params
     (Node        :     CUDA.Driver_Types.Graph_Node_T;
      Node_Params : out CUDA.Driver_Types.Host_Node_Params);

   procedure Graph_Host_Node_Set_Params
     (Node        : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.Host_Node_Params);

   function Graph_Add_Child_Graph_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Child_Graph : CUDA.Driver_Types.Graph_T)
      return CUDA.Driver_Types.Graph_Node_T;

   procedure Graph_Child_Graph_Node_Get_Graph
     (Node : CUDA.Driver_Types.Graph_Node_T; Graph : System.Address);

   function Graph_Add_Empty_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T)
      return CUDA.Driver_Types.Graph_Node_T;

   function Graph_Add_Event_Record_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Event : CUDA.Driver_Types.Event_T) return CUDA.Driver_Types.Graph_Node_T;

   function Graph_Event_Record_Node_Get_Event
     (Node : CUDA.Driver_Types.Graph_Node_T) return CUDA.Driver_Types.Event_T;

   procedure Graph_Event_Record_Node_Set_Event
     (Node  : CUDA.Driver_Types.Graph_Node_T;
      Event : CUDA.Driver_Types.Event_T);

   function Graph_Add_Event_Wait_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Event : CUDA.Driver_Types.Event_T) return CUDA.Driver_Types.Graph_Node_T;

   function Graph_Event_Wait_Node_Get_Event
     (Node : CUDA.Driver_Types.Graph_Node_T) return CUDA.Driver_Types.Event_T;

   procedure Graph_Event_Wait_Node_Set_Event
     (Node  : CUDA.Driver_Types.Graph_Node_T;
      Event : CUDA.Driver_Types.Event_T);

   function Graph_Add_External_Semaphores_Signal_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : CUDA.Driver_Types.External_Semaphore_Signal_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T;

   procedure Graph_External_Semaphores_Signal_Node_Get_Params
     (H_Node     :     CUDA.Driver_Types.Graph_Node_T;
      Params_Out : out CUDA.Driver_Types
        .External_Semaphore_Signal_Node_Params);

   procedure Graph_External_Semaphores_Signal_Node_Set_Params
     (H_Node      : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.External_Semaphore_Signal_Node_Params);

   function Graph_Add_External_Semaphores_Wait_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : CUDA.Driver_Types.External_Semaphore_Wait_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T;

   procedure Graph_External_Semaphores_Wait_Node_Get_Params
     (H_Node     :     CUDA.Driver_Types.Graph_Node_T;
      Params_Out : out CUDA.Driver_Types.External_Semaphore_Wait_Node_Params);

   procedure Graph_External_Semaphores_Wait_Node_Set_Params
     (H_Node      : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.External_Semaphore_Wait_Node_Params);

   function Graph_Add_Mem_Alloc_Node
     (Graph :     CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : out CUDA.Driver_Types.Mem_Alloc_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T;

   procedure Graph_Mem_Alloc_Node_Get_Params
     (Node       :     CUDA.Driver_Types.Graph_Node_T;
      Params_Out : out CUDA.Driver_Types.Mem_Alloc_Node_Params);

   function Graph_Add_Mem_Free_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Dptr  : System.Address) return CUDA.Driver_Types.Graph_Node_T;

   procedure Graph_Mem_Free_Node_Get_Params
     (Node : CUDA.Driver_Types.Graph_Node_T; Dptr_Out : System.Address);

   procedure Device_Graph_Mem_Trim (Device : Device_T);

   procedure Device_Get_Graph_Mem_Attribute
     (Device : Device_T; Attr : CUDA.Driver_Types.Graph_Mem_Attribute_Type;
      Value  : System.Address);

   procedure Device_Set_Graph_Mem_Attribute
     (Device : Device_T; Attr : CUDA.Driver_Types.Graph_Mem_Attribute_Type;
      Value  : System.Address);

   procedure Graph_Clone
     (Graph_Clone    : System.Address;
      Original_Graph : CUDA.Driver_Types.Graph_T);

   procedure Graph_Node_Find_In_Clone
     (Node : System.Address; Original_Node : CUDA.Driver_Types.Graph_Node_T;
      Cloned_Graph : CUDA.Driver_Types.Graph_T);

   procedure Graph_Node_Get_Type
     (Node   :     CUDA.Driver_Types.Graph_Node_T;
      P_Type : out CUDA.Driver_Types.Graph_Node_Type);

   procedure Graph_Get_Nodes
     (Graph     :     CUDA.Driver_Types.Graph_T; Nodes : System.Address;
      Num_Nodes : out CUDA.Stddef.Size_T);

   procedure Graph_Get_Root_Nodes
     (Graph          : CUDA.Driver_Types.Graph_T; Root_Nodes : System.Address;
      Num_Root_Nodes : out CUDA.Stddef.Size_T);

   procedure Graph_Get_Edges
     (Graph : CUDA.Driver_Types.Graph_T; From : System.Address;
      To    : System.Address; Num_Edges : out CUDA.Stddef.Size_T);

   procedure Graph_Node_Get_Dependencies
     (Node :     CUDA.Driver_Types.Graph_Node_T; Dependencies : System.Address;
      Num_Dependencies : out CUDA.Stddef.Size_T);

   procedure Graph_Node_Get_Dependent_Nodes
     (Node : CUDA.Driver_Types.Graph_Node_T; Dependent_Nodes : System.Address;
      Num_Dependent_Nodes : out CUDA.Stddef.Size_T);

   procedure Graph_Add_Dependencies
     (Graph : CUDA.Driver_Types.Graph_T; From : System.Address;
      To    : System.Address; Num_Dependencies : CUDA.Stddef.Size_T);

   procedure Graph_Remove_Dependencies
     (Graph : CUDA.Driver_Types.Graph_T; From : System.Address;
      To    : System.Address; Num_Dependencies : CUDA.Stddef.Size_T);

   procedure Graph_Destroy_Node (Node : CUDA.Driver_Types.Graph_Node_T);

   procedure Graph_Instantiate
     (Graph_Exec  : System.Address; Graph : CUDA.Driver_Types.Graph_T;
      Error_Node  : System.Address; Log_Buffer : String;
      Buffer_Size : CUDA.Stddef.Size_T);

   procedure Graph_Instantiate_With_Flags
     (Graph_Exec : System.Address; Graph : CUDA.Driver_Types.Graph_T;
      Flags      : Extensions.unsigned_long_long);

   procedure Graph_Exec_Kernel_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.Kernel_Node_Params.Kernel_Node_Params);

   procedure Graph_Exec_Memcpy_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.Memcpy_3D_Parms);

   procedure Graph_Exec_Memcpy_Node_Set_Params_To_Symbol
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T; Symbol : System.Address;
      Src          : System.Address; Count : CUDA.Stddef.Size_T;
      Offset       : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Graph_Exec_Memcpy_Node_Set_Params_From_Symbol
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T; Dst : System.Address;
      Symbol       : System.Address; Count : CUDA.Stddef.Size_T;
      Offset       : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind);

   procedure Graph_Exec_Memcpy_Node_Set_Params_1D
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T; Dst : System.Address;
      Src          : System.Address; Count : CUDA.Stddef.Size_T;
      Kind         : CUDA.Driver_Types.Memcpy_Kind);

   procedure Graph_Exec_Memset_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.Memset_Params);

   procedure Graph_Exec_Host_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.Host_Node_Params);

   procedure Graph_Exec_Child_Graph_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Child_Graph  : CUDA.Driver_Types.Graph_T);

   procedure Graph_Exec_Event_Record_Node_Set_Event
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      H_Node       : CUDA.Driver_Types.Graph_Node_T;
      Event        : CUDA.Driver_Types.Event_T);

   procedure Graph_Exec_Event_Wait_Node_Set_Event
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      H_Node       : CUDA.Driver_Types.Graph_Node_T;
      Event        : CUDA.Driver_Types.Event_T);

   procedure Graph_Exec_External_Semaphores_Signal_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      H_Node       : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.External_Semaphore_Signal_Node_Params);

   procedure Graph_Exec_External_Semaphores_Wait_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      H_Node       : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.External_Semaphore_Wait_Node_Params);

   procedure Graph_Exec_Update
     (H_Graph_Exec      :     CUDA.Driver_Types.Graph_Exec_T;
      H_Graph : CUDA.Driver_Types.Graph_T; H_Error_Node_Out : System.Address;
      Update_Result_Out : out CUDA.Driver_Types.Graph_Exec_Update_Result);

   procedure Graph_Upload
     (Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Stream     : CUDA.Driver_Types.Stream_T);

   procedure Graph_Launch
     (Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Stream     : CUDA.Driver_Types.Stream_T);

   procedure Graph_Exec_Destroy (Graph_Exec : CUDA.Driver_Types.Graph_Exec_T);

   procedure Graph_Destroy (Graph : CUDA.Driver_Types.Graph_T);

   procedure Graph_Debug_Dot_Print
     (Graph : CUDA.Driver_Types.Graph_T; Path : String; Flags : unsigned);

   procedure User_Object_Create
     (Object_Out : System.Address; Ptr : System.Address;
      Destroy    : CUDA.Driver_Types.Host_Fn_T; Initial_Refcount : unsigned;
      Flags      : unsigned);

   procedure User_Object_Retain
     (Object : CUDA.Driver_Types.User_Object_T; Count : unsigned);

   procedure User_Object_Release
     (Object : CUDA.Driver_Types.User_Object_T; Count : unsigned);

   procedure Graph_Retain_User_Object
     (Graph  : CUDA.Driver_Types.Graph_T;
      Object : CUDA.Driver_Types.User_Object_T; Count : unsigned;
      Flags  : unsigned);

   procedure Graph_Release_User_Object
     (Graph  : CUDA.Driver_Types.Graph_T;
      Object : CUDA.Driver_Types.User_Object_T; Count : unsigned);

   procedure Get_Driver_Entry_Point
     (Symbol : String; Func_Ptr : System.Address;
      Flags  : Extensions.unsigned_long_long);

   procedure Get_Export_Table
     (Pp_Export_Table : System.Address;
      Export_Table_Id : CUDA.Driver_Types.UUID_T);

   procedure Get_Func_By_Symbol
     (Function_Ptr : System.Address; Symbol_Ptr : System.Address);

end CUDA.Runtime_Api;
