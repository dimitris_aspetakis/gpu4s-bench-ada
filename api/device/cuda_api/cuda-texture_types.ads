with Interfaces.C; use Interfaces.C;
with CUDA.Driver_Types;
with udriver_types_h;
with Interfaces.C.Extensions;
with utexture_types_h;

package CUDA.Texture_Types is

   type Texture_Address_Mode is
     (Address_Mode_Wrap, Address_Mode_Clamp, Address_Mode_Mirror,
      Address_Mode_Border);

   type Texture_Filter_Mode is (Filter_Mode_Point, Filter_Mode_Linear);

   type Texture_Read_Mode is
     (Read_Mode_Element_Type, Read_Mode_Normalized_Float);

   type Texture_Reference_Address_Mode_Array is
     array (0 .. 2) of Texture_Address_Mode;

   type Reserved_Array is array (0 .. 13) of int;

   type Texture_Reference is record
      Normalized                     : int;
      Filter_Mode                    : Texture_Filter_Mode;
      Address_Mode                   : Texture_Reference_Address_Mode_Array;
      Channel_Desc                   : CUDA.Driver_Types.Channel_Format_Desc;
      S_RGB                          : int;
      Max_Anisotropy                 : unsigned;
      Mipmap_Filter_Mode             : Texture_Filter_Mode;
      Mipmap_Level_Bias              : float;
      Min_Mipmap_Level_Clamp         : float;
      Max_Mipmap_Level_Clamp         : float;
      Disable_Trilinear_Optimization : int;
      Reserved                       : Reserved_Array;

   end record;

   type Texture_Desc_Address_Mode_Array is
     array (0 .. 2) of Texture_Address_Mode;

   type Texture_Desc_Border_Color_Array is array (0 .. 3) of float;

   type Texture_Desc is record
      Address_Mode                   : Texture_Desc_Address_Mode_Array;
      Filter_Mode                    : Texture_Filter_Mode;
      Read_Mode                      : Texture_Read_Mode;
      S_RGB                          : int;
      Border_Color                   : Texture_Desc_Border_Color_Array;
      Normalized_Coords              : int;
      Max_Anisotropy                 : unsigned;
      Mipmap_Filter_Mode             : Texture_Filter_Mode;
      Mipmap_Level_Bias              : float;
      Min_Mipmap_Level_Clamp         : float;
      Max_Mipmap_Level_Clamp         : float;
      Disable_Trilinear_Optimization : int;

   end record;

   subtype Texture_Object_T is Extensions.unsigned_long_long;

end CUDA.Texture_Types;
