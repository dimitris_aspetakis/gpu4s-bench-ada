with ucuda_runtime_api_h;
use ucuda_runtime_api_h;

package body CUDA.Runtime_Api is
   function Grid_Dim return CUDA.Vector_Types.Dim3 is
      function Nctaid_X return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.nctaid.x";
      function Nctaid_Y return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.nctaid.y";
      function Nctaid_Z return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.nctaid.z";
   begin
      return (Nctaid_X, Nctaid_Y, Nctaid_Z);
   end Grid_Dim;

   function Block_Idx return CUDA.Vector_Types.Uint3 is
      function Ctaid_X return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.ctaid.x";
      function Ctaid_Y return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.ctaid.y";
      function Ctaid_Z return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.ctaid.z";
   begin
      return (Ctaid_X, Ctaid_Y, Ctaid_Z);
   end Block_Idx;

   function Block_Dim return CUDA.Vector_Types.Dim3 is
      function Ntid_X return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.ntid.x";
      function Ntid_Y return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.ntid.y";
      function Ntid_Z return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.ntid.z";
   begin
      return (Ntid_X, Ntid_Y, Ntid_Z);
   end Block_Dim;

   function Thread_Idx return CUDA.Vector_Types.Uint3 is
      function Tid_X return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.tid.x";
      function Tid_Y return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.tid.y";
      function Tid_Z return Interfaces.C.unsigned with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.tid.z";
   begin
      return (Tid_X, Tid_Y, Tid_Z);
   end Thread_Idx;

   function Warp_Size return Interfaces.C.Int is
      function warpsize return Interfaces.C.Int with
        Inline, Import, Convention => C,
        External_Name              => "*llvm.nvvm.read.ptx.sreg.warpsize";
   begin
      return warpsize;
   end Warp_Size;---
   -- Device_Reset --
   ---

   procedure Device_Reset is
      Temp_res_1 : Integer := Integer (ucuda_runtime_api_h.cudaDeviceReset);

   begin
      null;

      if Temp_res_1 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_1)));
      end if;

   end Device_Reset;

   ---
   -- Device_Synchronize --
   ---

   procedure Device_Synchronize is
      Temp_res_1 : Integer :=
        Integer (ucuda_runtime_api_h.cudaDeviceSynchronize);

   begin
      null;

      if Temp_res_1 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_1)));
      end if;

   end Device_Synchronize;

   ---
   -- Device_Set_Limit --
   ---

   procedure Device_Set_Limit
     (Limit : CUDA.Driver_Types.Limit; Value : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaLimit with
        Address => Limit'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Value'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceSetLimit
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Device_Set_Limit;

   ---
   -- Device_Get_Limit --
   ---

   function Device_Get_Limit
     (Limit : CUDA.Driver_Types.Limit) return CUDA.Stddef.Size_T
   is

      Temp_call_1  : aliased stddef_h.size_t;
      Temp_ret_2   : aliased CUDA.Stddef.Size_T with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaLimit with
        Address => Limit'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetLimit
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Device_Get_Limit;

   ---
   -- Device_Get_Texture_1D_Linear_Max_Width --
   ---

   function Device_Get_Texture_1D_Linear_Max_Width
     (Fmt_Desc : CUDA.Driver_Types.Channel_Format_Desc; Device : Device_T)
      return CUDA.Stddef.Size_T
   is

      Temp_call_1 : aliased stddef_h.size_t;
      Temp_ret_2  : aliased CUDA.Stddef.Size_T with
        Address => Temp_call_1'Address, Import;
      Temp_call_4 : aliased constant udriver_types_h.cudaChannelFormatDesc with
        Address => Fmt_Desc'Address, Import;
      Temp_res_5  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetTexture1DLinearMaxWidth
             (Temp_call_1'Unchecked_Access, Temp_call_4'Unchecked_Access,
               int (Device)));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

      return Temp_ret_2;
   end Device_Get_Texture_1D_Linear_Max_Width;

   ---
   -- Device_Get_Cache_Config --
   ---

   function Device_Get_Cache_Config return CUDA.Driver_Types.Func_Cache is

      Temp_call_1 : aliased udriver_types_h.cudaFuncCache;
      Temp_ret_2  : aliased CUDA.Driver_Types.Func_Cache with
        Address => Temp_call_1'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetCacheConfig
             (Temp_call_1'Unchecked_Access));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_2;
   end Device_Get_Cache_Config;

   ---
   -- Device_Get_Stream_Priority_Range --
   ---

   function Device_Get_Stream_Priority_Range return Stream_Priority_Range_T is
      Temp_least_2    : aliased int;
      Temp_greatest_3 : aliased int;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetStreamPriorityRange
             (Temp_least_2'Access, Temp_greatest_3'Access));

   begin
      null;
      declare
         Temp_result_1 : Stream_Priority_Range_T :=
           (Least    => Integer (Temp_least_2),
            Greatest => Integer (Temp_greatest_3));

      begin

         if Temp_res_6 /= 0 then
            Ada.Exceptions.Raise_Exception
              (CUDA.Exceptions.Exception_Registry.Element
                 (Integer (Temp_res_6)));
         end if;

         return Temp_result_1;
      end;
   end Device_Get_Stream_Priority_Range;

   ---
   -- Device_Set_Cache_Config --
   ---

   procedure Device_Set_Cache_Config
     (Cache_Config : CUDA.Driver_Types.Func_Cache)
   is
      Temp_local_1 : aliased udriver_types_h.cudaFuncCache with
        Address => Cache_Config'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaDeviceSetCacheConfig (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Device_Set_Cache_Config;

   ---
   -- Device_Get_Shared_Mem_Config --
   ---

   function Device_Get_Shared_Mem_Config
      return CUDA.Driver_Types.Shared_Mem_Config
   is

      Temp_call_1 : aliased udriver_types_h.cudaSharedMemConfig;
      Temp_ret_2  : aliased CUDA.Driver_Types.Shared_Mem_Config with
        Address => Temp_call_1'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetSharedMemConfig
             (Temp_call_1'Unchecked_Access));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_2;
   end Device_Get_Shared_Mem_Config;

   ---
   -- Device_Set_Shared_Mem_Config --
   ---

   procedure Device_Set_Shared_Mem_Config
     (Config : CUDA.Driver_Types.Shared_Mem_Config)
   is
      Temp_local_1 : aliased udriver_types_h.cudaSharedMemConfig with
        Address => Config'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceSetSharedMemConfig (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Device_Set_Shared_Mem_Config;

   ---
   -- Device_Get_By_PCIBus_Id --
   ---

   function Device_Get_By_PCIBus_Id (Pci_Bus_Id : String) return Device_T is

      Temp_call_1     : aliased int;
      Temp_ret_2      : aliased Device_T with
        Address => Temp_call_1'Address, Import;
      Temp_c_string_3 : Interfaces.C.Strings.chars_ptr :=
        Interfaces.C.Strings.New_String (Pci_Bus_Id);

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetByPCIBusId
             (Temp_call_1'Unchecked_Access, Temp_c_string_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;
      Interfaces.C.Strings.Free (Temp_c_string_3);

      return Temp_ret_2;
   end Device_Get_By_PCIBus_Id;

   ---
   -- Device_Get_PCIBus_Id --
   ---

   procedure Device_Get_PCIBus_Id
     (Pci_Bus_Id : String; Len : int; Device : Device_T)
   is
      Temp_c_string_4 : Interfaces.C.Strings.chars_ptr :=
        Interfaces.C.Strings.New_String (Pci_Bus_Id);
      Temp_local_2    : aliased Interfaces.C.int with
        Address => Len'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetPCIBusId
             (Temp_c_string_4, Temp_local_2, int (Device)));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;
      Interfaces.C.Strings.Free (Temp_c_string_4);

   end Device_Get_PCIBus_Id;

   ---
   -- Ipc_Get_Event_Handle --
   ---

   function Ipc_Get_Event_Handle
     (Event : CUDA.Driver_Types.Event_T)
      return CUDA.Driver_Types.Ipc_Event_Handle_T
   is

      Temp_call_1  : aliased udriver_types_h.cudaIpcEventHandle_t;
      Temp_ret_2   : aliased CUDA.Driver_Types.Ipc_Event_Handle_T with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaIpcGetEventHandle
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Ipc_Get_Event_Handle;

   ---
   -- Ipc_Open_Event_Handle --
   ---

   function Ipc_Open_Event_Handle
     (Handle : CUDA.Driver_Types.Ipc_Event_Handle_T)
      return CUDA.Driver_Types.Event_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Event_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaIpcEventHandle_t with
        Address => Handle'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaIpcOpenEventHandle
             (Temp_call_2, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_1;
   end Ipc_Open_Event_Handle;

   ---
   -- Ipc_Get_Mem_Handle --
   ---

   function Ipc_Get_Mem_Handle
     (Dev_Ptr : System.Address) return CUDA.Driver_Types.Ipc_Mem_Handle_T
   is

      Temp_call_1  : aliased udriver_types_h.cudaIpcMemHandle_t;
      Temp_ret_2   : aliased CUDA.Driver_Types.Ipc_Mem_Handle_T with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaIpcGetMemHandle
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Ipc_Get_Mem_Handle;

   ---
   -- Ipc_Open_Mem_Handle --
   ---

   procedure Ipc_Open_Mem_Handle
     (Dev_Ptr : System.Address; Handle : CUDA.Driver_Types.Ipc_Mem_Handle_T;
      Flags   : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaIpcMemHandle_t with
        Address => Handle'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaIpcOpenMemHandle
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Ipc_Open_Mem_Handle;

   ---
   -- Ipc_Close_Mem_Handle --
   ---

   procedure Ipc_Close_Mem_Handle (Dev_Ptr : System.Address) is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaIpcCloseMemHandle (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Ipc_Close_Mem_Handle;

   ---
   -- Device_Flush_GPUDirect_RDMAWrites --
   ---

   procedure Device_Flush_GPUDirect_RDMAWrites
     (Target : CUDA.Driver_Types.Flush_GPUDirect_RDMAWrites_Target;
      Scope  : CUDA.Driver_Types.Flush_GPUDirect_RDMAWrites_Scope)
   is
      Temp_local_1 :
        aliased udriver_types_h.cudaFlushGPUDirectRDMAWritesTarget with
        Address => Target'Address, Import;
      Temp_local_2 :
        aliased udriver_types_h.cudaFlushGPUDirectRDMAWritesScope with
        Address => Scope'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceFlushGPUDirectRDMAWrites
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Device_Flush_GPUDirect_RDMAWrites;

   ---
   -- Thread_Exit --
   ---

   procedure Thread_Exit is
      Temp_res_1 : Integer := Integer (ucuda_runtime_api_h.cudaThreadExit);

   begin
      null;

      if Temp_res_1 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_1)));
      end if;

   end Thread_Exit;

   ---
   -- Thread_Synchronize --
   ---

   procedure Thread_Synchronize is
      Temp_res_1 : Integer :=
        Integer (ucuda_runtime_api_h.cudaThreadSynchronize);

   begin
      null;

      if Temp_res_1 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_1)));
      end if;

   end Thread_Synchronize;

   ---
   -- Thread_Set_Limit --
   ---

   procedure Thread_Set_Limit
     (Limit : CUDA.Driver_Types.Limit; Value : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaLimit with
        Address => Limit'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Value'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaThreadSetLimit
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Thread_Set_Limit;

   ---
   -- Thread_Get_Limit --
   ---

   function Thread_Get_Limit
     (Limit : CUDA.Driver_Types.Limit) return CUDA.Stddef.Size_T
   is

      Temp_call_1  : aliased stddef_h.size_t;
      Temp_ret_2   : aliased CUDA.Stddef.Size_T with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaLimit with
        Address => Limit'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaThreadGetLimit
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Thread_Get_Limit;

   ---
   -- Thread_Get_Cache_Config --
   ---

   function Thread_Get_Cache_Config return CUDA.Driver_Types.Func_Cache is

      Temp_call_1 : aliased udriver_types_h.cudaFuncCache;
      Temp_ret_2  : aliased CUDA.Driver_Types.Func_Cache with
        Address => Temp_call_1'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaThreadGetCacheConfig
             (Temp_call_1'Unchecked_Access));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_2;
   end Thread_Get_Cache_Config;

   ---
   -- Thread_Set_Cache_Config --
   ---

   procedure Thread_Set_Cache_Config
     (Cache_Config : CUDA.Driver_Types.Func_Cache)
   is
      Temp_local_1 : aliased udriver_types_h.cudaFuncCache with
        Address => Cache_Config'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaThreadSetCacheConfig (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Thread_Set_Cache_Config;

   ---
   -- Get_Last_Error --
   ---

   function Get_Last_Error return CUDA.Driver_Types.Error_T is
      Temp_result_orig_1 : aliased udriver_types_h.cudaError_t :=
        ucuda_runtime_api_h.cudaGetLastError;

      Temp_result_wrapped_1 : aliased CUDA.Driver_Types.Error_T with
        Address => Temp_result_orig_1'Address, Import;

   begin

      return Temp_result_wrapped_1;
   end Get_Last_Error;

   ---
   -- Peek_At_Last_Error --
   ---

   function Peek_At_Last_Error return CUDA.Driver_Types.Error_T is
      Temp_result_orig_1 : aliased udriver_types_h.cudaError_t :=
        ucuda_runtime_api_h.cudaPeekAtLastError;

      Temp_result_wrapped_1 : aliased CUDA.Driver_Types.Error_T with
        Address => Temp_result_orig_1'Address, Import;

   begin

      return Temp_result_wrapped_1;
   end Peek_At_Last_Error;

   ---
   -- Get_Error_Name --
   ---

   function Get_Error_Name (Error : CUDA.Driver_Types.Error_T) return String is
      Temp_local_1 : aliased udriver_types_h.cudaError_t with
        Address => Error'Address, Import;

      Temp_c_string_2 : aliased Interfaces.C.Strings.chars_ptr :=
        ucuda_runtime_api_h.cudaGetErrorName (Temp_local_1);

   begin
      null;

      return Interfaces.C.Strings.Value (Temp_c_string_2);
   end Get_Error_Name;

   ---
   -- Get_Error_String --
   ---

   function Get_Error_String (Error : CUDA.Driver_Types.Error_T) return String
   is
      Temp_local_1 : aliased udriver_types_h.cudaError_t with
        Address => Error'Address, Import;

      Temp_c_string_2 : aliased Interfaces.C.Strings.chars_ptr :=
        ucuda_runtime_api_h.cudaGetErrorString (Temp_local_1);

   begin
      null;

      return Interfaces.C.Strings.Value (Temp_c_string_2);
   end Get_Error_String;

   ---
   -- Get_Device_Count --
   ---

   function Get_Device_Count return int is

      Temp_call_1 : aliased int;
      Temp_ret_2  : aliased int with
        Address => Temp_call_1'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetDeviceCount
             (Temp_call_1'Unchecked_Access));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_2;
   end Get_Device_Count;

   ---
   -- Get_Device_Properties --
   ---

   function Get_Device_Properties
     (Device : Device_T) return CUDA.Driver_Types.Device_Prop
   is

      Temp_call_1 : aliased udriver_types_h.cudaDeviceProp;
      Temp_ret_2  : aliased CUDA.Driver_Types.Device_Prop with
        Address => Temp_call_1'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetDeviceProperties
             (Temp_call_1'Unchecked_Access, int (Device)));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Get_Device_Properties;

   ---
   -- Device_Get_Attribute --
   ---

   function Device_Get_Attribute
     (Attr : CUDA.Driver_Types.Device_Attr; Device : Device_T) return int
   is

      Temp_call_1  : aliased int;
      Temp_ret_2   : aliased int with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaDeviceAttr with
        Address => Attr'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetAttribute
             (Temp_call_1'Unchecked_Access, Temp_local_2, int (Device)));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

      return Temp_ret_2;
   end Device_Get_Attribute;

   ---
   -- Device_Get_Default_Mem_Pool --
   ---

   procedure Device_Get_Default_Mem_Pool
     (Mem_Pool : System.Address; Device : Device_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Mem_Pool'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetDefaultMemPool
             (Temp_local_1, int (Device)));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Device_Get_Default_Mem_Pool;

   ---
   -- Device_Set_Mem_Pool --
   ---

   procedure Device_Set_Mem_Pool
     (Device : Device_T; Mem_Pool : CUDA.Driver_Types.Mem_Pool_T)
   is
      Temp_local_2 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceSetMemPool
             (int (Device), Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Device_Set_Mem_Pool;

   ---
   -- Device_Get_Mem_Pool --
   ---

   procedure Device_Get_Mem_Pool (Mem_Pool : System.Address; Device : Device_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Mem_Pool'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetMemPool
             (Temp_local_1, int (Device)));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Device_Get_Mem_Pool;

   ---
   -- Device_Get_Nv_Sci_Sync_Attributes --
   ---

   procedure Device_Get_Nv_Sci_Sync_Attributes
     (Nv_Sci_Sync_Attr_List : System.Address; Device : Device_T; Flags : int)
   is
      Temp_local_1 : aliased System.Address with
        Address => Nv_Sci_Sync_Attr_List'Address, Import;
      Temp_local_3 : aliased Interfaces.C.int with
        Address => Flags'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetNvSciSyncAttributes
             (Temp_local_1, int (Device), Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Device_Get_Nv_Sci_Sync_Attributes;

   ---
   -- Device_Get_P2_PAttribute --
   ---

   function Device_Get_P2_PAttribute
     (Attr       : CUDA.Driver_Types.Device_P2_PAttr; Src_Device : Device_T;
      Dst_Device : Device_T) return int
   is

      Temp_call_1  : aliased int;
      Temp_ret_2   : aliased int with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaDeviceP2PAttr with
        Address => Attr'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetP2PAttribute
             (Temp_call_1'Unchecked_Access, Temp_local_2, int (Src_Device),
              int (Dst_Device)));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

      return Temp_ret_2;
   end Device_Get_P2_PAttribute;

   ---
   -- Choose_Device --
   ---

   function Choose_Device
     (Prop : CUDA.Driver_Types.Device_Prop) return Device_T
   is

      Temp_call_1 : aliased int;
      Temp_ret_2  : aliased Device_T with
        Address => Temp_call_1'Address, Import;
      Temp_call_3 : aliased constant udriver_types_h.cudaDeviceProp with
        Address => Prop'Address, Import;
      Temp_res_4  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaChooseDevice
             (Temp_call_1'Unchecked_Access, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

      return Temp_ret_2;
   end Choose_Device;

   ---
   -- Set_Device --
   ---

   procedure Set_Device (Device : Device_T) is
      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaSetDevice (int (Device)));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Set_Device;

   ---
   -- Get_Device --
   ---

   function Get_Device return Device_T is

      Temp_call_1 : aliased int;
      Temp_ret_2  : aliased Device_T with
        Address => Temp_call_1'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetDevice (Temp_call_1'Unchecked_Access));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_2;
   end Get_Device;

   ---
   -- Set_Valid_Devices --
   ---

   function Set_Valid_Devices (Len : int) return Device_T is

      Temp_call_1  : aliased int;
      Temp_ret_2   : aliased Device_T with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased Interfaces.C.int with
        Address => Len'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaSetValidDevices
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Set_Valid_Devices;

   ---
   -- Set_Device_Flags --
   ---

   procedure Set_Device_Flags (Flags : unsigned) is
      Temp_local_1 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaSetDeviceFlags (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Set_Device_Flags;

   ---
   -- Get_Device_Flags --
   ---

   function Get_Device_Flags return unsigned is

      Temp_call_1 : aliased unsigned;
      Temp_ret_2  : aliased unsigned with
        Address => Temp_call_1'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetDeviceFlags
             (Temp_call_1'Unchecked_Access));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_2;
   end Get_Device_Flags;

   ---
   -- Stream_Create --
   ---

   function Stream_Create return CUDA.Driver_Types.Stream_T is

      Temp_ret_1  : aliased CUDA.Driver_Types.Stream_T;
      Temp_call_2 : aliased System.Address := Temp_ret_1'Address;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaStreamCreate (Temp_call_2));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_1;
   end Stream_Create;

   ---
   -- Stream_Create_With_Flags --
   ---

   function Stream_Create_With_Flags
     (Flags : unsigned) return CUDA.Driver_Types.Stream_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Stream_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamCreateWithFlags
             (Temp_call_2, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_1;
   end Stream_Create_With_Flags;

   ---
   -- Stream_Create_With_Priority --
   ---

   function Stream_Create_With_Priority
     (Flags : unsigned; Priority : int) return CUDA.Driver_Types.Stream_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Stream_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;
      Temp_local_3 : aliased Interfaces.C.int with
        Address => Priority'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamCreateWithPriority
             (Temp_call_2, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

      return Temp_ret_1;
   end Stream_Create_With_Priority;

   ---
   -- Stream_Get_Priority --
   ---

   procedure Stream_Get_Priority
     (H_Stream : CUDA.Driver_Types.Stream_T; Priority : out int)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => H_Stream'Address, Import;
      Temp_call_3  : aliased int with
        Address => Priority'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamGetPriority
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Stream_Get_Priority;

   ---
   -- Stream_Get_Flags --
   ---

   procedure Stream_Get_Flags
     (H_Stream : CUDA.Driver_Types.Stream_T; Flags : out unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => H_Stream'Address, Import;
      Temp_call_3  : aliased unsigned with
        Address => Flags'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamGetFlags
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Stream_Get_Flags;

   ---
   -- Ctx_Reset_Persisting_L2_Cache --
   ---

   procedure Ctx_Reset_Persisting_L2_Cache is
      Temp_res_1 : Integer :=
        Integer (ucuda_runtime_api_h.cudaCtxResetPersistingL2Cache);

   begin
      null;

      if Temp_res_1 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_1)));
      end if;

   end Ctx_Reset_Persisting_L2_Cache;

   ---
   -- Stream_Copy_Attributes --
   ---

   procedure Stream_Copy_Attributes
     (Dst : CUDA.Driver_Types.Stream_T; Src : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStream_t with
        Address => Src'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamCopyAttributes
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Stream_Copy_Attributes;

   ---
   -- Stream_Get_Attribute --
   ---

   procedure Stream_Get_Attribute
     (H_Stream  :     CUDA.Driver_Types.Stream_T;
      Attr      :     CUDA.Driver_Types.Stream_Attr_ID;
      Value_Out : out CUDA.Driver_Types.Stream_Attr_Value)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => H_Stream'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStreamAttrID with
        Address => Attr'Address, Import;
      Temp_call_4  : aliased udriver_types_h.cudaStreamAttrValue with
        Address => Value_Out'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamGetAttribute
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Stream_Get_Attribute;

   ---
   -- Stream_Set_Attribute --
   ---

   procedure Stream_Set_Attribute
     (H_Stream : CUDA.Driver_Types.Stream_T;
      Attr     : CUDA.Driver_Types.Stream_Attr_ID;
      Value    : CUDA.Driver_Types.Stream_Attr_Value)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => H_Stream'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStreamAttrID with
        Address => Attr'Address, Import;
      Temp_call_4  : aliased constant udriver_types_h.cudaStreamAttrValue with
        Address => Value'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamSetAttribute
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Stream_Set_Attribute;

   ---
   -- Stream_Destroy --
   ---

   procedure Stream_Destroy (Stream : CUDA.Driver_Types.Stream_T) is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaStreamDestroy (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Stream_Destroy;

   ---
   -- Stream_Wait_Event --
   ---

   procedure Stream_Wait_Event
     (Stream : CUDA.Driver_Types.Stream_T; Event : CUDA.Driver_Types.Event_T;
      Flags  : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamWaitEvent
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Stream_Wait_Event;

   ---
   -- Stream_Callback_T_Gen --
   ---

   procedure Stream_Callback_T_Gen
     (Arg1 : udriver_types_h.cudaStream_t; Arg2 : udriver_types_h.cudaError_t;
      Arg3 : System.Address)
   is
      Temp_local_1 : aliased CUDA.Driver_Types.Stream_T with
        Address => Arg1'Address, Import;
      Temp_local_2 : aliased CUDA.Driver_Types.Error_T with
        Address => Arg2'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Arg3'Address, Import;

   begin
      Temp_Call_1 (Temp_local_1, Temp_local_2, Temp_local_3);

   end Stream_Callback_T_Gen;

   ---
   -- Stream_Add_Callback --
   ---

   procedure Stream_Add_Callback
     (Stream    : CUDA.Driver_Types.Stream_T; Callback : Stream_Callback_T;
      User_Data : System.Address; Flags : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_local_2 : aliased ucuda_runtime_api_h.cudaStreamCallback_t with
        Address => Callback'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => User_Data'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamAddCallback
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Stream_Add_Callback;

   ---
   -- Stream_Synchronize --
   ---

   procedure Stream_Synchronize (Stream : CUDA.Driver_Types.Stream_T) is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaStreamSynchronize (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Stream_Synchronize;

   ---
   -- Stream_Query --
   ---

   procedure Stream_Query (Stream : CUDA.Driver_Types.Stream_T) is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaStreamQuery (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Stream_Query;

   ---
   -- Stream_Attach_Mem_Async --
   ---

   procedure Stream_Attach_Mem_Async
     (Stream : CUDA.Driver_Types.Stream_T; Dev_Ptr : System.Address;
      Length : CUDA.Stddef.Size_T; Flags : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Length'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamAttachMemAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Stream_Attach_Mem_Async;

   ---
   -- Stream_Begin_Capture --
   ---

   procedure Stream_Begin_Capture
     (Stream : CUDA.Driver_Types.Stream_T;
      Mode   : CUDA.Driver_Types.Stream_Capture_Mode)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStreamCaptureMode with
        Address => Mode'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamBeginCapture
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Stream_Begin_Capture;

   ---
   -- Thread_Exchange_Stream_Capture_Mode --
   ---

   procedure Thread_Exchange_Stream_Capture_Mode
     (Mode : out CUDA.Driver_Types.Stream_Capture_Mode)
   is
      Temp_call_2 : aliased udriver_types_h.cudaStreamCaptureMode with
        Address => Mode'Address, Import;
      Temp_res_3  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaThreadExchangeStreamCaptureMode
             (Temp_call_2'Unchecked_Access));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Thread_Exchange_Stream_Capture_Mode;

   ---
   -- Stream_End_Capture --
   ---

   procedure Stream_End_Capture
     (Stream : CUDA.Driver_Types.Stream_T; Graph : System.Address)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Graph'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamEndCapture
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Stream_End_Capture;

   ---
   -- Stream_Is_Capturing --
   ---

   procedure Stream_Is_Capturing
     (Stream         :     CUDA.Driver_Types.Stream_T;
      Capture_Status : out CUDA.Driver_Types.Stream_Capture_Status)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_call_3  : aliased udriver_types_h.cudaStreamCaptureStatus with
        Address => Capture_Status'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamIsCapturing
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Stream_Is_Capturing;

   ---
   -- Stream_Get_Capture_Info --
   ---

   procedure Stream_Get_Capture_Info
     (Stream         :     CUDA.Driver_Types.Stream_T;
      Capture_Status : out CUDA.Driver_Types.Stream_Capture_Status;
      Id             : out Extensions.unsigned_long_long)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_call_4  : aliased udriver_types_h.cudaStreamCaptureStatus with
        Address => Capture_Status'Address, Import;
      Temp_call_5  : aliased Extensions.unsigned_long_long with
        Address => Id'Address, Import;
      Temp_res_6   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamGetCaptureInfo
             (Temp_local_1, Temp_call_4'Unchecked_Access,
              Temp_call_5'Unchecked_Access));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Stream_Get_Capture_Info;

   ---
   -- Stream_Get_Capture_Info_V2 --
   ---

   procedure Stream_Get_Capture_Info_V2
     (Stream               :     CUDA.Driver_Types.Stream_T;
      Capture_Status_Out   : out CUDA.Driver_Types.Stream_Capture_Status;
      Id_Out : out Extensions.unsigned_long_long; Graph_Out : System.Address;
      Dependencies_Out     :     System.Address;
      Num_Dependencies_Out : out CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_call_7  : aliased udriver_types_h.cudaStreamCaptureStatus with
        Address => Capture_Status_Out'Address, Import;
      Temp_call_8  : aliased Extensions.unsigned_long_long with
        Address => Id_Out'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Graph_Out'Address, Import;
      Temp_local_5 : aliased System.Address with
        Address => Dependencies_Out'Address, Import;
      Temp_call_9  : aliased stddef_h.size_t with
        Address => Num_Dependencies_Out'Address, Import;
      Temp_res_10  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamGetCaptureInfo_v2
             (Temp_local_1, Temp_call_7'Unchecked_Access,
              Temp_call_8'Unchecked_Access, Temp_local_4, Temp_local_5,
              Temp_call_9'Unchecked_Access));

   begin
      null;

      if Temp_res_10 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element
              (Integer (Temp_res_10)));
      end if;

   end Stream_Get_Capture_Info_V2;

   ---
   -- Stream_Update_Capture_Dependencies --
   ---

   procedure Stream_Update_Capture_Dependencies
     (Stream : CUDA.Driver_Types.Stream_T; Dependencies : System.Address;
      Num_Dependencies : CUDA.Stddef.Size_T; Flags : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Dependencies'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Num_Dependencies'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaStreamUpdateCaptureDependencies
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Stream_Update_Capture_Dependencies;

   ---
   -- Event_Create --
   ---

   function Event_Create return CUDA.Driver_Types.Event_T is

      Temp_ret_1  : aliased CUDA.Driver_Types.Event_T;
      Temp_call_2 : aliased System.Address := Temp_ret_1'Address;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaEventCreate (Temp_call_2));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_1;
   end Event_Create;

   ---
   -- Event_Create_With_Flags --
   ---

   function Event_Create_With_Flags
     (Flags : unsigned) return CUDA.Driver_Types.Event_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Event_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaEventCreateWithFlags
             (Temp_call_2, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_1;
   end Event_Create_With_Flags;

   ---
   -- Event_Record --
   ---

   procedure Event_Record
     (Event : CUDA.Driver_Types.Event_T; Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaEventRecord (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Event_Record;

   ---
   -- Event_Record_With_Flags --
   ---

   procedure Event_Record_With_Flags
     (Event : CUDA.Driver_Types.Event_T; Stream : CUDA.Driver_Types.Stream_T;
      Flags : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaEventRecordWithFlags
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Event_Record_With_Flags;

   ---
   -- Event_Query --
   ---

   procedure Event_Query (Event : CUDA.Driver_Types.Event_T) is
      Temp_local_1 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaEventQuery (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Event_Query;

   ---
   -- Event_Synchronize --
   ---

   procedure Event_Synchronize (Event : CUDA.Driver_Types.Event_T) is
      Temp_local_1 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaEventSynchronize (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Event_Synchronize;

   ---
   -- Event_Destroy --
   ---

   procedure Event_Destroy (Event : CUDA.Driver_Types.Event_T) is
      Temp_local_1 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaEventDestroy (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Event_Destroy;

   ---
   -- Event_Elapsed_Time --
   ---

   function Event_Elapsed_Time
     (Start : CUDA.Driver_Types.Event_T; C_End : CUDA.Driver_Types.Event_T)
      return float
   is

      Temp_call_1  : aliased float;
      Temp_ret_2   : aliased float with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaEvent_t with
        Address => Start'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaEvent_t with
        Address => C_End'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaEventElapsedTime
             (Temp_call_1'Unchecked_Access, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

      return Temp_ret_2;
   end Event_Elapsed_Time;

   ---
   -- Import_External_Memory --
   ---

   procedure Import_External_Memory
     (Ext_Mem_Out     : System.Address;
      Mem_Handle_Desc : CUDA.Driver_Types.External_Memory_Handle_Desc)
   is
      Temp_local_1 : aliased System.Address with
        Address => Ext_Mem_Out'Address, Import;
      Temp_call_3  :
        aliased constant udriver_types_h.cudaExternalMemoryHandleDesc with
        Address => Mem_Handle_Desc'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaImportExternalMemory
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Import_External_Memory;

   ---
   -- External_Memory_Get_Mapped_Buffer --
   ---

   procedure External_Memory_Get_Mapped_Buffer
     (Dev_Ptr : System.Address; Ext_Mem : CUDA.Driver_Types.External_Memory_T;
      Buffer_Desc : CUDA.Driver_Types.External_Memory_Buffer_Desc)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaExternalMemory_t with
        Address => Ext_Mem'Address, Import;
      Temp_call_4  :
        aliased constant udriver_types_h.cudaExternalMemoryBufferDesc with
        Address => Buffer_Desc'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaExternalMemoryGetMappedBuffer
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end External_Memory_Get_Mapped_Buffer;

   ---
   -- External_Memory_Get_Mapped_Mipmapped_Array --
   ---

   procedure External_Memory_Get_Mapped_Mipmapped_Array
     (Mipmap : System.Address; Ext_Mem : CUDA.Driver_Types.External_Memory_T;
      Mipmap_Desc : CUDA.Driver_Types.External_Memory_Mipmapped_Array_Desc)
   is
      Temp_local_1 : aliased System.Address with
        Address => Mipmap'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaExternalMemory_t with
        Address => Ext_Mem'Address, Import;
      Temp_call_4  :
        aliased constant udriver_types_h
          .cudaExternalMemoryMipmappedArrayDesc with
        Address => Mipmap_Desc'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaExternalMemoryGetMappedMipmappedArray
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end External_Memory_Get_Mapped_Mipmapped_Array;

   ---
   -- Destroy_External_Memory --
   ---

   procedure Destroy_External_Memory
     (Ext_Mem : CUDA.Driver_Types.External_Memory_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaExternalMemory_t with
        Address => Ext_Mem'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaDestroyExternalMemory (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Destroy_External_Memory;

   ---
   -- Import_External_Semaphore --
   ---

   procedure Import_External_Semaphore
     (Ext_Sem_Out     : System.Address;
      Sem_Handle_Desc : CUDA.Driver_Types.External_Semaphore_Handle_Desc)
   is
      Temp_local_1 : aliased System.Address with
        Address => Ext_Sem_Out'Address, Import;
      Temp_call_3  :
        aliased constant udriver_types_h.cudaExternalSemaphoreHandleDesc with
        Address => Sem_Handle_Desc'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaImportExternalSemaphore
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Import_External_Semaphore;

   ---
   -- Signal_External_Semaphores_Async_V2 --
   ---

   procedure Signal_External_Semaphores_Async_V2
     (Ext_Sem_Array : System.Address;
      Params_Array  : CUDA.Driver_Types.External_Semaphore_Signal_Params;
      Num_Ext_Sems  : unsigned; Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Ext_Sem_Array'Address, Import;
      Temp_call_5  :
        aliased constant udriver_types_h.cudaExternalSemaphoreSignalParams with
        Address => Params_Array'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Num_Ext_Sems'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaSignalExternalSemaphoresAsync_v2
             (Temp_local_1, Temp_call_5'Unchecked_Access, Temp_local_3,
              Temp_local_4));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Signal_External_Semaphores_Async_V2;

   ---
   -- Wait_External_Semaphores_Async_V2 --
   ---

   procedure Wait_External_Semaphores_Async_V2
     (Ext_Sem_Array : System.Address;
      Params_Array  : CUDA.Driver_Types.External_Semaphore_Wait_Params;
      Num_Ext_Sems  : unsigned; Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Ext_Sem_Array'Address, Import;
      Temp_call_5  :
        aliased constant udriver_types_h.cudaExternalSemaphoreWaitParams with
        Address => Params_Array'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Num_Ext_Sems'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaWaitExternalSemaphoresAsync_v2
             (Temp_local_1, Temp_call_5'Unchecked_Access, Temp_local_3,
              Temp_local_4));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Wait_External_Semaphores_Async_V2;

   ---
   -- Destroy_External_Semaphore --
   ---

   procedure Destroy_External_Semaphore
     (Ext_Sem : CUDA.Driver_Types.External_Semaphore_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaExternalSemaphore_t with
        Address => Ext_Sem'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDestroyExternalSemaphore (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Destroy_External_Semaphore;

   ---
   -- Launch_Kernel --
   ---

   procedure Launch_Kernel
     (Func : System.Address; Grid_Dim : CUDA.Vector_Types.Class_Dim3.Dim3;
      Block_Dim  : CUDA.Vector_Types.Class_Dim3.Dim3; Args : System.Address;
      Shared_Mem : CUDA.Stddef.Size_T; Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Func'Address, Import;
      Temp_local_2 : aliased uvector_types_h.Class_dim3.dim3 with
        Address => Grid_Dim'Address, Import;
      Temp_local_3 : aliased uvector_types_h.Class_dim3.dim3 with
        Address => Block_Dim'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Args'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Shared_Mem'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaLaunchKernel
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Launch_Kernel;

   ---
   -- Launch_Cooperative_Kernel --
   ---

   procedure Launch_Cooperative_Kernel
     (Func : System.Address; Grid_Dim : CUDA.Vector_Types.Class_Dim3.Dim3;
      Block_Dim  : CUDA.Vector_Types.Class_Dim3.Dim3; Args : System.Address;
      Shared_Mem : CUDA.Stddef.Size_T; Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Func'Address, Import;
      Temp_local_2 : aliased uvector_types_h.Class_dim3.dim3 with
        Address => Grid_Dim'Address, Import;
      Temp_local_3 : aliased uvector_types_h.Class_dim3.dim3 with
        Address => Block_Dim'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Args'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Shared_Mem'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaLaunchCooperativeKernel
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Launch_Cooperative_Kernel;

   ---
   -- Launch_Cooperative_Kernel_Multi_Device --
   ---

   procedure Launch_Cooperative_Kernel_Multi_Device
     (Launch_Params_List : out CUDA.Driver_Types.Launch_Params.Launch_Params;
      Num_Devices        :     unsigned; Flags : unsigned)
   is
      Temp_call_4  :
        aliased udriver_types_h.Class_cudaLaunchParams.cudaLaunchParams with
        Address => Launch_Params_List'Address, Import;
      Temp_local_2 : aliased Interfaces.C.unsigned with
        Address => Num_Devices'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaLaunchCooperativeKernelMultiDevice
             (Temp_call_4'Unchecked_Access, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Launch_Cooperative_Kernel_Multi_Device;

   ---
   -- Func_Set_Cache_Config --
   ---

   procedure Func_Set_Cache_Config
     (Func : System.Address; Cache_Config : CUDA.Driver_Types.Func_Cache)
   is
      Temp_local_1 : aliased System.Address with
        Address => Func'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaFuncCache with
        Address => Cache_Config'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaFuncSetCacheConfig
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Func_Set_Cache_Config;

   ---
   -- Func_Set_Shared_Mem_Config --
   ---

   procedure Func_Set_Shared_Mem_Config
     (Func : System.Address; Config : CUDA.Driver_Types.Shared_Mem_Config)
   is
      Temp_local_1 : aliased System.Address with
        Address => Func'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaSharedMemConfig with
        Address => Config'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaFuncSetSharedMemConfig
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Func_Set_Shared_Mem_Config;

   ---
   -- Func_Get_Attributes --
   ---

   function Func_Get_Attributes
     (Func : System.Address) return CUDA.Driver_Types.Func_Attributes
   is

      Temp_call_1  : aliased udriver_types_h.cudaFuncAttributes;
      Temp_ret_2   : aliased CUDA.Driver_Types.Func_Attributes with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Func'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaFuncGetAttributes
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Func_Get_Attributes;

   ---
   -- Func_Set_Attribute --
   ---

   procedure Func_Set_Attribute
     (Func  : System.Address; Attr : CUDA.Driver_Types.Func_Attribute;
      Value : int)
   is
      Temp_local_1 : aliased System.Address with
        Address => Func'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaFuncAttribute with
        Address => Attr'Address, Import;
      Temp_local_3 : aliased Interfaces.C.int with
        Address => Value'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaFuncSetAttribute
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Func_Set_Attribute;

   ---
   -- Set_Double_For_Device --
   ---

   procedure Set_Double_For_Device (D : out double) is
      Temp_call_2 : aliased double with
        Address => D'Address, Import;
      Temp_res_3  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaSetDoubleForDevice
             (Temp_call_2'Unchecked_Access));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Set_Double_For_Device;

   ---
   -- Set_Double_For_Host --
   ---

   procedure Set_Double_For_Host (D : out double) is
      Temp_call_2 : aliased double with
        Address => D'Address, Import;
      Temp_res_3  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaSetDoubleForHost
             (Temp_call_2'Unchecked_Access));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Set_Double_For_Host;

   ---
   -- Launch_Host_Func --
   ---

   procedure Launch_Host_Func
     (Stream    : CUDA.Driver_Types.Stream_T; Fn : CUDA.Driver_Types.Host_Fn_T;
      User_Data : System.Address)
   is
      Temp_local_1 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaHostFn_t with
        Address => Fn'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => User_Data'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaLaunchHostFunc
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Launch_Host_Func;

   ---
   -- Occupancy_Max_Active_Blocks_Per_Multiprocessor --
   ---

   procedure Occupancy_Max_Active_Blocks_Per_Multiprocessor
     (Num_Blocks        : out int; Func : System.Address; Block_Size : int;
      Dynamic_SMem_Size :     CUDA.Stddef.Size_T)
   is
      Temp_call_5  : aliased int with
        Address => Num_Blocks'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Func'Address, Import;
      Temp_local_3 : aliased Interfaces.C.int with
        Address => Block_Size'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Dynamic_SMem_Size'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaOccupancyMaxActiveBlocksPerMultiprocessor
             (Temp_call_5'Unchecked_Access, Temp_local_2, Temp_local_3,
              Temp_local_4));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Occupancy_Max_Active_Blocks_Per_Multiprocessor;

   ---
   -- Occupancy_Available_Dynamic_SMem_Per_Block --
   ---

   procedure Occupancy_Available_Dynamic_SMem_Per_Block
     (Dynamic_Smem_Size : out CUDA.Stddef.Size_T; Func : System.Address;
      Num_Blocks        :     int; Block_Size : int)
   is
      Temp_call_5  : aliased stddef_h.size_t with
        Address => Dynamic_Smem_Size'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Func'Address, Import;
      Temp_local_3 : aliased Interfaces.C.int with
        Address => Num_Blocks'Address, Import;
      Temp_local_4 : aliased Interfaces.C.int with
        Address => Block_Size'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaOccupancyAvailableDynamicSMemPerBlock
             (Temp_call_5'Unchecked_Access, Temp_local_2, Temp_local_3,
              Temp_local_4));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Occupancy_Available_Dynamic_SMem_Per_Block;

   ---
   -- Occupancy_Max_Active_Blocks_Per_Multiprocessor_With_Flags --
   ---

   procedure Occupancy_Max_Active_Blocks_Per_Multiprocessor_With_Flags
     (Num_Blocks        : out int; Func : System.Address; Block_Size : int;
      Dynamic_SMem_Size :     CUDA.Stddef.Size_T; Flags : unsigned)
   is
      Temp_call_6  : aliased int with
        Address => Num_Blocks'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Func'Address, Import;
      Temp_local_3 : aliased Interfaces.C.int with
        Address => Block_Size'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Dynamic_SMem_Size'Address, Import;
      Temp_local_5 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h
             .cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
             (Temp_call_6'Unchecked_Access, Temp_local_2, Temp_local_3,
              Temp_local_4, Temp_local_5));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Occupancy_Max_Active_Blocks_Per_Multiprocessor_With_Flags;

   ---
   -- Malloc_Managed --
   ---

   function Malloc_Managed
     (Size : CUDA.Stddef.Size_T; Flags : unsigned) return System.Address
   is

      Temp_ret_1   : aliased System.Address;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Size'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMallocManaged
             (Temp_call_2, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

      return Temp_ret_1;
   end Malloc_Managed;

   ---
   -- Malloc --
   ---

   function Malloc (Size : CUDA.Stddef.Size_T) return System.Address is

      Temp_ret_1   : aliased System.Address;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Size'Address, Import;

      Temp_res_3 : Integer :=
        Integer (ucuda_runtime_api_h.cudaMalloc (Temp_call_2, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_1;
   end Malloc;

   ---
   -- Malloc_Host --
   ---

   function Malloc_Host (Size : CUDA.Stddef.Size_T) return System.Address is

      Temp_ret_1   : aliased System.Address;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Size'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMallocHost (Temp_call_2, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_1;
   end Malloc_Host;

   ---
   -- Malloc_Pitch --
   ---

   function Malloc_Pitch
     (Pitch  : out CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height :     CUDA.Stddef.Size_T) return System.Address
   is

      Temp_ret_1   : aliased System.Address;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_call_5  : aliased stddef_h.size_t with
        Address => Pitch'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Height'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMallocPitch
             (Temp_call_2, Temp_call_5'Unchecked_Access, Temp_local_3,
              Temp_local_4));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

      return Temp_ret_1;
   end Malloc_Pitch;

   ---
   -- Malloc_Array --
   ---

   procedure Malloc_Array
     (C_Array : System.Address; Desc : CUDA.Driver_Types.Channel_Format_Desc;
      Width   : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Flags   : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => C_Array'Address, Import;
      Temp_call_6 : aliased constant udriver_types_h.cudaChannelFormatDesc with
        Address => Desc'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_5 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMallocArray
             (Temp_local_1, Temp_call_6'Unchecked_Access, Temp_local_3,
              Temp_local_4, Temp_local_5));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Malloc_Array;

   ---
   -- Free --
   ---

   procedure Free (Dev_Ptr : System.Address) is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaFree (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Free;

   ---
   -- Free_Host --
   ---

   procedure Free_Host (Ptr : System.Address) is
      Temp_local_1 : aliased System.Address with
        Address => Ptr'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaFreeHost (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Free_Host;

   ---
   -- Free_Array --
   ---

   procedure Free_Array (C_Array : CUDA.Driver_Types.CUDA_Array_t) is
      Temp_local_1 : aliased udriver_types_h.cudaArray_t with
        Address => C_Array'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaFreeArray (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Free_Array;

   ---
   -- Free_Mipmapped_Array --
   ---

   procedure Free_Mipmapped_Array
     (Mipmapped_Array : CUDA.Driver_Types.Mipmapped_Array_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaMipmappedArray_t with
        Address => Mipmapped_Array'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaFreeMipmappedArray (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Free_Mipmapped_Array;

   ---
   -- Host_Alloc --
   ---

   procedure Host_Alloc
     (Host : System.Address; Size : CUDA.Stddef.Size_T; Flags : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Host'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Size'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaHostAlloc
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Host_Alloc;

   ---
   -- Host_Register --
   ---

   procedure Host_Register
     (Ptr : System.Address; Size : CUDA.Stddef.Size_T; Flags : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Ptr'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Size'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaHostRegister
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Host_Register;

   ---
   -- Host_Unregister --
   ---

   procedure Host_Unregister (Ptr : System.Address) is
      Temp_local_1 : aliased System.Address with
        Address => Ptr'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaHostUnregister (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Host_Unregister;

   ---
   -- Host_Get_Device_Pointer --
   ---

   procedure Host_Get_Device_Pointer
     (Device : System.Address; Host : System.Address; Flags : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Device'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Host'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaHostGetDevicePointer
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Host_Get_Device_Pointer;

   ---
   -- Host_Get_Flags --
   ---

   function Host_Get_Flags (Host : System.Address) return unsigned is

      Temp_call_1  : aliased unsigned;
      Temp_ret_2   : aliased unsigned with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Host'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaHostGetFlags
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Host_Get_Flags;

   ---
   -- Malloc_3D --
   ---

   procedure Malloc_3D
     (Pitched_Dev_Ptr : out CUDA.Driver_Types.Pitched_Ptr;
      Extent          :     CUDA.Driver_Types.Extent_T)
   is
      Temp_call_3  : aliased udriver_types_h.cudaPitchedPtr with
        Address => Pitched_Dev_Ptr'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaExtent with
        Address => Extent'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMalloc3D
             (Temp_call_3'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Malloc_3D;

   ---
   -- Malloc_3D_Array --
   ---

   procedure Malloc_3D_Array
     (C_Array : System.Address; Desc : CUDA.Driver_Types.Channel_Format_Desc;
      Extent  : CUDA.Driver_Types.Extent_T; Flags : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => C_Array'Address, Import;
      Temp_call_5 : aliased constant udriver_types_h.cudaChannelFormatDesc with
        Address => Desc'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaExtent with
        Address => Extent'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMalloc3DArray
             (Temp_local_1, Temp_call_5'Unchecked_Access, Temp_local_3,
              Temp_local_4));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Malloc_3D_Array;

   ---
   -- Malloc_Mipmapped_Array --
   ---

   procedure Malloc_Mipmapped_Array
     (Mipmapped_Array : System.Address;
      Desc            : CUDA.Driver_Types.Channel_Format_Desc;
      Extent          : CUDA.Driver_Types.Extent_T; Num_Levels : unsigned;
      Flags           : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Mipmapped_Array'Address, Import;
      Temp_call_6 : aliased constant udriver_types_h.cudaChannelFormatDesc with
        Address => Desc'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaExtent with
        Address => Extent'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Num_Levels'Address, Import;
      Temp_local_5 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMallocMipmappedArray
             (Temp_local_1, Temp_call_6'Unchecked_Access, Temp_local_3,
              Temp_local_4, Temp_local_5));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Malloc_Mipmapped_Array;

   ---
   -- Get_Mipmapped_Array_Level --
   ---

   procedure Get_Mipmapped_Array_Level
     (Level_Array     : System.Address;
      Mipmapped_Array : CUDA.Driver_Types.Mipmapped_Array_Const_T;
      Level           : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Level_Array'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaMipmappedArray_const_t with
        Address => Mipmapped_Array'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Level'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetMipmappedArrayLevel
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Get_Mipmapped_Array_Level;

   ---
   -- Memcpy_3D --
   ---

   procedure Memcpy_3D (P : CUDA.Driver_Types.Memcpy_3D_Parms) is
      Temp_call_2 : aliased constant udriver_types_h.cudaMemcpy3DParms with
        Address => P'Address, Import;
      Temp_res_3  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy3D (Temp_call_2'Unchecked_Access));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Memcpy_3D;

   ---
   -- Memcpy_3D_Peer --
   ---

   procedure Memcpy_3D_Peer (P : CUDA.Driver_Types.Memcpy_3D_Peer_Parms) is
      Temp_call_2 : aliased constant udriver_types_h.cudaMemcpy3DPeerParms with
        Address => P'Address, Import;
      Temp_res_3  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy3DPeer
             (Temp_call_2'Unchecked_Access));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Memcpy_3D_Peer;

   ---
   -- Memcpy_3D_Async --
   ---

   procedure Memcpy_3D_Async
     (P      : CUDA.Driver_Types.Memcpy_3D_Parms;
      Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_call_3  : aliased constant udriver_types_h.cudaMemcpy3DParms with
        Address => P'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy3DAsync
             (Temp_call_3'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Memcpy_3D_Async;

   ---
   -- Memcpy_3D_Peer_Async --
   ---

   procedure Memcpy_3D_Peer_Async
     (P      : CUDA.Driver_Types.Memcpy_3D_Peer_Parms;
      Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_call_3 : aliased constant udriver_types_h.cudaMemcpy3DPeerParms with
        Address => P'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy3DPeerAsync
             (Temp_call_3'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Memcpy_3D_Peer_Async;

   ---
   -- Mem_Get_Info --
   ---

   function Mem_Get_Info
     (Total : out CUDA.Stddef.Size_T) return CUDA.Stddef.Size_T
   is

      Temp_call_1 : aliased stddef_h.size_t;
      Temp_ret_2  : aliased CUDA.Stddef.Size_T with
        Address => Temp_call_1'Address, Import;
      Temp_call_3 : aliased stddef_h.size_t with
        Address => Total'Address, Import;
      Temp_res_4  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemGetInfo
             (Temp_call_1'Unchecked_Access, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

      return Temp_ret_2;
   end Mem_Get_Info;

   ---
   -- CUDA_ArrayGetInfo --
   ---

   function CUDA_ArrayGetInfo
     (Extent  : out CUDA.Driver_Types.Extent_T; Flags : out unsigned;
      C_Array :     CUDA.Driver_Types.CUDA_Array_t)
      return CUDA.Driver_Types.Channel_Format_Desc
   is

      Temp_call_1  : aliased udriver_types_h.cudaChannelFormatDesc;
      Temp_ret_2   : aliased CUDA.Driver_Types.Channel_Format_Desc with
        Address => Temp_call_1'Address, Import;
      Temp_call_5  : aliased udriver_types_h.cudaExtent with
        Address => Extent'Address, Import;
      Temp_call_6  : aliased unsigned with
        Address => Flags'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaArray_t with
        Address => C_Array'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaArrayGetInfo
             (Temp_call_1'Unchecked_Access, Temp_call_5'Unchecked_Access,
              Temp_call_6'Unchecked_Access, Temp_local_4));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

      return Temp_ret_2;
   end CUDA_ArrayGetInfo;

   ---
   -- CUDA_ArrayGetPlane --
   ---

   procedure CUDA_ArrayGetPlane
     (Plane_Array : System.Address; H_Array : CUDA.Driver_Types.CUDA_Array_t;
      Plane_Idx   : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Plane_Array'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaArray_t with
        Address => H_Array'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Plane_Idx'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaArrayGetPlane
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end CUDA_ArrayGetPlane;

   ---
   -- CUDA_ArrayGetSparseProperties --
   ---

   function CUDA_ArrayGetSparseProperties
     (C_Array : CUDA.Driver_Types.CUDA_Array_t)
      return CUDA.Driver_Types.CUDA_ArraySparseProperties
   is

      Temp_call_1  : aliased udriver_types_h.cudaArraySparseProperties;
      Temp_ret_2   : aliased CUDA.Driver_Types.CUDA_ArraySparseProperties with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaArray_t with
        Address => C_Array'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaArrayGetSparseProperties
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end CUDA_ArrayGetSparseProperties;

   ---
   -- Mipmapped_Array_Get_Sparse_Properties --
   ---

   function Mipmapped_Array_Get_Sparse_Properties
     (Mipmap : CUDA.Driver_Types.Mipmapped_Array_T)
      return CUDA.Driver_Types.CUDA_ArraySparseProperties
   is

      Temp_call_1  : aliased udriver_types_h.cudaArraySparseProperties;
      Temp_ret_2   : aliased CUDA.Driver_Types.CUDA_ArraySparseProperties with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaMipmappedArray_t with
        Address => Mipmap'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMipmappedArrayGetSparseProperties
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Mipmapped_Array_Get_Sparse_Properties;

   ---
   -- Memcpy --
   ---

   procedure Memcpy
     (Dst  : System.Address; Src : System.Address; Count : CUDA.Stddef.Size_T;
      Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Memcpy;

   ---
   -- Memcpy_Peer --
   ---

   procedure Memcpy_Peer
     (Dst        : System.Address; Dst_Device : Device_T; Src : System.Address;
      Src_Device : Device_T; Count : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyPeer
             (Temp_local_1, int (Dst_Device), Temp_local_3, int (Src_Device),
              Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Memcpy_Peer;

   ---
   -- Memcpy_2D --
   ---

   procedure Memcpy_2D
     (Dst : System.Address; Dpitch : CUDA.Stddef.Size_T; Src : System.Address;
      Spitch : CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Dpitch'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Spitch'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_7 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_8 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy2D
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7));

   begin
      null;

      if Temp_res_8 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_8)));
      end if;

   end Memcpy_2D;

   ---
   -- Memcpy_2D_To_Array --
   ---

   procedure Memcpy_2D_To_Array
     (Dst      : CUDA.Driver_Types.CUDA_Array_t; W_Offset : CUDA.Stddef.Size_T;
      H_Offset : CUDA.Stddef.Size_T; Src : System.Address;
      Spitch   : CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height   : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaArray_t with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => W_Offset'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => H_Offset'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Spitch'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_8 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_9 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy2DToArray
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7, Temp_local_8));

   begin
      null;

      if Temp_res_9 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_9)));
      end if;

   end Memcpy_2D_To_Array;

   ---
   -- Memcpy_2D_From_Array --
   ---

   procedure Memcpy_2D_From_Array
     (Dst      : System.Address; Dpitch : CUDA.Stddef.Size_T;
      Src      : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset : CUDA.Stddef.Size_T; H_Offset : CUDA.Stddef.Size_T;
      Width    : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Kind     : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Dpitch'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaArray_const_t with
        Address => Src'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => W_Offset'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => H_Offset'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_8 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_9 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy2DFromArray
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7, Temp_local_8));

   begin
      null;

      if Temp_res_9 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_9)));
      end if;

   end Memcpy_2D_From_Array;

   ---
   -- Memcpy_2D_Array_To_Array --
   ---

   procedure Memcpy_2D_Array_To_Array
     (Dst : CUDA.Driver_Types.CUDA_Array_t; W_Offset_Dst : CUDA.Stddef.Size_T;
      H_Offset_Dst : CUDA.Stddef.Size_T;
      Src          : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset_Src : CUDA.Stddef.Size_T; H_Offset_Src : CUDA.Stddef.Size_T;
      Width        : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Kind         : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaArray_t with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => W_Offset_Dst'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => H_Offset_Dst'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaArray_const_t with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => W_Offset_Src'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => H_Offset_Src'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_8 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_9 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_10 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy2DArrayToArray
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7, Temp_local_8,
              Temp_local_9));

   begin
      null;

      if Temp_res_10 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element
              (Integer (Temp_res_10)));
      end if;

   end Memcpy_2D_Array_To_Array;

   ---
   -- Memcpy_To_Symbol --
   ---

   procedure Memcpy_To_Symbol
     (Symbol : System.Address; Src : System.Address;
      Count  : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyToSymbol
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Memcpy_To_Symbol;

   ---
   -- Memcpy_From_Symbol --
   ---

   procedure Memcpy_From_Symbol
     (Dst   : System.Address; Symbol : System.Address;
      Count : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind  : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyFromSymbol
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Memcpy_From_Symbol;

   ---
   -- Memcpy_Async --
   ---

   procedure Memcpy_Async
     (Dst : System.Address; Src : System.Address; Count : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind;
      Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Memcpy_Async;

   ---
   -- Memcpy_Peer_Async --
   ---

   procedure Memcpy_Peer_Async
     (Dst        : System.Address; Dst_Device : Device_T; Src : System.Address;
      Src_Device : Device_T; Count : CUDA.Stddef.Size_T;
      Stream     : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyPeerAsync
             (Temp_local_1, int (Dst_Device), Temp_local_3, int (Src_Device),
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Memcpy_Peer_Async;

   ---
   -- Memcpy_2D_Async --
   ---

   procedure Memcpy_2D_Async
     (Dst : System.Address; Dpitch : CUDA.Stddef.Size_T; Src : System.Address;
      Spitch : CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind;
      Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Dpitch'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Spitch'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_7 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;
      Temp_local_8 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_9 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy2DAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7, Temp_local_8));

   begin
      null;

      if Temp_res_9 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_9)));
      end if;

   end Memcpy_2D_Async;

   ---
   -- Memcpy_2D_To_Array_Async --
   ---

   procedure Memcpy_2D_To_Array_Async
     (Dst      : CUDA.Driver_Types.CUDA_Array_t; W_Offset : CUDA.Stddef.Size_T;
      H_Offset : CUDA.Stddef.Size_T; Src : System.Address;
      Spitch   : CUDA.Stddef.Size_T; Width : CUDA.Stddef.Size_T;
      Height   : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind;
      Stream   : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaArray_t with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => W_Offset'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => H_Offset'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Spitch'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_8 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;
      Temp_local_9 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_10 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy2DToArrayAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7, Temp_local_8,
              Temp_local_9));

   begin
      null;

      if Temp_res_10 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element
              (Integer (Temp_res_10)));
      end if;

   end Memcpy_2D_To_Array_Async;

   ---
   -- Memcpy_2D_From_Array_Async --
   ---

   procedure Memcpy_2D_From_Array_Async
     (Dst      : System.Address; Dpitch : CUDA.Stddef.Size_T;
      Src      : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset : CUDA.Stddef.Size_T; H_Offset : CUDA.Stddef.Size_T;
      Width    : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Kind     : CUDA.Driver_Types.Memcpy_Kind;
      Stream   : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Dpitch'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaArray_const_t with
        Address => Src'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => W_Offset'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => H_Offset'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_8 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;
      Temp_local_9 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_10 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpy2DFromArrayAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7, Temp_local_8,
              Temp_local_9));

   begin
      null;

      if Temp_res_10 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element
              (Integer (Temp_res_10)));
      end if;

   end Memcpy_2D_From_Array_Async;

   ---
   -- Memcpy_To_Symbol_Async --
   ---

   procedure Memcpy_To_Symbol_Async
     (Symbol : System.Address; Src : System.Address;
      Count  : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind;
      Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyToSymbolAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Memcpy_To_Symbol_Async;

   ---
   -- Memcpy_From_Symbol_Async --
   ---

   procedure Memcpy_From_Symbol_Async
     (Dst    : System.Address; Symbol : System.Address;
      Count  : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind;
      Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyFromSymbolAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Memcpy_From_Symbol_Async;

   ---
   -- Memset --
   ---

   procedure Memset
     (Dev_Ptr : System.Address; Value : int; Count : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased Interfaces.C.int with
        Address => Value'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Count'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemset
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Memset;

   ---
   -- Memset_2D --
   ---

   procedure Memset_2D
     (Dev_Ptr : System.Address; Pitch : CUDA.Stddef.Size_T; Value : int;
      Width   : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Pitch'Address, Import;
      Temp_local_3 : aliased Interfaces.C.int with
        Address => Value'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Height'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemset2D
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Memset_2D;

   ---
   -- Memset_3D --
   ---

   procedure Memset_3D
     (Pitched_Dev_Ptr : CUDA.Driver_Types.Pitched_Ptr; Value : int;
      Extent          : CUDA.Driver_Types.Extent_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaPitchedPtr with
        Address => Pitched_Dev_Ptr'Address, Import;
      Temp_local_2 : aliased Interfaces.C.int with
        Address => Value'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaExtent with
        Address => Extent'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemset3D
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Memset_3D;

   ---
   -- Memset_Async --
   ---

   procedure Memset_Async
     (Dev_Ptr : System.Address; Value : int; Count : CUDA.Stddef.Size_T;
      Stream  : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased Interfaces.C.int with
        Address => Value'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemsetAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Memset_Async;

   ---
   -- Memset_2D_Async --
   ---

   procedure Memset_2D_Async
     (Dev_Ptr : System.Address; Pitch : CUDA.Stddef.Size_T; Value : int;
      Width   : CUDA.Stddef.Size_T; Height : CUDA.Stddef.Size_T;
      Stream  : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Pitch'Address, Import;
      Temp_local_3 : aliased Interfaces.C.int with
        Address => Value'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemset2DAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Memset_2D_Async;

   ---
   -- Memset_3D_Async --
   ---

   procedure Memset_3D_Async
     (Pitched_Dev_Ptr : CUDA.Driver_Types.Pitched_Ptr; Value : int;
      Extent : CUDA.Driver_Types.Extent_T; Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaPitchedPtr with
        Address => Pitched_Dev_Ptr'Address, Import;
      Temp_local_2 : aliased Interfaces.C.int with
        Address => Value'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaExtent with
        Address => Extent'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemset3DAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Memset_3D_Async;

   ---
   -- Get_Symbol_Address --
   ---

   procedure Get_Symbol_Address
     (Dev_Ptr : System.Address; Symbol : System.Address)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Symbol'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetSymbolAddress
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Get_Symbol_Address;

   ---
   -- Get_Symbol_Size --
   ---

   function Get_Symbol_Size (Symbol : System.Address) return CUDA.Stddef.Size_T
   is

      Temp_call_1  : aliased stddef_h.size_t;
      Temp_ret_2   : aliased CUDA.Stddef.Size_T with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Symbol'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetSymbolSize
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Get_Symbol_Size;

   ---
   -- Mem_Prefetch_Async --
   ---

   procedure Mem_Prefetch_Async
     (Dev_Ptr    : System.Address; Count : CUDA.Stddef.Size_T;
      Dst_Device : Device_T; Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPrefetchAsync
             (Temp_local_1, Temp_local_2, int (Dst_Device), Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Mem_Prefetch_Async;

   ---
   -- Mem_Advise --
   ---

   procedure Mem_Advise
     (Dev_Ptr : System.Address; Count : CUDA.Stddef.Size_T;
      Advice  : CUDA.Driver_Types.Memory_Advise; Device : Device_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaMemoryAdvise with
        Address => Advice'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemAdvise
             (Temp_local_1, Temp_local_2, Temp_local_3, int (Device)));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Mem_Advise;

   ---
   -- Mem_Range_Get_Attribute --
   ---

   procedure Mem_Range_Get_Attribute
     (Data      : System.Address; Data_Size : CUDA.Stddef.Size_T;
      Attribute : CUDA.Driver_Types.Mem_Range_Attribute;
      Dev_Ptr   : System.Address; Count : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Data'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Data_Size'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaMemRangeAttribute with
        Address => Attribute'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemRangeGetAttribute
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Mem_Range_Get_Attribute;

   ---
   -- Mem_Range_Get_Attributes --
   ---

   procedure Mem_Range_Get_Attributes
     (Data           :     System.Address; Data_Sizes : out CUDA.Stddef.Size_T;
      Attributes     : out CUDA.Driver_Types.Mem_Range_Attribute;
      Num_Attributes :     CUDA.Stddef.Size_T; Dev_Ptr : System.Address;
      Count          :     CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Data'Address, Import;
      Temp_call_7  : aliased stddef_h.size_t with
        Address => Data_Sizes'Address, Import;
      Temp_call_8  : aliased udriver_types_h.cudaMemRangeAttribute with
        Address => Attributes'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Num_Attributes'Address, Import;
      Temp_local_5 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Count'Address, Import;

      Temp_res_9 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemRangeGetAttributes
             (Temp_local_1, Temp_call_7'Unchecked_Access,
              Temp_call_8'Unchecked_Access, Temp_local_4, Temp_local_5,
              Temp_local_6));

   begin
      null;

      if Temp_res_9 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_9)));
      end if;

   end Mem_Range_Get_Attributes;

   ---
   -- Memcpy_To_Array --
   ---

   procedure Memcpy_To_Array
     (Dst      : CUDA.Driver_Types.CUDA_Array_t; W_Offset : CUDA.Stddef.Size_T;
      H_Offset : CUDA.Stddef.Size_T; Src : System.Address;
      Count    : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaArray_t with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => W_Offset'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => H_Offset'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyToArray
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Memcpy_To_Array;

   ---
   -- Memcpy_From_Array --
   ---

   procedure Memcpy_From_Array
     (Dst      : System.Address; Src : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset : CUDA.Stddef.Size_T; H_Offset : CUDA.Stddef.Size_T;
      Count    : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaArray_const_t with
        Address => Src'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => W_Offset'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => H_Offset'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyFromArray
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Memcpy_From_Array;

   ---
   -- Memcpy_Array_To_Array --
   ---

   procedure Memcpy_Array_To_Array
     (Dst : CUDA.Driver_Types.CUDA_Array_t; W_Offset_Dst : CUDA.Stddef.Size_T;
      H_Offset_Dst : CUDA.Stddef.Size_T;
      Src          : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset_Src : CUDA.Stddef.Size_T; H_Offset_Src : CUDA.Stddef.Size_T;
      Count        : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaArray_t with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => W_Offset_Dst'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => H_Offset_Dst'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaArray_const_t with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => W_Offset_Src'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => H_Offset_Src'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_8 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_9 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyArrayToArray
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7, Temp_local_8));

   begin
      null;

      if Temp_res_9 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_9)));
      end if;

   end Memcpy_Array_To_Array;

   ---
   -- Memcpy_To_Array_Async --
   ---

   procedure Memcpy_To_Array_Async
     (Dst      : CUDA.Driver_Types.CUDA_Array_t; W_Offset : CUDA.Stddef.Size_T;
      H_Offset : CUDA.Stddef.Size_T; Src : System.Address;
      Count    : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind;
      Stream   : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaArray_t with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => W_Offset'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => H_Offset'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;
      Temp_local_7 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_8 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyToArrayAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7));

   begin
      null;

      if Temp_res_8 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_8)));
      end if;

   end Memcpy_To_Array_Async;

   ---
   -- Memcpy_From_Array_Async --
   ---

   procedure Memcpy_From_Array_Async
     (Dst      : System.Address; Src : CUDA.Driver_Types.CUDA_Array_const_t;
      W_Offset : CUDA.Stddef.Size_T; H_Offset : CUDA.Stddef.Size_T;
      Count    : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind;
      Stream   : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaArray_const_t with
        Address => Src'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => W_Offset'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => H_Offset'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;
      Temp_local_7 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_8 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemcpyFromArrayAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7));

   begin
      null;

      if Temp_res_8 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_8)));
      end if;

   end Memcpy_From_Array_Async;

   ---
   -- Malloc_Async --
   ---

   function Malloc_Async
     (Size : CUDA.Stddef.Size_T; H_Stream : CUDA.Driver_Types.Stream_T)
      return System.Address
   is

      Temp_ret_1   : aliased System.Address;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Size'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaStream_t with
        Address => H_Stream'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMallocAsync
             (Temp_call_2, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

      return Temp_ret_1;
   end Malloc_Async;

   ---
   -- Free_Async --
   ---

   procedure Free_Async
     (Dev_Ptr : System.Address; H_Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStream_t with
        Address => H_Stream'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaFreeAsync (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Free_Async;

   ---
   -- Mem_Pool_Trim_To --
   ---

   procedure Mem_Pool_Trim_To
     (Mem_Pool          : CUDA.Driver_Types.Mem_Pool_T;
      Min_Bytes_To_Keep : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Min_Bytes_To_Keep'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolTrimTo (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Mem_Pool_Trim_To;

   ---
   -- Mem_Pool_Set_Attribute --
   ---

   procedure Mem_Pool_Set_Attribute
     (Mem_Pool : CUDA.Driver_Types.Mem_Pool_T;
      Attr     : CUDA.Driver_Types.Mem_Pool_Attr; Value : System.Address)
   is
      Temp_local_1 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaMemPoolAttr with
        Address => Attr'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Value'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolSetAttribute
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Mem_Pool_Set_Attribute;

   ---
   -- Mem_Pool_Get_Attribute --
   ---

   procedure Mem_Pool_Get_Attribute
     (Mem_Pool : CUDA.Driver_Types.Mem_Pool_T;
      Attr     : CUDA.Driver_Types.Mem_Pool_Attr; Value : System.Address)
   is
      Temp_local_1 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaMemPoolAttr with
        Address => Attr'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Value'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolGetAttribute
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Mem_Pool_Get_Attribute;

   ---
   -- Mem_Pool_Set_Access --
   ---

   procedure Mem_Pool_Set_Access
     (Mem_Pool  : CUDA.Driver_Types.Mem_Pool_T;
      Desc_List : CUDA.Driver_Types.Mem_Access_Desc;
      Count     : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;
      Temp_call_4  : aliased constant udriver_types_h.cudaMemAccessDesc with
        Address => Desc_List'Address, Import;
      Temp_local_3 : aliased stddef_h.size_t with
        Address => Count'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolSetAccess
             (Temp_local_1, Temp_call_4'Unchecked_Access, Temp_local_3));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Mem_Pool_Set_Access;

   ---
   -- Mem_Pool_Get_Access --
   ---

   function Mem_Pool_Get_Access
     (Mem_Pool :     CUDA.Driver_Types.Mem_Pool_T;
      Location : out CUDA.Driver_Types.Mem_Location)
      return CUDA.Driver_Types.Mem_Access_Flags
   is

      Temp_call_1  : aliased udriver_types_h.cudaMemAccessFlags;
      Temp_ret_2   : aliased CUDA.Driver_Types.Mem_Access_Flags with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;
      Temp_call_4  : aliased udriver_types_h.cudaMemLocation with
        Address => Location'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolGetAccess
             (Temp_call_1'Unchecked_Access, Temp_local_2,
              Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

      return Temp_ret_2;
   end Mem_Pool_Get_Access;

   ---
   -- Mem_Pool_Create --
   ---

   procedure Mem_Pool_Create
     (Mem_Pool : System.Address; Pool_Props : CUDA.Driver_Types.Mem_Pool_Props)
   is
      Temp_local_1 : aliased System.Address with
        Address => Mem_Pool'Address, Import;
      Temp_call_3  : aliased constant udriver_types_h.cudaMemPoolProps with
        Address => Pool_Props'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolCreate
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Mem_Pool_Create;

   ---
   -- Mem_Pool_Destroy --
   ---

   procedure Mem_Pool_Destroy (Mem_Pool : CUDA.Driver_Types.Mem_Pool_T) is
      Temp_local_1 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaMemPoolDestroy (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Mem_Pool_Destroy;

   ---
   -- Malloc_From_Pool_Async --
   ---

   procedure Malloc_From_Pool_Async
     (Ptr      : System.Address; Size : CUDA.Stddef.Size_T;
      Mem_Pool : CUDA.Driver_Types.Mem_Pool_T;
      Stream   : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Ptr'Address, Import;
      Temp_local_2 : aliased stddef_h.size_t with
        Address => Size'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;
      Temp_local_4 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMallocFromPoolAsync
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Malloc_From_Pool_Async;

   ---
   -- Mem_Pool_Export_To_Shareable_Handle --
   ---

   procedure Mem_Pool_Export_To_Shareable_Handle
     (Shareable_Handle : System.Address;
      Mem_Pool         : CUDA.Driver_Types.Mem_Pool_T;
      Handle_Type      : CUDA.Driver_Types.Mem_Allocation_Handle_Type;
      Flags            : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Shareable_Handle'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaMemAllocationHandleType with
        Address => Handle_Type'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolExportToShareableHandle
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Mem_Pool_Export_To_Shareable_Handle;

   ---
   -- Mem_Pool_Import_From_Shareable_Handle --
   ---

   procedure Mem_Pool_Import_From_Shareable_Handle
     (Mem_Pool    : System.Address; Shareable_Handle : System.Address;
      Handle_Type : CUDA.Driver_Types.Mem_Allocation_Handle_Type;
      Flags       : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Mem_Pool'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Shareable_Handle'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaMemAllocationHandleType with
        Address => Handle_Type'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolImportFromShareableHandle
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Mem_Pool_Import_From_Shareable_Handle;

   ---
   -- Mem_Pool_Export_Pointer --
   ---

   procedure Mem_Pool_Export_Pointer
     (Export_Data : out CUDA.Driver_Types.Mem_Pool_Ptr_Export_Data;
      Ptr         :     System.Address)
   is
      Temp_call_3  : aliased udriver_types_h.cudaMemPoolPtrExportData with
        Address => Export_Data'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Ptr'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolExportPointer
             (Temp_call_3'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Mem_Pool_Export_Pointer;

   ---
   -- Mem_Pool_Import_Pointer --
   ---

   procedure Mem_Pool_Import_Pointer
     (Ptr         : System.Address; Mem_Pool : CUDA.Driver_Types.Mem_Pool_T;
      Export_Data : out CUDA.Driver_Types.Mem_Pool_Ptr_Export_Data)
   is
      Temp_local_1 : aliased System.Address with
        Address => Ptr'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaMemPool_t with
        Address => Mem_Pool'Address, Import;
      Temp_call_4  : aliased udriver_types_h.cudaMemPoolPtrExportData with
        Address => Export_Data'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaMemPoolImportPointer
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Mem_Pool_Import_Pointer;

   ---
   -- Pointer_Get_Attributes --
   ---

   function Pointer_Get_Attributes
     (Ptr : System.Address) return CUDA.Driver_Types.Pointer_Attributes
   is

      Temp_call_1  : aliased udriver_types_h.cudaPointerAttributes;
      Temp_ret_2   : aliased CUDA.Driver_Types.Pointer_Attributes with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Ptr'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaPointerGetAttributes
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Pointer_Get_Attributes;

   ---
   -- Device_Can_Access_Peer --
   ---

   procedure Device_Can_Access_Peer
     (Can_Access_Peer : out int; Device : Device_T; Peer_Device : Device_T)
   is
      Temp_call_4 : aliased int with
        Address => Can_Access_Peer'Address, Import;
      Temp_res_5  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceCanAccessPeer
             (Temp_call_4'Unchecked_Access, int (Device), int (Peer_Device)));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Device_Can_Access_Peer;

   ---
   -- Device_Enable_Peer_Access --
   ---

   procedure Device_Enable_Peer_Access
     (Peer_Device : Device_T; Flags : unsigned)
   is
      Temp_local_2 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceEnablePeerAccess
             (int (Peer_Device), Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Device_Enable_Peer_Access;

   ---
   -- Device_Disable_Peer_Access --
   ---

   procedure Device_Disable_Peer_Access (Peer_Device : Device_T) is
      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceDisablePeerAccess
             (int (Peer_Device)));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Device_Disable_Peer_Access;

   ---
   -- Graphics_Unregister_Resource --
   ---

   procedure Graphics_Unregister_Resource
     (Resource : CUDA.Driver_Types.Graphics_Resource_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphicsResource_t with
        Address => Resource'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphicsUnregisterResource (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Graphics_Unregister_Resource;

   ---
   -- Graphics_Resource_Set_Map_Flags --
   ---

   procedure Graphics_Resource_Set_Map_Flags
     (Resource : CUDA.Driver_Types.Graphics_Resource_T; Flags : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphicsResource_t with
        Address => Resource'Address, Import;
      Temp_local_2 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphicsResourceSetMapFlags
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graphics_Resource_Set_Map_Flags;

   ---
   -- Graphics_Map_Resources --
   ---

   procedure Graphics_Map_Resources
     (Count  : int; Resources : System.Address;
      Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased Interfaces.C.int with
        Address => Count'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Resources'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphicsMapResources
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graphics_Map_Resources;

   ---
   -- Graphics_Unmap_Resources --
   ---

   procedure Graphics_Unmap_Resources
     (Count  : int; Resources : System.Address;
      Stream : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased Interfaces.C.int with
        Address => Count'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Resources'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphicsUnmapResources
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graphics_Unmap_Resources;

   ---
   -- Graphics_Resource_Get_Mapped_Pointer --
   ---

   procedure Graphics_Resource_Get_Mapped_Pointer
     (Dev_Ptr  : System.Address; Size : out CUDA.Stddef.Size_T;
      Resource : CUDA.Driver_Types.Graphics_Resource_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_call_4  : aliased stddef_h.size_t with
        Address => Size'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaGraphicsResource_t with
        Address => Resource'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphicsResourceGetMappedPointer
             (Temp_local_1, Temp_call_4'Unchecked_Access, Temp_local_3));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graphics_Resource_Get_Mapped_Pointer;

   ---
   -- Graphics_Sub_Resource_Get_Mapped_Array --
   ---

   procedure Graphics_Sub_Resource_Get_Mapped_Array
     (C_Array   : System.Address;
      Resource : CUDA.Driver_Types.Graphics_Resource_T; Array_Index : unsigned;
      Mip_Level : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => C_Array'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphicsResource_t with
        Address => Resource'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Array_Index'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Mip_Level'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphicsSubResourceGetMappedArray
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graphics_Sub_Resource_Get_Mapped_Array;

   ---
   -- Graphics_Resource_Get_Mapped_Mipmapped_Array --
   ---

   procedure Graphics_Resource_Get_Mapped_Mipmapped_Array
     (Mipmapped_Array : System.Address;
      Resource        : CUDA.Driver_Types.Graphics_Resource_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Mipmapped_Array'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphicsResource_t with
        Address => Resource'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphicsResourceGetMappedMipmappedArray
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graphics_Resource_Get_Mapped_Mipmapped_Array;

   ---
   -- Bind_Texture --
   ---

   procedure Bind_Texture
     (Offset : out CUDA.Stddef.Size_T;
      Texref : CUDA.Texture_Types.Texture_Reference; Dev_Ptr : System.Address;
      Desc : CUDA.Driver_Types.Channel_Format_Desc; Size : CUDA.Stddef.Size_T)
   is
      Temp_call_6  : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_call_7  : aliased constant utexture_types_h.textureReference with
        Address => Texref'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_call_8 : aliased constant udriver_types_h.cudaChannelFormatDesc with
        Address => Desc'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Size'Address, Import;

      Temp_res_9 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaBindTexture
             (Temp_call_6'Unchecked_Access, Temp_call_7'Unchecked_Access,
              Temp_local_3, Temp_call_8'Unchecked_Access, Temp_local_5));

   begin
      null;

      if Temp_res_9 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_9)));
      end if;

   end Bind_Texture;

   ---
   -- Bind_Texture_2D --
   ---

   procedure Bind_Texture_2D
     (Offset : out CUDA.Stddef.Size_T;
      Texref : CUDA.Texture_Types.Texture_Reference; Dev_Ptr : System.Address;
      Desc : CUDA.Driver_Types.Channel_Format_Desc; Width : CUDA.Stddef.Size_T;
      Height :     CUDA.Stddef.Size_T; Pitch : CUDA.Stddef.Size_T)
   is
      Temp_call_8  : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_call_9  : aliased constant utexture_types_h.textureReference with
        Address => Texref'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Dev_Ptr'Address, Import;
      Temp_call_10 :
        aliased constant udriver_types_h.cudaChannelFormatDesc with
        Address => Desc'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Width'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Height'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Pitch'Address, Import;

      Temp_res_11 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaBindTexture2D
             (Temp_call_8'Unchecked_Access, Temp_call_9'Unchecked_Access,
              Temp_local_3, Temp_call_10'Unchecked_Access, Temp_local_5,
              Temp_local_6, Temp_local_7));

   begin
      null;

      if Temp_res_11 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element
              (Integer (Temp_res_11)));
      end if;

   end Bind_Texture_2D;

   ---
   -- Bind_Texture_To_Array --
   ---

   procedure Bind_Texture_To_Array
     (Texref  : CUDA.Texture_Types.Texture_Reference;
      C_Array : CUDA.Driver_Types.CUDA_Array_const_t;
      Desc    : CUDA.Driver_Types.Channel_Format_Desc)
   is
      Temp_call_4  : aliased constant utexture_types_h.textureReference with
        Address => Texref'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaArray_const_t with
        Address => C_Array'Address, Import;
      Temp_call_5 : aliased constant udriver_types_h.cudaChannelFormatDesc with
        Address => Desc'Address, Import;
      Temp_res_6   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaBindTextureToArray
             (Temp_call_4'Unchecked_Access, Temp_local_2,
              Temp_call_5'Unchecked_Access));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Bind_Texture_To_Array;

   ---
   -- Bind_Texture_To_Mipmapped_Array --
   ---

   procedure Bind_Texture_To_Mipmapped_Array
     (Texref          : CUDA.Texture_Types.Texture_Reference;
      Mipmapped_Array : CUDA.Driver_Types.Mipmapped_Array_Const_T;
      Desc            : CUDA.Driver_Types.Channel_Format_Desc)
   is
      Temp_call_4  : aliased constant utexture_types_h.textureReference with
        Address => Texref'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaMipmappedArray_const_t with
        Address => Mipmapped_Array'Address, Import;
      Temp_call_5 : aliased constant udriver_types_h.cudaChannelFormatDesc with
        Address => Desc'Address, Import;
      Temp_res_6   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaBindTextureToMipmappedArray
             (Temp_call_4'Unchecked_Access, Temp_local_2,
              Temp_call_5'Unchecked_Access));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Bind_Texture_To_Mipmapped_Array;

   ---
   -- Unbind_Texture --
   ---

   procedure Unbind_Texture (Texref : CUDA.Texture_Types.Texture_Reference) is
      Temp_call_2 : aliased constant utexture_types_h.textureReference with
        Address => Texref'Address, Import;
      Temp_res_3  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaUnbindTexture
             (Temp_call_2'Unchecked_Access));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Unbind_Texture;

   ---
   -- Get_Texture_Alignment_Offset --
   ---

   function Get_Texture_Alignment_Offset
     (Texref : CUDA.Texture_Types.Texture_Reference) return CUDA.Stddef.Size_T
   is

      Temp_call_1 : aliased stddef_h.size_t;
      Temp_ret_2  : aliased CUDA.Stddef.Size_T with
        Address => Temp_call_1'Address, Import;
      Temp_call_3 : aliased constant utexture_types_h.textureReference with
        Address => Texref'Address, Import;
      Temp_res_4  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetTextureAlignmentOffset
             (Temp_call_1'Unchecked_Access, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

      return Temp_ret_2;
   end Get_Texture_Alignment_Offset;

   ---
   -- Get_Texture_Reference --
   ---

   procedure Get_Texture_Reference
     (Texref : System.Address; Symbol : System.Address)
   is
      Temp_local_1 : aliased System.Address with
        Address => Texref'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Symbol'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetTextureReference
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Get_Texture_Reference;

   ---
   -- Bind_Surface_To_Array --
   ---

   procedure Bind_Surface_To_Array
     (Surfref : CUDA.Surface_Types.Surface_Reference;
      C_Array : CUDA.Driver_Types.CUDA_Array_const_t;
      Desc    : CUDA.Driver_Types.Channel_Format_Desc)
   is
      Temp_call_4  : aliased constant usurface_types_h.surfaceReference with
        Address => Surfref'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaArray_const_t with
        Address => C_Array'Address, Import;
      Temp_call_5 : aliased constant udriver_types_h.cudaChannelFormatDesc with
        Address => Desc'Address, Import;
      Temp_res_6   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaBindSurfaceToArray
             (Temp_call_4'Unchecked_Access, Temp_local_2,
              Temp_call_5'Unchecked_Access));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Bind_Surface_To_Array;

   ---
   -- Get_Surface_Reference --
   ---

   procedure Get_Surface_Reference
     (Surfref : System.Address; Symbol : System.Address)
   is
      Temp_local_1 : aliased System.Address with
        Address => Surfref'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Symbol'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetSurfaceReference
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Get_Surface_Reference;

   ---
   -- Get_Channel_Desc --
   ---

   function Get_Channel_Desc
     (C_Array : CUDA.Driver_Types.CUDA_Array_const_t)
      return CUDA.Driver_Types.Channel_Format_Desc
   is

      Temp_call_1  : aliased udriver_types_h.cudaChannelFormatDesc;
      Temp_ret_2   : aliased CUDA.Driver_Types.Channel_Format_Desc with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaArray_const_t with
        Address => C_Array'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetChannelDesc
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Get_Channel_Desc;

   ---
   -- Create_Channel_Desc --
   ---

   function Create_Channel_Desc
     (X : int; Y : int; Z : int; W : int;
      F : CUDA.Driver_Types.Channel_Format_Kind)
      return CUDA.Driver_Types.Channel_Format_Desc
   is
      Temp_local_1 : aliased Interfaces.C.int with
        Address => X'Address, Import;
      Temp_local_2 : aliased Interfaces.C.int with
        Address => Y'Address, Import;
      Temp_local_3 : aliased Interfaces.C.int with
        Address => Z'Address, Import;
      Temp_local_4 : aliased Interfaces.C.int with
        Address => W'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaChannelFormatKind with
        Address => F'Address, Import;

      Temp_result_orig_1 : aliased udriver_types_h.cudaChannelFormatDesc :=
        ucuda_runtime_api_h.cudaCreateChannelDesc
          (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
           Temp_local_5);

      Temp_result_wrapped_6 :
        aliased CUDA.Driver_Types.Channel_Format_Desc with
        Address => Temp_result_orig_1'Address, Import;

   begin

      return Temp_result_wrapped_6;
   end Create_Channel_Desc;

   ---
   -- Create_Texture_Object --
   ---

   procedure Create_Texture_Object
     (Tex_Object    : out CUDA.Texture_Types.Texture_Object_T;
      Res_Desc      :     CUDA.Driver_Types.Resource_Desc;
      Tex_Desc      :     CUDA.Texture_Types.Texture_Desc;
      Res_View_Desc :     CUDA.Driver_Types.Resource_View_Desc)
   is
      Temp_call_5 : aliased utexture_types_h.cudaTextureObject_t with
        Address => Tex_Object'Address, Import;
      Temp_call_6 : aliased constant udriver_types_h.cudaResourceDesc with
        Address => Res_Desc'Address, Import;
      Temp_call_7 : aliased constant utexture_types_h.cudaTextureDesc with
        Address => Tex_Desc'Address, Import;
      Temp_call_8 : aliased constant udriver_types_h.cudaResourceViewDesc with
        Address => Res_View_Desc'Address, Import;
      Temp_res_9  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaCreateTextureObject
             (Temp_call_5'Unchecked_Access, Temp_call_6'Unchecked_Access,
              Temp_call_7'Unchecked_Access, Temp_call_8'Unchecked_Access));

   begin
      null;

      if Temp_res_9 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_9)));
      end if;

   end Create_Texture_Object;

   ---
   -- Destroy_Texture_Object --
   ---

   procedure Destroy_Texture_Object
     (Tex_Object : CUDA.Texture_Types.Texture_Object_T)
   is
      Temp_local_1 : aliased utexture_types_h.cudaTextureObject_t with
        Address => Tex_Object'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaDestroyTextureObject (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Destroy_Texture_Object;

   ---
   -- Get_Texture_Object_Resource_Desc --
   ---

   function Get_Texture_Object_Resource_Desc
     (Tex_Object : CUDA.Texture_Types.Texture_Object_T)
      return CUDA.Driver_Types.Resource_Desc
   is

      Temp_call_1  : aliased udriver_types_h.cudaResourceDesc;
      Temp_ret_2   : aliased CUDA.Driver_Types.Resource_Desc with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased utexture_types_h.cudaTextureObject_t with
        Address => Tex_Object'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetTextureObjectResourceDesc
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Get_Texture_Object_Resource_Desc;

   ---
   -- Get_Texture_Object_Texture_Desc --
   ---

   function Get_Texture_Object_Texture_Desc
     (Tex_Object : CUDA.Texture_Types.Texture_Object_T)
      return CUDA.Texture_Types.Texture_Desc
   is

      Temp_call_1  : aliased utexture_types_h.cudaTextureDesc;
      Temp_ret_2   : aliased CUDA.Texture_Types.Texture_Desc with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased utexture_types_h.cudaTextureObject_t with
        Address => Tex_Object'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetTextureObjectTextureDesc
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Get_Texture_Object_Texture_Desc;

   ---
   -- Get_Texture_Object_Resource_View_Desc --
   ---

   function Get_Texture_Object_Resource_View_Desc
     (Tex_Object : CUDA.Texture_Types.Texture_Object_T)
      return CUDA.Driver_Types.Resource_View_Desc
   is

      Temp_call_1  : aliased udriver_types_h.cudaResourceViewDesc;
      Temp_ret_2   : aliased CUDA.Driver_Types.Resource_View_Desc with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased utexture_types_h.cudaTextureObject_t with
        Address => Tex_Object'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetTextureObjectResourceViewDesc
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Get_Texture_Object_Resource_View_Desc;

   ---
   -- Create_Surface_Object --
   ---

   procedure Create_Surface_Object
     (Surf_Object : out CUDA.Surface_Types.Surface_Object_T;
      Res_Desc    :     CUDA.Driver_Types.Resource_Desc)
   is
      Temp_call_3 : aliased usurface_types_h.cudaSurfaceObject_t with
        Address => Surf_Object'Address, Import;
      Temp_call_4 : aliased constant udriver_types_h.cudaResourceDesc with
        Address => Res_Desc'Address, Import;
      Temp_res_5  : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaCreateSurfaceObject
             (Temp_call_3'Unchecked_Access, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Create_Surface_Object;

   ---
   -- Destroy_Surface_Object --
   ---

   procedure Destroy_Surface_Object
     (Surf_Object : CUDA.Surface_Types.Surface_Object_T)
   is
      Temp_local_1 : aliased usurface_types_h.cudaSurfaceObject_t with
        Address => Surf_Object'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaDestroySurfaceObject (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Destroy_Surface_Object;

   ---
   -- Get_Surface_Object_Resource_Desc --
   ---

   function Get_Surface_Object_Resource_Desc
     (Surf_Object : CUDA.Surface_Types.Surface_Object_T)
      return CUDA.Driver_Types.Resource_Desc
   is

      Temp_call_1  : aliased udriver_types_h.cudaResourceDesc;
      Temp_ret_2   : aliased CUDA.Driver_Types.Resource_Desc with
        Address => Temp_call_1'Address, Import;
      Temp_local_2 : aliased usurface_types_h.cudaSurfaceObject_t with
        Address => Surf_Object'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetSurfaceObjectResourceDesc
             (Temp_call_1'Unchecked_Access, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_2;
   end Get_Surface_Object_Resource_Desc;

   ---
   -- Driver_Get_Version --
   ---

   function Driver_Get_Version return int is

      Temp_call_1 : aliased int;
      Temp_ret_2  : aliased int with
        Address => Temp_call_1'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDriverGetVersion
             (Temp_call_1'Unchecked_Access));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_2;
   end Driver_Get_Version;

   ---
   -- Runtime_Get_Version --
   ---

   function Runtime_Get_Version return int is

      Temp_call_1 : aliased int;
      Temp_ret_2  : aliased int with
        Address => Temp_call_1'Address, Import;

      Temp_res_2 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaRuntimeGetVersion
             (Temp_call_1'Unchecked_Access));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

      return Temp_ret_2;
   end Runtime_Get_Version;

   ---
   -- Graph_Create --
   ---

   procedure Graph_Create (Graph : System.Address; Flags : unsigned) is
      Temp_local_1 : aliased System.Address with
        Address => Graph'Address, Import;
      Temp_local_2 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphCreate (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graph_Create;

   ---
   -- Graph_Add_Kernel_Node --
   ---

   function Graph_Add_Kernel_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : CUDA.Driver_Types.Kernel_Node_Params.Kernel_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_call_6  :
        aliased constant udriver_types_h.Class_cudaKernelNodeParams
          .cudaKernelNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_7   : Integer                :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddKernelNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_call_6'Unchecked_Access));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Kernel_Node;

   ---
   -- Graph_Kernel_Node_Get_Params --
   ---

   procedure Graph_Kernel_Node_Get_Params
     (Node        :     CUDA.Driver_Types.Graph_Node_T;
      Node_Params : out CUDA.Driver_Types.Kernel_Node_Params
        .Kernel_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  :
        aliased udriver_types_h.Class_cudaKernelNodeParams
          .cudaKernelNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphKernelNodeGetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Kernel_Node_Get_Params;

   ---
   -- Graph_Kernel_Node_Set_Params --
   ---

   procedure Graph_Kernel_Node_Set_Params
     (Node        : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.Kernel_Node_Params.Kernel_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  :
        aliased constant udriver_types_h.Class_cudaKernelNodeParams
          .cudaKernelNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphKernelNodeSetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Kernel_Node_Set_Params;

   ---
   -- Graph_Kernel_Node_Copy_Attributes --
   ---

   procedure Graph_Kernel_Node_Copy_Attributes
     (H_Src : CUDA.Driver_Types.Graph_Node_T;
      H_Dst : CUDA.Driver_Types.Graph_Node_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Src'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Dst'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphKernelNodeCopyAttributes
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graph_Kernel_Node_Copy_Attributes;

   ---
   -- Graph_Kernel_Node_Get_Attribute --
   ---

   procedure Graph_Kernel_Node_Get_Attribute
     (H_Node    :     CUDA.Driver_Types.Graph_Node_T;
      Attr      :     CUDA.Driver_Types.Kernel_Node_Attr_ID;
      Value_Out : out CUDA.Driver_Types.Kernel_Node_Attr_Value)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaKernelNodeAttrID with
        Address => Attr'Address, Import;
      Temp_call_4  : aliased udriver_types_h.cudaKernelNodeAttrValue with
        Address => Value_Out'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphKernelNodeGetAttribute
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Kernel_Node_Get_Attribute;

   ---
   -- Graph_Kernel_Node_Set_Attribute --
   ---

   procedure Graph_Kernel_Node_Set_Attribute
     (H_Node : CUDA.Driver_Types.Graph_Node_T;
      Attr   : CUDA.Driver_Types.Kernel_Node_Attr_ID;
      Value  : CUDA.Driver_Types.Kernel_Node_Attr_Value)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaKernelNodeAttrID with
        Address => Attr'Address, Import;
      Temp_call_4  :
        aliased constant udriver_types_h.cudaKernelNodeAttrValue with
        Address => Value'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphKernelNodeSetAttribute
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Kernel_Node_Set_Attribute;

   ---
   -- Graph_Add_Memcpy_Node --
   ---

   function Graph_Add_Memcpy_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Copy_Params : CUDA.Driver_Types.Memcpy_3D_Parms)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_call_6  : aliased constant udriver_types_h.cudaMemcpy3DParms with
        Address => Copy_Params'Address, Import;
      Temp_res_7   : Integer                :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddMemcpyNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_call_6'Unchecked_Access));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Memcpy_Node;

   ---
   -- Graph_Add_Memcpy_Node_To_Symbol --
   ---

   function Graph_Add_Memcpy_Node_To_Symbol
     (Graph  : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Symbol : System.Address; Src : System.Address;
      Count  : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind   : CUDA.Driver_Types.Memcpy_Kind)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_5 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_6 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_8 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_9 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_10 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddMemcpyNodeToSymbol
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_local_5, Temp_local_6, Temp_local_7,
              Temp_local_8, Temp_local_9));

   begin
      null;

      if Temp_res_10 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element
              (Integer (Temp_res_10)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Memcpy_Node_To_Symbol;

   ---
   -- Graph_Add_Memcpy_Node_From_Symbol --
   ---

   function Graph_Add_Memcpy_Node_From_Symbol
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Dst   : System.Address; Symbol : System.Address;
      Count : CUDA.Stddef.Size_T; Offset : CUDA.Stddef.Size_T;
      Kind  : CUDA.Driver_Types.Memcpy_Kind)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_5 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_6 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_8 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_9 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_10 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddMemcpyNodeFromSymbol
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_local_5, Temp_local_6, Temp_local_7,
              Temp_local_8, Temp_local_9));

   begin
      null;

      if Temp_res_10 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element
              (Integer (Temp_res_10)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Memcpy_Node_From_Symbol;

   ---
   -- Graph_Add_Memcpy_Node_1D --
   ---

   function Graph_Add_Memcpy_Node_1D
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Dst   : System.Address; Src : System.Address; Count : CUDA.Stddef.Size_T;
      Kind  : CUDA.Driver_Types.Memcpy_Kind)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_5 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_6 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_7 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_8 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_9 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddMemcpyNode1D
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_local_5, Temp_local_6, Temp_local_7,
              Temp_local_8));

   begin
      null;

      if Temp_res_9 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_9)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Memcpy_Node_1D;

   ---
   -- Graph_Memcpy_Node_Get_Params --
   ---

   procedure Graph_Memcpy_Node_Get_Params
     (Node        :     CUDA.Driver_Types.Graph_Node_T;
      Node_Params : out CUDA.Driver_Types.Memcpy_3D_Parms)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  : aliased udriver_types_h.cudaMemcpy3DParms with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphMemcpyNodeGetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Memcpy_Node_Get_Params;

   ---
   -- Graph_Memcpy_Node_Set_Params --
   ---

   procedure Graph_Memcpy_Node_Set_Params
     (Node        : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.Memcpy_3D_Parms)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  : aliased constant udriver_types_h.cudaMemcpy3DParms with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphMemcpyNodeSetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Memcpy_Node_Set_Params;

   ---
   -- Graph_Memcpy_Node_Set_Params_To_Symbol --
   ---

   procedure Graph_Memcpy_Node_Set_Params_To_Symbol
     (Node   : CUDA.Driver_Types.Graph_Node_T; Symbol : System.Address;
      Src    : System.Address; Count : CUDA.Stddef.Size_T;
      Offset : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphMemcpyNodeSetParamsToSymbol
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Graph_Memcpy_Node_Set_Params_To_Symbol;

   ---
   -- Graph_Memcpy_Node_Set_Params_From_Symbol --
   ---

   procedure Graph_Memcpy_Node_Set_Params_From_Symbol
     (Node   : CUDA.Driver_Types.Graph_Node_T; Dst : System.Address;
      Symbol : System.Address; Count : CUDA.Stddef.Size_T;
      Offset : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphMemcpyNodeSetParamsFromSymbol
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Graph_Memcpy_Node_Set_Params_From_Symbol;

   ---
   -- Graph_Memcpy_Node_Set_Params_1D --
   ---

   procedure Graph_Memcpy_Node_Set_Params_1D
     (Node : CUDA.Driver_Types.Graph_Node_T; Dst : System.Address;
      Src  : System.Address; Count : CUDA.Stddef.Size_T;
      Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphMemcpyNodeSetParams1D
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Graph_Memcpy_Node_Set_Params_1D;

   ---
   -- Graph_Add_Memset_Node --
   ---

   function Graph_Add_Memset_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Memset_Params : CUDA.Driver_Types.Memset_Params)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_call_6  : aliased constant udriver_types_h.cudaMemsetParams with
        Address => Memset_Params'Address, Import;
      Temp_res_7   : Integer                :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddMemsetNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_call_6'Unchecked_Access));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Memset_Node;

   ---
   -- Graph_Memset_Node_Get_Params --
   ---

   procedure Graph_Memset_Node_Get_Params
     (Node        :     CUDA.Driver_Types.Graph_Node_T;
      Node_Params : out CUDA.Driver_Types.Memset_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  : aliased udriver_types_h.cudaMemsetParams with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphMemsetNodeGetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Memset_Node_Get_Params;

   ---
   -- Graph_Memset_Node_Set_Params --
   ---

   procedure Graph_Memset_Node_Set_Params
     (Node        : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.Memset_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  : aliased constant udriver_types_h.cudaMemsetParams with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphMemsetNodeSetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Memset_Node_Set_Params;

   ---
   -- Graph_Add_Host_Node --
   ---

   function Graph_Add_Host_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : CUDA.Driver_Types.Host_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_call_6  : aliased constant udriver_types_h.cudaHostNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_7   : Integer                :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddHostNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_call_6'Unchecked_Access));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Host_Node;

   ---
   -- Graph_Host_Node_Get_Params --
   ---

   procedure Graph_Host_Node_Get_Params
     (Node        :     CUDA.Driver_Types.Graph_Node_T;
      Node_Params : out CUDA.Driver_Types.Host_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  : aliased udriver_types_h.cudaHostNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphHostNodeGetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Host_Node_Get_Params;

   ---
   -- Graph_Host_Node_Set_Params --
   ---

   procedure Graph_Host_Node_Set_Params
     (Node        : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.Host_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  : aliased constant udriver_types_h.cudaHostNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphHostNodeSetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Host_Node_Set_Params;

   ---
   -- Graph_Add_Child_Graph_Node --
   ---

   function Graph_Add_Child_Graph_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Child_Graph : CUDA.Driver_Types.Graph_T)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaGraph_t with
        Address => Child_Graph'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddChildGraphNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Child_Graph_Node;

   ---
   -- Graph_Child_Graph_Node_Get_Graph --
   ---

   procedure Graph_Child_Graph_Node_Get_Graph
     (Node : CUDA.Driver_Types.Graph_Node_T; Graph : System.Address)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Graph'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphChildGraphNodeGetGraph
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graph_Child_Graph_Node_Get_Graph;

   ---
   -- Graph_Add_Empty_Node --
   ---

   function Graph_Add_Empty_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddEmptyNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Empty_Node;

   ---
   -- Graph_Add_Event_Record_Node --
   ---

   function Graph_Add_Event_Record_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Event : CUDA.Driver_Types.Event_T) return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddEventRecordNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Event_Record_Node;

   ---
   -- Graph_Event_Record_Node_Get_Event --
   ---

   function Graph_Event_Record_Node_Get_Event
     (Node : CUDA.Driver_Types.Graph_Node_T) return CUDA.Driver_Types.Event_T
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;

      Temp_ret_1  : aliased CUDA.Driver_Types.Event_T;
      Temp_call_2 : aliased System.Address := Temp_ret_1'Address;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphEventRecordNodeGetEvent
             (Temp_local_1, Temp_call_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_1;
   end Graph_Event_Record_Node_Get_Event;

   ---
   -- Graph_Event_Record_Node_Set_Event --
   ---

   procedure Graph_Event_Record_Node_Set_Event
     (Node : CUDA.Driver_Types.Graph_Node_T; Event : CUDA.Driver_Types.Event_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphEventRecordNodeSetEvent
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graph_Event_Record_Node_Set_Event;

   ---
   -- Graph_Add_Event_Wait_Node --
   ---

   function Graph_Add_Event_Wait_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Event : CUDA.Driver_Types.Event_T) return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_5 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddEventWaitNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Event_Wait_Node;

   ---
   -- Graph_Event_Wait_Node_Get_Event --
   ---

   function Graph_Event_Wait_Node_Get_Event
     (Node : CUDA.Driver_Types.Graph_Node_T) return CUDA.Driver_Types.Event_T
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;

      Temp_ret_1  : aliased CUDA.Driver_Types.Event_T;
      Temp_call_2 : aliased System.Address := Temp_ret_1'Address;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphEventWaitNodeGetEvent
             (Temp_local_1, Temp_call_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

      return Temp_ret_1;
   end Graph_Event_Wait_Node_Get_Event;

   ---
   -- Graph_Event_Wait_Node_Set_Event --
   ---

   procedure Graph_Event_Wait_Node_Set_Event
     (Node : CUDA.Driver_Types.Graph_Node_T; Event : CUDA.Driver_Types.Event_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphEventWaitNodeSetEvent
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graph_Event_Wait_Node_Set_Event;

   ---
   -- Graph_Add_External_Semaphores_Signal_Node --
   ---

   function Graph_Add_External_Semaphores_Signal_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : CUDA.Driver_Types.External_Semaphore_Signal_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_call_6  :
        aliased constant udriver_types_h
          .cudaExternalSemaphoreSignalNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_7   : Integer                :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddExternalSemaphoresSignalNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_call_6'Unchecked_Access));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

      return Temp_ret_1;
   end Graph_Add_External_Semaphores_Signal_Node;

   ---
   -- Graph_External_Semaphores_Signal_Node_Get_Params --
   ---

   procedure Graph_External_Semaphores_Signal_Node_Get_Params
     (H_Node     :     CUDA.Driver_Types.Graph_Node_T;
      Params_Out : out CUDA.Driver_Types.External_Semaphore_Signal_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_call_3  :
        aliased udriver_types_h.cudaExternalSemaphoreSignalNodeParams with
        Address => Params_Out'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExternalSemaphoresSignalNodeGetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_External_Semaphores_Signal_Node_Get_Params;

   ---
   -- Graph_External_Semaphores_Signal_Node_Set_Params --
   ---

   procedure Graph_External_Semaphores_Signal_Node_Set_Params
     (H_Node      : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.External_Semaphore_Signal_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_call_3  :
        aliased constant udriver_types_h
          .cudaExternalSemaphoreSignalNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExternalSemaphoresSignalNodeSetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_External_Semaphores_Signal_Node_Set_Params;

   ---
   -- Graph_Add_External_Semaphores_Wait_Node --
   ---

   function Graph_Add_External_Semaphores_Wait_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : CUDA.Driver_Types.External_Semaphore_Wait_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_call_6  :
        aliased constant udriver_types_h
          .cudaExternalSemaphoreWaitNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_7   : Integer                :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddExternalSemaphoresWaitNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_call_6'Unchecked_Access));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

      return Temp_ret_1;
   end Graph_Add_External_Semaphores_Wait_Node;

   ---
   -- Graph_External_Semaphores_Wait_Node_Get_Params --
   ---

   procedure Graph_External_Semaphores_Wait_Node_Get_Params
     (H_Node     :     CUDA.Driver_Types.Graph_Node_T;
      Params_Out : out CUDA.Driver_Types.External_Semaphore_Wait_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_call_3  :
        aliased udriver_types_h.cudaExternalSemaphoreWaitNodeParams with
        Address => Params_Out'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExternalSemaphoresWaitNodeGetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_External_Semaphores_Wait_Node_Get_Params;

   ---
   -- Graph_External_Semaphores_Wait_Node_Set_Params --
   ---

   procedure Graph_External_Semaphores_Wait_Node_Set_Params
     (H_Node      : CUDA.Driver_Types.Graph_Node_T;
      Node_Params : CUDA.Driver_Types.External_Semaphore_Wait_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_call_3  :
        aliased constant udriver_types_h
          .cudaExternalSemaphoreWaitNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExternalSemaphoresWaitNodeSetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_External_Semaphores_Wait_Node_Set_Params;

   ---
   -- Graph_Add_Mem_Alloc_Node --
   ---

   function Graph_Add_Mem_Alloc_Node
     (Graph :     CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Node_Params : out CUDA.Driver_Types.Mem_Alloc_Node_Params)
      return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_call_6  : aliased udriver_types_h.cudaMemAllocNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_7   : Integer                :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddMemAllocNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_call_6'Unchecked_Access));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Mem_Alloc_Node;

   ---
   -- Graph_Mem_Alloc_Node_Get_Params --
   ---

   procedure Graph_Mem_Alloc_Node_Get_Params
     (Node       :     CUDA.Driver_Types.Graph_Node_T;
      Params_Out : out CUDA.Driver_Types.Mem_Alloc_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  : aliased udriver_types_h.cudaMemAllocNodeParams with
        Address => Params_Out'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphMemAllocNodeGetParams
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Mem_Alloc_Node_Get_Params;

   ---
   -- Graph_Add_Mem_Free_Node --
   ---

   function Graph_Add_Mem_Free_Node
     (Graph : CUDA.Driver_Types.Graph_T; Dependencies : Graph_Node_Array_T;
      Dptr  : System.Address) return CUDA.Driver_Types.Graph_Node_T
   is

      Temp_ret_1   : aliased CUDA.Driver_Types.Graph_Node_T;
      Temp_call_2  : aliased System.Address := Temp_ret_1'Address;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_5 : aliased System.Address with
        Address => Dptr'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddMemFreeNode
             (Temp_call_2, Temp_local_2, Dependencies'Address,
              Dependencies'Length, Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

      return Temp_ret_1;
   end Graph_Add_Mem_Free_Node;

   ---
   -- Graph_Mem_Free_Node_Get_Params --
   ---

   procedure Graph_Mem_Free_Node_Get_Params
     (Node : CUDA.Driver_Types.Graph_Node_T; Dptr_Out : System.Address)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Dptr_Out'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphMemFreeNodeGetParams
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graph_Mem_Free_Node_Get_Params;

   ---
   -- Device_Graph_Mem_Trim --
   ---

   procedure Device_Graph_Mem_Trim (Device : Device_T) is
      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaDeviceGraphMemTrim (int (Device)));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Device_Graph_Mem_Trim;

   ---
   -- Device_Get_Graph_Mem_Attribute --
   ---

   procedure Device_Get_Graph_Mem_Attribute
     (Device : Device_T; Attr : CUDA.Driver_Types.Graph_Mem_Attribute_Type;
      Value  : System.Address)
   is
      Temp_local_2 : aliased udriver_types_h.cudaGraphMemAttributeType with
        Address => Attr'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Value'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceGetGraphMemAttribute
             (int (Device), Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Device_Get_Graph_Mem_Attribute;

   ---
   -- Device_Set_Graph_Mem_Attribute --
   ---

   procedure Device_Set_Graph_Mem_Attribute
     (Device : Device_T; Attr : CUDA.Driver_Types.Graph_Mem_Attribute_Type;
      Value  : System.Address)
   is
      Temp_local_2 : aliased udriver_types_h.cudaGraphMemAttributeType with
        Address => Attr'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Value'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaDeviceSetGraphMemAttribute
             (int (Device), Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Device_Set_Graph_Mem_Attribute;

   ---
   -- Graph_Clone --
   ---

   procedure Graph_Clone
     (Graph_Clone : System.Address; Original_Graph : CUDA.Driver_Types.Graph_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Graph_Clone'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Original_Graph'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphClone (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graph_Clone;

   ---
   -- Graph_Node_Find_In_Clone --
   ---

   procedure Graph_Node_Find_In_Clone
     (Node : System.Address; Original_Node : CUDA.Driver_Types.Graph_Node_T;
      Cloned_Graph : CUDA.Driver_Types.Graph_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Node'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Original_Node'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaGraph_t with
        Address => Cloned_Graph'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphNodeFindInClone
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Node_Find_In_Clone;

   ---
   -- Graph_Node_Get_Type --
   ---

   procedure Graph_Node_Get_Type
     (Node   :     CUDA.Driver_Types.Graph_Node_T;
      P_Type : out CUDA.Driver_Types.Graph_Node_Type)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_3  : aliased udriver_types_h.cudaGraphNodeType with
        Address => P_Type'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphNodeGetType
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Node_Get_Type;

   ---
   -- Graph_Get_Nodes --
   ---

   procedure Graph_Get_Nodes
     (Graph     :     CUDA.Driver_Types.Graph_T; Nodes : System.Address;
      Num_Nodes : out CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Nodes'Address, Import;
      Temp_call_4  : aliased stddef_h.size_t with
        Address => Num_Nodes'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphGetNodes
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Get_Nodes;

   ---
   -- Graph_Get_Root_Nodes --
   ---

   procedure Graph_Get_Root_Nodes
     (Graph          : CUDA.Driver_Types.Graph_T; Root_Nodes : System.Address;
      Num_Root_Nodes : out CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Root_Nodes'Address, Import;
      Temp_call_4  : aliased stddef_h.size_t with
        Address => Num_Root_Nodes'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphGetRootNodes
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Get_Root_Nodes;

   ---
   -- Graph_Get_Edges --
   ---

   procedure Graph_Get_Edges
     (Graph : CUDA.Driver_Types.Graph_T; From : System.Address;
      To    : System.Address; Num_Edges : out CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => From'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => To'Address, Import;
      Temp_call_5  : aliased stddef_h.size_t with
        Address => Num_Edges'Address, Import;
      Temp_res_6   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphGetEdges
             (Temp_local_1, Temp_local_2, Temp_local_3,
              Temp_call_5'Unchecked_Access));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Graph_Get_Edges;

   ---
   -- Graph_Node_Get_Dependencies --
   ---

   procedure Graph_Node_Get_Dependencies
     (Node :     CUDA.Driver_Types.Graph_Node_T; Dependencies : System.Address;
      Num_Dependencies : out CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Dependencies'Address, Import;
      Temp_call_4  : aliased stddef_h.size_t with
        Address => Num_Dependencies'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphNodeGetDependencies
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Node_Get_Dependencies;

   ---
   -- Graph_Node_Get_Dependent_Nodes --
   ---

   procedure Graph_Node_Get_Dependent_Nodes
     (Node : CUDA.Driver_Types.Graph_Node_T; Dependent_Nodes : System.Address;
      Num_Dependent_Nodes : out CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Dependent_Nodes'Address, Import;
      Temp_call_4  : aliased stddef_h.size_t with
        Address => Num_Dependent_Nodes'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphNodeGetDependentNodes
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Node_Get_Dependent_Nodes;

   ---
   -- Graph_Add_Dependencies --
   ---

   procedure Graph_Add_Dependencies
     (Graph : CUDA.Driver_Types.Graph_T; From : System.Address;
      To    : System.Address; Num_Dependencies : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => From'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => To'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Num_Dependencies'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphAddDependencies
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Add_Dependencies;

   ---
   -- Graph_Remove_Dependencies --
   ---

   procedure Graph_Remove_Dependencies
     (Graph : CUDA.Driver_Types.Graph_T; From : System.Address;
      To    : System.Address; Num_Dependencies : CUDA.Stddef.Size_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => From'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => To'Address, Import;
      Temp_local_4 : aliased stddef_h.size_t with
        Address => Num_Dependencies'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphRemoveDependencies
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Remove_Dependencies;

   ---
   -- Graph_Destroy_Node --
   ---

   procedure Graph_Destroy_Node (Node : CUDA.Driver_Types.Graph_Node_T) is
      Temp_local_1 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaGraphDestroyNode (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Graph_Destroy_Node;

   ---
   -- Graph_Instantiate --
   ---

   procedure Graph_Instantiate
     (Graph_Exec  : System.Address; Graph : CUDA.Driver_Types.Graph_T;
      Error_Node  : System.Address; Log_Buffer : String;
      Buffer_Size : CUDA.Stddef.Size_T)
   is
      Temp_local_1    : aliased System.Address with
        Address => Graph_Exec'Address, Import;
      Temp_local_2    : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_3    : aliased System.Address with
        Address => Error_Node'Address, Import;
      Temp_c_string_6 : Interfaces.C.Strings.chars_ptr :=
        Interfaces.C.Strings.New_String (Log_Buffer);
      Temp_local_5    : aliased stddef_h.size_t with
        Address => Buffer_Size'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphInstantiate
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_c_string_6,
              Temp_local_5));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;
      Interfaces.C.Strings.Free (Temp_c_string_6);

   end Graph_Instantiate;

   ---
   -- Graph_Instantiate_With_Flags --
   ---

   procedure Graph_Instantiate_With_Flags
     (Graph_Exec : System.Address; Graph : CUDA.Driver_Types.Graph_T;
      Flags      : Extensions.unsigned_long_long)
   is
      Temp_local_1 : aliased System.Address with
        Address => Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_3 : aliased Interfaces.C.Extensions.unsigned_long_long with
        Address => Flags'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphInstantiateWithFlags
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Instantiate_With_Flags;

   ---
   -- Graph_Exec_Kernel_Node_Set_Params --
   ---

   procedure Graph_Exec_Kernel_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.Kernel_Node_Params.Kernel_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_4  :
        aliased constant udriver_types_h.Class_cudaKernelNodeParams
          .cudaKernelNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecKernelNodeSetParams
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Exec_Kernel_Node_Set_Params;

   ---
   -- Graph_Exec_Memcpy_Node_Set_Params --
   ---

   procedure Graph_Exec_Memcpy_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.Memcpy_3D_Parms)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_4  : aliased constant udriver_types_h.cudaMemcpy3DParms with
        Address => Node_Params'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecMemcpyNodeSetParams
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Exec_Memcpy_Node_Set_Params;

   ---
   -- Graph_Exec_Memcpy_Node_Set_Params_To_Symbol --
   ---

   procedure Graph_Exec_Memcpy_Node_Set_Params_To_Symbol
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T; Symbol : System.Address;
      Src          : System.Address; Count : CUDA.Stddef.Size_T;
      Offset       : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_7 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_8 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecMemcpyNodeSetParamsToSymbol
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7));

   begin
      null;

      if Temp_res_8 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_8)));
      end if;

   end Graph_Exec_Memcpy_Node_Set_Params_To_Symbol;

   ---
   -- Graph_Exec_Memcpy_Node_Set_Params_From_Symbol --
   ---

   procedure Graph_Exec_Memcpy_Node_Set_Params_From_Symbol
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T; Dst : System.Address;
      Symbol       : System.Address; Count : CUDA.Stddef.Size_T;
      Offset       : CUDA.Stddef.Size_T; Kind : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Symbol'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_6 : aliased stddef_h.size_t with
        Address => Offset'Address, Import;
      Temp_local_7 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_8 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecMemcpyNodeSetParamsFromSymbol
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6, Temp_local_7));

   begin
      null;

      if Temp_res_8 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_8)));
      end if;

   end Graph_Exec_Memcpy_Node_Set_Params_From_Symbol;

   ---
   -- Graph_Exec_Memcpy_Node_Set_Params_1D --
   ---

   procedure Graph_Exec_Memcpy_Node_Set_Params_1D
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T; Dst : System.Address;
      Src          : System.Address; Count : CUDA.Stddef.Size_T;
      Kind         : CUDA.Driver_Types.Memcpy_Kind)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => Dst'Address, Import;
      Temp_local_4 : aliased System.Address with
        Address => Src'Address, Import;
      Temp_local_5 : aliased stddef_h.size_t with
        Address => Count'Address, Import;
      Temp_local_6 : aliased udriver_types_h.cudaMemcpyKind with
        Address => Kind'Address, Import;

      Temp_res_7 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecMemcpyNodeSetParams1D
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5, Temp_local_6));

   begin
      null;

      if Temp_res_7 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_7)));
      end if;

   end Graph_Exec_Memcpy_Node_Set_Params_1D;

   ---
   -- Graph_Exec_Memset_Node_Set_Params --
   ---

   procedure Graph_Exec_Memset_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.Memset_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_4  : aliased constant udriver_types_h.cudaMemsetParams with
        Address => Node_Params'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecMemsetNodeSetParams
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Exec_Memset_Node_Set_Params;

   ---
   -- Graph_Exec_Host_Node_Set_Params --
   ---

   procedure Graph_Exec_Host_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.Host_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_call_4  : aliased constant udriver_types_h.cudaHostNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecHostNodeSetParams
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Exec_Host_Node_Set_Params;

   ---
   -- Graph_Exec_Child_Graph_Node_Set_Params --
   ---

   procedure Graph_Exec_Child_Graph_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Node         : CUDA.Driver_Types.Graph_Node_T;
      Child_Graph  : CUDA.Driver_Types.Graph_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => Node'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaGraph_t with
        Address => Child_Graph'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecChildGraphNodeSetParams
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Exec_Child_Graph_Node_Set_Params;

   ---
   -- Graph_Exec_Event_Record_Node_Set_Event --
   ---

   procedure Graph_Exec_Event_Record_Node_Set_Event
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      H_Node       : CUDA.Driver_Types.Graph_Node_T;
      Event        : CUDA.Driver_Types.Event_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecEventRecordNodeSetEvent
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Exec_Event_Record_Node_Set_Event;

   ---
   -- Graph_Exec_Event_Wait_Node_Set_Event --
   ---

   procedure Graph_Exec_Event_Wait_Node_Set_Event
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      H_Node       : CUDA.Driver_Types.Graph_Node_T;
      Event        : CUDA.Driver_Types.Event_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaEvent_t with
        Address => Event'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecEventWaitNodeSetEvent
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Exec_Event_Wait_Node_Set_Event;

   ---
   -- Graph_Exec_External_Semaphores_Signal_Node_Set_Params --
   ---

   procedure Graph_Exec_External_Semaphores_Signal_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      H_Node       : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.External_Semaphore_Signal_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_call_4  :
        aliased constant udriver_types_h
          .cudaExternalSemaphoreSignalNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h
             .cudaGraphExecExternalSemaphoresSignalNodeSetParams
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Exec_External_Semaphores_Signal_Node_Set_Params;

   ---
   -- Graph_Exec_External_Semaphores_Wait_Node_Set_Params --
   ---

   procedure Graph_Exec_External_Semaphores_Wait_Node_Set_Params
     (H_Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      H_Node       : CUDA.Driver_Types.Graph_Node_T;
      Node_Params  : CUDA.Driver_Types.External_Semaphore_Wait_Node_Params)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraphNode_t with
        Address => H_Node'Address, Import;
      Temp_call_4  :
        aliased constant udriver_types_h
          .cudaExternalSemaphoreWaitNodeParams with
        Address => Node_Params'Address, Import;
      Temp_res_5   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecExternalSemaphoresWaitNodeSetParams
             (Temp_local_1, Temp_local_2, Temp_call_4'Unchecked_Access));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Exec_External_Semaphores_Wait_Node_Set_Params;

   ---
   -- Graph_Exec_Update --
   ---

   procedure Graph_Exec_Update
     (H_Graph_Exec      :     CUDA.Driver_Types.Graph_Exec_T;
      H_Graph : CUDA.Driver_Types.Graph_T; H_Error_Node_Out : System.Address;
      Update_Result_Out : out CUDA.Driver_Types.Graph_Exec_Update_Result)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => H_Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaGraph_t with
        Address => H_Graph'Address, Import;
      Temp_local_3 : aliased System.Address with
        Address => H_Error_Node_Out'Address, Import;
      Temp_call_5  : aliased udriver_types_h.cudaGraphExecUpdateResult with
        Address => Update_Result_Out'Address, Import;
      Temp_res_6   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphExecUpdate
             (Temp_local_1, Temp_local_2, Temp_local_3,
              Temp_call_5'Unchecked_Access));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end Graph_Exec_Update;

   ---
   -- Graph_Upload --
   ---

   procedure Graph_Upload
     (Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Stream     : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphUpload (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graph_Upload;

   ---
   -- Graph_Launch --
   ---

   procedure Graph_Launch
     (Graph_Exec : CUDA.Driver_Types.Graph_Exec_T;
      Stream     : CUDA.Driver_Types.Stream_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => Graph_Exec'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaStream_t with
        Address => Stream'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphLaunch (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Graph_Launch;

   ---
   -- Graph_Exec_Destroy --
   ---

   procedure Graph_Exec_Destroy (Graph_Exec : CUDA.Driver_Types.Graph_Exec_T)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraphExec_t with
        Address => Graph_Exec'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaGraphExecDestroy (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Graph_Exec_Destroy;

   ---
   -- Graph_Destroy --
   ---

   procedure Graph_Destroy (Graph : CUDA.Driver_Types.Graph_T) is
      Temp_local_1 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;

      Temp_res_2 : Integer :=
        Integer (ucuda_runtime_api_h.cudaGraphDestroy (Temp_local_1));

   begin
      null;

      if Temp_res_2 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_2)));
      end if;

   end Graph_Destroy;

   ---
   -- Graph_Debug_Dot_Print --
   ---

   procedure Graph_Debug_Dot_Print
     (Graph : CUDA.Driver_Types.Graph_T; Path : String; Flags : unsigned)
   is
      Temp_local_1    : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_c_string_4 : Interfaces.C.Strings.chars_ptr :=
        Interfaces.C.Strings.New_String (Path);
      Temp_local_3    : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphDebugDotPrint
             (Temp_local_1, Temp_c_string_4, Temp_local_3));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;
      Interfaces.C.Strings.Free (Temp_c_string_4);

   end Graph_Debug_Dot_Print;

   ---
   -- User_Object_Create --
   ---

   procedure User_Object_Create
     (Object_Out : System.Address; Ptr : System.Address;
      Destroy    : CUDA.Driver_Types.Host_Fn_T; Initial_Refcount : unsigned;
      Flags      : unsigned)
   is
      Temp_local_1 : aliased System.Address with
        Address => Object_Out'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Ptr'Address, Import;
      Temp_local_3 : aliased udriver_types_h.cudaHostFn_t with
        Address => Destroy'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Initial_Refcount'Address, Import;
      Temp_local_5 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_6 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaUserObjectCreate
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4,
              Temp_local_5));

   begin
      null;

      if Temp_res_6 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_6)));
      end if;

   end User_Object_Create;

   ---
   -- User_Object_Retain --
   ---

   procedure User_Object_Retain
     (Object : CUDA.Driver_Types.User_Object_T; Count : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaUserObject_t with
        Address => Object'Address, Import;
      Temp_local_2 : aliased Interfaces.C.unsigned with
        Address => Count'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaUserObjectRetain
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end User_Object_Retain;

   ---
   -- User_Object_Release --
   ---

   procedure User_Object_Release
     (Object : CUDA.Driver_Types.User_Object_T; Count : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaUserObject_t with
        Address => Object'Address, Import;
      Temp_local_2 : aliased Interfaces.C.unsigned with
        Address => Count'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaUserObjectRelease
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end User_Object_Release;

   ---
   -- Graph_Retain_User_Object --
   ---

   procedure Graph_Retain_User_Object
     (Graph  : CUDA.Driver_Types.Graph_T;
      Object : CUDA.Driver_Types.User_Object_T; Count : unsigned;
      Flags  : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaUserObject_t with
        Address => Object'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Count'Address, Import;
      Temp_local_4 : aliased Interfaces.C.unsigned with
        Address => Flags'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphRetainUserObject
             (Temp_local_1, Temp_local_2, Temp_local_3, Temp_local_4));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;

   end Graph_Retain_User_Object;

   ---
   -- Graph_Release_User_Object --
   ---

   procedure Graph_Release_User_Object
     (Graph  : CUDA.Driver_Types.Graph_T;
      Object : CUDA.Driver_Types.User_Object_T; Count : unsigned)
   is
      Temp_local_1 : aliased udriver_types_h.cudaGraph_t with
        Address => Graph'Address, Import;
      Temp_local_2 : aliased udriver_types_h.cudaUserObject_t with
        Address => Object'Address, Import;
      Temp_local_3 : aliased Interfaces.C.unsigned with
        Address => Count'Address, Import;

      Temp_res_4 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGraphReleaseUserObject
             (Temp_local_1, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Graph_Release_User_Object;

   ---
   -- Get_Driver_Entry_Point --
   ---

   procedure Get_Driver_Entry_Point
     (Symbol : String; Func_Ptr : System.Address;
      Flags  : Extensions.unsigned_long_long)
   is
      Temp_c_string_4 : Interfaces.C.Strings.chars_ptr :=
        Interfaces.C.Strings.New_String (Symbol);
      Temp_local_2    : aliased System.Address with
        Address => Func_Ptr'Address, Import;
      Temp_local_3    : aliased Interfaces.C.Extensions.unsigned_long_long with
        Address => Flags'Address, Import;

      Temp_res_5 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetDriverEntryPoint
             (Temp_c_string_4, Temp_local_2, Temp_local_3));

   begin
      null;

      if Temp_res_5 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_5)));
      end if;
      Interfaces.C.Strings.Free (Temp_c_string_4);

   end Get_Driver_Entry_Point;

   ---
   -- Get_Export_Table --
   ---

   procedure Get_Export_Table
     (Pp_Export_Table : System.Address;
      Export_Table_Id : CUDA.Driver_Types.UUID_T)
   is
      Temp_local_1 : aliased System.Address with
        Address => Pp_Export_Table'Address, Import;
      Temp_call_3  : aliased constant udriver_types_h.cudaUUID_t with
        Address => Export_Table_Id'Address, Import;
      Temp_res_4   : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetExportTable
             (Temp_local_1, Temp_call_3'Unchecked_Access));

   begin
      null;

      if Temp_res_4 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_4)));
      end if;

   end Get_Export_Table;

   ---
   -- Get_Func_By_Symbol --
   ---

   procedure Get_Func_By_Symbol
     (Function_Ptr : System.Address; Symbol_Ptr : System.Address)
   is
      Temp_local_1 : aliased System.Address with
        Address => Function_Ptr'Address, Import;
      Temp_local_2 : aliased System.Address with
        Address => Symbol_Ptr'Address, Import;

      Temp_res_3 : Integer :=
        Integer
          (ucuda_runtime_api_h.cudaGetFuncBySymbol
             (Temp_local_1, Temp_local_2));

   begin
      null;

      if Temp_res_3 /= 0 then
         Ada.Exceptions.Raise_Exception
           (CUDA.Exceptions.Exception_Registry.Element (Integer (Temp_res_3)));
      end if;

   end Get_Func_By_Symbol;

begin
   null;

end CUDA.Runtime_Api;
