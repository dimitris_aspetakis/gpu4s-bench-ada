with uvector_types_h;
use uvector_types_h;

package body CUDA.Vector_Types is
   package body Class_Dim3 is
      ---
      -- New_Dim3 --
      ---

      function New_Dim3
        (Vx : unsigned; Vy : unsigned; Vz : unsigned) return Dim3
      is
         Temp_local_1 : aliased Interfaces.C.unsigned with
           Address => Vx'Address, Import;
         Temp_local_2 : aliased Interfaces.C.unsigned with
           Address => Vy'Address, Import;
         Temp_local_3 : aliased Interfaces.C.unsigned with
           Address => Vz'Address, Import;

         Temp_result_orig_1 : aliased uvector_types_h.Class_dim3.dim3 :=
           uvector_types_h.Class_dim3.New_dim3
             (Temp_local_1, Temp_local_2, Temp_local_3);

         Temp_result_wrapped_4 : aliased Dim3 with
           Address => Temp_result_orig_1'Address, Import;

      begin

         return Temp_result_wrapped_4;
      end New_Dim3;

      ---
      -- New_Dim3 --
      ---

      function New_Dim3 (V : Uint3) return Dim3 is
         Temp_local_1 : aliased uvector_types_h.uint3 with
           Address => V'Address, Import;

         Temp_result_orig_1 : aliased uvector_types_h.Class_dim3.dim3 :=
           uvector_types_h.Class_dim3.New_dim3 (Temp_local_1);

         Temp_result_wrapped_2 : aliased Dim3 with
           Address => Temp_result_orig_1'Address, Import;

      begin

         return Temp_result_wrapped_2;
      end New_Dim3;

      ---
      -- Operator_1 --
      ---

      function Operator_1 (This : Dim3) return Uint3 is
         Temp_call_2        : aliased constant Dim3 with
           Address => This'Address, Import;
         Temp_result_orig_1 : aliased uvector_types_h.uint3 :=
           uvector_types_h.Class_dim3.operator_1
             (Temp_call_2'Unchecked_Access);

         Temp_result_wrapped_3 : aliased Uint3 with
           Address => Temp_result_orig_1'Address, Import;

      begin

         return Temp_result_wrapped_3;
      end Operator_1;

   begin
      null;

   end Class_Dim3;
begin
   null;

end CUDA.Vector_Types;
