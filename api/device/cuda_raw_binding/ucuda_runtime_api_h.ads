pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with udriver_types_h;
with stddef_h;
with Interfaces.C.Strings;
with System;
with Interfaces.C.Extensions;
with uvector_types_h;
with utexture_types_h;
with usurface_types_h;

package ucuda_runtime_api_h is

   CUDART_VERSION : constant := 11050;  --  /usr/local/cuda-11.5/include//cuda_runtime_api.h:138
   --  unsupported macro: cudaSignalExternalSemaphoresAsync __CUDART_API_PTSZ(cudaSignalExternalSemaphoresAsync_v2)
   --  unsupported macro: cudaWaitExternalSemaphoresAsync __CUDART_API_PTSZ(cudaWaitExternalSemaphoresAsync_v2)

   function cudaDeviceReset return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:306
   pragma Import (C, cudaDeviceReset, "cudaDeviceReset");

   function cudaDeviceSynchronize return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:327
   pragma Import (C, cudaDeviceSynchronize, "cudaDeviceSynchronize");

   function cudaDeviceSetLimit (limit : udriver_types_h.cudaLimit; value : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:414
   pragma Import (C, cudaDeviceSetLimit, "cudaDeviceSetLimit");

   function cudaDeviceGetLimit (pValue : access stddef_h.size_t; limit : udriver_types_h.cudaLimit) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:449
   pragma Import (C, cudaDeviceGetLimit, "cudaDeviceGetLimit");

   function cudaDeviceGetTexture1DLinearMaxWidth
     (maxWidthInElements : access stddef_h.size_t;
      fmtDesc : access constant udriver_types_h.cudaChannelFormatDesc;
      device : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:472
   pragma Import (C, cudaDeviceGetTexture1DLinearMaxWidth, "cudaDeviceGetTexture1DLinearMaxWidth");

   function cudaDeviceGetCacheConfig (pCacheConfig : access udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:506
   pragma Import (C, cudaDeviceGetCacheConfig, "cudaDeviceGetCacheConfig");

   function cudaDeviceGetStreamPriorityRange (leastPriority : access int; greatestPriority : access int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:543
   pragma Import (C, cudaDeviceGetStreamPriorityRange, "cudaDeviceGetStreamPriorityRange");

   function cudaDeviceSetCacheConfig (cacheConfig : udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:587
   pragma Import (C, cudaDeviceSetCacheConfig, "cudaDeviceSetCacheConfig");

   function cudaDeviceGetSharedMemConfig (pConfig : access udriver_types_h.cudaSharedMemConfig) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:618
   pragma Import (C, cudaDeviceGetSharedMemConfig, "cudaDeviceGetSharedMemConfig");

   function cudaDeviceSetSharedMemConfig (config : udriver_types_h.cudaSharedMemConfig) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:662
   pragma Import (C, cudaDeviceSetSharedMemConfig, "cudaDeviceSetSharedMemConfig");

   function cudaDeviceGetByPCIBusId (device : access int; pciBusId : Interfaces.C.Strings.chars_ptr) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:689
   pragma Import (C, cudaDeviceGetByPCIBusId, "cudaDeviceGetByPCIBusId");

   function cudaDeviceGetPCIBusId
     (pciBusId : Interfaces.C.Strings.chars_ptr;
      len : int;
      device : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:719
   pragma Import (C, cudaDeviceGetPCIBusId, "cudaDeviceGetPCIBusId");

   function cudaIpcGetEventHandle (handle : access udriver_types_h.cudaIpcEventHandle_t; event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:767
   pragma Import (C, cudaIpcGetEventHandle, "cudaIpcGetEventHandle");

   function cudaIpcOpenEventHandle (event : System.Address; handle : udriver_types_h.cudaIpcEventHandle_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:808
   pragma Import (C, cudaIpcOpenEventHandle, "cudaIpcOpenEventHandle");

   function cudaIpcGetMemHandle (handle : access udriver_types_h.cudaIpcMemHandle_t; devPtr : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:851
   pragma Import (C, cudaIpcGetMemHandle, "cudaIpcGetMemHandle");

   function cudaIpcOpenMemHandle
     (devPtr : System.Address;
      handle : udriver_types_h.cudaIpcMemHandle_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:915
   pragma Import (C, cudaIpcOpenMemHandle, "cudaIpcOpenMemHandle");

   function cudaIpcCloseMemHandle (devPtr : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:951
   pragma Import (C, cudaIpcCloseMemHandle, "cudaIpcCloseMemHandle");

   function cudaDeviceFlushGPUDirectRDMAWrites (target : udriver_types_h.cudaFlushGPUDirectRDMAWritesTarget; scope : udriver_types_h.cudaFlushGPUDirectRDMAWritesScope) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:983
   pragma Import (C, cudaDeviceFlushGPUDirectRDMAWrites, "cudaDeviceFlushGPUDirectRDMAWrites");

   function cudaThreadExit return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1027
   pragma Import (C, cudaThreadExit, "cudaThreadExit");

   function cudaThreadSynchronize return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1053
   pragma Import (C, cudaThreadSynchronize, "cudaThreadSynchronize");

   function cudaThreadSetLimit (limit : udriver_types_h.cudaLimit; value : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1102
   pragma Import (C, cudaThreadSetLimit, "cudaThreadSetLimit");

   function cudaThreadGetLimit (pValue : access stddef_h.size_t; limit : udriver_types_h.cudaLimit) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1135
   pragma Import (C, cudaThreadGetLimit, "cudaThreadGetLimit");

   function cudaThreadGetCacheConfig (pCacheConfig : access udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1171
   pragma Import (C, cudaThreadGetCacheConfig, "cudaThreadGetCacheConfig");

   function cudaThreadSetCacheConfig (cacheConfig : udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1218
   pragma Import (C, cudaThreadSetCacheConfig, "cudaThreadSetCacheConfig");

   function cudaGetLastError return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1281
   pragma Import (C, cudaGetLastError, "cudaGetLastError");

   function cudaPeekAtLastError return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1329
   pragma Import (C, cudaPeekAtLastError, "cudaPeekAtLastError");

   function cudaGetErrorName (error : udriver_types_h.cudaError_t) return Interfaces.C.Strings.chars_ptr;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1345
   pragma Import (C, cudaGetErrorName, "cudaGetErrorName");

   function cudaGetErrorString (error : udriver_types_h.cudaError_t) return Interfaces.C.Strings.chars_ptr;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1361
   pragma Import (C, cudaGetErrorString, "cudaGetErrorString");

   function cudaGetDeviceCount (count : access int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1389
   pragma Import (C, cudaGetDeviceCount, "cudaGetDeviceCount");

   function cudaGetDeviceProperties (prop : access udriver_types_h.cudaDeviceProp; device : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1667
   pragma Import (C, cudaGetDeviceProperties, "cudaGetDeviceProperties");

   function cudaDeviceGetAttribute
     (value : access int;
      attr : udriver_types_h.cudaDeviceAttr;
      device : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1868
   pragma Import (C, cudaDeviceGetAttribute, "cudaDeviceGetAttribute");

   function cudaDeviceGetDefaultMemPool (memPool : System.Address; device : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1886
   pragma Import (C, cudaDeviceGetDefaultMemPool, "cudaDeviceGetDefaultMemPool");

   function cudaDeviceSetMemPool (device : int; memPool : udriver_types_h.cudaMemPool_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1910
   pragma Import (C, cudaDeviceSetMemPool, "cudaDeviceSetMemPool");

   function cudaDeviceGetMemPool (memPool : System.Address; device : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1930
   pragma Import (C, cudaDeviceGetMemPool, "cudaDeviceGetMemPool");

   function cudaDeviceGetNvSciSyncAttributes
     (nvSciSyncAttrList : System.Address;
      device : int;
      flags : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:1978
   pragma Import (C, cudaDeviceGetNvSciSyncAttributes, "cudaDeviceGetNvSciSyncAttributes");

   function cudaDeviceGetP2PAttribute
     (value : access int;
      attr : udriver_types_h.cudaDeviceP2PAttr;
      srcDevice : int;
      dstDevice : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2018
   pragma Import (C, cudaDeviceGetP2PAttribute, "cudaDeviceGetP2PAttribute");

   function cudaChooseDevice (device : access int; prop : access constant udriver_types_h.cudaDeviceProp) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2039
   pragma Import (C, cudaChooseDevice, "cudaChooseDevice");

   function cudaSetDevice (device : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2080
   pragma Import (C, cudaSetDevice, "cudaSetDevice");

   function cudaGetDevice (device : access int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2101
   pragma Import (C, cudaGetDevice, "cudaGetDevice");

   function cudaSetValidDevices (device_arr : access int; len : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2132
   pragma Import (C, cudaSetValidDevices, "cudaSetValidDevices");

   function cudaSetDeviceFlags (flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2197
   pragma Import (C, cudaSetDeviceFlags, "cudaSetDeviceFlags");

   function cudaGetDeviceFlags (flags : access unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2241
   pragma Import (C, cudaGetDeviceFlags, "cudaGetDeviceFlags");

   function cudaStreamCreate (pStream : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2281
   pragma Import (C, cudaStreamCreate, "cudaStreamCreate");

   function cudaStreamCreateWithFlags (pStream : System.Address; flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2313
   pragma Import (C, cudaStreamCreateWithFlags, "cudaStreamCreateWithFlags");

   function cudaStreamCreateWithPriority
     (pStream : System.Address;
      flags : unsigned;
      priority : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2359
   pragma Import (C, cudaStreamCreateWithPriority, "cudaStreamCreateWithPriority");

   function cudaStreamGetPriority (hStream : udriver_types_h.cudaStream_t; priority : access int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2386
   pragma Import (C, cudaStreamGetPriority, "cudaStreamGetPriority");

   function cudaStreamGetFlags (hStream : udriver_types_h.cudaStream_t; flags : access unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2411
   pragma Import (C, cudaStreamGetFlags, "cudaStreamGetFlags");

   function cudaCtxResetPersistingL2Cache return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2426
   pragma Import (C, cudaCtxResetPersistingL2Cache, "cudaCtxResetPersistingL2Cache");

   function cudaStreamCopyAttributes (dst : udriver_types_h.cudaStream_t; src : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2446
   pragma Import (C, cudaStreamCopyAttributes, "cudaStreamCopyAttributes");

   function cudaStreamGetAttribute
     (hStream : udriver_types_h.cudaStream_t;
      attr : udriver_types_h.cudaStreamAttrID;
      value_out : access udriver_types_h.cudaStreamAttrValue) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2467
   pragma Import (C, cudaStreamGetAttribute, "cudaStreamGetAttribute");

   function cudaStreamSetAttribute
     (hStream : udriver_types_h.cudaStream_t;
      attr : udriver_types_h.cudaStreamAttrID;
      value : access constant udriver_types_h.cudaStreamAttrValue) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2491
   pragma Import (C, cudaStreamSetAttribute, "cudaStreamSetAttribute");

   function cudaStreamDestroy (stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2525
   pragma Import (C, cudaStreamDestroy, "cudaStreamDestroy");

   function cudaStreamWaitEvent
     (stream : udriver_types_h.cudaStream_t;
      event : udriver_types_h.cudaEvent_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2556
   pragma Import (C, cudaStreamWaitEvent, "cudaStreamWaitEvent");

   type cudaStreamCallback_t is access procedure
        (arg1 : udriver_types_h.cudaStream_t;
         arg2 : udriver_types_h.cudaError_t;
         arg3 : System.Address);
   pragma Convention (C, cudaStreamCallback_t);  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2564

   function cudaStreamAddCallback
     (stream : udriver_types_h.cudaStream_t;
      callback : cudaStreamCallback_t;
      userData : System.Address;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2631
   pragma Import (C, cudaStreamAddCallback, "cudaStreamAddCallback");

   function cudaStreamSynchronize (stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2655
   pragma Import (C, cudaStreamSynchronize, "cudaStreamSynchronize");

   function cudaStreamQuery (stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2680
   pragma Import (C, cudaStreamQuery, "cudaStreamQuery");

   function cudaStreamAttachMemAsync
     (stream : udriver_types_h.cudaStream_t;
      devPtr : System.Address;
      length : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2764
   pragma Import (C, cudaStreamAttachMemAsync, "cudaStreamAttachMemAsync");

   function cudaStreamBeginCapture (stream : udriver_types_h.cudaStream_t; mode : udriver_types_h.cudaStreamCaptureMode) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2803
   pragma Import (C, cudaStreamBeginCapture, "cudaStreamBeginCapture");

   function cudaThreadExchangeStreamCaptureMode (mode : access udriver_types_h.cudaStreamCaptureMode) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2854
   pragma Import (C, cudaThreadExchangeStreamCaptureMode, "cudaThreadExchangeStreamCaptureMode");

   function cudaStreamEndCapture (stream : udriver_types_h.cudaStream_t; pGraph : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2882
   pragma Import (C, cudaStreamEndCapture, "cudaStreamEndCapture");

   function cudaStreamIsCapturing (stream : udriver_types_h.cudaStream_t; pCaptureStatus : access udriver_types_h.cudaStreamCaptureStatus) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2920
   pragma Import (C, cudaStreamIsCapturing, "cudaStreamIsCapturing");

   function cudaStreamGetCaptureInfo
     (stream : udriver_types_h.cudaStream_t;
      pCaptureStatus : access udriver_types_h.cudaStreamCaptureStatus;
      pId : access Extensions.unsigned_long_long) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:2952
   pragma Import (C, cudaStreamGetCaptureInfo, "cudaStreamGetCaptureInfo");

   function cudaStreamGetCaptureInfo_v2
     (stream : udriver_types_h.cudaStream_t;
      captureStatus_out : access udriver_types_h.cudaStreamCaptureStatus;
      id_out : access Extensions.unsigned_long_long;
      graph_out : System.Address;
      dependencies_out : System.Address;
      numDependencies_out : access stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3007
   pragma Import (C, cudaStreamGetCaptureInfo_v2, "cudaStreamGetCaptureInfo_v2");

   function cudaStreamUpdateCaptureDependencies
     (stream : udriver_types_h.cudaStream_t;
      dependencies : System.Address;
      numDependencies : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3040
   pragma Import (C, cudaStreamUpdateCaptureDependencies, "cudaStreamUpdateCaptureDependencies");

   function cudaEventCreate (event : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3077
   pragma Import (C, cudaEventCreate, "cudaEventCreate");

   function cudaEventCreateWithFlags (event : System.Address; flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3114
   pragma Import (C, cudaEventCreateWithFlags, "cudaEventCreateWithFlags");

   function cudaEventRecord (event : udriver_types_h.cudaEvent_t; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3154
   pragma Import (C, cudaEventRecord, "cudaEventRecord");

   function cudaEventRecordWithFlags
     (event : udriver_types_h.cudaEvent_t;
      stream : udriver_types_h.cudaStream_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3201
   pragma Import (C, cudaEventRecordWithFlags, "cudaEventRecordWithFlags");

   function cudaEventQuery (event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3233
   pragma Import (C, cudaEventQuery, "cudaEventQuery");

   function cudaEventSynchronize (event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3263
   pragma Import (C, cudaEventSynchronize, "cudaEventSynchronize");

   function cudaEventDestroy (event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3292
   pragma Import (C, cudaEventDestroy, "cudaEventDestroy");

   function cudaEventElapsedTime
     (ms : access float;
      start : udriver_types_h.cudaEvent_t;
      c_end : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3335
   pragma Import (C, cudaEventElapsedTime, "cudaEventElapsedTime");

   function cudaImportExternalMemory (extMem_out : System.Address; memHandleDesc : access constant udriver_types_h.cudaExternalMemoryHandleDesc) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3515
   pragma Import (C, cudaImportExternalMemory, "cudaImportExternalMemory");

   function cudaExternalMemoryGetMappedBuffer
     (devPtr : System.Address;
      extMem : udriver_types_h.cudaExternalMemory_t;
      bufferDesc : access constant udriver_types_h.cudaExternalMemoryBufferDesc) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3570
   pragma Import (C, cudaExternalMemoryGetMappedBuffer, "cudaExternalMemoryGetMappedBuffer");

   function cudaExternalMemoryGetMappedMipmappedArray
     (mipmap : System.Address;
      extMem : udriver_types_h.cudaExternalMemory_t;
      mipmapDesc : access constant udriver_types_h.cudaExternalMemoryMipmappedArrayDesc) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3630
   pragma Import (C, cudaExternalMemoryGetMappedMipmappedArray, "cudaExternalMemoryGetMappedMipmappedArray");

   function cudaDestroyExternalMemory (extMem : udriver_types_h.cudaExternalMemory_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3654
   pragma Import (C, cudaDestroyExternalMemory, "cudaDestroyExternalMemory");

   function cudaImportExternalSemaphore (extSem_out : System.Address; semHandleDesc : access constant udriver_types_h.cudaExternalSemaphoreHandleDesc) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3807
   pragma Import (C, cudaImportExternalSemaphore, "cudaImportExternalSemaphore");

   function cudaSignalExternalSemaphoresAsync_v2
     (extSemArray : System.Address;
      paramsArray : access constant udriver_types_h.cudaExternalSemaphoreSignalParams;
      numExtSems : unsigned;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3874
   pragma Import (C, cudaSignalExternalSemaphoresAsync_v2, "cudaSignalExternalSemaphoresAsync_v2");

   function cudaWaitExternalSemaphoresAsync_v2
     (extSemArray : System.Address;
      paramsArray : access constant udriver_types_h.cudaExternalSemaphoreWaitParams;
      numExtSems : unsigned;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3950
   pragma Import (C, cudaWaitExternalSemaphoresAsync_v2, "cudaWaitExternalSemaphoresAsync_v2");

   function cudaDestroyExternalSemaphore (extSem : udriver_types_h.cudaExternalSemaphore_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:3973
   pragma Import (C, cudaDestroyExternalSemaphore, "cudaDestroyExternalSemaphore");

   function cudaLaunchKernel
     (func : System.Address;
      gridDim : uvector_types_h.Class_dim3.dim3;
      blockDim : uvector_types_h.Class_dim3.dim3;
      args : System.Address;
      sharedMem : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4040
   pragma Import (C, cudaLaunchKernel, "cudaLaunchKernel");

   function cudaLaunchCooperativeKernel
     (func : System.Address;
      gridDim : uvector_types_h.Class_dim3.dim3;
      blockDim : uvector_types_h.Class_dim3.dim3;
      args : System.Address;
      sharedMem : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4097
   pragma Import (C, cudaLaunchCooperativeKernel, "cudaLaunchCooperativeKernel");

   function cudaLaunchCooperativeKernelMultiDevice
     (launchParamsList : access udriver_types_h.Class_cudaLaunchParams.cudaLaunchParams;
      numDevices : unsigned;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4198
   pragma Import (C, cudaLaunchCooperativeKernelMultiDevice, "cudaLaunchCooperativeKernelMultiDevice");

   function cudaFuncSetCacheConfig (func : System.Address; cacheConfig : udriver_types_h.cudaFuncCache) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4245
   pragma Import (C, cudaFuncSetCacheConfig, "cudaFuncSetCacheConfig");

   function cudaFuncSetSharedMemConfig (func : System.Address; config : udriver_types_h.cudaSharedMemConfig) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4300
   pragma Import (C, cudaFuncSetSharedMemConfig, "cudaFuncSetSharedMemConfig");

   function cudaFuncGetAttributes (attr : access udriver_types_h.cudaFuncAttributes; func : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4333
   pragma Import (C, cudaFuncGetAttributes, "cudaFuncGetAttributes");

   function cudaFuncSetAttribute
     (func : System.Address;
      attr : udriver_types_h.cudaFuncAttribute;
      value : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4370
   pragma Import (C, cudaFuncSetAttribute, "cudaFuncSetAttribute");

   function cudaSetDoubleForDevice (d : access double) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4396
   pragma Import (C, cudaSetDoubleForDevice, "cudaSetDoubleForDevice");

   function cudaSetDoubleForHost (d : access double) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4420
   pragma Import (C, cudaSetDoubleForHost, "cudaSetDoubleForHost");

   function cudaLaunchHostFunc
     (stream : udriver_types_h.cudaStream_t;
      fn : udriver_types_h.cudaHostFn_t;
      userData : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4488
   pragma Import (C, cudaLaunchHostFunc, "cudaLaunchHostFunc");

   function cudaOccupancyMaxActiveBlocksPerMultiprocessor
     (numBlocks : access int;
      func : System.Address;
      blockSize : int;
      dynamicSMemSize : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4545
   pragma Import (C, cudaOccupancyMaxActiveBlocksPerMultiprocessor, "cudaOccupancyMaxActiveBlocksPerMultiprocessor");

   function cudaOccupancyAvailableDynamicSMemPerBlock
     (dynamicSmemSize : access stddef_h.size_t;
      func : System.Address;
      numBlocks : int;
      blockSize : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4574
   pragma Import (C, cudaOccupancyAvailableDynamicSMemPerBlock, "cudaOccupancyAvailableDynamicSMemPerBlock");

   function cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags
     (numBlocks : access int;
      func : System.Address;
      blockSize : int;
      dynamicSMemSize : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4619
   pragma Import (C, cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags, "cudaOccupancyMaxActiveBlocksPerMultiprocessorWithFlags");

   function cudaMallocManaged
     (devPtr : System.Address;
      size : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4740
   pragma Import (C, cudaMallocManaged, "cudaMallocManaged");

   function cudaMalloc (devPtr : System.Address; size : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4773
   pragma Import (C, cudaMalloc, "cudaMalloc");

   function cudaMallocHost (ptr : System.Address; size : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4806
   pragma Import (C, cudaMallocHost, "cudaMallocHost");

   function cudaMallocPitch
     (devPtr : System.Address;
      pitch : access stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4849
   pragma Import (C, cudaMallocPitch, "cudaMallocPitch");

   function cudaMallocArray
     (c_array : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4898
   pragma Import (C, cudaMallocArray, "cudaMallocArray");

   function cudaFree (devPtr : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4927
   pragma Import (C, cudaFree, "cudaFree");

   function cudaFreeHost (ptr : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4950
   pragma Import (C, cudaFreeHost, "cudaFreeHost");

   function cudaFreeArray (c_array : udriver_types_h.cudaArray_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4973
   pragma Import (C, cudaFreeArray, "cudaFreeArray");

   function cudaFreeMipmappedArray (mipmappedArray : udriver_types_h.cudaMipmappedArray_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:4996
   pragma Import (C, cudaFreeMipmappedArray, "cudaFreeMipmappedArray");

   function cudaHostAlloc
     (pHost : System.Address;
      size : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5062
   pragma Import (C, cudaHostAlloc, "cudaHostAlloc");

   function cudaHostRegister
     (ptr : System.Address;
      size : stddef_h.size_t;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5155
   pragma Import (C, cudaHostRegister, "cudaHostRegister");

   function cudaHostUnregister (ptr : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5178
   pragma Import (C, cudaHostUnregister, "cudaHostUnregister");

   function cudaHostGetDevicePointer
     (pDevice : System.Address;
      pHost : System.Address;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5223
   pragma Import (C, cudaHostGetDevicePointer, "cudaHostGetDevicePointer");

   function cudaHostGetFlags (pFlags : access unsigned; pHost : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5245
   pragma Import (C, cudaHostGetFlags, "cudaHostGetFlags");

   function cudaMalloc3D (pitchedDevPtr : access udriver_types_h.cudaPitchedPtr; extent : udriver_types_h.cudaExtent) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5284
   pragma Import (C, cudaMalloc3D, "cudaMalloc3D");

   function cudaMalloc3DArray
     (c_array : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      extent : udriver_types_h.cudaExtent;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5426
   pragma Import (C, cudaMalloc3DArray, "cudaMalloc3DArray");

   function cudaMallocMipmappedArray
     (mipmappedArray : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      extent : udriver_types_h.cudaExtent;
      numLevels : unsigned;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5568
   pragma Import (C, cudaMallocMipmappedArray, "cudaMallocMipmappedArray");

   function cudaGetMipmappedArrayLevel
     (levelArray : System.Address;
      mipmappedArray : udriver_types_h.cudaMipmappedArray_const_t;
      level : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5601
   pragma Import (C, cudaGetMipmappedArrayLevel, "cudaGetMipmappedArrayLevel");

   function cudaMemcpy3D (p : access constant udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5706
   pragma Import (C, cudaMemcpy3D, "cudaMemcpy3D");

   function cudaMemcpy3DPeer (p : access constant udriver_types_h.cudaMemcpy3DPeerParms) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5737
   pragma Import (C, cudaMemcpy3DPeer, "cudaMemcpy3DPeer");

   function cudaMemcpy3DAsync (p : access constant udriver_types_h.cudaMemcpy3DParms; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5855
   pragma Import (C, cudaMemcpy3DAsync, "cudaMemcpy3DAsync");

   function cudaMemcpy3DPeerAsync (p : access constant udriver_types_h.cudaMemcpy3DPeerParms; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5881
   pragma Import (C, cudaMemcpy3DPeerAsync, "cudaMemcpy3DPeerAsync");

   function cudaMemGetInfo (free : access stddef_h.size_t; total : access stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5904
   pragma Import (C, cudaMemGetInfo, "cudaMemGetInfo");

   function cudaArrayGetInfo
     (desc : access udriver_types_h.cudaChannelFormatDesc;
      extent : access udriver_types_h.cudaExtent;
      flags : access unsigned;
      c_array : udriver_types_h.cudaArray_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5930
   pragma Import (C, cudaArrayGetInfo, "cudaArrayGetInfo");

   function cudaArrayGetPlane
     (pPlaneArray : System.Address;
      hArray : udriver_types_h.cudaArray_t;
      planeIdx : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5959
   pragma Import (C, cudaArrayGetPlane, "cudaArrayGetPlane");

   function cudaArrayGetSparseProperties (sparseProperties : access udriver_types_h.cudaArraySparseProperties; c_array : udriver_types_h.cudaArray_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:5987
   pragma Import (C, cudaArrayGetSparseProperties, "cudaArrayGetSparseProperties");

   function cudaMipmappedArrayGetSparseProperties (sparseProperties : access udriver_types_h.cudaArraySparseProperties; mipmap : udriver_types_h.cudaMipmappedArray_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6017
   pragma Import (C, cudaMipmappedArrayGetSparseProperties, "cudaMipmappedArrayGetSparseProperties");

   function cudaMemcpy
     (dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6062
   pragma Import (C, cudaMemcpy, "cudaMemcpy");

   function cudaMemcpyPeer
     (dst : System.Address;
      dstDevice : int;
      src : System.Address;
      srcDevice : int;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6097
   pragma Import (C, cudaMemcpyPeer, "cudaMemcpyPeer");

   function cudaMemcpy2D
     (dst : System.Address;
      dpitch : stddef_h.size_t;
      src : System.Address;
      spitch : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6146
   pragma Import (C, cudaMemcpy2D, "cudaMemcpy2D");

   function cudaMemcpy2DToArray
     (dst : udriver_types_h.cudaArray_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      src : System.Address;
      spitch : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6196
   pragma Import (C, cudaMemcpy2DToArray, "cudaMemcpy2DToArray");

   function cudaMemcpy2DFromArray
     (dst : System.Address;
      dpitch : stddef_h.size_t;
      src : udriver_types_h.cudaArray_const_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6246
   pragma Import (C, cudaMemcpy2DFromArray, "cudaMemcpy2DFromArray");

   function cudaMemcpy2DArrayToArray
     (dst : udriver_types_h.cudaArray_t;
      wOffsetDst : stddef_h.size_t;
      hOffsetDst : stddef_h.size_t;
      src : udriver_types_h.cudaArray_const_t;
      wOffsetSrc : stddef_h.size_t;
      hOffsetSrc : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6293
   pragma Import (C, cudaMemcpy2DArrayToArray, "cudaMemcpy2DArrayToArray");

   function cudaMemcpyToSymbol
     (symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6336
   pragma Import (C, cudaMemcpyToSymbol, "cudaMemcpyToSymbol");

   function cudaMemcpyFromSymbol
     (dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6379
   pragma Import (C, cudaMemcpyFromSymbol, "cudaMemcpyFromSymbol");

   function cudaMemcpyAsync
     (dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6436
   pragma Import (C, cudaMemcpyAsync, "cudaMemcpyAsync");

   function cudaMemcpyPeerAsync
     (dst : System.Address;
      dstDevice : int;
      src : System.Address;
      srcDevice : int;
      count : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6471
   pragma Import (C, cudaMemcpyPeerAsync, "cudaMemcpyPeerAsync");

   function cudaMemcpy2DAsync
     (dst : System.Address;
      dpitch : stddef_h.size_t;
      src : System.Address;
      spitch : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6534
   pragma Import (C, cudaMemcpy2DAsync, "cudaMemcpy2DAsync");

   function cudaMemcpy2DToArrayAsync
     (dst : udriver_types_h.cudaArray_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      src : System.Address;
      spitch : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6592
   pragma Import (C, cudaMemcpy2DToArrayAsync, "cudaMemcpy2DToArrayAsync");

   function cudaMemcpy2DFromArrayAsync
     (dst : System.Address;
      dpitch : stddef_h.size_t;
      src : udriver_types_h.cudaArray_const_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6649
   pragma Import (C, cudaMemcpy2DFromArrayAsync, "cudaMemcpy2DFromArrayAsync");

   function cudaMemcpyToSymbolAsync
     (symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6700
   pragma Import (C, cudaMemcpyToSymbolAsync, "cudaMemcpyToSymbolAsync");

   function cudaMemcpyFromSymbolAsync
     (dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6751
   pragma Import (C, cudaMemcpyFromSymbolAsync, "cudaMemcpyFromSymbolAsync");

   function cudaMemset
     (devPtr : System.Address;
      value : int;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6780
   pragma Import (C, cudaMemset, "cudaMemset");

   function cudaMemset2D
     (devPtr : System.Address;
      pitch : stddef_h.size_t;
      value : int;
      width : stddef_h.size_t;
      height : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6814
   pragma Import (C, cudaMemset2D, "cudaMemset2D");

   function cudaMemset3D
     (pitchedDevPtr : udriver_types_h.cudaPitchedPtr;
      value : int;
      extent : udriver_types_h.cudaExtent) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6860
   pragma Import (C, cudaMemset3D, "cudaMemset3D");

   function cudaMemsetAsync
     (devPtr : System.Address;
      value : int;
      count : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6896
   pragma Import (C, cudaMemsetAsync, "cudaMemsetAsync");

   function cudaMemset2DAsync
     (devPtr : System.Address;
      pitch : stddef_h.size_t;
      value : int;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6937
   pragma Import (C, cudaMemset2DAsync, "cudaMemset2DAsync");

   function cudaMemset3DAsync
     (pitchedDevPtr : udriver_types_h.cudaPitchedPtr;
      value : int;
      extent : udriver_types_h.cudaExtent;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:6990
   pragma Import (C, cudaMemset3DAsync, "cudaMemset3DAsync");

   function cudaGetSymbolAddress (devPtr : System.Address; symbol : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7018
   pragma Import (C, cudaGetSymbolAddress, "cudaGetSymbolAddress");

   function cudaGetSymbolSize (size : access stddef_h.size_t; symbol : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7045
   pragma Import (C, cudaGetSymbolSize, "cudaGetSymbolSize");

   function cudaMemPrefetchAsync
     (devPtr : System.Address;
      count : stddef_h.size_t;
      dstDevice : int;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7115
   pragma Import (C, cudaMemPrefetchAsync, "cudaMemPrefetchAsync");

   function cudaMemAdvise
     (devPtr : System.Address;
      count : stddef_h.size_t;
      advice : udriver_types_h.cudaMemoryAdvise;
      device : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7231
   pragma Import (C, cudaMemAdvise, "cudaMemAdvise");

   function cudaMemRangeGetAttribute
     (data : System.Address;
      dataSize : stddef_h.size_t;
      attribute : udriver_types_h.cudaMemRangeAttribute;
      devPtr : System.Address;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7290
   pragma Import (C, cudaMemRangeGetAttribute, "cudaMemRangeGetAttribute");

   function cudaMemRangeGetAttributes
     (data : System.Address;
      dataSizes : access stddef_h.size_t;
      attributes : access udriver_types_h.cudaMemRangeAttribute;
      numAttributes : stddef_h.size_t;
      devPtr : System.Address;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7329
   pragma Import (C, cudaMemRangeGetAttributes, "cudaMemRangeGetAttributes");

   function cudaMemcpyToArray
     (dst : udriver_types_h.cudaArray_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7389
   pragma Import (C, cudaMemcpyToArray, "cudaMemcpyToArray");

   function cudaMemcpyFromArray
     (dst : System.Address;
      src : udriver_types_h.cudaArray_const_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7431
   pragma Import (C, cudaMemcpyFromArray, "cudaMemcpyFromArray");

   function cudaMemcpyArrayToArray
     (dst : udriver_types_h.cudaArray_t;
      wOffsetDst : stddef_h.size_t;
      hOffsetDst : stddef_h.size_t;
      src : udriver_types_h.cudaArray_const_t;
      wOffsetSrc : stddef_h.size_t;
      hOffsetSrc : stddef_h.size_t;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7474
   pragma Import (C, cudaMemcpyArrayToArray, "cudaMemcpyArrayToArray");

   function cudaMemcpyToArrayAsync
     (dst : udriver_types_h.cudaArray_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7525
   pragma Import (C, cudaMemcpyToArrayAsync, "cudaMemcpyToArrayAsync");

   function cudaMemcpyFromArrayAsync
     (dst : System.Address;
      src : udriver_types_h.cudaArray_const_t;
      wOffset : stddef_h.size_t;
      hOffset : stddef_h.size_t;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7575
   pragma Import (C, cudaMemcpyFromArrayAsync, "cudaMemcpyFromArrayAsync");

   function cudaMallocAsync
     (devPtr : System.Address;
      size : stddef_h.size_t;
      hStream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7644
   pragma Import (C, cudaMallocAsync, "cudaMallocAsync");

   function cudaFreeAsync (devPtr : System.Address; hStream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7670
   pragma Import (C, cudaFreeAsync, "cudaFreeAsync");

   function cudaMemPoolTrimTo (memPool : udriver_types_h.cudaMemPool_t; minBytesToKeep : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7695
   pragma Import (C, cudaMemPoolTrimTo, "cudaMemPoolTrimTo");

   function cudaMemPoolSetAttribute
     (memPool : udriver_types_h.cudaMemPool_t;
      attr : udriver_types_h.cudaMemPoolAttr;
      value : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7733
   pragma Import (C, cudaMemPoolSetAttribute, "cudaMemPoolSetAttribute");

   function cudaMemPoolGetAttribute
     (memPool : udriver_types_h.cudaMemPool_t;
      attr : udriver_types_h.cudaMemPoolAttr;
      value : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7771
   pragma Import (C, cudaMemPoolGetAttribute, "cudaMemPoolGetAttribute");

   function cudaMemPoolSetAccess
     (memPool : udriver_types_h.cudaMemPool_t;
      descList : access constant udriver_types_h.cudaMemAccessDesc;
      count : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7786
   pragma Import (C, cudaMemPoolSetAccess, "cudaMemPoolSetAccess");

   function cudaMemPoolGetAccess
     (flags : access udriver_types_h.cudaMemAccessFlags;
      memPool : udriver_types_h.cudaMemPool_t;
      location : access udriver_types_h.cudaMemLocation) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7799
   pragma Import (C, cudaMemPoolGetAccess, "cudaMemPoolGetAccess");

   function cudaMemPoolCreate (memPool : System.Address; poolProps : access constant udriver_types_h.cudaMemPoolProps) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7819
   pragma Import (C, cudaMemPoolCreate, "cudaMemPoolCreate");

   function cudaMemPoolDestroy (memPool : udriver_types_h.cudaMemPool_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7841
   pragma Import (C, cudaMemPoolDestroy, "cudaMemPoolDestroy");

   function cudaMallocFromPoolAsync
     (ptr : System.Address;
      size : stddef_h.size_t;
      memPool : udriver_types_h.cudaMemPool_t;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7877
   pragma Import (C, cudaMallocFromPoolAsync, "cudaMallocFromPoolAsync");

   function cudaMemPoolExportToShareableHandle
     (shareableHandle : System.Address;
      memPool : udriver_types_h.cudaMemPool_t;
      handleType : udriver_types_h.cudaMemAllocationHandleType;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7902
   pragma Import (C, cudaMemPoolExportToShareableHandle, "cudaMemPoolExportToShareableHandle");

   function cudaMemPoolImportFromShareableHandle
     (memPool : System.Address;
      shareableHandle : System.Address;
      handleType : udriver_types_h.cudaMemAllocationHandleType;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7929
   pragma Import (C, cudaMemPoolImportFromShareableHandle, "cudaMemPoolImportFromShareableHandle");

   function cudaMemPoolExportPointer (exportData : access udriver_types_h.cudaMemPoolPtrExportData; ptr : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7952
   pragma Import (C, cudaMemPoolExportPointer, "cudaMemPoolExportPointer");

   function cudaMemPoolImportPointer
     (ptr : System.Address;
      memPool : udriver_types_h.cudaMemPool_t;
      exportData : access udriver_types_h.cudaMemPoolPtrExportData) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:7981
   pragma Import (C, cudaMemPoolImportPointer, "cudaMemPoolImportPointer");

   function cudaPointerGetAttributes (attributes : access udriver_types_h.cudaPointerAttributes; ptr : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8133
   pragma Import (C, cudaPointerGetAttributes, "cudaPointerGetAttributes");

   function cudaDeviceCanAccessPeer
     (canAccessPeer : access int;
      device : int;
      peerDevice : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8174
   pragma Import (C, cudaDeviceCanAccessPeer, "cudaDeviceCanAccessPeer");

   function cudaDeviceEnablePeerAccess (peerDevice : int; flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8216
   pragma Import (C, cudaDeviceEnablePeerAccess, "cudaDeviceEnablePeerAccess");

   function cudaDeviceDisablePeerAccess (peerDevice : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8238
   pragma Import (C, cudaDeviceDisablePeerAccess, "cudaDeviceDisablePeerAccess");

   function cudaGraphicsUnregisterResource (resource : udriver_types_h.cudaGraphicsResource_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8302
   pragma Import (C, cudaGraphicsUnregisterResource, "cudaGraphicsUnregisterResource");

   function cudaGraphicsResourceSetMapFlags (resource : udriver_types_h.cudaGraphicsResource_t; flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8337
   pragma Import (C, cudaGraphicsResourceSetMapFlags, "cudaGraphicsResourceSetMapFlags");

   function cudaGraphicsMapResources
     (count : int;
      resources : System.Address;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8376
   pragma Import (C, cudaGraphicsMapResources, "cudaGraphicsMapResources");

   function cudaGraphicsUnmapResources
     (count : int;
      resources : System.Address;
      stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8411
   pragma Import (C, cudaGraphicsUnmapResources, "cudaGraphicsUnmapResources");

   function cudaGraphicsResourceGetMappedPointer
     (devPtr : System.Address;
      size : access stddef_h.size_t;
      resource : udriver_types_h.cudaGraphicsResource_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8443
   pragma Import (C, cudaGraphicsResourceGetMappedPointer, "cudaGraphicsResourceGetMappedPointer");

   function cudaGraphicsSubResourceGetMappedArray
     (c_array : System.Address;
      resource : udriver_types_h.cudaGraphicsResource_t;
      arrayIndex : unsigned;
      mipLevel : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8481
   pragma Import (C, cudaGraphicsSubResourceGetMappedArray, "cudaGraphicsSubResourceGetMappedArray");

   function cudaGraphicsResourceGetMappedMipmappedArray (mipmappedArray : System.Address; resource : udriver_types_h.cudaGraphicsResource_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8510
   pragma Import (C, cudaGraphicsResourceGetMappedMipmappedArray, "cudaGraphicsResourceGetMappedMipmappedArray");

   function cudaBindTexture
     (offset : access stddef_h.size_t;
      texref : access constant utexture_types_h.textureReference;
      devPtr : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      size : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8581
   pragma Import (C, cudaBindTexture, "cudaBindTexture");

   function cudaBindTexture2D
     (offset : access stddef_h.size_t;
      texref : access constant utexture_types_h.textureReference;
      devPtr : System.Address;
      desc : access constant udriver_types_h.cudaChannelFormatDesc;
      width : stddef_h.size_t;
      height : stddef_h.size_t;
      pitch : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8640
   pragma Import (C, cudaBindTexture2D, "cudaBindTexture2D");

   function cudaBindTextureToArray
     (texref : access constant utexture_types_h.textureReference;
      c_array : udriver_types_h.cudaArray_const_t;
      desc : access constant udriver_types_h.cudaChannelFormatDesc) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8678
   pragma Import (C, cudaBindTextureToArray, "cudaBindTextureToArray");

   function cudaBindTextureToMipmappedArray
     (texref : access constant utexture_types_h.textureReference;
      mipmappedArray : udriver_types_h.cudaMipmappedArray_const_t;
      desc : access constant udriver_types_h.cudaChannelFormatDesc) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8718
   pragma Import (C, cudaBindTextureToMipmappedArray, "cudaBindTextureToMipmappedArray");

   function cudaUnbindTexture (texref : access constant utexture_types_h.textureReference) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8744
   pragma Import (C, cudaUnbindTexture, "cudaUnbindTexture");

   function cudaGetTextureAlignmentOffset (offset : access stddef_h.size_t; texref : access constant utexture_types_h.textureReference) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8773
   pragma Import (C, cudaGetTextureAlignmentOffset, "cudaGetTextureAlignmentOffset");

   function cudaGetTextureReference (texref : System.Address; symbol : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8803
   pragma Import (C, cudaGetTextureReference, "cudaGetTextureReference");

   function cudaBindSurfaceToArray
     (surfref : access constant usurface_types_h.surfaceReference;
      c_array : udriver_types_h.cudaArray_const_t;
      desc : access constant udriver_types_h.cudaChannelFormatDesc) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8848
   pragma Import (C, cudaBindSurfaceToArray, "cudaBindSurfaceToArray");

   function cudaGetSurfaceReference (surfref : System.Address; symbol : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8873
   pragma Import (C, cudaGetSurfaceReference, "cudaGetSurfaceReference");

   function cudaGetChannelDesc (desc : access udriver_types_h.cudaChannelFormatDesc; c_array : udriver_types_h.cudaArray_const_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8908
   pragma Import (C, cudaGetChannelDesc, "cudaGetChannelDesc");

   function cudaCreateChannelDesc
     (x : int;
      y : int;
      z : int;
      w : int;
      f : udriver_types_h.cudaChannelFormatKind) return udriver_types_h.cudaChannelFormatDesc;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:8938
   pragma Import (C, cudaCreateChannelDesc, "cudaCreateChannelDesc");

   function cudaCreateTextureObject
     (pTexObject : access utexture_types_h.cudaTextureObject_t;
      pResDesc : access constant udriver_types_h.cudaResourceDesc;
      pTexDesc : access constant utexture_types_h.cudaTextureDesc;
      pResViewDesc : access constant udriver_types_h.cudaResourceViewDesc) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9156
   pragma Import (C, cudaCreateTextureObject, "cudaCreateTextureObject");

   function cudaDestroyTextureObject (texObject : utexture_types_h.cudaTextureObject_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9176
   pragma Import (C, cudaDestroyTextureObject, "cudaDestroyTextureObject");

   function cudaGetTextureObjectResourceDesc (pResDesc : access udriver_types_h.cudaResourceDesc; texObject : utexture_types_h.cudaTextureObject_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9196
   pragma Import (C, cudaGetTextureObjectResourceDesc, "cudaGetTextureObjectResourceDesc");

   function cudaGetTextureObjectTextureDesc (pTexDesc : access utexture_types_h.cudaTextureDesc; texObject : utexture_types_h.cudaTextureObject_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9216
   pragma Import (C, cudaGetTextureObjectTextureDesc, "cudaGetTextureObjectTextureDesc");

   function cudaGetTextureObjectResourceViewDesc (pResViewDesc : access udriver_types_h.cudaResourceViewDesc; texObject : utexture_types_h.cudaTextureObject_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9237
   pragma Import (C, cudaGetTextureObjectResourceViewDesc, "cudaGetTextureObjectResourceViewDesc");

   function cudaCreateSurfaceObject (pSurfObject : access usurface_types_h.cudaSurfaceObject_t; pResDesc : access constant udriver_types_h.cudaResourceDesc) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9282
   pragma Import (C, cudaCreateSurfaceObject, "cudaCreateSurfaceObject");

   function cudaDestroySurfaceObject (surfObject : usurface_types_h.cudaSurfaceObject_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9302
   pragma Import (C, cudaDestroySurfaceObject, "cudaDestroySurfaceObject");

   function cudaGetSurfaceObjectResourceDesc (pResDesc : access udriver_types_h.cudaResourceDesc; surfObject : usurface_types_h.cudaSurfaceObject_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9321
   pragma Import (C, cudaGetSurfaceObjectResourceDesc, "cudaGetSurfaceObjectResourceDesc");

   function cudaDriverGetVersion (driverVersion : access int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9355
   pragma Import (C, cudaDriverGetVersion, "cudaDriverGetVersion");

   function cudaRuntimeGetVersion (runtimeVersion : access int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9380
   pragma Import (C, cudaRuntimeGetVersion, "cudaRuntimeGetVersion");

   function cudaGraphCreate (pGraph : System.Address; flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9427
   pragma Import (C, cudaGraphCreate, "cudaGraphCreate");

   function cudaGraphAddKernelNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      pNodeParams : access constant udriver_types_h.Class_cudaKernelNodeParams.cudaKernelNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9524
   pragma Import (C, cudaGraphAddKernelNode, "cudaGraphAddKernelNode");

   function cudaGraphKernelNodeGetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access udriver_types_h.Class_cudaKernelNodeParams.cudaKernelNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9557
   pragma Import (C, cudaGraphKernelNodeGetParams, "cudaGraphKernelNodeGetParams");

   function cudaGraphKernelNodeSetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access constant udriver_types_h.Class_cudaKernelNodeParams.cudaKernelNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9582
   pragma Import (C, cudaGraphKernelNodeSetParams, "cudaGraphKernelNodeSetParams");

   function cudaGraphKernelNodeCopyAttributes (hSrc : udriver_types_h.cudaGraphNode_t; hDst : udriver_types_h.cudaGraphNode_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9602
   pragma Import (C, cudaGraphKernelNodeCopyAttributes, "cudaGraphKernelNodeCopyAttributes");

   function cudaGraphKernelNodeGetAttribute
     (hNode : udriver_types_h.cudaGraphNode_t;
      attr : udriver_types_h.cudaKernelNodeAttrID;
      value_out : access udriver_types_h.cudaKernelNodeAttrValue) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9625
   pragma Import (C, cudaGraphKernelNodeGetAttribute, "cudaGraphKernelNodeGetAttribute");

   function cudaGraphKernelNodeSetAttribute
     (hNode : udriver_types_h.cudaGraphNode_t;
      attr : udriver_types_h.cudaKernelNodeAttrID;
      value : access constant udriver_types_h.cudaKernelNodeAttrValue) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9649
   pragma Import (C, cudaGraphKernelNodeSetAttribute, "cudaGraphKernelNodeSetAttribute");

   function cudaGraphAddMemcpyNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      pCopyParams : access constant udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9699
   pragma Import (C, cudaGraphAddMemcpyNode, "cudaGraphAddMemcpyNode");

   function cudaGraphAddMemcpyNodeToSymbol
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9758
   pragma Import (C, cudaGraphAddMemcpyNodeToSymbol, "cudaGraphAddMemcpyNodeToSymbol");

   function cudaGraphAddMemcpyNodeFromSymbol
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9827
   pragma Import (C, cudaGraphAddMemcpyNodeFromSymbol, "cudaGraphAddMemcpyNodeFromSymbol");

   function cudaGraphAddMemcpyNode1D
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9895
   pragma Import (C, cudaGraphAddMemcpyNode1D, "cudaGraphAddMemcpyNode1D");

   function cudaGraphMemcpyNodeGetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9927
   pragma Import (C, cudaGraphMemcpyNodeGetParams, "cudaGraphMemcpyNodeGetParams");

   function cudaGraphMemcpyNodeSetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access constant udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9953
   pragma Import (C, cudaGraphMemcpyNodeSetParams, "cudaGraphMemcpyNodeSetParams");

   function cudaGraphMemcpyNodeSetParamsToSymbol
     (node : udriver_types_h.cudaGraphNode_t;
      symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:9992
   pragma Import (C, cudaGraphMemcpyNodeSetParamsToSymbol, "cudaGraphMemcpyNodeSetParamsToSymbol");

   function cudaGraphMemcpyNodeSetParamsFromSymbol
     (node : udriver_types_h.cudaGraphNode_t;
      dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10038
   pragma Import (C, cudaGraphMemcpyNodeSetParamsFromSymbol, "cudaGraphMemcpyNodeSetParamsFromSymbol");

   function cudaGraphMemcpyNodeSetParams1D
     (node : udriver_types_h.cudaGraphNode_t;
      dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10084
   pragma Import (C, cudaGraphMemcpyNodeSetParams1D, "cudaGraphMemcpyNodeSetParams1D");

   function cudaGraphAddMemsetNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      pMemsetParams : access constant udriver_types_h.cudaMemsetParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10131
   pragma Import (C, cudaGraphAddMemsetNode, "cudaGraphAddMemsetNode");

   function cudaGraphMemsetNodeGetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access udriver_types_h.cudaMemsetParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10154
   pragma Import (C, cudaGraphMemsetNodeGetParams, "cudaGraphMemsetNodeGetParams");

   function cudaGraphMemsetNodeSetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access constant udriver_types_h.cudaMemsetParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10177
   pragma Import (C, cudaGraphMemsetNodeSetParams, "cudaGraphMemsetNodeSetParams");

   function cudaGraphAddHostNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      pNodeParams : access constant udriver_types_h.cudaHostNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10218
   pragma Import (C, cudaGraphAddHostNode, "cudaGraphAddHostNode");

   function cudaGraphHostNodeGetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access udriver_types_h.cudaHostNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10241
   pragma Import (C, cudaGraphHostNodeGetParams, "cudaGraphHostNodeGetParams");

   function cudaGraphHostNodeSetParams (node : udriver_types_h.cudaGraphNode_t; pNodeParams : access constant udriver_types_h.cudaHostNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10264
   pragma Import (C, cudaGraphHostNodeSetParams, "cudaGraphHostNodeSetParams");

   function cudaGraphAddChildGraphNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      childGraph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10304
   pragma Import (C, cudaGraphAddChildGraphNode, "cudaGraphAddChildGraphNode");

   function cudaGraphChildGraphNodeGetGraph (node : udriver_types_h.cudaGraphNode_t; pGraph : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10331
   pragma Import (C, cudaGraphChildGraphNodeGetGraph, "cudaGraphChildGraphNodeGetGraph");

   function cudaGraphAddEmptyNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10368
   pragma Import (C, cudaGraphAddEmptyNode, "cudaGraphAddEmptyNode");

   function cudaGraphAddEventRecordNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10411
   pragma Import (C, cudaGraphAddEventRecordNode, "cudaGraphAddEventRecordNode");

   function cudaGraphEventRecordNodeGetEvent (node : udriver_types_h.cudaGraphNode_t; event_out : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10438
   pragma Import (C, cudaGraphEventRecordNodeGetEvent, "cudaGraphEventRecordNodeGetEvent");

   function cudaGraphEventRecordNodeSetEvent (node : udriver_types_h.cudaGraphNode_t; event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10465
   pragma Import (C, cudaGraphEventRecordNodeSetEvent, "cudaGraphEventRecordNodeSetEvent");

   function cudaGraphAddEventWaitNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10511
   pragma Import (C, cudaGraphAddEventWaitNode, "cudaGraphAddEventWaitNode");

   function cudaGraphEventWaitNodeGetEvent (node : udriver_types_h.cudaGraphNode_t; event_out : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10538
   pragma Import (C, cudaGraphEventWaitNodeGetEvent, "cudaGraphEventWaitNodeGetEvent");

   function cudaGraphEventWaitNodeSetEvent (node : udriver_types_h.cudaGraphNode_t; event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10565
   pragma Import (C, cudaGraphEventWaitNodeSetEvent, "cudaGraphEventWaitNodeSetEvent");

   function cudaGraphAddExternalSemaphoresSignalNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      nodeParams : access constant udriver_types_h.cudaExternalSemaphoreSignalNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10614
   pragma Import (C, cudaGraphAddExternalSemaphoresSignalNode, "cudaGraphAddExternalSemaphoresSignalNode");

   function cudaGraphExternalSemaphoresSignalNodeGetParams (hNode : udriver_types_h.cudaGraphNode_t; params_out : access udriver_types_h.cudaExternalSemaphoreSignalNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10647
   pragma Import (C, cudaGraphExternalSemaphoresSignalNodeGetParams, "cudaGraphExternalSemaphoresSignalNodeGetParams");

   function cudaGraphExternalSemaphoresSignalNodeSetParams (hNode : udriver_types_h.cudaGraphNode_t; nodeParams : access constant udriver_types_h.cudaExternalSemaphoreSignalNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10674
   pragma Import (C, cudaGraphExternalSemaphoresSignalNodeSetParams, "cudaGraphExternalSemaphoresSignalNodeSetParams");

   function cudaGraphAddExternalSemaphoresWaitNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      nodeParams : access constant udriver_types_h.cudaExternalSemaphoreWaitNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10723
   pragma Import (C, cudaGraphAddExternalSemaphoresWaitNode, "cudaGraphAddExternalSemaphoresWaitNode");

   function cudaGraphExternalSemaphoresWaitNodeGetParams (hNode : udriver_types_h.cudaGraphNode_t; params_out : access udriver_types_h.cudaExternalSemaphoreWaitNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10756
   pragma Import (C, cudaGraphExternalSemaphoresWaitNodeGetParams, "cudaGraphExternalSemaphoresWaitNodeGetParams");

   function cudaGraphExternalSemaphoresWaitNodeSetParams (hNode : udriver_types_h.cudaGraphNode_t; nodeParams : access constant udriver_types_h.cudaExternalSemaphoreWaitNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10783
   pragma Import (C, cudaGraphExternalSemaphoresWaitNodeSetParams, "cudaGraphExternalSemaphoresWaitNodeSetParams");

   function cudaGraphAddMemAllocNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      nodeParams : access udriver_types_h.cudaMemAllocNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10860
   pragma Import (C, cudaGraphAddMemAllocNode, "cudaGraphAddMemAllocNode");

   function cudaGraphMemAllocNodeGetParams (node : udriver_types_h.cudaGraphNode_t; params_out : access udriver_types_h.cudaMemAllocNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10887
   pragma Import (C, cudaGraphMemAllocNodeGetParams, "cudaGraphMemAllocNodeGetParams");

   function cudaGraphAddMemFreeNode
     (pGraphNode : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pDependencies : System.Address;
      numDependencies : stddef_h.size_t;
      dptr : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10947
   pragma Import (C, cudaGraphAddMemFreeNode, "cudaGraphAddMemFreeNode");

   function cudaGraphMemFreeNodeGetParams (node : udriver_types_h.cudaGraphNode_t; dptr_out : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10971
   pragma Import (C, cudaGraphMemFreeNodeGetParams, "cudaGraphMemFreeNodeGetParams");

   function cudaDeviceGraphMemTrim (device : int) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:10999
   pragma Import (C, cudaDeviceGraphMemTrim, "cudaDeviceGraphMemTrim");

   function cudaDeviceGetGraphMemAttribute
     (device : int;
      attr : udriver_types_h.cudaGraphMemAttributeType;
      value : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11036
   pragma Import (C, cudaDeviceGetGraphMemAttribute, "cudaDeviceGetGraphMemAttribute");

   function cudaDeviceSetGraphMemAttribute
     (device : int;
      attr : udriver_types_h.cudaGraphMemAttributeType;
      value : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11070
   pragma Import (C, cudaDeviceSetGraphMemAttribute, "cudaDeviceSetGraphMemAttribute");

   function cudaGraphClone (pGraphClone : System.Address; originalGraph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11098
   pragma Import (C, cudaGraphClone, "cudaGraphClone");

   function cudaGraphNodeFindInClone
     (pNode : System.Address;
      originalNode : udriver_types_h.cudaGraphNode_t;
      clonedGraph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11126
   pragma Import (C, cudaGraphNodeFindInClone, "cudaGraphNodeFindInClone");

   function cudaGraphNodeGetType (node : udriver_types_h.cudaGraphNode_t; pType : access udriver_types_h.cudaGraphNodeType) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11157
   pragma Import (C, cudaGraphNodeGetType, "cudaGraphNodeGetType");

   function cudaGraphGetNodes
     (graph : udriver_types_h.cudaGraph_t;
      nodes : System.Address;
      numNodes : access stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11188
   pragma Import (C, cudaGraphGetNodes, "cudaGraphGetNodes");

   function cudaGraphGetRootNodes
     (graph : udriver_types_h.cudaGraph_t;
      pRootNodes : System.Address;
      pNumRootNodes : access stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11219
   pragma Import (C, cudaGraphGetRootNodes, "cudaGraphGetRootNodes");

   function cudaGraphGetEdges
     (graph : udriver_types_h.cudaGraph_t;
      from : System.Address;
      to : System.Address;
      numEdges : access stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11253
   pragma Import (C, cudaGraphGetEdges, "cudaGraphGetEdges");

   function cudaGraphNodeGetDependencies
     (node : udriver_types_h.cudaGraphNode_t;
      pDependencies : System.Address;
      pNumDependencies : access stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11284
   pragma Import (C, cudaGraphNodeGetDependencies, "cudaGraphNodeGetDependencies");

   function cudaGraphNodeGetDependentNodes
     (node : udriver_types_h.cudaGraphNode_t;
      pDependentNodes : System.Address;
      pNumDependentNodes : access stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11316
   pragma Import (C, cudaGraphNodeGetDependentNodes, "cudaGraphNodeGetDependentNodes");

   function cudaGraphAddDependencies
     (graph : udriver_types_h.cudaGraph_t;
      from : System.Address;
      to : System.Address;
      numDependencies : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11347
   pragma Import (C, cudaGraphAddDependencies, "cudaGraphAddDependencies");

   function cudaGraphRemoveDependencies
     (graph : udriver_types_h.cudaGraph_t;
      from : System.Address;
      to : System.Address;
      numDependencies : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11378
   pragma Import (C, cudaGraphRemoveDependencies, "cudaGraphRemoveDependencies");

   function cudaGraphDestroyNode (node : udriver_types_h.cudaGraphNode_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11408
   pragma Import (C, cudaGraphDestroyNode, "cudaGraphDestroyNode");

   function cudaGraphInstantiate
     (pGraphExec : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      pErrorNode : System.Address;
      pLogBuffer : Interfaces.C.Strings.chars_ptr;
      bufferSize : stddef_h.size_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11446
   pragma Import (C, cudaGraphInstantiate, "cudaGraphInstantiate");

   function cudaGraphInstantiateWithFlags
     (pGraphExec : System.Address;
      graph : udriver_types_h.cudaGraph_t;
      flags : Extensions.unsigned_long_long) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11489
   pragma Import (C, cudaGraphInstantiateWithFlags, "cudaGraphInstantiateWithFlags");

   function cudaGraphExecKernelNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      pNodeParams : access constant udriver_types_h.Class_cudaKernelNodeParams.cudaKernelNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11533
   pragma Import (C, cudaGraphExecKernelNodeSetParams, "cudaGraphExecKernelNodeSetParams");

   function cudaGraphExecMemcpyNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      pNodeParams : access constant udriver_types_h.cudaMemcpy3DParms) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11583
   pragma Import (C, cudaGraphExecMemcpyNodeSetParams, "cudaGraphExecMemcpyNodeSetParams");

   function cudaGraphExecMemcpyNodeSetParamsToSymbol
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      symbol : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11638
   pragma Import (C, cudaGraphExecMemcpyNodeSetParamsToSymbol, "cudaGraphExecMemcpyNodeSetParamsToSymbol");

   function cudaGraphExecMemcpyNodeSetParamsFromSymbol
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      dst : System.Address;
      symbol : System.Address;
      count : stddef_h.size_t;
      offset : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11701
   pragma Import (C, cudaGraphExecMemcpyNodeSetParamsFromSymbol, "cudaGraphExecMemcpyNodeSetParamsFromSymbol");

   function cudaGraphExecMemcpyNodeSetParams1D
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      dst : System.Address;
      src : System.Address;
      count : stddef_h.size_t;
      kind : udriver_types_h.cudaMemcpyKind) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11762
   pragma Import (C, cudaGraphExecMemcpyNodeSetParams1D, "cudaGraphExecMemcpyNodeSetParams1D");

   function cudaGraphExecMemsetNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      pNodeParams : access constant udriver_types_h.cudaMemsetParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11816
   pragma Import (C, cudaGraphExecMemsetNodeSetParams, "cudaGraphExecMemsetNodeSetParams");

   function cudaGraphExecHostNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      pNodeParams : access constant udriver_types_h.cudaHostNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11855
   pragma Import (C, cudaGraphExecHostNodeSetParams, "cudaGraphExecHostNodeSetParams");

   function cudaGraphExecChildGraphNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      node : udriver_types_h.cudaGraphNode_t;
      childGraph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11901
   pragma Import (C, cudaGraphExecChildGraphNodeSetParams, "cudaGraphExecChildGraphNodeSetParams");

   function cudaGraphExecEventRecordNodeSetEvent
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hNode : udriver_types_h.cudaGraphNode_t;
      event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11945
   pragma Import (C, cudaGraphExecEventRecordNodeSetEvent, "cudaGraphExecEventRecordNodeSetEvent");

   function cudaGraphExecEventWaitNodeSetEvent
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hNode : udriver_types_h.cudaGraphNode_t;
      event : udriver_types_h.cudaEvent_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:11989
   pragma Import (C, cudaGraphExecEventWaitNodeSetEvent, "cudaGraphExecEventWaitNodeSetEvent");

   function cudaGraphExecExternalSemaphoresSignalNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hNode : udriver_types_h.cudaGraphNode_t;
      nodeParams : access constant udriver_types_h.cudaExternalSemaphoreSignalNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12036
   pragma Import (C, cudaGraphExecExternalSemaphoresSignalNodeSetParams, "cudaGraphExecExternalSemaphoresSignalNodeSetParams");

   function cudaGraphExecExternalSemaphoresWaitNodeSetParams
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hNode : udriver_types_h.cudaGraphNode_t;
      nodeParams : access constant udriver_types_h.cudaExternalSemaphoreWaitNodeParams) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12083
   pragma Import (C, cudaGraphExecExternalSemaphoresWaitNodeSetParams, "cudaGraphExecExternalSemaphoresWaitNodeSetParams");

   function cudaGraphExecUpdate
     (hGraphExec : udriver_types_h.cudaGraphExec_t;
      hGraph : udriver_types_h.cudaGraph_t;
      hErrorNode_out : System.Address;
      updateResult_out : access udriver_types_h.cudaGraphExecUpdateResult) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12158
   pragma Import (C, cudaGraphExecUpdate, "cudaGraphExecUpdate");

   function cudaGraphUpload (graphExec : udriver_types_h.cudaGraphExec_t; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12183
   pragma Import (C, cudaGraphUpload, "cudaGraphUpload");

   function cudaGraphLaunch (graphExec : udriver_types_h.cudaGraphExec_t; stream : udriver_types_h.cudaStream_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12214
   pragma Import (C, cudaGraphLaunch, "cudaGraphLaunch");

   function cudaGraphExecDestroy (graphExec : udriver_types_h.cudaGraphExec_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12237
   pragma Import (C, cudaGraphExecDestroy, "cudaGraphExecDestroy");

   function cudaGraphDestroy (graph : udriver_types_h.cudaGraph_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12258
   pragma Import (C, cudaGraphDestroy, "cudaGraphDestroy");

   function cudaGraphDebugDotPrint
     (graph : udriver_types_h.cudaGraph_t;
      path : Interfaces.C.Strings.chars_ptr;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12277
   pragma Import (C, cudaGraphDebugDotPrint, "cudaGraphDebugDotPrint");

   function cudaUserObjectCreate
     (object_out : System.Address;
      ptr : System.Address;
      destroy : udriver_types_h.cudaHostFn_t;
      initialRefcount : unsigned;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12313
   pragma Import (C, cudaUserObjectCreate, "cudaUserObjectCreate");

   function cudaUserObjectRetain (object : udriver_types_h.cudaUserObject_t; count : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12337
   pragma Import (C, cudaUserObjectRetain, "cudaUserObjectRetain");

   function cudaUserObjectRelease (object : udriver_types_h.cudaUserObject_t; count : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12365
   pragma Import (C, cudaUserObjectRelease, "cudaUserObjectRelease");

   function cudaGraphRetainUserObject
     (graph : udriver_types_h.cudaGraph_t;
      object : udriver_types_h.cudaUserObject_t;
      count : unsigned;
      flags : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12393
   pragma Import (C, cudaGraphRetainUserObject, "cudaGraphRetainUserObject");

   function cudaGraphReleaseUserObject
     (graph : udriver_types_h.cudaGraph_t;
      object : udriver_types_h.cudaUserObject_t;
      count : unsigned) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12418
   pragma Import (C, cudaGraphReleaseUserObject, "cudaGraphReleaseUserObject");

   function cudaGetDriverEntryPoint
     (symbol : Interfaces.C.Strings.chars_ptr;
      funcPtr : System.Address;
      flags : Extensions.unsigned_long_long) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12484
   pragma Import (C, cudaGetDriverEntryPoint, "cudaGetDriverEntryPoint");

   function cudaGetExportTable (ppExportTable : System.Address; pExportTableId : access constant udriver_types_h.cudaUUID_t) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12489
   pragma Import (C, cudaGetExportTable, "cudaGetExportTable");

   function cudaGetFuncBySymbol (functionPtr : System.Address; symbolPtr : System.Address) return udriver_types_h.cudaError_t;  -- /usr/local/cuda-11.5/include//cuda_runtime_api.h:12665
   pragma Import (C, cudaGetFuncBySymbol, "cudaGetFuncBySymbol");

end ucuda_runtime_api_h;
