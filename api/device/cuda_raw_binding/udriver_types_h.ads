pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with System;
with Interfaces.C.Extensions;
with stddef_h;
with uvector_types_h;

package udriver_types_h is

   cudaHostAllocDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:85
   cudaHostAllocPortable : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:86
   cudaHostAllocMapped : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:87
   cudaHostAllocWriteCombined : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:88

   cudaHostRegisterDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:90
   cudaHostRegisterPortable : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:91
   cudaHostRegisterMapped : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:92
   cudaHostRegisterIoMemory : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:93
   cudaHostRegisterReadOnly : constant := 16#08#;  --  /usr/local/cuda-11.5/include//driver_types.h:94

   cudaPeerAccessDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:96

   cudaStreamDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:98
   cudaStreamNonBlocking : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:99
   --  unsupported macro: cudaStreamLegacy ((cudaStream_t)0x1)
   --  unsupported macro: cudaStreamPerThread ((cudaStream_t)0x2)

   cudaEventDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:121
   cudaEventBlockingSync : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:122
   cudaEventDisableTiming : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:123
   cudaEventInterprocess : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:124

   cudaEventRecordDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:126
   cudaEventRecordExternal : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:127

   cudaEventWaitDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:129
   cudaEventWaitExternal : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:130

   cudaDeviceScheduleAuto : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:132
   cudaDeviceScheduleSpin : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:133
   cudaDeviceScheduleYield : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:134
   cudaDeviceScheduleBlockingSync : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:135
   cudaDeviceBlockingSync : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:136

   cudaDeviceScheduleMask : constant := 16#07#;  --  /usr/local/cuda-11.5/include//driver_types.h:139
   cudaDeviceMapHost : constant := 16#08#;  --  /usr/local/cuda-11.5/include//driver_types.h:140
   cudaDeviceLmemResizeToMax : constant := 16#10#;  --  /usr/local/cuda-11.5/include//driver_types.h:141
   cudaDeviceMask : constant := 16#1f#;  --  /usr/local/cuda-11.5/include//driver_types.h:142

   cudaArrayDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:144
   cudaArrayLayered : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:145
   cudaArraySurfaceLoadStore : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:146
   cudaArrayCubemap : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:147
   cudaArrayTextureGather : constant := 16#08#;  --  /usr/local/cuda-11.5/include//driver_types.h:148
   cudaArrayColorAttachment : constant := 16#20#;  --  /usr/local/cuda-11.5/include//driver_types.h:149
   cudaArraySparse : constant := 16#40#;  --  /usr/local/cuda-11.5/include//driver_types.h:150

   cudaIpcMemLazyEnablePeerAccess : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:152

   cudaMemAttachGlobal : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:154
   cudaMemAttachHost : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:155
   cudaMemAttachSingle : constant := 16#04#;  --  /usr/local/cuda-11.5/include//driver_types.h:156

   cudaOccupancyDefault : constant := 16#00#;  --  /usr/local/cuda-11.5/include//driver_types.h:158
   cudaOccupancyDisableCachingOverride : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:159
   --  unsupported macro: cudaCpuDeviceId ((int)-1)
   --  unsupported macro: cudaInvalidDeviceId ((int)-2)

   cudaCooperativeLaunchMultiDeviceNoPreSync : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:169

   cudaCooperativeLaunchMultiDeviceNoPostSync : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:176

   cudaArraySparsePropertiesSingleMipTail : constant := 16#1#;  --  /usr/local/cuda-11.5/include//driver_types.h:1144
   --  unsupported macro: cudaDevicePropDontCare { {'\0'}, {{0}}, {'\0'}, 0, 0, 0, 0, 0, 0, 0, {0, 0, 0}, {0, 0, 0}, 0, 0, -1, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, {0, 0}, {0, 0}, {0, 0, 0}, {0, 0}, {0, 0, 0}, {0, 0, 0}, 0, {0, 0}, {0, 0, 0}, {0, 0}, 0, {0, 0}, {0, 0, 0}, {0, 0}, {0, 0, 0}, 0, {0, 0}, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, }

   CUDA_IPC_HANDLE_SIZE : constant := 64;  --  /usr/local/cuda-11.5/include//driver_types.h:2367

   cudaExternalMemoryDedicated : constant := 16#1#;  --  /usr/local/cuda-11.5/include//driver_types.h:2426

   cudaExternalSemaphoreSignalSkipNvSciBufMemSync : constant := 16#01#;  --  /usr/local/cuda-11.5/include//driver_types.h:2435

   cudaExternalSemaphoreWaitSkipNvSciBufMemSync : constant := 16#02#;  --  /usr/local/cuda-11.5/include//driver_types.h:2444

   cudaNvSciSyncAttrSignal : constant := 16#1#;  --  /usr/local/cuda-11.5/include//driver_types.h:2451

   cudaNvSciSyncAttrWait : constant := 16#2#;  --  /usr/local/cuda-11.5/include//driver_types.h:2458

   subtype cudaError is unsigned;
   cudaSuccess : constant cudaError := 0;
   cudaErrorInvalidValue : constant cudaError := 1;
   cudaErrorMemoryAllocation : constant cudaError := 2;
   cudaErrorInitializationError : constant cudaError := 3;
   cudaErrorCudartUnloading : constant cudaError := 4;
   cudaErrorProfilerDisabled : constant cudaError := 5;
   cudaErrorProfilerNotInitialized : constant cudaError := 6;
   cudaErrorProfilerAlreadyStarted : constant cudaError := 7;
   cudaErrorProfilerAlreadyStopped : constant cudaError := 8;
   cudaErrorInvalidConfiguration : constant cudaError := 9;
   cudaErrorInvalidPitchValue : constant cudaError := 12;
   cudaErrorInvalidSymbol : constant cudaError := 13;
   cudaErrorInvalidHostPointer : constant cudaError := 16;
   cudaErrorInvalidDevicePointer : constant cudaError := 17;
   cudaErrorInvalidTexture : constant cudaError := 18;
   cudaErrorInvalidTextureBinding : constant cudaError := 19;
   cudaErrorInvalidChannelDescriptor : constant cudaError := 20;
   cudaErrorInvalidMemcpyDirection : constant cudaError := 21;
   cudaErrorAddressOfConstant : constant cudaError := 22;
   cudaErrorTextureFetchFailed : constant cudaError := 23;
   cudaErrorTextureNotBound : constant cudaError := 24;
   cudaErrorSynchronizationError : constant cudaError := 25;
   cudaErrorInvalidFilterSetting : constant cudaError := 26;
   cudaErrorInvalidNormSetting : constant cudaError := 27;
   cudaErrorMixedDeviceExecution : constant cudaError := 28;
   cudaErrorNotYetImplemented : constant cudaError := 31;
   cudaErrorMemoryValueTooLarge : constant cudaError := 32;
   cudaErrorStubLibrary : constant cudaError := 34;
   cudaErrorInsufficientDriver : constant cudaError := 35;
   cudaErrorCallRequiresNewerDriver : constant cudaError := 36;
   cudaErrorInvalidSurface : constant cudaError := 37;
   cudaErrorDuplicateVariableName : constant cudaError := 43;
   cudaErrorDuplicateTextureName : constant cudaError := 44;
   cudaErrorDuplicateSurfaceName : constant cudaError := 45;
   cudaErrorDevicesUnavailable : constant cudaError := 46;
   cudaErrorIncompatibleDriverContext : constant cudaError := 49;
   cudaErrorMissingConfiguration : constant cudaError := 52;
   cudaErrorPriorLaunchFailure : constant cudaError := 53;
   cudaErrorLaunchMaxDepthExceeded : constant cudaError := 65;
   cudaErrorLaunchFileScopedTex : constant cudaError := 66;
   cudaErrorLaunchFileScopedSurf : constant cudaError := 67;
   cudaErrorSyncDepthExceeded : constant cudaError := 68;
   cudaErrorLaunchPendingCountExceeded : constant cudaError := 69;
   cudaErrorInvalidDeviceFunction : constant cudaError := 98;
   cudaErrorNoDevice : constant cudaError := 100;
   cudaErrorInvalidDevice : constant cudaError := 101;
   cudaErrorDeviceNotLicensed : constant cudaError := 102;
   cudaErrorSoftwareValidityNotEstablished : constant cudaError := 103;
   cudaErrorStartupFailure : constant cudaError := 127;
   cudaErrorInvalidKernelImage : constant cudaError := 200;
   cudaErrorDeviceUninitialized : constant cudaError := 201;
   cudaErrorMapBufferObjectFailed : constant cudaError := 205;
   cudaErrorUnmapBufferObjectFailed : constant cudaError := 206;
   cudaErrorArrayIsMapped : constant cudaError := 207;
   cudaErrorAlreadyMapped : constant cudaError := 208;
   cudaErrorNoKernelImageForDevice : constant cudaError := 209;
   cudaErrorAlreadyAcquired : constant cudaError := 210;
   cudaErrorNotMapped : constant cudaError := 211;
   cudaErrorNotMappedAsArray : constant cudaError := 212;
   cudaErrorNotMappedAsPointer : constant cudaError := 213;
   cudaErrorECCUncorrectable : constant cudaError := 214;
   cudaErrorUnsupportedLimit : constant cudaError := 215;
   cudaErrorDeviceAlreadyInUse : constant cudaError := 216;
   cudaErrorPeerAccessUnsupported : constant cudaError := 217;
   cudaErrorInvalidPtx : constant cudaError := 218;
   cudaErrorInvalidGraphicsContext : constant cudaError := 219;
   cudaErrorNvlinkUncorrectable : constant cudaError := 220;
   cudaErrorJitCompilerNotFound : constant cudaError := 221;
   cudaErrorUnsupportedPtxVersion : constant cudaError := 222;
   cudaErrorJitCompilationDisabled : constant cudaError := 223;
   cudaErrorUnsupportedExecAffinity : constant cudaError := 224;
   cudaErrorInvalidSource : constant cudaError := 300;
   cudaErrorFileNotFound : constant cudaError := 301;
   cudaErrorSharedObjectSymbolNotFound : constant cudaError := 302;
   cudaErrorSharedObjectInitFailed : constant cudaError := 303;
   cudaErrorOperatingSystem : constant cudaError := 304;
   cudaErrorInvalidResourceHandle : constant cudaError := 400;
   cudaErrorIllegalState : constant cudaError := 401;
   cudaErrorSymbolNotFound : constant cudaError := 500;
   cudaErrorNotReady : constant cudaError := 600;
   cudaErrorIllegalAddress : constant cudaError := 700;
   cudaErrorLaunchOutOfResources : constant cudaError := 701;
   cudaErrorLaunchTimeout : constant cudaError := 702;
   cudaErrorLaunchIncompatibleTexturing : constant cudaError := 703;
   cudaErrorPeerAccessAlreadyEnabled : constant cudaError := 704;
   cudaErrorPeerAccessNotEnabled : constant cudaError := 705;
   cudaErrorSetOnActiveProcess : constant cudaError := 708;
   cudaErrorContextIsDestroyed : constant cudaError := 709;
   cudaErrorAssert : constant cudaError := 710;
   cudaErrorTooManyPeers : constant cudaError := 711;
   cudaErrorHostMemoryAlreadyRegistered : constant cudaError := 712;
   cudaErrorHostMemoryNotRegistered : constant cudaError := 713;
   cudaErrorHardwareStackError : constant cudaError := 714;
   cudaErrorIllegalInstruction : constant cudaError := 715;
   cudaErrorMisalignedAddress : constant cudaError := 716;
   cudaErrorInvalidAddressSpace : constant cudaError := 717;
   cudaErrorInvalidPc : constant cudaError := 718;
   cudaErrorLaunchFailure : constant cudaError := 719;
   cudaErrorCooperativeLaunchTooLarge : constant cudaError := 720;
   cudaErrorNotPermitted : constant cudaError := 800;
   cudaErrorNotSupported : constant cudaError := 801;
   cudaErrorSystemNotReady : constant cudaError := 802;
   cudaErrorSystemDriverMismatch : constant cudaError := 803;
   cudaErrorCompatNotSupportedOnDevice : constant cudaError := 804;
   cudaErrorMpsConnectionFailed : constant cudaError := 805;
   cudaErrorMpsRpcFailure : constant cudaError := 806;
   cudaErrorMpsServerNotReady : constant cudaError := 807;
   cudaErrorMpsMaxClientsReached : constant cudaError := 808;
   cudaErrorMpsMaxConnectionsReached : constant cudaError := 809;
   cudaErrorStreamCaptureUnsupported : constant cudaError := 900;
   cudaErrorStreamCaptureInvalidated : constant cudaError := 901;
   cudaErrorStreamCaptureMerge : constant cudaError := 902;
   cudaErrorStreamCaptureUnmatched : constant cudaError := 903;
   cudaErrorStreamCaptureUnjoined : constant cudaError := 904;
   cudaErrorStreamCaptureIsolation : constant cudaError := 905;
   cudaErrorStreamCaptureImplicit : constant cudaError := 906;
   cudaErrorCapturedEvent : constant cudaError := 907;
   cudaErrorStreamCaptureWrongThread : constant cudaError := 908;
   cudaErrorTimeout : constant cudaError := 909;
   cudaErrorGraphExecUpdateFailure : constant cudaError := 910;
   cudaErrorExternalDevice : constant cudaError := 911;
   cudaErrorUnknown : constant cudaError := 999;
   cudaErrorApiFailureBase : constant cudaError := 10000;  -- /usr/local/cuda-11.5/include//driver_types.h:201

   type cudaChannelFormatKind is 
     (cudaChannelFormatKindSigned,
      cudaChannelFormatKindUnsigned,
      cudaChannelFormatKindFloat,
      cudaChannelFormatKindNone,
      cudaChannelFormatKindNV12,
      cudaChannelFormatKindUnsignedNormalized8X1,
      cudaChannelFormatKindUnsignedNormalized8X2,
      cudaChannelFormatKindUnsignedNormalized8X4,
      cudaChannelFormatKindUnsignedNormalized16X1,
      cudaChannelFormatKindUnsignedNormalized16X2,
      cudaChannelFormatKindUnsignedNormalized16X4,
      cudaChannelFormatKindSignedNormalized8X1,
      cudaChannelFormatKindSignedNormalized8X2,
      cudaChannelFormatKindSignedNormalized8X4,
      cudaChannelFormatKindSignedNormalized16X1,
      cudaChannelFormatKindSignedNormalized16X2,
      cudaChannelFormatKindSignedNormalized16X4,
      cudaChannelFormatKindUnsignedBlockCompressed1,
      cudaChannelFormatKindUnsignedBlockCompressed1SRGB,
      cudaChannelFormatKindUnsignedBlockCompressed2,
      cudaChannelFormatKindUnsignedBlockCompressed2SRGB,
      cudaChannelFormatKindUnsignedBlockCompressed3,
      cudaChannelFormatKindUnsignedBlockCompressed3SRGB,
      cudaChannelFormatKindUnsignedBlockCompressed4,
      cudaChannelFormatKindSignedBlockCompressed4,
      cudaChannelFormatKindUnsignedBlockCompressed5,
      cudaChannelFormatKindSignedBlockCompressed5,
      cudaChannelFormatKindUnsignedBlockCompressed6H,
      cudaChannelFormatKindSignedBlockCompressed6H,
      cudaChannelFormatKindUnsignedBlockCompressed7,
      cudaChannelFormatKindUnsignedBlockCompressed7SRGB);
   pragma Convention (C, cudaChannelFormatKind);  -- /usr/local/cuda-11.5/include//driver_types.h:1070

   type cudaChannelFormatDesc is record
      x : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1110
      y : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1111
      z : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1112
      w : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1113
      f : aliased cudaChannelFormatKind;  -- /usr/local/cuda-11.5/include//driver_types.h:1114
   end record;
   pragma Convention (C_Pass_By_Copy, cudaChannelFormatDesc);  -- /usr/local/cuda-11.5/include//driver_types.h:1108

   --  skipped empty struct cudaArray

   type cudaArray_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1120

   type cudaArray_const_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1125

   --  skipped empty struct cudaMipmappedArray

   type cudaMipmappedArray_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1132

   type cudaMipmappedArray_const_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1137

   type cudaArraySparseProperties;
   type anon_1 is record
      width : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1151
      height : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1152
      depth : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1153
   end record;
   pragma Convention (C_Pass_By_Copy, anon_1);
   type cudaArraySparseProperties_reserved_array is array (0 .. 3) of aliased unsigned;
   type cudaArraySparseProperties is record
      tileExtent : aliased anon_1;  -- /usr/local/cuda-11.5/include//driver_types.h:1154
      miptailFirstLevel : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1155
      miptailSize : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:1156
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1157
      reserved : aliased cudaArraySparseProperties_reserved_array;  -- /usr/local/cuda-11.5/include//driver_types.h:1158
   end record;
   pragma Convention (C_Pass_By_Copy, cudaArraySparseProperties);  -- /usr/local/cuda-11.5/include//driver_types.h:1149

   type cudaMemoryType is 
     (cudaMemoryTypeUnregistered,
      cudaMemoryTypeHost,
      cudaMemoryTypeDevice,
      cudaMemoryTypeManaged);
   pragma Convention (C, cudaMemoryType);  -- /usr/local/cuda-11.5/include//driver_types.h:1164

   type cudaMemcpyKind is 
     (cudaMemcpyHostToHost,
      cudaMemcpyHostToDevice,
      cudaMemcpyDeviceToHost,
      cudaMemcpyDeviceToDevice,
      cudaMemcpyDefault);
   pragma Convention (C, cudaMemcpyKind);  -- /usr/local/cuda-11.5/include//driver_types.h:1175

   type cudaPitchedPtr is record
      ptr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1191
      pitch : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1192
      xsize : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1193
      ysize : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1194
   end record;
   pragma Convention (C_Pass_By_Copy, cudaPitchedPtr);  -- /usr/local/cuda-11.5/include//driver_types.h:1189

   type cudaExtent is record
      width : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1204
      height : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1205
      depth : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1206
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExtent);  -- /usr/local/cuda-11.5/include//driver_types.h:1202

   type cudaPos is record
      x : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1216
      y : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1217
      z : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1218
   end record;
   pragma Convention (C_Pass_By_Copy, cudaPos);  -- /usr/local/cuda-11.5/include//driver_types.h:1214

   type cudaMemcpy3DParms is record
      srcArray : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1226
      srcPos : aliased cudaPos;  -- /usr/local/cuda-11.5/include//driver_types.h:1227
      srcPtr : aliased cudaPitchedPtr;  -- /usr/local/cuda-11.5/include//driver_types.h:1228
      dstArray : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1230
      dstPos : aliased cudaPos;  -- /usr/local/cuda-11.5/include//driver_types.h:1231
      dstPtr : aliased cudaPitchedPtr;  -- /usr/local/cuda-11.5/include//driver_types.h:1232
      extent : aliased cudaExtent;  -- /usr/local/cuda-11.5/include//driver_types.h:1234
      kind : aliased cudaMemcpyKind;  -- /usr/local/cuda-11.5/include//driver_types.h:1235
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemcpy3DParms);  -- /usr/local/cuda-11.5/include//driver_types.h:1224

   type cudaMemcpy3DPeerParms is record
      srcArray : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1243
      srcPos : aliased cudaPos;  -- /usr/local/cuda-11.5/include//driver_types.h:1244
      srcPtr : aliased cudaPitchedPtr;  -- /usr/local/cuda-11.5/include//driver_types.h:1245
      srcDevice : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1246
      dstArray : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1248
      dstPos : aliased cudaPos;  -- /usr/local/cuda-11.5/include//driver_types.h:1249
      dstPtr : aliased cudaPitchedPtr;  -- /usr/local/cuda-11.5/include//driver_types.h:1250
      dstDevice : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1251
      extent : aliased cudaExtent;  -- /usr/local/cuda-11.5/include//driver_types.h:1253
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemcpy3DPeerParms);  -- /usr/local/cuda-11.5/include//driver_types.h:1241

   type cudaMemsetParams is record
      dst : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1260
      pitch : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1261
      value : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1262
      elementSize : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1263
      width : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1264
      height : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1265
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemsetParams);  -- /usr/local/cuda-11.5/include//driver_types.h:1259

   type cudaAccessProperty is 
     (cudaAccessPropertyNormal,
      cudaAccessPropertyStreaming,
      cudaAccessPropertyPersisting);
   pragma Convention (C, cudaAccessProperty);  -- /usr/local/cuda-11.5/include//driver_types.h:1271

   type cudaAccessPolicyWindow is record
      base_ptr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1289
      num_bytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1290
      hitRatio : aliased float;  -- /usr/local/cuda-11.5/include//driver_types.h:1291
      hitProp : aliased cudaAccessProperty;  -- /usr/local/cuda-11.5/include//driver_types.h:1292
      missProp : aliased cudaAccessProperty;  -- /usr/local/cuda-11.5/include//driver_types.h:1293
   end record;
   pragma Convention (C_Pass_By_Copy, cudaAccessPolicyWindow);  -- /usr/local/cuda-11.5/include//driver_types.h:1288

   type cudaHostFn_t is access procedure (arg1 : System.Address);
   pragma Convention (C, cudaHostFn_t);  -- /usr/local/cuda-11.5/include//driver_types.h:1306

   type cudaHostNodeParams is record
      fn : cudaHostFn_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1312
      userData : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1313
   end record;
   pragma Convention (C_Pass_By_Copy, cudaHostNodeParams);  -- /usr/local/cuda-11.5/include//driver_types.h:1311

   type cudaStreamCaptureStatus is 
     (cudaStreamCaptureStatusNone,
      cudaStreamCaptureStatusActive,
      cudaStreamCaptureStatusInvalidated);
   pragma Convention (C, cudaStreamCaptureStatus);  -- /usr/local/cuda-11.5/include//driver_types.h:1319

   type cudaStreamCaptureMode is 
     (cudaStreamCaptureModeGlobal,
      cudaStreamCaptureModeThreadLocal,
      cudaStreamCaptureModeRelaxed);
   pragma Convention (C, cudaStreamCaptureMode);  -- /usr/local/cuda-11.5/include//driver_types.h:1330

   subtype cudaSynchronizationPolicy is unsigned;
   cudaSyncPolicyAuto : constant cudaSynchronizationPolicy := 1;
   cudaSyncPolicySpin : constant cudaSynchronizationPolicy := 2;
   cudaSyncPolicyYield : constant cudaSynchronizationPolicy := 3;
   cudaSyncPolicyBlockingSync : constant cudaSynchronizationPolicy := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:1336

   subtype cudaStreamAttrID is unsigned;
   cudaStreamAttributeAccessPolicyWindow : constant cudaStreamAttrID := 1;
   cudaStreamAttributeSynchronizationPolicy : constant cudaStreamAttrID := 3;  -- /usr/local/cuda-11.5/include//driver_types.h:1346

   type cudaStreamAttrValue (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            accessPolicyWindow : aliased cudaAccessPolicyWindow;  -- /usr/local/cuda-11.5/include//driver_types.h:1355
         when others =>
            syncPolicy : aliased cudaSynchronizationPolicy;  -- /usr/local/cuda-11.5/include//driver_types.h:1356
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, cudaStreamAttrValue);
   pragma Unchecked_Union (cudaStreamAttrValue);  -- /usr/local/cuda-11.5/include//driver_types.h:1354

   type cudaStreamUpdateCaptureDependenciesFlags is 
     (cudaStreamAddCaptureDependencies,
      cudaStreamSetCaptureDependencies);
   pragma Convention (C, cudaStreamUpdateCaptureDependenciesFlags);  -- /usr/local/cuda-11.5/include//driver_types.h:1362

   subtype cudaUserObjectFlags is unsigned;
   cudaUserObjectNoDestructorSync : constant cudaUserObjectFlags := 1;  -- /usr/local/cuda-11.5/include//driver_types.h:1370

   subtype cudaUserObjectRetainFlags is unsigned;
   cudaGraphUserObjectMove : constant cudaUserObjectRetainFlags := 1;  -- /usr/local/cuda-11.5/include//driver_types.h:1377

   --  skipped empty struct cudaGraphicsResource

   subtype cudaGraphicsRegisterFlags is unsigned;
   cudaGraphicsRegisterFlagsNone : constant cudaGraphicsRegisterFlags := 0;
   cudaGraphicsRegisterFlagsReadOnly : constant cudaGraphicsRegisterFlags := 1;
   cudaGraphicsRegisterFlagsWriteDiscard : constant cudaGraphicsRegisterFlags := 2;
   cudaGraphicsRegisterFlagsSurfaceLoadStore : constant cudaGraphicsRegisterFlags := 4;
   cudaGraphicsRegisterFlagsTextureGather : constant cudaGraphicsRegisterFlags := 8;  -- /usr/local/cuda-11.5/include//driver_types.h:1389

   type cudaGraphicsMapFlags is 
     (cudaGraphicsMapFlagsNone,
      cudaGraphicsMapFlagsReadOnly,
      cudaGraphicsMapFlagsWriteDiscard);
   pragma Convention (C, cudaGraphicsMapFlags);  -- /usr/local/cuda-11.5/include//driver_types.h:1401

   type cudaGraphicsCubeFace is 
     (cudaGraphicsCubeFacePositiveX,
      cudaGraphicsCubeFaceNegativeX,
      cudaGraphicsCubeFacePositiveY,
      cudaGraphicsCubeFaceNegativeY,
      cudaGraphicsCubeFacePositiveZ,
      cudaGraphicsCubeFaceNegativeZ);
   pragma Convention (C, cudaGraphicsCubeFace);  -- /usr/local/cuda-11.5/include//driver_types.h:1411

   subtype cudaKernelNodeAttrID is unsigned;
   cudaKernelNodeAttributeAccessPolicyWindow : constant cudaKernelNodeAttrID := 1;
   cudaKernelNodeAttributeCooperative : constant cudaKernelNodeAttrID := 2;  -- /usr/local/cuda-11.5/include//driver_types.h:1424

   type cudaKernelNodeAttrValue (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            accessPolicyWindow : aliased cudaAccessPolicyWindow;  -- /usr/local/cuda-11.5/include//driver_types.h:1433
         when others =>
            cooperative : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1434
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, cudaKernelNodeAttrValue);
   pragma Unchecked_Union (cudaKernelNodeAttrValue);  -- /usr/local/cuda-11.5/include//driver_types.h:1432

   type cudaResourceType is 
     (cudaResourceTypeArray,
      cudaResourceTypeMipmappedArray,
      cudaResourceTypeLinear,
      cudaResourceTypePitch2D);
   pragma Convention (C, cudaResourceType);  -- /usr/local/cuda-11.5/include//driver_types.h:1440

   type cudaResourceViewFormat is 
     (cudaResViewFormatNone,
      cudaResViewFormatUnsignedChar1,
      cudaResViewFormatUnsignedChar2,
      cudaResViewFormatUnsignedChar4,
      cudaResViewFormatSignedChar1,
      cudaResViewFormatSignedChar2,
      cudaResViewFormatSignedChar4,
      cudaResViewFormatUnsignedShort1,
      cudaResViewFormatUnsignedShort2,
      cudaResViewFormatUnsignedShort4,
      cudaResViewFormatSignedShort1,
      cudaResViewFormatSignedShort2,
      cudaResViewFormatSignedShort4,
      cudaResViewFormatUnsignedInt1,
      cudaResViewFormatUnsignedInt2,
      cudaResViewFormatUnsignedInt4,
      cudaResViewFormatSignedInt1,
      cudaResViewFormatSignedInt2,
      cudaResViewFormatSignedInt4,
      cudaResViewFormatHalf1,
      cudaResViewFormatHalf2,
      cudaResViewFormatHalf4,
      cudaResViewFormatFloat1,
      cudaResViewFormatFloat2,
      cudaResViewFormatFloat4,
      cudaResViewFormatUnsignedBlockCompressed1,
      cudaResViewFormatUnsignedBlockCompressed2,
      cudaResViewFormatUnsignedBlockCompressed3,
      cudaResViewFormatUnsignedBlockCompressed4,
      cudaResViewFormatSignedBlockCompressed4,
      cudaResViewFormatUnsignedBlockCompressed5,
      cudaResViewFormatSignedBlockCompressed5,
      cudaResViewFormatUnsignedBlockCompressed6H,
      cudaResViewFormatSignedBlockCompressed6H,
      cudaResViewFormatUnsignedBlockCompressed7);
   pragma Convention (C, cudaResourceViewFormat);  -- /usr/local/cuda-11.5/include//driver_types.h:1451

   type cudaResourceDesc;
   type anon_2;
   type anon_3 is record
      c_array : cudaArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1498
   end record;
   pragma Convention (C_Pass_By_Copy, anon_3);
   type anon_4 is record
      mipmap : cudaMipmappedArray_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1501
   end record;
   pragma Convention (C_Pass_By_Copy, anon_4);
   type anon_5 is record
      devPtr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1504
      desc : aliased cudaChannelFormatDesc;  -- /usr/local/cuda-11.5/include//driver_types.h:1505
      sizeInBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1506
   end record;
   pragma Convention (C_Pass_By_Copy, anon_5);
   type anon_6 is record
      devPtr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1509
      desc : aliased cudaChannelFormatDesc;  -- /usr/local/cuda-11.5/include//driver_types.h:1510
      width : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1511
      height : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1512
      pitchInBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1513
   end record;
   pragma Convention (C_Pass_By_Copy, anon_6);
   type anon_2 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            c_array : aliased anon_3;  -- /usr/local/cuda-11.5/include//driver_types.h:1499
         when 1 =>
            mipmap : aliased anon_4;  -- /usr/local/cuda-11.5/include//driver_types.h:1502
         when 2 =>
            linear : aliased anon_5;  -- /usr/local/cuda-11.5/include//driver_types.h:1507
         when others =>
            pitch2D : aliased anon_6;  -- /usr/local/cuda-11.5/include//driver_types.h:1514
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, anon_2);
   pragma Unchecked_Union (anon_2);type cudaResourceDesc is record
      resType : aliased cudaResourceType;  -- /usr/local/cuda-11.5/include//driver_types.h:1494
      res : aliased anon_2;  -- /usr/local/cuda-11.5/include//driver_types.h:1515
   end record;
   pragma Convention (C_Pass_By_Copy, cudaResourceDesc);  -- /usr/local/cuda-11.5/include//driver_types.h:1493

   type cudaResourceViewDesc is record
      format : aliased cudaResourceViewFormat;  -- /usr/local/cuda-11.5/include//driver_types.h:1523
      width : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1524
      height : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1525
      depth : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1526
      firstMipmapLevel : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1527
      lastMipmapLevel : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1528
      firstLayer : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1529
      lastLayer : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:1530
   end record;
   pragma Convention (C_Pass_By_Copy, cudaResourceViewDesc);  -- /usr/local/cuda-11.5/include//driver_types.h:1521

   type cudaPointerAttributes is record
      c_type : aliased cudaMemoryType;  -- /usr/local/cuda-11.5/include//driver_types.h:1542
      device : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1553
      devicePointer : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1559
      hostPointer : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:1568
   end record;
   pragma Convention (C_Pass_By_Copy, cudaPointerAttributes);  -- /usr/local/cuda-11.5/include//driver_types.h:1536

   type cudaFuncAttributes is record
      sharedSizeBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1581
      constSizeBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1587
      localSizeBytes : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:1592
      maxThreadsPerBlock : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1599
      numRegs : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1604
      ptxVersion : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1611
      binaryVersion : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1618
      cacheModeCA : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1624
      maxDynamicSharedSizeBytes : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1631
      preferredShmemCarveout : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:1640
   end record;
   pragma Convention (C_Pass_By_Copy, cudaFuncAttributes);  -- /usr/local/cuda-11.5/include//driver_types.h:1574

   subtype cudaFuncAttribute is unsigned;
   cudaFuncAttributeMaxDynamicSharedMemorySize : constant cudaFuncAttribute := 8;
   cudaFuncAttributePreferredSharedMemoryCarveout : constant cudaFuncAttribute := 9;
   cudaFuncAttributeMax : constant cudaFuncAttribute := 10;  -- /usr/local/cuda-11.5/include//driver_types.h:1695

   type cudaFuncCache is 
     (cudaFuncCachePreferNone,
      cudaFuncCachePreferShared,
      cudaFuncCachePreferL1,
      cudaFuncCachePreferEqual);
   pragma Convention (C, cudaFuncCache);  -- /usr/local/cuda-11.5/include//driver_types.h:1713

   type cudaSharedMemConfig is 
     (cudaSharedMemBankSizeDefault,
      cudaSharedMemBankSizeFourByte,
      cudaSharedMemBankSizeEightByte);
   pragma Convention (C, cudaSharedMemConfig);  -- /usr/local/cuda-11.5/include//driver_types.h:1725

   subtype cudaSharedCarveout is unsigned;
   cudaSharedmemCarveoutDefault : constant cudaSharedCarveout := -1;
   cudaSharedmemCarveoutMaxShared : constant cudaSharedCarveout := 100;
   cudaSharedmemCarveoutMaxL1 : constant cudaSharedCarveout := 0;  -- /usr/local/cuda-11.5/include//driver_types.h:1735

   type cudaComputeMode is 
     (cudaComputeModeDefault,
      cudaComputeModeExclusive,
      cudaComputeModeProhibited,
      cudaComputeModeExclusiveProcess);
   pragma Convention (C, cudaComputeMode);  -- /usr/local/cuda-11.5/include//driver_types.h:1755

   type cudaLimit is 
     (cudaLimitStackSize,
      cudaLimitPrintfFifoSize,
      cudaLimitMallocHeapSize,
      cudaLimitDevRuntimeSyncDepth,
      cudaLimitDevRuntimePendingLaunchCount,
      cudaLimitMaxL2FetchGranularity,
      cudaLimitPersistingL2CacheSize);
   pragma Convention (C, cudaLimit);  -- /usr/local/cuda-11.5/include//driver_types.h:1766

   subtype cudaMemoryAdvise is unsigned;
   cudaMemAdviseSetReadMostly : constant cudaMemoryAdvise := 1;
   cudaMemAdviseUnsetReadMostly : constant cudaMemoryAdvise := 2;
   cudaMemAdviseSetPreferredLocation : constant cudaMemoryAdvise := 3;
   cudaMemAdviseUnsetPreferredLocation : constant cudaMemoryAdvise := 4;
   cudaMemAdviseSetAccessedBy : constant cudaMemoryAdvise := 5;
   cudaMemAdviseUnsetAccessedBy : constant cudaMemoryAdvise := 6;  -- /usr/local/cuda-11.5/include//driver_types.h:1780

   subtype cudaMemRangeAttribute is unsigned;
   cudaMemRangeAttributeReadMostly : constant cudaMemRangeAttribute := 1;
   cudaMemRangeAttributePreferredLocation : constant cudaMemRangeAttribute := 2;
   cudaMemRangeAttributeAccessedBy : constant cudaMemRangeAttribute := 3;
   cudaMemRangeAttributeLastPrefetchLocation : constant cudaMemRangeAttribute := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:1793

   type cudaOutputMode is 
     (cudaKeyValuePair,
      cudaCSV);
   pragma Convention (C, cudaOutputMode);  -- /usr/local/cuda-11.5/include//driver_types.h:1804

   subtype cudaFlushGPUDirectRDMAWritesOptions is unsigned;
   cudaFlushGPUDirectRDMAWritesOptionHost : constant cudaFlushGPUDirectRDMAWritesOptions := 1;
   cudaFlushGPUDirectRDMAWritesOptionMemOps : constant cudaFlushGPUDirectRDMAWritesOptions := 2;  -- /usr/local/cuda-11.5/include//driver_types.h:1813

   subtype cudaGPUDirectRDMAWritesOrdering is unsigned;
   cudaGPUDirectRDMAWritesOrderingNone : constant cudaGPUDirectRDMAWritesOrdering := 0;
   cudaGPUDirectRDMAWritesOrderingOwner : constant cudaGPUDirectRDMAWritesOrdering := 100;
   cudaGPUDirectRDMAWritesOrderingAllDevices : constant cudaGPUDirectRDMAWritesOrdering := 200;  -- /usr/local/cuda-11.5/include//driver_types.h:1821

   subtype cudaFlushGPUDirectRDMAWritesScope is unsigned;
   cudaFlushGPUDirectRDMAWritesToOwner : constant cudaFlushGPUDirectRDMAWritesScope := 100;
   cudaFlushGPUDirectRDMAWritesToAllDevices : constant cudaFlushGPUDirectRDMAWritesScope := 200;  -- /usr/local/cuda-11.5/include//driver_types.h:1830

   type cudaFlushGPUDirectRDMAWritesTarget is 
     (cudaFlushGPUDirectRDMAWritesTargetCurrentDevice);
   pragma Convention (C, cudaFlushGPUDirectRDMAWritesTarget);  -- /usr/local/cuda-11.5/include//driver_types.h:1838

   subtype cudaDeviceAttr is unsigned;
   cudaDevAttrMaxThreadsPerBlock : constant cudaDeviceAttr := 1;
   cudaDevAttrMaxBlockDimX : constant cudaDeviceAttr := 2;
   cudaDevAttrMaxBlockDimY : constant cudaDeviceAttr := 3;
   cudaDevAttrMaxBlockDimZ : constant cudaDeviceAttr := 4;
   cudaDevAttrMaxGridDimX : constant cudaDeviceAttr := 5;
   cudaDevAttrMaxGridDimY : constant cudaDeviceAttr := 6;
   cudaDevAttrMaxGridDimZ : constant cudaDeviceAttr := 7;
   cudaDevAttrMaxSharedMemoryPerBlock : constant cudaDeviceAttr := 8;
   cudaDevAttrTotalConstantMemory : constant cudaDeviceAttr := 9;
   cudaDevAttrWarpSize : constant cudaDeviceAttr := 10;
   cudaDevAttrMaxPitch : constant cudaDeviceAttr := 11;
   cudaDevAttrMaxRegistersPerBlock : constant cudaDeviceAttr := 12;
   cudaDevAttrClockRate : constant cudaDeviceAttr := 13;
   cudaDevAttrTextureAlignment : constant cudaDeviceAttr := 14;
   cudaDevAttrGpuOverlap : constant cudaDeviceAttr := 15;
   cudaDevAttrMultiProcessorCount : constant cudaDeviceAttr := 16;
   cudaDevAttrKernelExecTimeout : constant cudaDeviceAttr := 17;
   cudaDevAttrIntegrated : constant cudaDeviceAttr := 18;
   cudaDevAttrCanMapHostMemory : constant cudaDeviceAttr := 19;
   cudaDevAttrComputeMode : constant cudaDeviceAttr := 20;
   cudaDevAttrMaxTexture1DWidth : constant cudaDeviceAttr := 21;
   cudaDevAttrMaxTexture2DWidth : constant cudaDeviceAttr := 22;
   cudaDevAttrMaxTexture2DHeight : constant cudaDeviceAttr := 23;
   cudaDevAttrMaxTexture3DWidth : constant cudaDeviceAttr := 24;
   cudaDevAttrMaxTexture3DHeight : constant cudaDeviceAttr := 25;
   cudaDevAttrMaxTexture3DDepth : constant cudaDeviceAttr := 26;
   cudaDevAttrMaxTexture2DLayeredWidth : constant cudaDeviceAttr := 27;
   cudaDevAttrMaxTexture2DLayeredHeight : constant cudaDeviceAttr := 28;
   cudaDevAttrMaxTexture2DLayeredLayers : constant cudaDeviceAttr := 29;
   cudaDevAttrSurfaceAlignment : constant cudaDeviceAttr := 30;
   cudaDevAttrConcurrentKernels : constant cudaDeviceAttr := 31;
   cudaDevAttrEccEnabled : constant cudaDeviceAttr := 32;
   cudaDevAttrPciBusId : constant cudaDeviceAttr := 33;
   cudaDevAttrPciDeviceId : constant cudaDeviceAttr := 34;
   cudaDevAttrTccDriver : constant cudaDeviceAttr := 35;
   cudaDevAttrMemoryClockRate : constant cudaDeviceAttr := 36;
   cudaDevAttrGlobalMemoryBusWidth : constant cudaDeviceAttr := 37;
   cudaDevAttrL2CacheSize : constant cudaDeviceAttr := 38;
   cudaDevAttrMaxThreadsPerMultiProcessor : constant cudaDeviceAttr := 39;
   cudaDevAttrAsyncEngineCount : constant cudaDeviceAttr := 40;
   cudaDevAttrUnifiedAddressing : constant cudaDeviceAttr := 41;
   cudaDevAttrMaxTexture1DLayeredWidth : constant cudaDeviceAttr := 42;
   cudaDevAttrMaxTexture1DLayeredLayers : constant cudaDeviceAttr := 43;
   cudaDevAttrMaxTexture2DGatherWidth : constant cudaDeviceAttr := 45;
   cudaDevAttrMaxTexture2DGatherHeight : constant cudaDeviceAttr := 46;
   cudaDevAttrMaxTexture3DWidthAlt : constant cudaDeviceAttr := 47;
   cudaDevAttrMaxTexture3DHeightAlt : constant cudaDeviceAttr := 48;
   cudaDevAttrMaxTexture3DDepthAlt : constant cudaDeviceAttr := 49;
   cudaDevAttrPciDomainId : constant cudaDeviceAttr := 50;
   cudaDevAttrTexturePitchAlignment : constant cudaDeviceAttr := 51;
   cudaDevAttrMaxTextureCubemapWidth : constant cudaDeviceAttr := 52;
   cudaDevAttrMaxTextureCubemapLayeredWidth : constant cudaDeviceAttr := 53;
   cudaDevAttrMaxTextureCubemapLayeredLayers : constant cudaDeviceAttr := 54;
   cudaDevAttrMaxSurface1DWidth : constant cudaDeviceAttr := 55;
   cudaDevAttrMaxSurface2DWidth : constant cudaDeviceAttr := 56;
   cudaDevAttrMaxSurface2DHeight : constant cudaDeviceAttr := 57;
   cudaDevAttrMaxSurface3DWidth : constant cudaDeviceAttr := 58;
   cudaDevAttrMaxSurface3DHeight : constant cudaDeviceAttr := 59;
   cudaDevAttrMaxSurface3DDepth : constant cudaDeviceAttr := 60;
   cudaDevAttrMaxSurface1DLayeredWidth : constant cudaDeviceAttr := 61;
   cudaDevAttrMaxSurface1DLayeredLayers : constant cudaDeviceAttr := 62;
   cudaDevAttrMaxSurface2DLayeredWidth : constant cudaDeviceAttr := 63;
   cudaDevAttrMaxSurface2DLayeredHeight : constant cudaDeviceAttr := 64;
   cudaDevAttrMaxSurface2DLayeredLayers : constant cudaDeviceAttr := 65;
   cudaDevAttrMaxSurfaceCubemapWidth : constant cudaDeviceAttr := 66;
   cudaDevAttrMaxSurfaceCubemapLayeredWidth : constant cudaDeviceAttr := 67;
   cudaDevAttrMaxSurfaceCubemapLayeredLayers : constant cudaDeviceAttr := 68;
   cudaDevAttrMaxTexture1DLinearWidth : constant cudaDeviceAttr := 69;
   cudaDevAttrMaxTexture2DLinearWidth : constant cudaDeviceAttr := 70;
   cudaDevAttrMaxTexture2DLinearHeight : constant cudaDeviceAttr := 71;
   cudaDevAttrMaxTexture2DLinearPitch : constant cudaDeviceAttr := 72;
   cudaDevAttrMaxTexture2DMipmappedWidth : constant cudaDeviceAttr := 73;
   cudaDevAttrMaxTexture2DMipmappedHeight : constant cudaDeviceAttr := 74;
   cudaDevAttrComputeCapabilityMajor : constant cudaDeviceAttr := 75;
   cudaDevAttrComputeCapabilityMinor : constant cudaDeviceAttr := 76;
   cudaDevAttrMaxTexture1DMipmappedWidth : constant cudaDeviceAttr := 77;
   cudaDevAttrStreamPrioritiesSupported : constant cudaDeviceAttr := 78;
   cudaDevAttrGlobalL1CacheSupported : constant cudaDeviceAttr := 79;
   cudaDevAttrLocalL1CacheSupported : constant cudaDeviceAttr := 80;
   cudaDevAttrMaxSharedMemoryPerMultiprocessor : constant cudaDeviceAttr := 81;
   cudaDevAttrMaxRegistersPerMultiprocessor : constant cudaDeviceAttr := 82;
   cudaDevAttrManagedMemory : constant cudaDeviceAttr := 83;
   cudaDevAttrIsMultiGpuBoard : constant cudaDeviceAttr := 84;
   cudaDevAttrMultiGpuBoardGroupID : constant cudaDeviceAttr := 85;
   cudaDevAttrHostNativeAtomicSupported : constant cudaDeviceAttr := 86;
   cudaDevAttrSingleToDoublePrecisionPerfRatio : constant cudaDeviceAttr := 87;
   cudaDevAttrPageableMemoryAccess : constant cudaDeviceAttr := 88;
   cudaDevAttrConcurrentManagedAccess : constant cudaDeviceAttr := 89;
   cudaDevAttrComputePreemptionSupported : constant cudaDeviceAttr := 90;
   cudaDevAttrCanUseHostPointerForRegisteredMem : constant cudaDeviceAttr := 91;
   cudaDevAttrReserved92 : constant cudaDeviceAttr := 92;
   cudaDevAttrReserved93 : constant cudaDeviceAttr := 93;
   cudaDevAttrReserved94 : constant cudaDeviceAttr := 94;
   cudaDevAttrCooperativeLaunch : constant cudaDeviceAttr := 95;
   cudaDevAttrCooperativeMultiDeviceLaunch : constant cudaDeviceAttr := 96;
   cudaDevAttrMaxSharedMemoryPerBlockOptin : constant cudaDeviceAttr := 97;
   cudaDevAttrCanFlushRemoteWrites : constant cudaDeviceAttr := 98;
   cudaDevAttrHostRegisterSupported : constant cudaDeviceAttr := 99;
   cudaDevAttrPageableMemoryAccessUsesHostPageTables : constant cudaDeviceAttr := 100;
   cudaDevAttrDirectManagedMemAccessFromHost : constant cudaDeviceAttr := 101;
   cudaDevAttrMaxBlocksPerMultiprocessor : constant cudaDeviceAttr := 106;
   cudaDevAttrMaxPersistingL2CacheSize : constant cudaDeviceAttr := 108;
   cudaDevAttrMaxAccessPolicyWindowSize : constant cudaDeviceAttr := 109;
   cudaDevAttrReservedSharedMemoryPerBlock : constant cudaDeviceAttr := 111;
   cudaDevAttrSparseCudaArraySupported : constant cudaDeviceAttr := 112;
   cudaDevAttrHostRegisterReadOnlySupported : constant cudaDeviceAttr := 113;
   cudaDevAttrTimelineSemaphoreInteropSupported : constant cudaDeviceAttr := 114;
   cudaDevAttrMaxTimelineSemaphoreInteropSupported : constant cudaDeviceAttr := 114;
   cudaDevAttrMemoryPoolsSupported : constant cudaDeviceAttr := 115;
   cudaDevAttrGPUDirectRDMASupported : constant cudaDeviceAttr := 116;
   cudaDevAttrGPUDirectRDMAFlushWritesOptions : constant cudaDeviceAttr := 117;
   cudaDevAttrGPUDirectRDMAWritesOrdering : constant cudaDeviceAttr := 118;
   cudaDevAttrMemoryPoolSupportedHandleTypes : constant cudaDeviceAttr := 119;
   cudaDevAttrMax : constant cudaDeviceAttr := 120;  -- /usr/local/cuda-11.5/include//driver_types.h:1846

   subtype cudaMemPoolAttr is unsigned;
   cudaMemPoolReuseFollowEventDependencies : constant cudaMemPoolAttr := 1;
   cudaMemPoolReuseAllowOpportunistic : constant cudaMemPoolAttr := 2;
   cudaMemPoolReuseAllowInternalDependencies : constant cudaMemPoolAttr := 3;
   cudaMemPoolAttrReleaseThreshold : constant cudaMemPoolAttr := 4;
   cudaMemPoolAttrReservedMemCurrent : constant cudaMemPoolAttr := 5;
   cudaMemPoolAttrReservedMemHigh : constant cudaMemPoolAttr := 6;
   cudaMemPoolAttrUsedMemCurrent : constant cudaMemPoolAttr := 7;
   cudaMemPoolAttrUsedMemHigh : constant cudaMemPoolAttr := 8;  -- /usr/local/cuda-11.5/include//driver_types.h:1970

   type cudaMemLocationType is 
     (cudaMemLocationTypeInvalid,
      cudaMemLocationTypeDevice);
   pragma Convention (C, cudaMemLocationType);  -- /usr/local/cuda-11.5/include//driver_types.h:2038

   type cudaMemLocation is record
      c_type : aliased cudaMemLocationType;  -- /usr/local/cuda-11.5/include//driver_types.h:2049
      id : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2050
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemLocation);  -- /usr/local/cuda-11.5/include//driver_types.h:2048

   subtype cudaMemAccessFlags is unsigned;
   cudaMemAccessFlagsProtNone : constant cudaMemAccessFlags := 0;
   cudaMemAccessFlagsProtRead : constant cudaMemAccessFlags := 1;
   cudaMemAccessFlagsProtReadWrite : constant cudaMemAccessFlags := 3;  -- /usr/local/cuda-11.5/include//driver_types.h:2056

   type cudaMemAccessDesc is record
      location : aliased cudaMemLocation;  -- /usr/local/cuda-11.5/include//driver_types.h:2066
      flags : aliased cudaMemAccessFlags;  -- /usr/local/cuda-11.5/include//driver_types.h:2067
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemAccessDesc);  -- /usr/local/cuda-11.5/include//driver_types.h:2065

   subtype cudaMemAllocationType is unsigned;
   cudaMemAllocationTypeInvalid : constant cudaMemAllocationType := 0;
   cudaMemAllocationTypePinned : constant cudaMemAllocationType := 1;
   cudaMemAllocationTypeMax : constant cudaMemAllocationType := 2147483647;  -- /usr/local/cuda-11.5/include//driver_types.h:2073

   subtype cudaMemAllocationHandleType is unsigned;
   cudaMemHandleTypeNone : constant cudaMemAllocationHandleType := 0;
   cudaMemHandleTypePosixFileDescriptor : constant cudaMemAllocationHandleType := 1;
   cudaMemHandleTypeWin32 : constant cudaMemAllocationHandleType := 2;
   cudaMemHandleTypeWin32Kmt : constant cudaMemAllocationHandleType := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:2085

   type cudaMemPoolProps_reserved_array is array (0 .. 63) of aliased unsigned_char;
   type cudaMemPoolProps is record
      allocType : aliased cudaMemAllocationType;  -- /usr/local/cuda-11.5/include//driver_types.h:2096
      handleTypes : aliased cudaMemAllocationHandleType;  -- /usr/local/cuda-11.5/include//driver_types.h:2097
      location : aliased cudaMemLocation;  -- /usr/local/cuda-11.5/include//driver_types.h:2098
      win32SecurityAttributes : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2105
      reserved : aliased cudaMemPoolProps_reserved_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2106
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemPoolProps);  -- /usr/local/cuda-11.5/include//driver_types.h:2095

   type cudaMemPoolPtrExportData_reserved_array is array (0 .. 63) of aliased unsigned_char;
   type cudaMemPoolPtrExportData is record
      reserved : aliased cudaMemPoolPtrExportData_reserved_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2113
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemPoolPtrExportData);  -- /usr/local/cuda-11.5/include//driver_types.h:2112

   type cudaMemAllocNodeParams is record
      poolProps : aliased cudaMemPoolProps;  -- /usr/local/cuda-11.5/include//driver_types.h:2124
      accessDescs : access constant cudaMemAccessDesc;  -- /usr/local/cuda-11.5/include//driver_types.h:2125
      accessDescCount : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2126
      bytesize : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2127
      dptr : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2128
   end record;
   pragma Convention (C_Pass_By_Copy, cudaMemAllocNodeParams);  -- /usr/local/cuda-11.5/include//driver_types.h:2119

   subtype cudaGraphMemAttributeType is unsigned;
   cudaGraphMemAttrUsedMemCurrent : constant cudaGraphMemAttributeType := 1;
   cudaGraphMemAttrUsedMemHigh : constant cudaGraphMemAttributeType := 2;
   cudaGraphMemAttrReservedMemCurrent : constant cudaGraphMemAttributeType := 3;
   cudaGraphMemAttrReservedMemHigh : constant cudaGraphMemAttributeType := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:2134

   subtype cudaDeviceP2PAttr is unsigned;
   cudaDevP2PAttrPerformanceRank : constant cudaDeviceP2PAttr := 1;
   cudaDevP2PAttrAccessSupported : constant cudaDeviceP2PAttr := 2;
   cudaDevP2PAttrNativeAtomicSupported : constant cudaDeviceP2PAttr := 3;
   cudaDevP2PAttrCudaArrayAccessSupported : constant cudaDeviceP2PAttr := 4;  -- /usr/local/cuda-11.5/include//driver_types.h:2167

   subtype CUuuid_st_bytes_array is Interfaces.C.char_array (0 .. 15);
   type CUuuid_st is record
      bytes : aliased CUuuid_st_bytes_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2180
   end record;
   pragma Convention (C_Pass_By_Copy, CUuuid_st);  -- /usr/local/cuda-11.5/include//driver_types.h:2179

   subtype CUuuid is CUuuid_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2182

   subtype cudaUUID_t is CUuuid_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2184

   subtype cudaDeviceProp_name_array is Interfaces.C.char_array (0 .. 255);
   subtype cudaDeviceProp_luid_array is Interfaces.C.char_array (0 .. 7);
   type cudaDeviceProp_maxThreadsDim_array is array (0 .. 2) of aliased int;
   type cudaDeviceProp_maxGridSize_array is array (0 .. 2) of aliased int;
   type cudaDeviceProp_maxTexture2D_array is array (0 .. 1) of aliased int;
   type cudaDeviceProp_maxTexture2DMipmap_array is array (0 .. 1) of aliased int;
   type cudaDeviceProp_maxTexture2DLinear_array is array (0 .. 2) of aliased int;
   type cudaDeviceProp_maxTexture2DGather_array is array (0 .. 1) of aliased int;
   type cudaDeviceProp_maxTexture3D_array is array (0 .. 2) of aliased int;
   type cudaDeviceProp_maxTexture3DAlt_array is array (0 .. 2) of aliased int;
   type cudaDeviceProp_maxTexture1DLayered_array is array (0 .. 1) of aliased int;
   type cudaDeviceProp_maxTexture2DLayered_array is array (0 .. 2) of aliased int;
   type cudaDeviceProp_maxTextureCubemapLayered_array is array (0 .. 1) of aliased int;
   type cudaDeviceProp_maxSurface2D_array is array (0 .. 1) of aliased int;
   type cudaDeviceProp_maxSurface3D_array is array (0 .. 2) of aliased int;
   type cudaDeviceProp_maxSurface1DLayered_array is array (0 .. 1) of aliased int;
   type cudaDeviceProp_maxSurface2DLayered_array is array (0 .. 2) of aliased int;
   type cudaDeviceProp_maxSurfaceCubemapLayered_array is array (0 .. 1) of aliased int;
   type cudaDeviceProp is record
      name : aliased cudaDeviceProp_name_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2191
      uuid : aliased cudaUUID_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2192
      luid : aliased cudaDeviceProp_luid_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2193
      luidDeviceNodeMask : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2194
      totalGlobalMem : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2195
      sharedMemPerBlock : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2196
      regsPerBlock : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2197
      warpSize : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2198
      memPitch : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2199
      maxThreadsPerBlock : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2200
      maxThreadsDim : aliased cudaDeviceProp_maxThreadsDim_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2201
      maxGridSize : aliased cudaDeviceProp_maxGridSize_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2202
      clockRate : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2203
      totalConstMem : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2204
      major : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2205
      minor : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2206
      textureAlignment : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2207
      texturePitchAlignment : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2208
      deviceOverlap : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2209
      multiProcessorCount : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2210
      kernelExecTimeoutEnabled : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2211
      integrated : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2212
      canMapHostMemory : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2213
      computeMode : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2214
      maxTexture1D : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2215
      maxTexture1DMipmap : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2216
      maxTexture1DLinear : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2217
      maxTexture2D : aliased cudaDeviceProp_maxTexture2D_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2218
      maxTexture2DMipmap : aliased cudaDeviceProp_maxTexture2DMipmap_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2219
      maxTexture2DLinear : aliased cudaDeviceProp_maxTexture2DLinear_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2220
      maxTexture2DGather : aliased cudaDeviceProp_maxTexture2DGather_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2221
      maxTexture3D : aliased cudaDeviceProp_maxTexture3D_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2222
      maxTexture3DAlt : aliased cudaDeviceProp_maxTexture3DAlt_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2223
      maxTextureCubemap : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2224
      maxTexture1DLayered : aliased cudaDeviceProp_maxTexture1DLayered_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2225
      maxTexture2DLayered : aliased cudaDeviceProp_maxTexture2DLayered_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2226
      maxTextureCubemapLayered : aliased cudaDeviceProp_maxTextureCubemapLayered_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2227
      maxSurface1D : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2228
      maxSurface2D : aliased cudaDeviceProp_maxSurface2D_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2229
      maxSurface3D : aliased cudaDeviceProp_maxSurface3D_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2230
      maxSurface1DLayered : aliased cudaDeviceProp_maxSurface1DLayered_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2231
      maxSurface2DLayered : aliased cudaDeviceProp_maxSurface2DLayered_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2232
      maxSurfaceCubemap : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2233
      maxSurfaceCubemapLayered : aliased cudaDeviceProp_maxSurfaceCubemapLayered_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2234
      surfaceAlignment : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2235
      concurrentKernels : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2236
      ECCEnabled : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2237
      pciBusID : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2238
      pciDeviceID : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2239
      pciDomainID : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2240
      tccDriver : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2241
      asyncEngineCount : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2242
      unifiedAddressing : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2243
      memoryClockRate : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2244
      memoryBusWidth : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2245
      l2CacheSize : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2246
      persistingL2CacheMaxSize : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2247
      maxThreadsPerMultiProcessor : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2248
      streamPrioritiesSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2249
      globalL1CacheSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2250
      localL1CacheSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2251
      sharedMemPerMultiprocessor : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2252
      regsPerMultiprocessor : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2253
      managedMemory : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2254
      isMultiGpuBoard : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2255
      multiGpuBoardGroupID : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2256
      hostNativeAtomicSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2257
      singleToDoublePrecisionPerfRatio : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2258
      pageableMemoryAccess : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2259
      concurrentManagedAccess : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2260
      computePreemptionSupported : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2261
      canUseHostPointerForRegisteredMem : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2262
      cooperativeLaunch : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2263
      cooperativeMultiDeviceLaunch : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2264
      sharedMemPerBlockOptin : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2265
      pageableMemoryAccessUsesHostPageTables : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2266
      directManagedMemAccessFromHost : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2267
      maxBlocksPerMultiProcessor : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2268
      accessPolicyMaxWindowSize : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2269
      reservedSharedMemPerBlock : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2270
   end record;
   pragma Convention (C_Pass_By_Copy, cudaDeviceProp);  -- /usr/local/cuda-11.5/include//driver_types.h:2189

   subtype cudaIpcEventHandle_st_reserved_array is Interfaces.C.char_array (0 .. 63);
   type cudaIpcEventHandle_st is record
      reserved : aliased cudaIpcEventHandle_st_reserved_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2374
   end record;
   pragma Convention (C_Pass_By_Copy, cudaIpcEventHandle_st);  -- /usr/local/cuda-11.5/include//driver_types.h:2372

   subtype cudaIpcEventHandle_t is cudaIpcEventHandle_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2375

   subtype cudaIpcMemHandle_st_reserved_array is Interfaces.C.char_array (0 .. 63);
   type cudaIpcMemHandle_st is record
      reserved : aliased cudaIpcMemHandle_st_reserved_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2382
   end record;
   pragma Convention (C_Pass_By_Copy, cudaIpcMemHandle_st);  -- /usr/local/cuda-11.5/include//driver_types.h:2380

   subtype cudaIpcMemHandle_t is cudaIpcMemHandle_st;  -- /usr/local/cuda-11.5/include//driver_types.h:2383

   subtype cudaExternalMemoryHandleType is unsigned;
   cudaExternalMemoryHandleTypeOpaqueFd : constant cudaExternalMemoryHandleType := 1;
   cudaExternalMemoryHandleTypeOpaqueWin32 : constant cudaExternalMemoryHandleType := 2;
   cudaExternalMemoryHandleTypeOpaqueWin32Kmt : constant cudaExternalMemoryHandleType := 3;
   cudaExternalMemoryHandleTypeD3D12Heap : constant cudaExternalMemoryHandleType := 4;
   cudaExternalMemoryHandleTypeD3D12Resource : constant cudaExternalMemoryHandleType := 5;
   cudaExternalMemoryHandleTypeD3D11Resource : constant cudaExternalMemoryHandleType := 6;
   cudaExternalMemoryHandleTypeD3D11ResourceKmt : constant cudaExternalMemoryHandleType := 7;
   cudaExternalMemoryHandleTypeNvSciBuf : constant cudaExternalMemoryHandleType := 8;  -- /usr/local/cuda-11.5/include//driver_types.h:2388

   type cudaExternalMemoryHandleDesc;
   type anon_7;
   type anon_8 is record
      handle : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2494
      name : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2499
   end record;
   pragma Convention (C_Pass_By_Copy, anon_8);
   type anon_7 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fd : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2474
         when 1 =>
            win32 : aliased anon_8;  -- /usr/local/cuda-11.5/include//driver_types.h:2500
         when others =>
            nvSciBufObject : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2505
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, anon_7);
   pragma Unchecked_Union (anon_7);type cudaExternalMemoryHandleDesc is record
      c_type : aliased cudaExternalMemoryHandleType;  -- /usr/local/cuda-11.5/include//driver_types.h:2467
      handle : aliased anon_7;  -- /usr/local/cuda-11.5/include//driver_types.h:2506
      size : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2510
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2514
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalMemoryHandleDesc);  -- /usr/local/cuda-11.5/include//driver_types.h:2463

   type cudaExternalMemoryBufferDesc is record
      offset : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2524
      size : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2528
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2532
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalMemoryBufferDesc);  -- /usr/local/cuda-11.5/include//driver_types.h:2520

   type cudaExternalMemoryMipmappedArrayDesc is record
      offset : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2543
      formatDesc : aliased cudaChannelFormatDesc;  -- /usr/local/cuda-11.5/include//driver_types.h:2547
      extent : aliased cudaExtent;  -- /usr/local/cuda-11.5/include//driver_types.h:2551
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2556
      numLevels : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2560
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalMemoryMipmappedArrayDesc);  -- /usr/local/cuda-11.5/include//driver_types.h:2538

   subtype cudaExternalSemaphoreHandleType is unsigned;
   cudaExternalSemaphoreHandleTypeOpaqueFd : constant cudaExternalSemaphoreHandleType := 1;
   cudaExternalSemaphoreHandleTypeOpaqueWin32 : constant cudaExternalSemaphoreHandleType := 2;
   cudaExternalSemaphoreHandleTypeOpaqueWin32Kmt : constant cudaExternalSemaphoreHandleType := 3;
   cudaExternalSemaphoreHandleTypeD3D12Fence : constant cudaExternalSemaphoreHandleType := 4;
   cudaExternalSemaphoreHandleTypeD3D11Fence : constant cudaExternalSemaphoreHandleType := 5;
   cudaExternalSemaphoreHandleTypeNvSciSync : constant cudaExternalSemaphoreHandleType := 6;
   cudaExternalSemaphoreHandleTypeKeyedMutex : constant cudaExternalSemaphoreHandleType := 7;
   cudaExternalSemaphoreHandleTypeKeyedMutexKmt : constant cudaExternalSemaphoreHandleType := 8;
   cudaExternalSemaphoreHandleTypeTimelineSemaphoreFd : constant cudaExternalSemaphoreHandleType := 9;
   cudaExternalSemaphoreHandleTypeTimelineSemaphoreWin32 : constant cudaExternalSemaphoreHandleType := 10;  -- /usr/local/cuda-11.5/include//driver_types.h:2566

   type cudaExternalSemaphoreHandleDesc;
   type anon_9;
   type anon_10 is record
      handle : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2644
      name : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2649
   end record;
   pragma Convention (C_Pass_By_Copy, anon_10);
   type anon_9 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fd : aliased int;  -- /usr/local/cuda-11.5/include//driver_types.h:2624
         when 1 =>
            win32 : aliased anon_10;  -- /usr/local/cuda-11.5/include//driver_types.h:2650
         when others =>
            nvSciSyncObj : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2654
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, anon_9);
   pragma Unchecked_Union (anon_9);type cudaExternalSemaphoreHandleDesc is record
      c_type : aliased cudaExternalSemaphoreHandleType;  -- /usr/local/cuda-11.5/include//driver_types.h:2616
      handle : aliased anon_9;  -- /usr/local/cuda-11.5/include//driver_types.h:2655
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2659
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalSemaphoreHandleDesc);  -- /usr/local/cuda-11.5/include//driver_types.h:2612

   type cudaExternalSemaphoreSignalParams_v1;
   type anon_11;
   type anon_12 is record
      value : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2674
   end record;
   pragma Convention (C_Pass_By_Copy, anon_12);
   type anon_13 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fence : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2681
         when others =>
            reserved : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2682
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, anon_13);
   pragma Unchecked_Union (anon_13);type anon_14 is record
      key : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2691
   end record;
   pragma Convention (C_Pass_By_Copy, anon_14);
   type anon_11 is record
      fence : aliased anon_12;  -- /usr/local/cuda-11.5/include//driver_types.h:2675
      nvSciSync : aliased anon_13;  -- /usr/local/cuda-11.5/include//driver_types.h:2683
      keyedMutex : aliased anon_14;  -- /usr/local/cuda-11.5/include//driver_types.h:2692
   end record;
   pragma Convention (C_Pass_By_Copy, anon_11);
   type cudaExternalSemaphoreSignalParams_v1 is record
      params : aliased anon_11;  -- /usr/local/cuda-11.5/include//driver_types.h:2693
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2704
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalSemaphoreSignalParams_v1);  -- /usr/local/cuda-11.5/include//driver_types.h:2665

   type cudaExternalSemaphoreWaitParams_v1;
   type anon_15;
   type anon_16 is record
      value : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2719
   end record;
   pragma Convention (C_Pass_By_Copy, anon_16);
   type anon_17 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fence : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2726
         when others =>
            reserved : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2727
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, anon_17);
   pragma Unchecked_Union (anon_17);type anon_18 is record
      key : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2736
      timeoutMs : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2740
   end record;
   pragma Convention (C_Pass_By_Copy, anon_18);
   type anon_15 is record
      fence : aliased anon_16;  -- /usr/local/cuda-11.5/include//driver_types.h:2720
      nvSciSync : aliased anon_17;  -- /usr/local/cuda-11.5/include//driver_types.h:2728
      keyedMutex : aliased anon_18;  -- /usr/local/cuda-11.5/include//driver_types.h:2741
   end record;
   pragma Convention (C_Pass_By_Copy, anon_15);
   type cudaExternalSemaphoreWaitParams_v1 is record
      params : aliased anon_15;  -- /usr/local/cuda-11.5/include//driver_types.h:2742
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2753
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalSemaphoreWaitParams_v1);  -- /usr/local/cuda-11.5/include//driver_types.h:2710

   type cudaExternalSemaphoreSignalParams;
   type anon_19;
   type anon_20 is record
      value : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2768
   end record;
   pragma Convention (C_Pass_By_Copy, anon_20);
   type anon_21 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fence : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2775
         when others =>
            reserved : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2776
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, anon_21);
   pragma Unchecked_Union (anon_21);type anon_22 is record
      key : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2785
   end record;
   pragma Convention (C_Pass_By_Copy, anon_22);
   type cudaExternalSemaphoreSignalParams_reserved_array is array (0 .. 11) of aliased unsigned;
   type anon_19 is record
      fence : aliased anon_20;  -- /usr/local/cuda-11.5/include//driver_types.h:2769
      nvSciSync : aliased anon_21;  -- /usr/local/cuda-11.5/include//driver_types.h:2777
      keyedMutex : aliased anon_22;  -- /usr/local/cuda-11.5/include//driver_types.h:2786
      reserved : aliased cudaExternalSemaphoreSignalParams_reserved_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2787
   end record;
   pragma Convention (C_Pass_By_Copy, anon_19);
   type cudaExternalSemaphoreSignalParams_reserved_array is array (0 .. 15) of aliased unsigned;
   type cudaExternalSemaphoreSignalParams is record
      params : aliased anon_19;  -- /usr/local/cuda-11.5/include//driver_types.h:2788
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2799
      reserved : aliased cudaExternalSemaphoreSignalParams_reserved_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2800
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalSemaphoreSignalParams);  -- /usr/local/cuda-11.5/include//driver_types.h:2759

   type cudaExternalSemaphoreWaitParams;
   type anon_23;
   type anon_24 is record
      value : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2815
   end record;
   pragma Convention (C_Pass_By_Copy, anon_24);
   type anon_25 (discr : unsigned := 0) is record
      case discr is
         when 0 =>
            fence : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2822
         when others =>
            reserved : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2823
      end case;
   end record;
   pragma Convention (C_Pass_By_Copy, anon_25);
   pragma Unchecked_Union (anon_25);type anon_26 is record
      key : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//driver_types.h:2832
      timeoutMs : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2836
   end record;
   pragma Convention (C_Pass_By_Copy, anon_26);
   type cudaExternalSemaphoreWaitParams_reserved_array is array (0 .. 9) of aliased unsigned;
   type anon_23 is record
      fence : aliased anon_24;  -- /usr/local/cuda-11.5/include//driver_types.h:2816
      nvSciSync : aliased anon_25;  -- /usr/local/cuda-11.5/include//driver_types.h:2824
      keyedMutex : aliased anon_26;  -- /usr/local/cuda-11.5/include//driver_types.h:2837
      reserved : aliased cudaExternalSemaphoreWaitParams_reserved_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2838
   end record;
   pragma Convention (C_Pass_By_Copy, anon_23);
   type cudaExternalSemaphoreWaitParams_reserved_array is array (0 .. 15) of aliased unsigned;
   type cudaExternalSemaphoreWaitParams is record
      params : aliased anon_23;  -- /usr/local/cuda-11.5/include//driver_types.h:2839
      flags : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2850
      reserved : aliased cudaExternalSemaphoreWaitParams_reserved_array;  -- /usr/local/cuda-11.5/include//driver_types.h:2851
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalSemaphoreWaitParams);  -- /usr/local/cuda-11.5/include//driver_types.h:2806

   subtype cudaError_t is cudaError;  -- /usr/local/cuda-11.5/include//driver_types.h:2864

   --  skipped empty struct CUstream_st

   type cudaStream_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2869

   --  skipped empty struct CUevent_st

   type cudaEvent_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2874

   type cudaGraphicsResource_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2879

   subtype cudaOutputMode_t is cudaOutputMode;  -- /usr/local/cuda-11.5/include//driver_types.h:2884

   --  skipped empty struct CUexternalMemory_st

   type cudaExternalMemory_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2889

   --  skipped empty struct CUexternalSemaphore_st

   type cudaExternalSemaphore_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2894

   --  skipped empty struct CUgraph_st

   type cudaGraph_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2899

   --  skipped empty struct CUgraphNode_st

   type cudaGraphNode_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2904

   --  skipped empty struct CUuserObject_st

   type cudaUserObject_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2909

   --  skipped empty struct CUfunc_st

   type cudaFunction_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2914

   --  skipped empty struct CUmemPoolHandle_st

   type cudaMemPool_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2919

   type cudaCGScope is 
     (cudaCGScopeInvalid,
      cudaCGScopeGrid,
      cudaCGScopeMultiGrid);
   pragma Convention (C, cudaCGScope);  -- /usr/local/cuda-11.5/include//driver_types.h:2924

   package Class_cudaLaunchParams is
      type cudaLaunchParams is limited record
         func : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2935
         gridDim : aliased uvector_types_h.Class_dim3.dim3;  -- /usr/local/cuda-11.5/include//driver_types.h:2936
         blockDim : aliased uvector_types_h.Class_dim3.dim3;  -- /usr/local/cuda-11.5/include//driver_types.h:2937
         args : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2938
         sharedMem : aliased stddef_h.size_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2939
         stream : cudaStream_t;  -- /usr/local/cuda-11.5/include//driver_types.h:2940
      end record;
      pragma Import (CPP, cudaLaunchParams);
   end;
   use Class_cudaLaunchParams;
   package Class_cudaKernelNodeParams is
      type cudaKernelNodeParams is limited record
         func : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2947
         gridDim : aliased uvector_types_h.Class_dim3.dim3;  -- /usr/local/cuda-11.5/include//driver_types.h:2948
         blockDim : aliased uvector_types_h.Class_dim3.dim3;  -- /usr/local/cuda-11.5/include//driver_types.h:2949
         sharedMemBytes : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2950
         kernelParams : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2951
         extra : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2952
      end record;
      pragma Import (CPP, cudaKernelNodeParams);
   end;
   use Class_cudaKernelNodeParams;
   type cudaExternalSemaphoreSignalNodeParams is record
      extSemArray : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2959
      paramsArray : access constant cudaExternalSemaphoreSignalParams;  -- /usr/local/cuda-11.5/include//driver_types.h:2960
      numExtSems : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2961
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalSemaphoreSignalNodeParams);  -- /usr/local/cuda-11.5/include//driver_types.h:2958

   type cudaExternalSemaphoreWaitNodeParams is record
      extSemArray : System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2968
      paramsArray : access constant cudaExternalSemaphoreWaitParams;  -- /usr/local/cuda-11.5/include//driver_types.h:2969
      numExtSems : aliased unsigned;  -- /usr/local/cuda-11.5/include//driver_types.h:2970
   end record;
   pragma Convention (C_Pass_By_Copy, cudaExternalSemaphoreWaitNodeParams);  -- /usr/local/cuda-11.5/include//driver_types.h:2967

   type cudaGraphNodeType is 
     (cudaGraphNodeTypeKernel,
      cudaGraphNodeTypeMemcpy,
      cudaGraphNodeTypeMemset,
      cudaGraphNodeTypeHost,
      cudaGraphNodeTypeGraph,
      cudaGraphNodeTypeEmpty,
      cudaGraphNodeTypeWaitEvent,
      cudaGraphNodeTypeEventRecord,
      cudaGraphNodeTypeExtSemaphoreSignal,
      cudaGraphNodeTypeExtSemaphoreWait,
      cudaGraphNodeTypeMemAlloc,
      cudaGraphNodeTypeMemFree,
      cudaGraphNodeTypeCount);
   pragma Convention (C, cudaGraphNodeType);  -- /usr/local/cuda-11.5/include//driver_types.h:2976

   --  skipped empty struct CUgraphExec_st

   type cudaGraphExec_t is new System.Address;  -- /usr/local/cuda-11.5/include//driver_types.h:2995

   type cudaGraphExecUpdateResult is 
     (cudaGraphExecUpdateSuccess,
      cudaGraphExecUpdateError,
      cudaGraphExecUpdateErrorTopologyChanged,
      cudaGraphExecUpdateErrorNodeTypeChanged,
      cudaGraphExecUpdateErrorFunctionChanged,
      cudaGraphExecUpdateErrorParametersChanged,
      cudaGraphExecUpdateErrorNotSupported,
      cudaGraphExecUpdateErrorUnsupportedFunctionChange);
   pragma Convention (C, cudaGraphExecUpdateResult);  -- /usr/local/cuda-11.5/include//driver_types.h:3000

   type cudaGetDriverEntryPointFlags is 
     (cudaEnableDefault,
      cudaEnableLegacyStream,
      cudaEnablePerThreadDefaultStream);
   pragma Convention (C, cudaGetDriverEntryPointFlags);  -- /usr/local/cuda-11.5/include//driver_types.h:3015

   subtype cudaGraphDebugDotFlags is unsigned;
   cudaGraphDebugDotFlagsVerbose : constant cudaGraphDebugDotFlags := 1;
   cudaGraphDebugDotFlagsKernelNodeParams : constant cudaGraphDebugDotFlags := 4;
   cudaGraphDebugDotFlagsMemcpyNodeParams : constant cudaGraphDebugDotFlags := 8;
   cudaGraphDebugDotFlagsMemsetNodeParams : constant cudaGraphDebugDotFlags := 16;
   cudaGraphDebugDotFlagsHostNodeParams : constant cudaGraphDebugDotFlags := 32;
   cudaGraphDebugDotFlagsEventNodeParams : constant cudaGraphDebugDotFlags := 64;
   cudaGraphDebugDotFlagsExtSemasSignalNodeParams : constant cudaGraphDebugDotFlags := 128;
   cudaGraphDebugDotFlagsExtSemasWaitNodeParams : constant cudaGraphDebugDotFlags := 256;
   cudaGraphDebugDotFlagsKernelNodeAttributes : constant cudaGraphDebugDotFlags := 512;
   cudaGraphDebugDotFlagsHandles : constant cudaGraphDebugDotFlags := 1024;  -- /usr/local/cuda-11.5/include//driver_types.h:3024

   subtype cudaGraphInstantiateFlags is unsigned;
   cudaGraphInstantiateFlagAutoFreeOnLaunch : constant cudaGraphInstantiateFlags := 1;  -- /usr/local/cuda-11.5/include//driver_types.h:3040

end udriver_types_h;
