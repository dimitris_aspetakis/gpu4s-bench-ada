pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions;

package uvector_types_h is

   type char1 is record
      x : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:102
   end record;
   pragma Convention (C_Pass_By_Copy, char1);  -- /usr/local/cuda-11.5/include//vector_types.h:100

   type uchar1 is record
      x : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:107
   end record;
   pragma Convention (C_Pass_By_Copy, uchar1);  -- /usr/local/cuda-11.5/include//vector_types.h:105

   type char2 is record
      x : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:113
      y : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:113
   end record;
   pragma Convention (C_Pass_By_Copy, char2);  -- /usr/local/cuda-11.5/include//vector_types.h:111

   type uchar2 is record
      x : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:118
      y : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:118
   end record;
   pragma Convention (C_Pass_By_Copy, uchar2);  -- /usr/local/cuda-11.5/include//vector_types.h:116

   type char3 is record
      x : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:123
      y : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:123
      z : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:123
   end record;
   pragma Convention (C_Pass_By_Copy, char3);  -- /usr/local/cuda-11.5/include//vector_types.h:121

   type uchar3 is record
      x : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:128
      y : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:128
      z : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:128
   end record;
   pragma Convention (C_Pass_By_Copy, uchar3);  -- /usr/local/cuda-11.5/include//vector_types.h:126

   type char4 is record
      x : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:133
      y : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:133
      z : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:133
      w : aliased signed_char;  -- /usr/local/cuda-11.5/include//vector_types.h:133
   end record;
   pragma Convention (C_Pass_By_Copy, char4);  -- /usr/local/cuda-11.5/include//vector_types.h:131

   type uchar4 is record
      x : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:138
      y : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:138
      z : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:138
      w : aliased unsigned_char;  -- /usr/local/cuda-11.5/include//vector_types.h:138
   end record;
   pragma Convention (C_Pass_By_Copy, uchar4);  -- /usr/local/cuda-11.5/include//vector_types.h:136

   type short1 is record
      x : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:143
   end record;
   pragma Convention (C_Pass_By_Copy, short1);  -- /usr/local/cuda-11.5/include//vector_types.h:141

   type ushort1 is record
      x : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:148
   end record;
   pragma Convention (C_Pass_By_Copy, ushort1);  -- /usr/local/cuda-11.5/include//vector_types.h:146

   type short2 is record
      x : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:153
      y : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:153
   end record;
   pragma Convention (C_Pass_By_Copy, short2);  -- /usr/local/cuda-11.5/include//vector_types.h:151

   type ushort2 is record
      x : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:158
      y : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:158
   end record;
   pragma Convention (C_Pass_By_Copy, ushort2);  -- /usr/local/cuda-11.5/include//vector_types.h:156

   type short3 is record
      x : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:163
      y : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:163
      z : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:163
   end record;
   pragma Convention (C_Pass_By_Copy, short3);  -- /usr/local/cuda-11.5/include//vector_types.h:161

   type ushort3 is record
      x : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:168
      y : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:168
      z : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:168
   end record;
   pragma Convention (C_Pass_By_Copy, ushort3);  -- /usr/local/cuda-11.5/include//vector_types.h:166

   type short4 is record
      x : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:171
      y : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:171
      z : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:171
      w : aliased short;  -- /usr/local/cuda-11.5/include//vector_types.h:171
   end record;
   pragma Convention (C_Pass_By_Copy, short4);  -- /usr/local/cuda-11.5/include//vector_types.h:171

   type ushort4 is record
      x : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:172
      y : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:172
      z : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:172
      w : aliased unsigned_short;  -- /usr/local/cuda-11.5/include//vector_types.h:172
   end record;
   pragma Convention (C_Pass_By_Copy, ushort4);  -- /usr/local/cuda-11.5/include//vector_types.h:172

   type int1 is record
      x : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:176
   end record;
   pragma Convention (C_Pass_By_Copy, int1);  -- /usr/local/cuda-11.5/include//vector_types.h:174

   type uint1 is record
      x : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:181
   end record;
   pragma Convention (C_Pass_By_Copy, uint1);  -- /usr/local/cuda-11.5/include//vector_types.h:179

   type int2 is record
      x : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:184
      y : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:184
   end record;
   pragma Convention (C_Pass_By_Copy, int2);  -- /usr/local/cuda-11.5/include//vector_types.h:184

   type uint2 is record
      x : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:185
      y : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:185
   end record;
   pragma Convention (C_Pass_By_Copy, uint2);  -- /usr/local/cuda-11.5/include//vector_types.h:185

   type int3 is record
      x : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:189
      y : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:189
      z : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:189
   end record;
   pragma Convention (C_Pass_By_Copy, int3);  -- /usr/local/cuda-11.5/include//vector_types.h:187

   type uint3 is record
      x : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:194
      y : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:194
      z : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:194
   end record;
   pragma Convention (C_Pass_By_Copy, uint3);  -- /usr/local/cuda-11.5/include//vector_types.h:192

   type int4 is record
      x : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:199
      y : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:199
      z : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:199
      w : aliased int;  -- /usr/local/cuda-11.5/include//vector_types.h:199
   end record;
   pragma Convention (C_Pass_By_Copy, int4);  -- /usr/local/cuda-11.5/include//vector_types.h:197

   type uint4 is record
      x : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:204
      y : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:204
      z : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:204
      w : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:204
   end record;
   pragma Convention (C_Pass_By_Copy, uint4);  -- /usr/local/cuda-11.5/include//vector_types.h:202

   type long1 is record
      x : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:209
   end record;
   pragma Convention (C_Pass_By_Copy, long1);  -- /usr/local/cuda-11.5/include//vector_types.h:207

   type ulong1 is record
      x : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:214
   end record;
   pragma Convention (C_Pass_By_Copy, ulong1);  -- /usr/local/cuda-11.5/include//vector_types.h:212

   type long2 is record
      x : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:224
      y : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:224
   end record;
   pragma Convention (C_Pass_By_Copy, long2);  -- /usr/local/cuda-11.5/include//vector_types.h:222

   type ulong2 is record
      x : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:229
      y : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:229
   end record;
   pragma Convention (C_Pass_By_Copy, ulong2);  -- /usr/local/cuda-11.5/include//vector_types.h:227

   type long3 is record
      x : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:236
      y : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:236
      z : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:236
   end record;
   pragma Convention (C_Pass_By_Copy, long3);  -- /usr/local/cuda-11.5/include//vector_types.h:234

   type ulong3 is record
      x : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:241
      y : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:241
      z : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:241
   end record;
   pragma Convention (C_Pass_By_Copy, ulong3);  -- /usr/local/cuda-11.5/include//vector_types.h:239

   type long4 is record
      x : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:246
      y : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:246
      z : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:246
      w : aliased long;  -- /usr/local/cuda-11.5/include//vector_types.h:246
   end record;
   pragma Convention (C_Pass_By_Copy, long4);  -- /usr/local/cuda-11.5/include//vector_types.h:244

   type ulong4 is record
      x : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:251
      y : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:251
      z : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:251
      w : aliased unsigned_long;  -- /usr/local/cuda-11.5/include//vector_types.h:251
   end record;
   pragma Convention (C_Pass_By_Copy, ulong4);  -- /usr/local/cuda-11.5/include//vector_types.h:249

   type float1 is record
      x : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:256
   end record;
   pragma Convention (C_Pass_By_Copy, float1);  -- /usr/local/cuda-11.5/include//vector_types.h:254

   type float2 is record
      x : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:276
      y : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:276
   end record;
   pragma Convention (C_Pass_By_Copy, float2);  -- /usr/local/cuda-11.5/include//vector_types.h:276

   type float3 is record
      x : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:283
      y : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:283
      z : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:283
   end record;
   pragma Convention (C_Pass_By_Copy, float3);  -- /usr/local/cuda-11.5/include//vector_types.h:281

   type float4 is record
      x : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:288
      y : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:288
      z : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:288
      w : aliased float;  -- /usr/local/cuda-11.5/include//vector_types.h:288
   end record;
   pragma Convention (C_Pass_By_Copy, float4);  -- /usr/local/cuda-11.5/include//vector_types.h:286

   type longlong1 is record
      x : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:293
   end record;
   pragma Convention (C_Pass_By_Copy, longlong1);  -- /usr/local/cuda-11.5/include//vector_types.h:291

   type ulonglong1 is record
      x : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:298
   end record;
   pragma Convention (C_Pass_By_Copy, ulonglong1);  -- /usr/local/cuda-11.5/include//vector_types.h:296

   type longlong2 is record
      x : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:303
      y : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:303
   end record;
   pragma Convention (C_Pass_By_Copy, longlong2);  -- /usr/local/cuda-11.5/include//vector_types.h:301

   type ulonglong2 is record
      x : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:308
      y : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:308
   end record;
   pragma Convention (C_Pass_By_Copy, ulonglong2);  -- /usr/local/cuda-11.5/include//vector_types.h:306

   type longlong3 is record
      x : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:313
      y : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:313
      z : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:313
   end record;
   pragma Convention (C_Pass_By_Copy, longlong3);  -- /usr/local/cuda-11.5/include//vector_types.h:311

   type ulonglong3 is record
      x : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:318
      y : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:318
      z : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:318
   end record;
   pragma Convention (C_Pass_By_Copy, ulonglong3);  -- /usr/local/cuda-11.5/include//vector_types.h:316

   type longlong4 is record
      x : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:323
      y : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:323
      z : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:323
      w : aliased Long_Long_Integer;  -- /usr/local/cuda-11.5/include//vector_types.h:323
   end record;
   pragma Convention (C_Pass_By_Copy, longlong4);  -- /usr/local/cuda-11.5/include//vector_types.h:321

   type ulonglong4 is record
      x : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:328
      y : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:328
      z : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:328
      w : aliased Extensions.unsigned_long_long;  -- /usr/local/cuda-11.5/include//vector_types.h:328
   end record;
   pragma Convention (C_Pass_By_Copy, ulonglong4);  -- /usr/local/cuda-11.5/include//vector_types.h:326

   type double1 is record
      x : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:333
   end record;
   pragma Convention (C_Pass_By_Copy, double1);  -- /usr/local/cuda-11.5/include//vector_types.h:331

   type double2 is record
      x : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:338
      y : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:338
   end record;
   pragma Convention (C_Pass_By_Copy, double2);  -- /usr/local/cuda-11.5/include//vector_types.h:336

   type double3 is record
      x : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:343
      y : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:343
      z : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:343
   end record;
   pragma Convention (C_Pass_By_Copy, double3);  -- /usr/local/cuda-11.5/include//vector_types.h:341

   type double4 is record
      x : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:348
      y : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:348
      z : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:348
      w : aliased double;  -- /usr/local/cuda-11.5/include//vector_types.h:348
   end record;
   pragma Convention (C_Pass_By_Copy, double4);  -- /usr/local/cuda-11.5/include//vector_types.h:346

   package Class_dim3 is
      type dim3 is limited record
         x : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:420
         y : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:420
         z : aliased unsigned;  -- /usr/local/cuda-11.5/include//vector_types.h:420
      end record;
      pragma Import (CPP, dim3);

      function New_dim3
        (vx : unsigned;
         vy : unsigned;
         vz : unsigned) return dim3;  -- /usr/local/cuda-11.5/include//vector_types.h:423
      pragma CPP_Constructor (New_dim3, "_ZN4dim3C1Ejjj");

      function New_dim3 (v : uint3) return dim3;  -- /usr/local/cuda-11.5/include//vector_types.h:424
      pragma CPP_Constructor (New_dim3, "_ZN4dim3C1E5uint3");

      function operator_1 (this : access constant dim3) return uint3;  -- /usr/local/cuda-11.5/include//vector_types.h:425
      pragma Import (CPP, operator_1, "_ZNK4dim3cv5uint3Ev");
   end;
   use Class_dim3;
end uvector_types_h;
