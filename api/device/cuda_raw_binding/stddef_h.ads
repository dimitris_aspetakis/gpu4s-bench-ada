pragma Ada_2005;
pragma Style_Checks (Off);

with Interfaces.C; use Interfaces.C;

package stddef_h is

   --  unsupported macro: NULL __null
   --  arg-macro: procedure offsetof (TYPE, MEMBER)
   --    __builtin_offsetof (TYPE, MEMBER)
   subtype ptrdiff_t is long;  -- /usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h:149

   subtype size_t is unsigned_long;  -- /usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h:216

   type max_align_t is record
      uu_max_align_ll : aliased Long_Long_Integer;  -- /usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h:427
      uu_max_align_ld : aliased long_double;  -- /usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h:428
   end record;
   pragma Convention (C_Pass_By_Copy, max_align_t);  -- /usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h:437

   --  skipped anonymous struct anon_0

   subtype nullptr_t is ;  -- /usr/lib/gcc/x86_64-linux-gnu/7/include/stddef.h:444

end stddef_h;
