with Text_IO;
with Ada.Numerics.Float_Random;
with Ada.Numerics.Discrete_Random;
with Ada.Numerics.Generic_Elementary_Functions;

package body Matrix_Utilities is

   procedure Correlation2D_Serial (A, B : Matrix; R : out Float) is

      package Float_Elementary_Functions is new Ada.Numerics
        .Generic_Elementary_Functions
        (Float);

      Mean_Matrix_A, Mean_Matrix_B             : Float;
      Result_Mean_A, Result_Mean_B             : Float;
      Accum_Val_AB, Accum_Val_AA, Accum_Val_BB : Float;

      function Mean_Matrix (A : Matrix) return Float is
         Sum : Float;
      begin

         Sum := 0.0;

         for I in A'Range (1) loop
            for J in A'Range (2) loop
               Sum := Sum + A (I, J);
            end loop;
         end loop;

         return Sum / Float (A'Length (1) * A'Length (2));

      end Mean_Matrix;

   begin

      Mean_Matrix_A := Mean_Matrix (A);
      Mean_Matrix_B := Mean_Matrix (B);

      for I in A'Range (1) loop
         for J in A'Range (2) loop
            Result_Mean_A := A (I, J) - Mean_Matrix_A;
            Result_Mean_B := B (I, J) - Mean_Matrix_B;
            Accum_Val_AB  := Result_Mean_A * Result_Mean_B;
            Accum_Val_AA  := Result_Mean_A * Result_Mean_A;
            Accum_Val_BB  := Result_Mean_B * Result_Mean_B;
         end loop;
      end loop;

      R :=
        Accum_Val_AB /
        Float_Elementary_Functions.Sqrt (Accum_Val_AA * Accum_Val_BB);

   end Correlation2D_Serial;

   procedure Generate_Matrix (M : out Matrix) is

      FG : Ada.Numerics.Float_Random.Generator;
      package Integer_Random is new Ada.Numerics.Discrete_Random (Integer);
      DG : Integer_Random.Generator;

   begin

      -- Initialize the random number generator
      Integer_Random.Reset (DG);
      Ada.Numerics.Float_Random.Reset (FG, Integer_Random.Random (DG));

      M :=
        (others =>
           (others =>
              2.0 * Value_Range * Ada.Numerics.Float_Random.Random (FG) -
              Value_Range));

   end Generate_Matrix;

   procedure Print_Matrix (M : Matrix) is
   begin

      for i in M'Range (1) loop
         for j in M'Range (2) loop
            Text_IO.Put (Float'Image (M (i, j)));
            Text_IO.Put (" ");
         end loop;
         Text_IO.New_Line;
      end loop;

   end Print_Matrix;

end Matrix_Utilities;
