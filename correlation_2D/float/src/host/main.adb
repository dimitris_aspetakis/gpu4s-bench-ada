with System;
with Interfaces.C; use Interfaces.C;

with Ada.Real_Time; use Ada.Real_Time;
with Ada.Text_IO;   use Ada.Text_IO;

with CUDA.Driver_Types;   use CUDA.Driver_Types;
with CUDA.Runtime_Api;    use CUDA.Runtime_Api;
with CUDA.Vector_Types;   use CUDA.Vector_Types;
with CUDA.Stddef;
with CUDA.Storage_Models; use CUDA.Storage_Models;

with Kernel;           use Kernel;
with Matrix_Utilities; use Matrix_Utilities;

with Ada.Unchecked_Deallocation;
with Ada.Numerics.Elementary_Functions; use Ada.Numerics.Elementary_Functions;

procedure Main is

   type Matrix_Host_Access is access all Matrix;

   procedure Free is new Ada.Unchecked_Deallocation
     (Matrix, Matrix_Host_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Matrix, Matrix_Device_Access);

   procedure Free is new Ada.Unchecked_Deallocation
     (Integer, Int_Device_Access);

   Matrix_Dimension : Integer := 256;

   H_A, H_B : Matrix_Host_Access;
   D_A, D_B : Matrix_Device_Access;

   R, R_Model : Float;

   Threads_Per_Block : Dim3 := (32, 32, 1);
   Blocks_Per_Grid   : Dim3 :=
     ((unsigned (Matrix_Dimension) + Threads_Per_Block.X - 1) /
      Threads_Per_Block.X,
      (unsigned (Matrix_Dimension) + Threads_Per_Block.Y - 1) /
      Threads_Per_Block.Y,
      1);

   Start_Time   : Time;
   Elapsed_Time : Time_Span;
   Err          : Error_T;

   procedure Correlation2D
     (A, B                               : Matrix_Device_Access; R : out Float;
      Blocks_Per_Grid, Threads_Per_Block : Dim3)
   is

      Mean_Matrix_A, Mean_Matrix_B : Int_Device_Access := new Integer;
      Accum_Val_AB, Accum_Val_AA, Accum_Val_BB : Int_Device_Access :=
        new Integer;

   begin

      Mean_Matrix_A.all := 0;
      Mean_Matrix_B.all := 0;
      pragma Cuda_Execute
        (Mean_Matrices (A, B, Mean_Matrix_A, Mean_Matrix_B), Blocks_Per_Grid,
         Threads_Per_Block);

      Mean_Matrix_A.all :=
        Integer
          (Float (Mean_Matrix_A.all) / Float (A'Length (1) * A'Length (2)));
      Mean_Matrix_B.all :=
        Integer
          (Float (Mean_Matrix_B.all) / Float (B'Length (1) * B'Length (2)));

      Accum_Val_AB.all := 0;
      Accum_Val_AA.all := 0;
      Accum_Val_BB.all := 0;

      pragma Cuda_Execute
        (Accum_Values
           (A, B, Mean_Matrix_A, Mean_Matrix_B, Accum_Val_AB, Accum_Val_AA,
            Accum_Val_BB),
         Blocks_Per_Grid, Threads_Per_Block);

      -- TODO: change Integers to Floats when Atomic_Add is supported for
      --       Floats.
      R :=
        Float (Accum_Val_AB.all) /
        (Sqrt (Float (Accum_Val_AA.all * Accum_Val_BB.all)));

      Free (Mean_Matrix_A);
      Free (Mean_Matrix_B);
      Free (Accum_Val_AB);
      Free (Accum_Val_AA);
      Free (Accum_Val_BB);

   end Correlation2D;

begin

   -- Allocate host matrices
   H_A := new Matrix (0 .. Matrix_Dimension - 1, 0 .. Matrix_Dimension - 1);
   H_B := new Matrix (0 .. Matrix_Dimension - 1, 0 .. Matrix_Dimension - 1);

   if H_A = null or else H_B = null then
      Put_Line ("Failed to allocate host vectors!");
      return;
   end if;

   -- Initialize matrices
   Generate_Matrix (H_A.all);
   Generate_Matrix (H_B.all);

   -- Construct the model output
   Correlation2D_Serial (H_A.all, H_B.all, R_Model);

   -- Allocate and initialize device matrices
   D_A := new Matrix'(H_A.all);
   D_B := new Matrix'(H_B.all);

   Put_Line
     ("CUDA kernel launch with " & Blocks_Per_Grid'Img & " blocks of " &
      Threads_Per_Block'Img & "  threads");

   -- Correlate A and B (while measuring elapsed time too)
   Start_Time := Clock;
   Correlation2D (D_A, D_B, R, Blocks_Per_Grid, Threads_Per_Block);

   Err          := Get_Last_Error;
   Elapsed_Time := Clock - Start_Time;

   -- Test with serial implementation
   if abs (R - R_Model) >= 1.0E-7 then
      Put_Line ("Test FAILED");

      Free (D_A);
      Free (D_B);

      Free (H_A);
      Free (H_B);

      return;
   end if;

   -- Print the duration of the benchmark's kernel
   Put_Line ("Elapsed kernel time: " & Time_Span'Image (Elapsed_Time));

   -- Free matrices
   Free (D_A);
   Free (D_B);

   Free (H_A);
   Free (H_B);

   Put_Line ("Test SUCCEEDED");

end Main;
