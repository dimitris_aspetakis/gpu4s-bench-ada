with CUDA.Storage_Models; use CUDA.Storage_Models;

package Kernel with
  SPARK_Mode
is

   type Matrix is array (Natural range <>, Natural range <>) of Float;

   type Matrix_Device_Access is access Matrix with
     Designated_Storage_Model => CUDA.Storage_Models.Model;

   type Int_Device_Access is access Integer with
     Designated_Storage_Model => CUDA.Storage_Models.Model;

   procedure Mean_Matrices
     (A, B                         : Matrix_Device_Access;
      Mean_Matrix_A, Mean_Matrix_B : Int_Device_Access) with
     Pre =>
      A'Length (1) = B'Length (1) and A'Length (2) = B'Length (2) and
      Mean_Matrix_A.all = 0 and Mean_Matrix_B.all = 0,
     Cuda_Global;

   procedure Accum_Values
     (A, B                                     : Matrix_Device_Access;
      Mean_Matrix_A, Mean_Matrix_B             : Int_Device_Access;
      Accum_Val_AB, Accum_Val_AA, Accum_Val_BB : Int_Device_Access) with
     Pre =>
      A'Length (1) = B'Length (1) and A'Length (2) = B'Length (2) and
      Accum_Val_AB.all = 0 and Accum_Val_AA.all = 0 and Accum_Val_BB.all = 0,
     Cuda_Global;

end Kernel;
