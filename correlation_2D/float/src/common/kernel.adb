with CUDA.Runtime_Api;
use CUDA.Runtime_Api; -- Block_Dim, Block_IDx, Thread_IDx
with Interfaces.C;
use Interfaces.C; -- Operators for Block_Dim, Block_IDx, Thread_IDx
with CUDA.Device_Atomic_Functions; use CUDA.Device_Atomic_Functions;

package body Kernel with
  SPARK_Mode
is

   procedure Mean_Matrices
     (A, B                         : Matrix_Device_Access;
      Mean_Matrix_A, Mean_Matrix_B : Int_Device_Access)
   is

      Unused : Integer;

      X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);
      Y : Integer := Integer (Block_Dim.Y * Block_Idx.Y + Thread_Idx.Y);

   begin

      if Y < A'Length (1) and X < A'Length (2) then
         Unused := Atomic_Add (Mean_Matrix_A, Integer (A (Y, X)));
         Unused := Atomic_Add (Mean_Matrix_B, Integer (B (Y, X)));
      end if;

   end Mean_Matrices;

   procedure Accum_Values
     (A, B                                     : Matrix_Device_Access;
      Mean_Matrix_A, Mean_Matrix_B             : Int_Device_Access;
      Accum_Val_AB, Accum_Val_AA, Accum_Val_BB : Int_Device_Access)
   is

      Unused                       : Integer;
      Result_Mean_A, Result_Mean_B : Float;

      X : Integer := Integer (Block_Dim.X * Block_Idx.X + Thread_Idx.X);
      Y : Integer := Integer (Block_Dim.Y * Block_Idx.Y + Thread_Idx.Y);

   begin

      if Y < A'Length (1) and X < A'Length (2) then
         Result_Mean_A := A (Y, X) - Float (Mean_Matrix_A.all);
         Result_Mean_B := B (Y, X) - Float (Mean_Matrix_B.all);
         Unused        :=
           Atomic_Add (Accum_Val_AB, Integer (Result_Mean_A * Result_Mean_B));
         Unused        :=
           Atomic_Add (Accum_Val_AA, Integer (Result_Mean_A * Result_Mean_A));
         Unused        :=
           Atomic_Add (Accum_Val_BB, Integer (Result_Mean_B * Result_Mean_B));
      end if;

   end Accum_Values;

end Kernel;
