package Convolution_2D with
   SPARK_Mode
is
   type Matrix is array (Natural range <>, Natural range <>) of Float;

   Value_Range : Float := 1_000.0;

   procedure Generate_Matrix (M : out Matrix);

   procedure Convolve (A : Matrix; Kernel : Matrix; B : out Matrix) with
      Pre => A'Length (1) = A'Length (2) and B'Length (1) = B'Length (2) and
      A'Length (1) = B'Length (1) and Kernel'Length (1) = Kernel'Length (2) and
      Kernel'Length (1) < A'Length (1);

   procedure Print_Matrix (M : Matrix);

end Convolution_2D;
