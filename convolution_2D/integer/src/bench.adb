with Ada.Command_Line;
with Convolution_2D;
with Ada.Real_Time;
with Text_IO;
with Ada.Unchecked_Deallocation;

procedure Bench is

   use Convolution_2D;
   use Ada.Real_Time;
   use Text_IO;

   Start_Time   : Time;
   Elapsed_Time : Time_Span;

begin

   declare

      type Matrix_Ref is access Matrix;

      procedure Free_Matrix is new Ada.Unchecked_Deallocation
        (Object => Matrix, Name => Matrix_Ref);

      Image_Size : constant Positive :=
        Positive'Value (Ada.Command_Line.Argument (1));
      Kernel_Size : constant Positive :=
        Positive'Value (Ada.Command_Line.Argument (2));
      A      : Matrix_Ref;
      Kernel : Matrix_Ref;
      B      : Matrix_Ref;

   begin

      -- Allocate matrices and initialize matrix C
      A      := new Matrix (0 .. Image_Size - 1, 0 .. Image_Size - 1);
      Kernel := new Matrix (0 .. Kernel_Size - 1, 0 .. Kernel_Size - 1);
      B      := new Matrix (0 .. Image_Size - 1, 0 .. Image_Size - 1);
      B.all  := (others => (others => 0));

      -- Populate matrix A and the Kernel matrix
      Generate_Matrix (A.all);
      Generate_Matrix (Kernel.all);

-- Convolve A with Kernel, resulting in B (while measuring elapsed time too)
      Start_Time := Clock;
      Convolve (A.all, Kernel.all, B.all);
      Elapsed_Time := Clock - Start_Time;

      -- Free matrices
      Free_Matrix (A);
      Free_Matrix (Kernel);
      Free_Matrix (B);

      -- Print the duration of the benchmark's kernel
      Put_Line ("Elapsed kernel time: " & Time_Span'Image (Elapsed_Time));

   end;

end Bench;
