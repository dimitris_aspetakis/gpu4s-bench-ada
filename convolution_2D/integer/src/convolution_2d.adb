with Text_IO;
with Ada.Numerics.Discrete_Random;

package body Convolution_2D with
   SPARK_Mode
is

   procedure Generate_Matrix (M : out Matrix) is

      pragma SPARK_Mode (Off);
      package Rand_Integer is new Ada.Numerics.Discrete_Random (Integer);
      G : Rand_Integer.Generator;

   begin

      -- Initialize the random number generator
      Rand_Integer.Reset (G);

      for i in M'Range (1) loop
         for j in M'Range (2) loop
            M (i, j) := Rand_Integer.Random (G) rem Value_Range;
         end loop;
      end loop;

   end Generate_Matrix;

   procedure Convolve (A : Matrix; Kernel : Matrix; B : out Matrix) is

      Image_Size  : Positive := A'Length (1);
      Kernel_Size : Positive := Kernel'Length (1);
      Kernel_Rad  : Positive := Kernel_Size / 2;
      K_X, K_Y    : Integer;
      Sum         : Integer;
      Value       : Integer;

   begin

      for X in B'Range (1) loop
         for Y in B'Range (2) loop
            Sum := 0;
            for K in 0 .. Kernel_Size**2 - 1 loop
               Value := 0;
               K_X   := (K / Kernel_Size) - Kernel_Rad;
               K_Y   := (K mod Kernel_Size) - Kernel_Rad;
               if not (K_X + X < 0 or K_Y + Y < 0) and
                 not (K_X + X > Image_Size - 1 or K_Y + Y > Image_Size - 1)
               then
                  Value := A (X + K_X, Y + K_Y);
               end if;
               Sum :=
                 Sum + Value * Kernel (K_X + Kernel_Rad, K_Y + Kernel_Rad);
            end loop;
            B (X, Y) := Sum;
         end loop;
      end loop;

   end Convolve;

   procedure Print_Matrix (M : Matrix) is
   begin

      for i in M'Range (1) loop
         for j in M'Range (2) loop
            Text_IO.Put (Integer'Image (M (i, j)));
            Text_IO.Put (" ");
         end loop;
         Text_IO.New_Line;
      end loop;

   end Print_Matrix;

end Convolution_2D;
